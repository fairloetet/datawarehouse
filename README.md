# Datawarehouse

## Dagster

### Datenbank Vorbereitung

Dies geschieht automatisch, wenn Postgres den Standard-Superuser postgres/postgres hat. Wenn nicht, muss man es manuell machen:

a) PostgreSQL console vom Host aus aufrufen (DB-User gemäß .db.env, dieser hier ist nicht korrekt):
docker compose exec db psql -U db-user

b) Datawarehouse Datenbank mit User anlegen, Credentials siehe .dwh.env (diese hier sind nicht korrekt)
CREATE DATABASE dwh_db;
CREATE USER dwh_user WITH PASSWORD '123';
GRANT ALL PRIVILEGES ON DATABASE dwh_db TO dwh_user;

c) In der Datenbank das public-Schema freilegen
\c dwh_db
GRANT ALL ON SCHEMA public TO dwh_user;

### Docker

Dagster doesn't seem to have an official Docker image, although https://hub.docker.com/r/dagster/dagster-celery-k8s 
seems to be updated regulary and may fit. But the documentation in 
https://docs.dagster.io/deployment/guides/docker#deploying-dagster-to-docker
* doesn't refer to an official image
* instructs the application specific dagster.yaml and workspace.yaml to be woven into the image

### Development

The Dagster UI can be started with "dagster dev". Here's the run config for VS Code:

        {
            "name": "Dagster: Dagster UI",
            "type": "python",
            "request": "launch",
            "module": "dagster",
            "justMyCode": true,
            "cwd": "${workspaceFolder}/data_pipeline",
            "args": ["dev"]
        }        

## Booting

--> Note that it's normal to initially get "Could not reach user code server. gRPC Error code: UNAVAILABLE" erros in the daemon and webserver logs because startup of the grpc server takes some time and there's a healtycheck procedure missing in the dependencies within 
the docker compose. In the Dagster UI, die Deployment needs to be clean. In case it is not, a yellow warning sign is
shown next to the "Deployment" entry. In case it runs in a timeout, the sign remains, but the code location may be
manually reloaded (e,g, within the error message).

## Usage

For a quick start, you can run the all_asset_job job as well to generate all regardless of sensors and auto-materialization.

For the automatic long-term run, first start all sensors as well as the auto-materializing deamon.
The first will check for the latest external data, realizes that it has never been triggered and starts generating all external assets.
The second will look for newly generated external data and starts generating allk dependent assets.

