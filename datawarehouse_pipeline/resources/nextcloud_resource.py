from dagster import ConfigurableResource
from pydantic import PrivateAttr
# from nc_py_api import Nextcloud  # Deinstalliert
from io import BytesIO
import os

# Problem hier:
# a) Die nc_py_api Bibliothek erwartet eigentlich Pydantic > v2, aber Dagster erwartet noch < v2
#    Man könnte nc_py_api versuchen mit Pydantic < v2 zu betreiben
# b) Das Übergeben eiens BytesIO an das pandas.read_excel resultierte immer im Fehler "File is not a zip file"
#    Evtl. kann man aber mittels eines with nc.open() as file: arbeiten


# class NextcloudResource(ConfigurableResource):
#
#    def get_file(self, filename: str) -> BytesIO:
#        nc = Nextcloud(nextcloud_url="http://{}:80".format(os.getenv("NEXTCLOUD_HOST", "")),
#                       nc_auth_user=os.getenv("NEXTCLOUD_USER", ""),
#                       nc_auth_pass=os.getenv("NEXTCLOUD_PASSWORD", ""))
#        file = nc.files.download(filename)
#        return BytesIO(file)
