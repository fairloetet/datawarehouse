from dagster import file_relative_path
from dagster_dbt import DbtCliResource

SEMANTIC_LAYER_DBT_PROJECT_DIR = file_relative_path(__file__, "../assets/semantic_layer")
dbt_resource = DbtCliResource(project_dir=SEMANTIC_LAYER_DBT_PROJECT_DIR)