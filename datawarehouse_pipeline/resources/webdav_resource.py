from dagster import ConfigurableResource
from pydantic import PrivateAttr
from webdav4.fsspec import WebdavFileSystem  # pip install webdav4[fsspec]
import os

# Ich hätte gerne mit Dagster's EnvVar gearbeitet, habe aber nicht verstanden wie es funktioniert
# https://docs.dagster.io/guides/dagster/using-environment-variables-and-secrets


class WebDavResource(ConfigurableResource):

    _fs: WebdavFileSystem = PrivateAttr()

    def __init__(self):
        self._fs = WebdavFileSystem(os.getenv("WEBDAV_URL", ""),
                                    auth=(os.getenv("WEBDAV_USER", ""),
                                          os.getenv("WEBDAV_PASSWORD", "")))
        super().__init__()

    def open(self, filename: str):
        filename = os.getenv("WEBDAV_FOLDER", "") + filename
        files = self._fs.glob(filename)
        assert len(files) == 1, f"There is no or more than one file as {filename}; exactly one expected."
        file = files[0]
        assert self._fs.isfile(file), f"{file} found, but it doesn't seem to be a readable file."
        return self._fs.open(file, mode='rb', cache_type='readahead')

    def glob(self, foldername: str):
        foldername = os.getenv("WEBDAV_FOLDER", "") + foldername
        return [str(x).removeprefix(os.getenv("WEBDAV_FOLDER", "")) for x in self._fs.glob(foldername)]
