import wbgapi as wb  # https://github.com/tgherzog/wbgapi/tree/master/examples
import pandas as pd
import pandera as pa
from typing import Mapping, Tuple, Any, Dict, cast
from dagster import MetadataValue, ConfigurableResource
from pandera.typing import Series, Index


class WorldbankWdiResource(ConfigurableResource):
    '''
      World Development Indicators by The World Bank.

      See https://databank.worldbank.org/source/world-development-indicators.

      Using https://pypi.org/project/wbgapi/
    '''

    def _compile_metadata(self, series: str, df: pd.DataFrame) -> Dict[str, Any]:
        result: Dict[str, Any] = dict()
        wbmd = wb.series.metadata.get(series)
        if wbmd:
            result["name"] = MetadataValue.md(wbmd.metadata['IndicatorName'])
            result["description"] = MetadataValue.md(wbmd.metadata['Longdefinition'])
            result["license"] = MetadataValue.md(f"[{wbmd.metadata['License_Type']}]({wbmd.metadata['License_URL']})")
        # TODO Get data sources as well, e.g. wb.data.footnote('SL.TLF.0714.WK.TM', 'AFG', 2011)
        result["source"] = MetadataValue.md(f"Series [{series}](https://data.worldbank.org/indicator/{series}) "
                                            "in World Bank's [World Development Indicators]"
                                            "(https://datatopics.worldbank.org/world-development-indicators/)")
        # df["source"] = (f"Series {series} (https://data.worldbank.org/indicator/{series}) "
        #                 "in World Bank's World Development Indicators "
        #                 "(https://datatopics.worldbank.org/world-development-indicators/)")
        df["source"] = series
        result["number of countries"] = len(df.index)
        result["number of years"] = len(df.columns) - 2
        result["preview"] = MetadataValue.md(df.T.head(n=3).T.head().fillna("").to_markdown())
        # TODO Plot it, e.g. wb.data.DataFrame(....).plot(figsize=(10, 6)) ; plt.show()
        return result

    def get_wdi_update_date(self) -> str:
        wdi_db = 2
        wdi_metadata: Dict = cast(Dict, wb.source.get(wdi_db))
        return wdi_metadata['lastupdated']

    def get_most_recent_values_as_dataframe(self, series: str) -> Tuple[pd.DataFrame, Dict[str, Any]]:
        tmp_db = wb.db
        wb.db = 2  # World Development Indicators
        df: pd.DataFrame
        df = (wb.data.DataFrame(series=series, economy='all', skipAggs=True, time='all',
                                mrnev=1, timeColumns=True, labels=True)
                     .rename(columns={series: "value", series+":T": "year", "Country": "country"})
                     .astype({"year": "int"})
                     .reset_index()
                     .sort_values(by="country")
                     )
        wb.db = tmp_db
        md = self._compile_metadata(series, df)
        return df, md

    def get_all_values_as_dataframe(self, series: str) -> Tuple[pd.DataFrame, Dict[str, Any]]:
        tmp_db = wb.db
        wb.db = 2  # World Development Indicators
        df = (wb.data.DataFrame(series=series, economy='all', skipAggs=True, time='all',
                                columns='time', numericTimeKeys=False, labels=True)
                     .rename(columns={"Country": "country"})
                     .convert_dtypes()
                     .reset_index()
                     .sort_values(by="country")
                     )
        wb.db = tmp_db
        md = self._compile_metadata(series, df)
        return df, md
