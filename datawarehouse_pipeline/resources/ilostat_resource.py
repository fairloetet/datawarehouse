from .sdmx_resource import SdmxResource
import pandas as pd
from typing import Dict, Tuple, Any, List
from dagster import MetadataValue, TableRecord, TableSchema, TableColumn


class IlostatResource(SdmxResource):

    '''
      Indicators by International Labour Organization (ILO) via ILOSTAT

      See https://ilostat.ilo.org/

      Using https://pypi.org/project/sdmx1/ with Source "ILO"
    '''

    def __init__(self):
        super().__init__("ILO")

    def _compile_metadata(self, indicator: str, keys: Dict[str, str], df: pd.DataFrame,
                          additional_remarks: List[str] = []) -> Dict[str, Any]:
        result: Dict[str, Any] = {}
        metadata = self.get_resource("DF_" + indicator)
        result["name"] = MetadataValue.md(metadata.name.en)  # type: ignore
        result["description"] = MetadataValue.md("\"" + metadata.description.en + "\" (description provided by ILOSTAT)" +  # type: ignore
                                                 " ".join(additional_remarks))
        result["keys"] = MetadataValue.table(
            records=[TableRecord(data={"key": key, "value": value}) for key, value in keys.items()],
            schema=TableSchema(columns=[TableColumn(name="key", type="string"),
                                        TableColumn(name="value", type="string")])
        )
        result["license"] = MetadataValue.md("[ILOSTAT data](https://www.ilo.org/global/copyright/lang--en/index.htm)")
        # TODO Are data sources obtainable?
        result["source"] = MetadataValue.md(f"Indicator [{indicator}]"
                                            "(https://www.ilo.org/shinyapps/bulkexplorer49/?lang=en&id={dataflow}) "
                                            "from International Labour Organizations's [ILOSTAT]"
                                            "(https://ilostat.ilo.org/)")
        df["source"] = indicator
        result["number of countries"] = len(df.index)
        result["preview"] = MetadataValue.md(df.T.head(n=3).T.head().to_markdown())
        # TODO Plot it, e.g. df.plot(figsize=(10, 6)) ; plt.show()
        return result

    def get_ilostat_data(self, dataflow: str, keys: Dict[str, Any], params: Dict[str, Any]) -> pd.DataFrame:
        df = self.get_data("DF_" + dataflow, keys=keys, params=params).dropna()
        return df.replace("KOS", "XXK")  # ILO uses KOS for Kosovo

    def get_most_recent_values_as_dataframe(self, indicator: str, keys: Dict[str, Any],
                                            additional_descriptions: List[str] = [])\
            -> Tuple[pd.DataFrame, Dict[str, Any]]:
        params = {"lastNObservations": 1}
        additional_descriptions += "Only the latest available year with known data is taken."
        df, md = self.get_values_as_dataframe(indicator, keys, params, additional_descriptions)
        df["source"] = indicator
        return df, md

    def get_mean_values_as_dataframe(self, indicator: str, keys: Dict[str, Any],
                                     number_of_years: int,
                                     params: Dict[str, Any] = {},
                                     additional_descriptions: List[str] = [])\
            -> Tuple[pd.DataFrame, Dict[str, Any]]:
        params = {"lastNObservations": number_of_years}
        additional_descriptions += f"The {number_of_years} latest years with data are taken into account and the mean value of these is taken."
        df, md = self.get_values_as_dataframe(indicator, keys, params, additional_descriptions)
        df = df.groupby("country").agg({"value": "mean", "year": "max"}).reset_index()
        df["source"] = indicator
        return df, md

    def get_values_as_dataframe(self, indicator: str, keys: Dict[str, Any],
                                params: Dict[str, Any] = {},
                                additional_descriptions: List[str] = [])\
            -> Tuple[pd.DataFrame, Dict[str, Any]]:
        params = {"startPeriod": "1980"} | params
        additional_descriptions += "Earliest year taken into account is down to 1980."
        df: pd.DataFrame
        df = self.get_ilostat_data(indicator, keys=keys, params=params)
        df = df.reset_index()\
               .rename(columns={"REF_AREA": "country",
                                "TIME_PERIOD": "year"})\
               .astype({"year": "int"})
        df = df[['country', 'year', 'value']]
        md = self._compile_metadata(indicator, keys | params, df, additional_descriptions)
        return df, md
