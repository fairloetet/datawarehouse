from numpy import source
import pandas as pd
from typing import Any, Dict
from dagster import ConfigurableResource, InitResourceContext
from pydantic import PrivateAttr
from sdmx.rest import Resource
from sdmx.client import Client
import sdmx


class SdmxResource(ConfigurableResource):

    # See https://docs.dagster.io/concepts/resources#resource-lifecycle
    _source: str = PrivateAttr()
    _client: Client = PrivateAttr()
    _dataflows: Dict[str, Resource] = PrivateAttr()

    def __init__(self, source: str):
        self._source = source
        super().__init__()

    def setup_for_execution(self, context: InitResourceContext) -> None:
        assert self._source and self._source in sdmx.list_sources()
        self._client = sdmx.Client(self._source)
        self._dataflows = self._client.dataflow().dataflow  # type: ignore

    def get_client(self) -> Client:
        return self._client
    
    def get_resource(self, key: str) -> Resource:
        return self._dataflows[key]
    
    def get_data(self, dataflow: str, keys: Dict[str, Any], params: Dict[str, Any]) -> pd.DataFrame:
        assert dataflow in self._dataflows
        response = self.get_client().data(dataflow, key=keys, params=params)
        return sdmx.to_pandas(response)
