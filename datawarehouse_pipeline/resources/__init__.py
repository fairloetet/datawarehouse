from .worldbank_wdi_resource import WorldbankWdiResource
from .ilostat_resource import IlostatResource
#from .nextcloud_resource import NextcloudResource
from .webdav_resource import WebDavResource
from .dbt_resource import dbt_resource

wb_wdi_client = WorldbankWdiResource()
ilostat_sdmx_client = IlostatResource()
#nextcloud_client = NextcloudResource()
webdav_client = WebDavResource()
