from dagster import Definitions

from .assets import indicator_assets, resource_assets, regime_assets, material_assets, \
                    part_assets, device_assets, portfolio_assets
from .assets.semantic_layer.semantic_layer import semantic_layer_assets
from .resources import wb_wdi_client, ilostat_sdmx_client, webdav_client, dbt_resource  # , nextcloud_client
from .iomanager import pandas_io_manager, local_io_manager, pandas_dict_io_manager
from .jobs import worldbank_wdi_job, all_assets_job
from .sensors import new_wdi_update_sensor, new_mcs_update_sensor
from .schedules import downloaded_assets_schedule, ilostat_schedule  #, fairtonics_schedule
from dagster_duckdb_pandas import DuckDBPandasIOManager
import os


defs = Definitions(
    assets=[*indicator_assets, *resource_assets, *regime_assets, *material_assets, 
            *part_assets, *device_assets, *portfolio_assets, semantic_layer_assets],
    jobs=[worldbank_wdi_job, all_assets_job],
    schedules=[ilostat_schedule, downloaded_assets_schedule],  # fairtonics_schedule
    resources={
        "io_manager": local_io_manager,
        "pandas_io_manager": pandas_io_manager,
        "pandas_dict_io_manager": pandas_dict_io_manager,
        "regimes_duckdb_io_manager": DuckDBPandasIOManager(
            database=os.getenv("PIPELINE_ASSETS_DB_FILE", "./assets.duckdb"), 
            schema="regimes"),
        "resources_duckdb_io_manager": DuckDBPandasIOManager(
            database=os.getenv("PIPELINE_ASSETS_DB_FILE", "./assets.duckdb"), 
            schema="resources"),
        "indicators_duckdb_io_manager": DuckDBPandasIOManager(
            database=os.getenv("PIPELINE_ASSETS_DB_FILE", "./assets.duckdb"), 
            schema="indicators"),
        "materials_duckdb_io_manager": DuckDBPandasIOManager(
            database=os.getenv("PIPELINE_ASSETS_DB_FILE", "./assets.duckdb"), 
            schema="materials"),
        "parts_duckdb_io_manager": DuckDBPandasIOManager(
            database=os.getenv("PIPELINE_ASSETS_DB_FILE", "./assets.duckdb"), 
            schema="parts"),
        "devices_duckdb_io_manager": DuckDBPandasIOManager(
            database=os.getenv("PIPELINE_ASSETS_DB_FILE", "./assets.duckdb"), 
            schema="devices"),
        "portfolios_duckdb_io_manager": DuckDBPandasIOManager(
            database=os.getenv("PIPELINE_ASSETS_DB_FILE", "./assets.duckdb"), 
            schema="portfolios"),
        "wb_wdi_api": wb_wdi_client,
        "ilostat_sdmx_client": ilostat_sdmx_client,
        "webdav_client": webdav_client,
        # "nextcloud_client": nextcloud_client
        "dbt": dbt_resource,
    },
    sensors=[new_wdi_update_sensor, new_mcs_update_sensor] 
)
