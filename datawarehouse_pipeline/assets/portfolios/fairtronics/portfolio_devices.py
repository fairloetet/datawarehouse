from dagster import asset, Output, MetadataValue, AutoMaterializePolicy, AssetExecutionContext, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

#import pandera as pa
#from dagster_pandera import pandera_schema_to_dagster_type
#from pandera.typing import Series
#
#class ManufacturersModel(pa.SchemaModel):
#    slug: Series[str] = pa.Field(description="Key")
#    name: Series[str] = pa.Field(description="Name of mine")
#    short_name: Series[str] = pa.Field(description="Short name für mines")
#    company_url: Series[str] = pa.Field(description="Web address of company")

@asset(io_manager_key="portfolios_duckdb_io_manager",
#       dagster_type=pandera_schema_to_dagster_type(ManufacturersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"devices": AssetIn(key=["devices", "devices"]),
            "portfolios": AssetIn(key=["portfolios", "portfolios"])},
       key_prefix="portfolios",
       compute_kind="Fairtronics")
def portfolio_devices(context: AssetExecutionContext,
                      devices: pd.DataFrame,
                      portfolios: pd.DataFrame) -> Output[pd.DataFrame]:
    """
    Prepares devices per portfolio data for data import.
    """
    print("Start converting portfolios")
    p_d_dfs = []
    for portfolio in portfolios['slug']:
        devices_file = "./datawarehouse_pipeline/assets/portfolios/fairtronics/portfolios/" + portfolio + ".devices.csv"
        p_d_df = pd.read_csv(devices_file, comment='#')
        p_d_dfs.append(p_d_df)
    full_df = pd.concat(p_d_dfs, sort=False, ignore_index=True)
    os.makedirs("pipeline_run/result", exist_ok=True)
    full_df.to_csv("pipeline_run/result/portfolio_devices.csv", index=False,
                   columns=['Portfolio.slug', 'Device.slug', 'amount', 'source'], 
                   header=['Portfolio.slug', 'Device.slug', 'amount', 'source'])
    dataframe = full_df.rename({"Portfolio.slug": "portfolio_slug", "Device.slug": "device_slug"}, axis=1) \
                       .filter(["portfolio_slug", "device_slug", "amount", "source"])
    metadata = dict()
    metadata["name"] = "List of parts as used in Fairtronics"
    metadata["source"] = MetadataValue.md("Compiled from user defined portfolio->device relations")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
