import pandas as pd
from dagster import asset, Output, MetadataValue
from datawarehouse_pipeline.resources import WebDavResource
import country_converter as coco


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="Walk Free")
def global_slavery_index(webdav_client: WebDavResource) -> Output[pd.DataFrame]:
    """
       Global Slavery Index by Walk Free

       See https://www.walkfree.org/global-slavery-index/

       Reading downloaded Excel sheet
    """
    # TODO Use from importlib import resources instead (but how?)
    with webdav_client.open("Indikatoren/*Global-Slavery-Index-Data*.xlsx") as file:
        dataframe = pd.read_excel(file, sheet_name="GSI 2023 summary data", header=2)
    # To translate country names to codes, use the codebook sheet
    # Note that BACI uses Comtrade Country Codes (https://unstats.un.org/wiki/display/comtrade/Country+Codes)
    # Unfortunately, quite a few names are different in the two sheets and some are missing
    # "Kosovo" is in the GSI as well, but there's no code for it. It will be omitted in the following merge
    cc = coco.CountryConverter()
    dataframe['iso_3166-1_numeric'] = cc.pandas_convert(series=dataframe["Country"], to='ISOnumeric', not_found="0")
    dataframe['iso_3166-1_numeric'] = dataframe['iso_3166-1_numeric'].astype(str).str.zfill(3)
    number_of_unknown_slavery = int(dataframe['Estimated number of people in modern slavery'].isna().sum())
    metadata = dict()
    metadata["name"] = "Global Slavery Index 2023 by Walk Free"
    metadata["source"] = MetadataValue.md(
        "Dataset taken from https://www.walkfree.org/global-slavery-index/downloads/")
    metadata["unknown values"] = number_of_unknown_slavery
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
