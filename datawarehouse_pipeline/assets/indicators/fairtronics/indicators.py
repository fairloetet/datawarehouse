import pandas as pd
import datetime
import os
from .utils import compile_metadata
from dagster import multi_asset, observable_source_asset, AutoMaterializePolicy, AssetOut, Output, \
                    ExperimentalWarning, DataVersion, AssetIn
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def custom_indicator_data():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/indicators/fairtronics/custom_indicator_data.csv")))


@multi_asset(outs={"indicators": AssetOut(io_manager_key="indicators_duckdb_io_manager",
                                          auto_materialize_policy=AutoMaterializePolicy.eager(),
                                          key_prefix="indicators"),
                   "indicator_regimes": AssetOut(io_manager_key="indicators_duckdb_io_manager",
                                                 auto_materialize_policy=AutoMaterializePolicy.eager(),
                                                 key_prefix="indicators")},
             deps=[custom_indicator_data],
       ins={"child_labour": AssetIn(key=["indicators", "child_labour"]),
            "forced_labour": AssetIn(key=["indicators", "forced_labour"]),
            "wage_risk": AssetIn(key=["indicators", "wage_risk"]),
            "wage_chance": AssetIn(key=["indicators", "wage_chance"]),
            "human_rights": AssetIn(key=["indicators", "human_rights"]),
            "unsafe_work": AssetIn(key=["indicators", "unsafe_work"]),
            "excessive_working_hours": AssetIn(key=["indicators", "excessive_working_hours"])
#            "with_collective_bargaining": AssetIn(key=["indicators", "with_collective_bargaining"]),
#            "no_collective_bargaining": AssetIn(key=["indicators", "no_collective_bargaining"]),
#            "with_social_protection": AssetIn(key=["indicators", "with_social_protection"]),
#            "no_social_protection": AssetIn(key=["indicators", "no_social_protection"])
            },
             compute_kind="Fairtronics")
def indicators(child_labour: pd.DataFrame,
               forced_labour: pd.DataFrame,
               wage_risk: pd.DataFrame,
               wage_chance: pd.DataFrame,
               human_rights: pd.DataFrame,
               unsafe_work: pd.DataFrame,
               excessive_working_hours: pd.DataFrame
#               with_collective_bargaining: pd.DataFrame,
#               no_collective_bargaining: pd.DataFrame,
#               with_social_protection: pd.DataFrame,
#               no_social_protection: pd.DataFrame
               ) -> tuple[Output[pd.DataFrame], Output[pd.DataFrame]]:
    """
    Collection of all Fairtronics' indicators
    """
    os.makedirs("pipeline_run/result", exist_ok=True)
    indicator_order = ["child-labour", "wage-risk", "wage-chance",
#                       "collective-bargaining", "no-collective-bargaining",
                       "forced-labour", "health", "hours-of-work",
#                       "social-protection", "no-social-protection", 
                       "human-rights"]
    # TODO Would be better to have each indicator handle its own business:
    #   slug name, parameters in indicators.csv. Or?? At least,
    #   carry and reuse metadata
    indicator_dict = {
        "child-labour": {
              "values": child_labour,
              "metadata": {
                     "name": "Child Labour",
                     "short_name": "Child Labour",
                     "direction": "ascending",
                     "probability_based": True,
                     "composed": False,
                     "date": datetime.date(2023, 1, 1),
                     "data_base": "Indicators SL.TLF.0714.ZS, SL.TLF.0714.WK.TM, SL.TLF.ACTI.ZS, SP.POP.0014.TO, "
                                  "and P.POP.1564.TO provided by The World Bank in its latest 'World Development "
                                  "Indicators' at https://databank.worldbank.org/source/world-development-indicators"}
              },
        "forced-labour": {
              "values": forced_labour,
              "metadata": {
                     "name": "Forced Labour",
                     "short_name": "Forced Labour",
                     "direction": "ascending",
                     "probability_based": True,
                     "composed": False,
                     "date": datetime.date(2023, 1, 1),
                     "data_base": "'Estimated prevalence of modern slavery by country (victims per 1,000 population)' "
                                  "as provided by the Global Slavery Index 2023 Dataset, Walk Free Foundation, "
                                  "available from https://www.walkfree.org/global-slavery-index/"}
              },
        "wage-risk": {
              "values": wage_risk,
              "metadata": {
                     "name": "Non-Living Wage",
                     "short_name": "Non-Living Wage",
                     "direction": "ascending",
                     "probability_based": True,
                     "composed": False,
                     "date": datetime.date(2023, 1, 1),
                     "data_base": "TODO"}
              },
        "wage-chance": {
              "values": wage_chance,
              "metadata": {
                     "name": "Living Wage",
                     "short_name": "Living Wage",
                     "direction": "descending",
                     "probability_based": True,
                     "composed": False,
                     "date": datetime.date(2023, 1, 1),
                     "data_base": "TODO"}
              },
        "human-rights": {
              "values": human_rights,
              "metadata": {
                     "name": "Human Rights Violations",
                     "short_name": "Human Rights Violations",
                     "direction": "ascending",
                     "probability_based": True,
                     "composed": True,
                     "date": datetime.date(2023, 1, 1),
                     "data_base": "Child Labour & Forced Labour"}
              },
        "health": {
              "values": unsafe_work,
              "metadata": {
                     "name": "Unsafe Working Conditions",
                     "short_name": "Unsafe Working Conditions",
                     "direction": "ascending",
                     "probability_based": True,
                     "composed": False,
                     "date": datetime.date(2023, 1, 1),
                     "data_base": "Including fatal and non-fatal injuries"}
              },
        "hours-of-work": {
              "values": excessive_working_hours,
              "metadata": {
                     "name": "Excessive Working Hours",
                     "short_name": "Excessive Working Hours",
                     "direction": "ascending",
                     "probability_based": True,
                     "composed": False,
                     "date": datetime.date(2023, 1, 1),
                     "data_base": "TODO"}
              },
#        "collective-bargaining": {
#              "values": with_collective_bargaining,
#              "metadata": {
#                     "name": "With Collective Bargaining",
#                     "short_name": "With Collective Bargaining",
#                     "direction": "descending",
#                     "probability_based": True,
#                     "composed": False,
#                     "date": datetime.date(2023, 1, 1),
#                     "data_base": "TODO"}
#              },
#        "no-collective-bargaining": {
#              "values": no_collective_bargaining,
#              "metadata": {
#                     "name": "No Collective Bargaining",
#                     "short_name": "No Collective Bargaining",
#                     "direction": "ascending",
#                     "probability_based": True,
#                     "composed": False,
#                     "date": datetime.date(2023, 1, 1),
#                     "data_base": "TODO"}
#              },
#        "social-protection": {
#              "values": with_social_protection,
#              "metadata": {
#                     "name": "Having Social Protection",
#                     "short_name": "Social Protection",
#                     "direction": "descending",
#                     "probability_based": True,
#                     "composed": False,
#                     "date": datetime.date(2023, 1, 1),
#                     "data_base": "TODO"}
#              },
#        "no-social-protection": {
#              "values": no_social_protection,
#              "metadata": {
#                     "name": "No Social Protection",
#                     "short_name": "No Social Protection",
#                     "direction": "ascending",
#                     "probability_based": True,
#                     "composed": False,
#                     "date": datetime.date(2023, 1, 1),
#                     "data_base": "TODO"}
#              }
        }
    indicators_data_df = pd.DataFrame()
    indicators_list = []
    for indicator_slug in indicator_order:
        indicator_data = indicator_dict[indicator_slug]
        indicator_df = indicator_data["values"]
        indicator_df['slug'] = indicator_slug
        indicator_metadata = indicator_data["metadata"]
        indicators_list.append({"slug": indicator_slug} | indicator_metadata)
        indicators_data_df = pd.concat([indicators_data_df, indicator_df])
    indicators_data_df = indicators_data_df.rename({"slug": "indicator_slug"}, axis=1)
    
    # TODO Work with file_relative_path
    custom_indicators_df = pd.read_csv("./datawarehouse_pipeline/assets/indicators/fairtronics/custom_indicator_data.csv",
                                       comment="#")
    indicators_data_df = pd.concat([custom_indicators_df, indicators_data_df], ignore_index=True)
    indicators_data_df.to_csv("pipeline_run/result/indicator_data.csv", index=False,
                              columns=["indicator_slug", "regime_slug", "amount", "source"],
                              header=["Indicator.slug", "Location.slug", "amount", "source"])

    indicators_list_df = pd.DataFrame(indicators_list)
    indicators_list_df.to_csv("pipeline_run/result/indicators.csv", index=False)

    indicators_data_dataframe = indicators_data_df
    indicators_list_dataframe = indicators_list_df
    indicators_data_metadata = compile_metadata(indicators_data_dataframe,
                                                "All indicators combined",
                                                "",  # TODO descrpition
                                                "Based on " + ", ".join(indicator_order))
    indicators_list_metadata = compile_metadata(indicators_list_dataframe,
                                                "Data on all indicators",
                                                "",  # TODO descrpition
                                                "Based on " + ", ".join(indicator_order))
    
    return Output(value=indicators_list_dataframe, metadata=indicators_data_metadata),\
           Output(value=indicators_data_dataframe, metadata=indicators_list_metadata)
