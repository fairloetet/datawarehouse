import pandas as pd
from dagster import asset, AutoMaterializePolicy, Output, ExperimentalWarning, AssetIn
from datawarehouse_pipeline.types import ft_indicator_type
from .utils import compile_metadata
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       dagster_type=ft_indicator_type,
       ins={"employed_persons_having_excessive_working_time": AssetIn(key=["indicators", "employed_persons_having_excessive_working_time"]),
            "labour_force": AssetIn(key=["indicators", "labour_force"]),
            "countries": AssetIn(key=["regimes", "countries"])},
       key_prefix="indicators",
       compute_kind="Fairtronics")
def excessive_working_hours(employed_persons_having_excessive_working_time: pd.DataFrame,
                            labour_force: pd.DataFrame,
                            countries: pd.DataFrame) -> Output[pd.DataFrame]:
    """
    Share of people having excessive working hours
    """
    # Merge both indicators and add values
    df = pd.merge(employed_persons_having_excessive_working_time, labour_force,
                  how="inner", left_on="country", right_on="country")
    df['amount'] = df['value_x'] / df['value_y']  # both are in thousend
    # Merge with countries
    df = pd.merge(df, countries, how="inner", left_on="country", right_on="alpha_3")
    df['regime'] = df['slug']
    df['source'] = "Entry of country " + df['country'] + " for excessive working hours (>= 49 hours a week)"
    dataframe = df.rename({"regime": "regime_slug"}, axis=1) \
                  .filter(["regime_slug", "amount", "source"]) \
                  .sort_values("regime_slug")
    metadata = compile_metadata(dataframe,
                                "Excessive Working Hours",
                                "",  # TODO descrpition
                                "Based on ILOSTAT data")
    return Output(value=dataframe, metadata=metadata)
