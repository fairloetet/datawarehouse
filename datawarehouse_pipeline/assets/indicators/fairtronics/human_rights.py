import pandas as pd
from dagster import asset, AutoMaterializePolicy, Output, ExperimentalWarning, AssetIn
from datawarehouse_pipeline.types import ft_indicator_type
from .utils import compile_metadata
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


def _calc_amount(forced_labour_amount: float, child_labour_amount: float):
    # XOR'es, i.e. fl + cl - (fl-cl), which equals to 1 - (1-fl) * (1-cl)
    # If any of these is 0.0, at least the other get accounted as is.
    return forced_labour_amount + child_labour_amount - (forced_labour_amount * child_labour_amount)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       dagster_type=ft_indicator_type,
       ins={"child_labour": AssetIn(key=["indicators", "child_labour"]),
            "forced_labour": AssetIn(key=["indicators", "forced_labour"])},
       key_prefix="indicators",
       compute_kind="Fairtronics")
def human_rights(child_labour: pd.DataFrame,
                 forced_labour: pd.DataFrame) -> Output[pd.DataFrame]:
    """
    Human Rights Threat (= child or forced labour) Indicator for Fairtronics
    """

    # Take outer join, i.e. include all countries where we have at least one of both values
    df = pd.merge(forced_labour, child_labour, how='outer', on='regime_slug', suffixes=("_fl", "_cl"))
    # ... and set the other value to 0.0.
    df.fillna(0.0, inplace=True)

    df['amount'] = df.apply(lambda row: _calc_amount(row['amount_fl'], row['amount_cl']), axis=1)
    df['source'] = ("Compiled as cl + fl - cl * fl [= 1-(1-cl)*(1-fl)], where cl = child-labour and "
                    "fl = forced-labour for this country, 0 if any of them is unknown")

    dataframe = df.rename({"regime": "regime_slug"}, axis=1) \
                  .filter(["regime_slug", "amount", "source"]) \
                  .sort_values("regime_slug")
    metadata = compile_metadata(dataframe,
                                "Human Rights indicator for Fairtronics",
                                "It's composed using the Fairtronics' Child and Forced Labour indicators",
                                "")
    return Output(value=dataframe, metadata=metadata)
