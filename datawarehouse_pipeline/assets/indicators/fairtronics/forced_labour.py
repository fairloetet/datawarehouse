import pandas as pd
from dagster import asset, AutoMaterializePolicy, Output, ExperimentalWarning, AssetIn
from datawarehouse_pipeline.types import ft_indicator_type
from .utils import compile_metadata
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       dagster_type=ft_indicator_type,
       ins={"global_slavery_index": AssetIn(key=["indicators", "global_slavery_index"]),
            "countries": AssetIn(key=["regimes", "countries"])},
       key_prefix="indicators",
       compute_kind="Fairtronics")
def forced_labour(global_slavery_index: pd.DataFrame,
                  countries: pd.DataFrame) -> Output[pd.DataFrame]:
    """
    Forced Labour Indicator for Fairtronics
    """

    # These should better be taken from metadata of the GSI asset instead
    DATABASE = "Global Slavery Index 2023 by Walk Free"
    CONCERNED_COLUMN = "Estimated number of people in modern slavery"
    # COMPLETE_COLUMN = "Population"
    PREVELANCE_COLUMN = "Estimated prevalence of modern slavery per 1,000 population"

    # Drop rows where there's no prevalence reported
    global_slavery_index.dropna(subset=[CONCERNED_COLUMN], inplace=True)
    # Merge with countries
    df = pd.merge(global_slavery_index, countries, how="inner", left_on="iso_3166-1_numeric", right_on="country_code")

    # df['amount'] = df[CONCERNED_COLUMN] / df[COMPLETE_COLUMN] - Kosovo has 0 population which will result in inf
    df['amount'] = df[PREVELANCE_COLUMN] / 1000.0
    df['regime'] = df['slug']
    df['source'] = "Entry of country " + df['Country'] + " in " + DATABASE
    dataframe = df.rename({"regime": "regime_slug"}, axis=1) \
                  .filter(["regime_slug", "amount", "source"]) \
                  .sort_values("regime_slug")
    metadata = compile_metadata(dataframe,
                                "Forced Labour indicator for Fairtronics",
                                "",  # TODO descrpition
                                "Based on Global Slavery Index 2023, "
                                "see https://www.walkfree.org/global-slavery-index/downloads/")
    return Output(value=dataframe, metadata=metadata)
