import pandas as pd
import numpy as np
from dagster import multi_asset, AutoMaterializePolicy, AssetOut, Output, ExperimentalWarning, AssetIn
from datawarehouse_pipeline.types import ft_indicator_type
from .utils import compile_metadata
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@multi_asset(outs={"wage_risk": AssetOut(io_manager_key="indicators_duckdb_io_manager",
                                         auto_materialize_policy=AutoMaterializePolicy.eager(),
                                         key_prefix="indicators"),
                   "wage_chance": AssetOut(io_manager_key="indicators_duckdb_io_manager",
                                           auto_materialize_policy=AutoMaterializePolicy.eager(),
                                           key_prefix="indicators")},
       ins={"monthly_mining_earnings": AssetIn(key=["indicators", "monthly_mining_earnings"]),
            "monthly_living_wage": AssetIn(key=["indicators", "monthly_living_wage"]),
            "price_index_change": AssetIn(key=["indicators", "price_index_change"]),
            "countries": AssetIn(key=["regimes", "countries"])},
             compute_kind="Fairtronics")
def wage_risk_chance(monthly_mining_earnings: pd.DataFrame,
                     monthly_living_wage: pd.DataFrame,
                     price_index_change: pd.DataFrame,
                     countries: pd.DataFrame) -> tuple[Output[pd.DataFrame], Output[pd.DataFrame]]:
    """
    a) Risk of not earning a living wage in mining, and
    b) Chance of earning a living wage in mining, both
    approximated as the monetory distance between earnings in mining (ILO data) and
    living wage (Valuing Nature data) for a country or region.
    """

    def _wage_indicator(monthly_mining_earnings: pd.DataFrame,
                        monthly_living_wage: pd.DataFrame,
                        price_index_change: pd.DataFrame,
                        countries: pd.DataFrame,
                        chance_or_risk: str) -> pd.DataFrame:
        # Connect wages with country slugs
        df_mw = pd.merge(monthly_mining_earnings, countries, how="left", left_on="country", right_on="alpha_3")
        df_lw = pd.merge(monthly_living_wage, countries, how="left", left_on="country", right_on="alpha_3")
        df_pic = pd.merge(price_index_change, countries, how="left", left_on="country", right_on="alpha_3")
        # First take only those countries where we have data of both values
        df = pd.merge(df_lw, df_mw, how='inner', on='slug', suffixes=("_lw", "_mw"))
        # Then compute percentage of mining wage in living wage, 0.0 if it's even higher
        df['amount'] = df.apply(lambda row: _calc_amount(row['value_mw'], row['year_mw'],
                                row['value_lw'], row['year_lw'], row['slug'], chance_or_risk,
                                df_pic.pivot(index='slug', columns='year', values='value')), axis=1)
        df['regime'] = df['slug']
        return df

    def _raised_wage(wage: float, base_year: int, year_count: int, country: str,
                     inflation_indicator: pd.DataFrame) -> float:
        if country in inflation_indicator.index:
            additional_year = 0
            mean_infl = inflation_indicator.mean(axis=1)[country]
            while additional_year < year_count:
                year = base_year + additional_year
                try:
                    infl = inflation_indicator.at[country, year]
                except KeyError:
                    # Expect an average of the inflations rates of a country if no data is available for a single year
                    infl = mean_infl
                # Expect an average of the inflations rates of a country if no data is available for a single year
                if np.isnan(infl):
                    infl = mean_infl
                wage += wage * (infl / 100.0)
                additional_year += 1
        else:
            # Expect an inflation of 3.0 for each year if no data for country is available
            infl = 5.0  # TODO Better take year's mean inflation for all countries (of similar development)
            wage = wage * (1 + infl / 100.0) ** year_count  # Formula for compound interest
        return wage

    def _calc_amount(mining_wage: float, mw_year: int, living_wage: float,
                     lw_year: int, country: str, chance_or_risk: str,
                     inflation_indicator: pd.DataFrame):
        if mw_year < lw_year:
            mining_wage = _raised_wage(mining_wage, mw_year, lw_year - mw_year, country,
                                       inflation_indicator)
        if lw_year < mw_year:
            living_wage = _raised_wage(living_wage, lw_year, mw_year - lw_year, country,
                                       inflation_indicator)
        if chance_or_risk == "chance":
            amount = 1 - living_wage / mining_wage if mining_wage > living_wage else 0.0
        else:  # 'risk'
            amount = 1 - mining_wage / living_wage if mining_wage < living_wage else 0.0
        return amount

    # Wage chance
    chance_df = _wage_indicator(monthly_mining_earnings, monthly_living_wage, price_index_change,
                                countries, "chance")
    chance_df['source'] = "TODO"  # TODO
    chance_dataframe = chance_df.rename({"regime": "regime_slug"}, axis=1) \
                                .filter(["regime_slug", "amount", "source"]) \
                                .sort_values("regime_slug")
    chance_metadata = compile_metadata(chance_dataframe,
                                       "Chance of earning a living wage",
                                       "",  # TODO descrpition
                                       "Based on living wage and mining wage data")
    # Wage risk
    risk_df = _wage_indicator(monthly_mining_earnings, monthly_living_wage, price_index_change,
                              countries, "risk")
    risk_df['source'] = "TODO"  # TODO
    risk_dataframe = risk_df.rename({"regime": "regime_slug"}, axis=1) \
                            .filter(["regime_slug", "amount", "source"]) \
                            .sort_values("regime_slug")
    risk_metadata = compile_metadata(risk_dataframe,
                                     "Risk of earning a non-living Wage",
                                     "",  # TODO descrpition
                                     "Based on living wage and mining wage data")
    return Output(value=risk_dataframe, metadata=risk_metadata),\
           Output(value=chance_dataframe, metadata=chance_metadata)
