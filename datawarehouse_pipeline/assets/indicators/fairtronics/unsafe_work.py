import pandas as pd
from dagster import asset, AutoMaterializePolicy, Output, ExperimentalWarning, AssetIn
from datawarehouse_pipeline.types import ft_indicator_type
from .utils import compile_metadata
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       dagster_type=ft_indicator_type,
       ins={"fatal_occupational_injuries": AssetIn(key=["indicators", "fatal_occupational_injuries"]),
            "non_fatal_occupational_injuries": AssetIn(key=["indicators", "non_fatal_occupational_injuries"]),
            "countries": AssetIn(key=["regimes", "countries"])},
       key_prefix="indicators",
       compute_kind="Fairtronics")
def unsafe_work(fatal_occupational_injuries: pd.DataFrame,
                non_fatal_occupational_injuries: pd.DataFrame,
                countries: pd.DataFrame) -> Output[pd.DataFrame]:
    """
    Unsafe Working Condition Indicator for Fairtronics
    """
    # Merge both indicators and add values
    df = pd.merge(non_fatal_occupational_injuries, fatal_occupational_injuries,
                  how="inner", left_on="country", right_on="country")
    df['amount'] = (df['value_x'] + df['value_y']) / 100000
    # Merge with countries
    df = pd.merge(df, countries, how="inner", left_on="country", right_on="alpha_3")
    df['regime'] = df['slug']
    df['source'] = "Entry of country " + df['country'] + " for fatal and non-fatal injuries during work"
    dataframe = df.rename({"regime": "regime_slug"}, axis=1) \
                  .filter(["regime_slug", "amount", "source"]) \
                  .sort_values("regime_slug")
    metadata = compile_metadata(dataframe,
                                "Unsafe Working Conditions for Fairtronics",
                                "",  # TODO descrpition
                                "Based on fatal and non-fatal injuries during work, by ILOSTAT")
    return Output(value=dataframe, metadata=metadata)
