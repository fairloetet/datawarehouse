import pandas as pd
from dagster import multi_asset, AutoMaterializePolicy, Output, ExperimentalWarning, AssetOut, AssetIn
from datawarehouse_pipeline.types import ft_indicator_type
from .utils import compile_metadata
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@multi_asset(outs={"with_social_protection": AssetOut(
                            io_manager_key="indicators_duckdb_io_manager",
                            auto_materialize_policy=AutoMaterializePolicy.eager(),
                            dagster_type=ft_indicator_type,
                            key_prefix="indicators"),
                   "no_social_protection": AssetOut(
                            io_manager_key="indicators_duckdb_io_manager",
                            auto_materialize_policy=AutoMaterializePolicy.eager(),
                            dagster_type=ft_indicator_type,
                            key_prefix="indicators")},
       ins={"social_protection_coverage": AssetIn(key=["indicators", "social_protection_coverage"]),
            "countries": AssetIn(key=["regimes", "countries"])},
             compute_kind="Fairtronics")
def social_protection(social_protection_coverage: pd.DataFrame,
                      countries: pd.DataFrame) -> tuple[Output[pd.DataFrame], Output[pd.DataFrame]]:
    """
    Social protection coverage rate, positive or negative
    """
    # Merge with countries
    df = pd.merge(social_protection_coverage, countries, how="inner", left_on="country", right_on="alpha_3")

    # Positive indicator
    with_df = pd.DataFrame()
    with_df['regime'] = df['slug']
    with_df['amount'] = df['value'] / 100
    with_df['source'] = "Entry for country " + df['country'] + " in ILOSTAT social protection coverage rate"
    with_dataframe = with_df.rename({"regime": "regime_slug"}, axis=1) \
                            .filter(["regime_slug", "amount", "source"]) \
                            .sort_values("regime_slug")
    with_metadata = compile_metadata(with_dataframe,
                                     "With Social protection",
                                     "",  # TODO descrpition
                                     "Based on ILOSTAT data")
    # Negative indicator
    no_df = pd.DataFrame()
    no_df['regime'] = df['slug']
    no_df['amount'] = 1 - (df['value'] / 100)
    no_df['source'] = "Inverse value of entry for country " + df['country'] + " in ILOSTAT social protection coverage rate"
    no_dataframe = no_df.rename({"regime": "regime_slug"}, axis=1) \
                        .filter(["regime_slug", "amount", "source"]) \
                        .sort_values("regime_slug")
    no_metadata = compile_metadata(with_dataframe,
                                   "Without Social protection",
                                   "",  # TODO descrpition
                                   "Based on ILOSTAT data")

    return Output(value=with_dataframe, metadata=with_metadata), Output(value=no_dataframe, metadata=no_metadata)
