import pandas as pd
from dagster import asset, AutoMaterializePolicy, Output, ExperimentalWarning, AssetIn
from datawarehouse_pipeline.types import ft_indicator_type
from .utils import compile_metadata
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       dagster_type=ft_indicator_type,
       ins={"share_of_working_children_in_labor_force": AssetIn(key=["indicators", "share_of_working_children_in_labor_force"]),
            "avg_working_hours_per_week_of_working_child": AssetIn(key=["indicators", "avg_working_hours_per_week_of_working_child"]),
            "countries": AssetIn(key=["regimes", "countries"])},
       key_prefix="indicators",
       compute_kind="Fairtronics")
def child_labour(share_of_working_children_in_labor_force: pd.DataFrame,
                 avg_working_hours_per_week_of_working_child: pd.DataFrame,
                 countries: pd.DataFrame) -> Output[pd.DataFrame]:
    """
    Child Labour Indicator for Fairtronics
    """
    df = pd.merge(share_of_working_children_in_labor_force,  # _x
                  avg_working_hours_per_week_of_working_child,  # _Y
                  how="inner", on=["economy", "country"])
    # Amount of child labour is number of children multiplied with duration of child work per full week
    df["amount"] = df["value_x"] * (df["value_y"] / 48)  # Expects typical work week to have 48 hours
    df["source"] = df["source_x"] + " * (" + df["source_y"] + " / 48 hours a week)"
    # Translate economy to Fairtronics country slug using the alpha-3 key
    df["regime"] = df["economy"].map(countries.dropna(subset="alpha_3").set_index("alpha_3")["slug"])
    dataframe = df.rename({"regime": "regime_slug"}, axis=1) \
                  .filter(["regime_slug", "amount", "source"]) \
                  .sort_values("regime_slug")
    metadata = compile_metadata(dataframe,
                                "Child Labour indicator for Fairtronics",
                                "Opposed to many other, Fairtronics' indicator for child labour considers the working "
                                "time (i.e. not working all day lowers the probability of child labour) as well "
                                "the number of working children within the whole work force of a country.",
                                "Based on ILOSTAT data")
    return Output(value=dataframe, metadata=metadata)
