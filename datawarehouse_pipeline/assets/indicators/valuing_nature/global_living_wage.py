import pandas as pd
from dagster import asset, Output, MetadataValue, AssetIn
#from datawarehouse_pipeline.resources import NextcloudResource
from datawarehouse_pipeline.resources import WebDavResource
import country_converter as coco


@asset(io_manager_key="indicators_duckdb_io_manager", 
       ins={"exchange_rate": AssetIn(key=["indicators", "exchange_rate"])},
       key_prefix="indicators",
       compute_kind="Valuing Nature")
def monthly_living_wage(webdav_client: WebDavResource,
                        exchange_rate: pd.DataFrame) -> Output[pd.DataFrame]:
    """
       Global Living Wage data from 2022 in local currency

       See https://www.valuingnature.ch/blog/categories/data

       Reading latest provided Dataset Excel sheet
    """
    # First read living wage data
    COUNTRY_CODE_COLUMN = "Country_code"
    COUNTRY_NAME_COLUMN = "Country_name"
    REGION_COLUMN = "Region"
    CITY_COLUMN = "City"
    LIVING_WAGE_COLUMN = "LW for a Typical family (USD/year)"
    SOURCE_COLUMN = "Source (A/B)"
    with webdav_client.open("Indikatoren/Living_Wage_Dataset_2023*.xlsx") as file:
        living_wage = pd.read_excel(file, sheet_name=1, na_values="-", thousands='.', decimal=',')\
               .dropna(subset=[COUNTRY_CODE_COLUMN, LIVING_WAGE_COLUMN])
    living_wage["country"] = living_wage[COUNTRY_CODE_COLUMN]
    # Valuing Nature's living wage is given as yearly wage. Convert it to monthly wage.
    living_wage['value'] = living_wage[LIVING_WAGE_COLUMN] / 12.0
    living_wage['year'] = 2022
    living_wage['source'] = living_wage[SOURCE_COLUMN].map({
       "b": "Linear regression model",
       "a": "Numbeo model (https://www.numbeo.com/cost-of-living/)",
       "a/b": "Linear regression and Numbeo model (https://www.numbeo.com/cost-of-living/)"
    })
    living_wage['source'] = living_wage[COUNTRY_NAME_COLUMN] + " value (local currency) within Global Living Wage " + \
        "Dataset 2022 by Valuing Nature, using the " + living_wage['source']
    living_wage = living_wage[living_wage[CITY_COLUMN] == "Country"]
    # Then merge with exchange rate
    exchange_rate['value'] = exchange_rate['YR2022']
    exchange_rate.dropna(subset="value", inplace=True)
    df = pd.merge(living_wage, exchange_rate, how='inner', suffixes=('_x', '_y'), left_on='country', right_on='economy') 
    df['value'] = df['value_x'] * df['value_y']
    df['source'] = df['source_x']
    df['country'] = df['country_x']
    dataframe = df[['country', 'year', 'value', 'source']]
    metadata = dict()
    metadata["name"] = "Global Living Wage Dataset 2023 by Valuing Nature"
    metadata["source"] = MetadataValue.md(
       "Dataset provided according to https://www.valuingnature.ch/post/global-living-wage-dataset-2022")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="pandas_io_manager", 
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="indicators",
       compute_kind="Valuing Nature")
def monthly_living_wage_2019(webdav_client: WebDavResource) -> Output[pd.DataFrame]:
    """
       Global Living Wage data from 2019 in local currency

       See https://www.valuingnature.ch/blog/categories/data

       Reading provided Dataset Excel sheet
    """
    COUNTRY_NAME_COLUMN = "Country Name"
    REGION_CITY_COLUMN = "Region/City"
    LIVING_WAGE_COLUMN = "Living wage - Typ. Family (local currency)"
    SOURCE_COLUMN = "Source (a=WageIndicator, b=Numbeo, c=PPP model)"
    with webdav_client.open("Indikatoren/ValuingNature_LivingWageDataset*.xlsx") as file:
        df = pd.read_excel(file, sheet_name=1, na_values="-", thousands='.', decimal=',')\
               .dropna(subset=[COUNTRY_NAME_COLUMN, LIVING_WAGE_COLUMN])
    cc = coco.CountryConverter()
    df["country"] = cc.pandas_convert(series=df[COUNTRY_NAME_COLUMN], to='ISO3')
    # Valuing Nature's living wage is given as yearly wage. Convert it to monthly wage.
    df['value'] = df[LIVING_WAGE_COLUMN] / 12.0
    df['year'] = 2019
    df['source'] = df[SOURCE_COLUMN].map({
       "c": "PPP model",
       "a": "WageIndicator model",
       "b": "Numbeo model"
    })
    df['source'] = df[COUNTRY_NAME_COLUMN] + " value within Global Living Wage Dataset 2022 by Valuing Nature, using the " + df['source']
    df = df[df[REGION_CITY_COLUMN] == "Country"]
    dataframe = df[['country', 'year', 'value', 'source']]
    metadata = dict()
    metadata["name"] = "Global Living Wage Dataset from 2019 by Valuing Nature"
    metadata["source"] = MetadataValue.md(
       "Dataset provided according to https://www.valuingnature.ch/post/global-living-wage-dataset-2022")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
