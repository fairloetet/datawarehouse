from datawarehouse_pipeline.resources import IlostatResource
from datawarehouse_pipeline.types import ilostat_sdmx_type
import pandas as pd
from dagster import asset, Output


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def monthly_mining_earnings(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Average monthly earnings of employees (local currency) within sector 'Mining and Quarrying'
    """
    indicator = "EAR_4MTH_SEX_ECO_CUR_NB"
    dataframe, metadata = ilostat_sdmx_client.get_most_recent_values_as_dataframe(
        indicator, keys={"ECO": "ECO_ISIC4_B+ECO_ISIC3_C+ECO_ISIC2_2",  # Mining and Quarrying
                         "CUR": "CUR_TYPE_LCU",  # Local currency
                         "SEX": "SEX_T",  # Total (female+male)
                         "FREQ": "A"})  # Annual
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def price_index_change(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    National consumer price index (CPI) on 'Individual consumption expenditure of households',
    percentage change from previous year
    """
    indicator = "CPI_NCYR_COI_RT"
    dataframe, metadata = ilostat_sdmx_client.get_values_as_dataframe(
        indicator, keys={"FREQ": "A",
                         "COI": "COI_COICOP_CP01T12"})
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def fatal_occupational_injuries(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Fatal occupational injuries  per year, in percent of workers in the mining sectorin the mining sector per 100'000 workers per year
    """
    indicator = "INJ_FATL_ECO_RT"
    dataframe, metadata = ilostat_sdmx_client.get_mean_values_as_dataframe(
        indicator, keys={"ECO": "ECO_ISIC4_B+ECO_ISIC3_C+ECO_ISIC2_2",
                         "FREQ": "A"},
        number_of_years=3)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def non_fatal_occupational_injuries(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Non-fatal occupational injuries in the mining sector per 100'000 workers per year
    """
    indicator = "INJ_NFTL_ECO_RT"
    dataframe, metadata = ilostat_sdmx_client.get_mean_values_as_dataframe(
        indicator, keys={"ECO": "ECO_ISIC4_B+ECO_ISIC3_C+ECO_ISIC2_2",
                         "FREQ": "A"},
        number_of_years=3)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def employed_persons_having_excessive_working_time(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Employed persons (in thousend) having weekly hours actually worked above 48; not specific to mining
    """
    indicator = "EMP_TEMP_SEX_HOW_NB"
    dataframe, metadata = ilostat_sdmx_client.get_most_recent_values_as_dataframe(
        indicator, keys={"HOW": "HOW_BANDS_HGE49",
                         "SEX": "SEX_T",
                         "FREQ": "A"})
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def labour_force(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Number of people working (in thousend), not sector specific
    """
    indicator = "EAP_TEAP_SEX_AGE_NB"
    dataframe, metadata = ilostat_sdmx_client.get_most_recent_values_as_dataframe(
        indicator, keys={"AGE": "AGE_AGGREGATE_TOTAL",
                         "SEX": "SEX_T",
                         "FREQ": "A"})
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def mean_working_hours(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Mean weekly hours actually worked per employed person in mining sector
    """
    indicator = "HOW_TEMP_SEX_ECO_NB"
    dataframe, metadata = ilostat_sdmx_client.get_most_recent_values_as_dataframe(
        indicator, keys={"ECO": "ECO_ISIC4_B+ECO_ISIC3_C+ECO_ISIC2_2",
                         "SEX": "SEX_T",
                         "FREQ": "A"})
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def collective_bargaining_coverage(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Collective bargaining coverage rate, not sector-specific
    """
    indicator = "ILR_CBCT_NOC_RT"
    dataframe, metadata = ilostat_sdmx_client.get_most_recent_values_as_dataframe(
        indicator, keys={"FREQ": "A"})
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="ILOSTAT", 
       dagster_type=ilostat_sdmx_type)
def social_protection_coverage(ilostat_sdmx_client: IlostatResource) -> Output[pd.DataFrame]:
    """
    Proportion of population covered by at least one social protection benefit out of
    * Persons above retirement age receiving a pension
    * Persons with severe disabilities collecting disability social protection benefits
    * Unemployed receiving unemployment benefits
    * Mothers with newborns receiving maternity benefits
    * Employed covered in the event of work injury
    * Children/households receiving child/family cash benefits
    * Poor persons covered by social protection systems
    * Vulnerable persons covered by social assistance
    """
    indicator = "SDG_0131_SEX_SOC_RT"
    dataframe, metadata = ilostat_sdmx_client.get_most_recent_values_as_dataframe(
        indicator, keys={"FREQ": "A",
                         "SEX": "SEX_T",
                         "SOC": "SOC_CONTIG_TOTAL"})
    return Output(value=dataframe, metadata=metadata)
