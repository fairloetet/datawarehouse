from typing import Dict, Any
from dagster import MetadataValue
import pandas as pd


def compile_metadata(df: pd.DataFrame, name: str, description: str, head_count: int = 3) -> Dict[str, Any]:
    result: Dict[str, Any] = dict()
    result["name"] = MetadataValue.md(name)
    result["description"] = MetadataValue.md(description)
    result["number of countries"] = len(df.index)
    result["number of years"] = len(df.columns) - 2
    result["preview"] = MetadataValue.md(df.T.head(n=head_count).T.head().to_markdown())
    return result


def specialize_columns(df: pd.DataFrame, name: str) -> pd.DataFrame:
    df.rename(columns={"value": name, "year": "year_" + name})
    return df
