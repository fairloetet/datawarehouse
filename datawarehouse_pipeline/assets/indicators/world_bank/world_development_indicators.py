from datawarehouse_pipeline.resources import WorldbankWdiResource
from datawarehouse_pipeline.types import wb_wdi_type
import pandas as pd
from dagster import asset, Output


@asset(io_manager_key="indicators_duckdb_io_manager", 
       compute_kind="World Bank", 
       key_prefix="indicators",
       dagster_type=wb_wdi_type)
def avg_working_hours_per_week_of_working_child(wb_wdi_api: WorldbankWdiResource) -> Output[pd.DataFrame]:
    """
    Average working hours of children, working only, ages 7-14 (hours per week)
    """
    series = "SL.TLF.0714.WK.TM"
    dataframe, metadata = wb_wdi_api.get_most_recent_values_as_dataframe(series)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       compute_kind="World Bank", 
       key_prefix="indicators",
       dagster_type=wb_wdi_type)
def children_in_employment(wb_wdi_api: WorldbankWdiResource) -> Output[pd.DataFrame]:
    """
    Children in employment, total (% of children ages 7-14)
    """
    series = "SL.TLF.0714.ZS"
    dataframe, metadata = wb_wdi_api.get_most_recent_values_as_dataframe(series)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="World Bank", 
       dagster_type=wb_wdi_type)
def employed_children_in_work(wb_wdi_api: WorldbankWdiResource) -> Output[pd.DataFrame]:
    """
    Children in employment, work only (% of children in employment, ages 7-14)
    """
    series = "SL.TLF.0714.WK.ZS"
    dataframe, metadata = wb_wdi_api.get_most_recent_values_as_dataframe(series)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="World Bank")
def labor_force_participation_rate_grownups(wb_wdi_api: WorldbankWdiResource) -> Output[pd.DataFrame]:
    """
    Labor force participation rate, total (% of total population ages 15-64) (modeled ILO estimate)
    """
    series = "SL.TLF.ACTI.ZS"
    dataframe, metadata = wb_wdi_api.get_most_recent_values_as_dataframe(series)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="World Bank")
def population_children_total(wb_wdi_api: WorldbankWdiResource) -> Output[pd.DataFrame]:
    """
    Population ages 0-14, total
    """
    series = "SP.POP.0014.TO"
    # TODO Achtung: 0-14 sind zu viele Kinder, da sich die Zahlen der arbeitenden Kinder auf % von 7-14 beziehen
    # TODO  Es gibt in den WDI keine total population ages 7-15, nur prozentuale Anteile von 5-9 und 10-14 Jahre,
    # TODO  und dies auch noch nur nach Geschlecht getrennt, so dass man dies auch noch weiter rechnen muss
    # TODO  bis man zu einer tatsächlichen Anzahl 5-15 oder 10-15 kommt. Sehr mühsam.
    dataframe, metadata = wb_wdi_api.get_most_recent_values_as_dataframe(series)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="World Bank")
def population_grownups_total(wb_wdi_api: WorldbankWdiResource) -> Output[pd.DataFrame]:
    """
    Population ages 15-64, total
    """
    series = "SP.POP.1564.TO"
    dataframe, metadata = wb_wdi_api.get_most_recent_values_as_dataframe(series)
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager", 
       key_prefix="indicators",
       compute_kind="World Bank")
def exchange_rate(wb_wdi_api: WorldbankWdiResource) -> Output[pd.DataFrame]:
    """
    Exchange rate (LCU per US$, period average) as DEC alternative conversion factor
    """
    series = "PA.NUS.ATLS"
    dataframe, metadata = wb_wdi_api.get_all_values_as_dataframe(series)
    return Output(value=dataframe, metadata=metadata)
