from dagster import asset, Output, AutoMaterializePolicy, AssetIn
from .utils import compile_metadata
import pandas as pd
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"population_grownups_total": AssetIn(key=["indicators", "population_grownups_total"]),
            "labor_force_participation_rate_grownups": AssetIn(key=["indicators", "labor_force_participation_rate_grownups"])},
       key_prefix="indicators",
       compute_kind="World Bank (compiled)")
def number_of_grownup_workers(population_grownups_total: pd.DataFrame,
                              labor_force_participation_rate_grownups: pd.DataFrame) -> Output[pd.DataFrame]:
    '''
    Number of grownups in employment (ages 15-64)
    '''
    df = pd.merge(population_grownups_total,  # _x
                  labor_force_participation_rate_grownups,  # _y
                  on=["economy", "country"])
    df["value"] = (df["value_y"] / 100) * df["value_x"]
    df["source"] = "(" + df["source_y"] + " / 100) * " + df["source_x"]
    df["year"] = df[["year_y", "year_x"]].min(axis=1)
    dataframe = df[["economy", "country", "value", "year", "source"]]
    metadata = compile_metadata(dataframe, "Number of grownups in employment (ages 15-64)", "")
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"population_children_total": AssetIn(key=["indicators", "population_children_total"]),
            "children_in_employment": AssetIn(key=["indicators", "children_in_employment"]),
            "employed_children_in_work": AssetIn(key=["indicators", "employed_children_in_work"])},
       key_prefix="indicators",
       compute_kind="World Bank (compiled)")
def number_of_child_workers(population_children_total: pd.DataFrame,
                            children_in_employment: pd.DataFrame,
                            employed_children_in_work: pd.DataFrame) -> Output[pd.DataFrame]:
    '''
    Number of children in employment (ages 7-14)
    '''
    # TODO Multiply with "employed_children_in_work" as well... unten auskommentiert
    df = pd.merge(population_children_total,  # _x
                  children_in_employment,  # _y
                  on=["economy", "country"]) \
           # .merge(employed_children_in_work,  # without suffix
           #        on=["economy", "country"])
    # df["value"] = ((df["value_y"] / 100) * (df["value"] / 100) * df["value_x"]).round()
    # df["source"] = "((" + df["source_y"] + " / 100) * (" + df["source"] + " / 100) * " + df["source_x"] + ")"
    # df["year"] = df[["year_y", "year_x", "year"]].min(axis=1)
    df["value"] = ((df["value_y"] / 100) * df["value_x"]).round()
    df["source"] = "(" + df["source_y"] + " / 100) * " + df["source_x"]
    df["year"] = df[["year_y", "year_x"]].min(axis=1)
    dataframe = df[["economy", "country", "value", "year", "source"]]
    metadata = compile_metadata(dataframe, "Number of children in employment (ages 7-14)", "")
    return Output(value=dataframe, metadata=metadata)


@asset(io_manager_key="indicators_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"number_of_child_workers": AssetIn(key=["indicators", "number_of_child_workers"]),
            "number_of_grownup_workers": AssetIn(key=["indicators", "number_of_grownup_workers"])},
       key_prefix="indicators",
       compute_kind="World Bank (compiled)")
def share_of_working_children_in_labor_force(number_of_child_workers: pd.DataFrame,
                                             number_of_grownup_workers: pd.DataFrame) -> Output[pd.DataFrame]:
    '''
    Share of working children (ages 7-14) within labor force (ages 4-64)
    '''
    df = pd.merge(number_of_child_workers,  # _x
                  number_of_grownup_workers,  # _y
                  on=["economy", "country"])
    df["value"] = df["value_x"] / (df["value_x"] + df["value_y"])
    df["source"] = "((" + df["source_x"] + " / (" + df["source_x"] + " + " + df["source_y"] + "))"
    df["year"] = df[["year_y", "year_x"]].min(axis=1)
    dataframe = df[["economy", "country", "value", "year", "source"]]
    metadata = compile_metadata(dataframe, "Share of working children (ages 7-14) within labor force (ages 7-64)", "")
    return Output(value=dataframe, metadata=metadata)
