from dagster import asset, Output, MetadataValue, AutoMaterializePolicy, AssetExecutionContext, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

#import pandera as pa
#from dagster_pandera import pandera_schema_to_dagster_type
#from pandera.typing import Series
#
#class ManufacturersModel(pa.SchemaModel):
#    slug: Series[str] = pa.Field(description="Key")
#    name: Series[str] = pa.Field(description="Name of mine")
#    short_name: Series[str] = pa.Field(description="Short name für mines")
#    company_url: Series[str] = pa.Field(description="Web address of company")

@asset(io_manager_key="devices_duckdb_io_manager",
#       dagster_type=pandera_schema_to_dagster_type(ManufacturersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"devices": AssetIn(key=["devices", "devices"]),
            "device_makers": AssetIn(key=["regimes", "device_makers"]),
            "parts": AssetIn(key=["parts", "parts"])},
       key_prefix="devices",
       compute_kind="Fairtronics")
def device_parts(context: AssetExecutionContext,
                 devices: pd.DataFrame,
                 device_makers: pd.DataFrame,  # Eigentlich nötig, wenn ich einen Markt zu den Parts habe, oder?
                 parts: pd.DataFrame) -> Output[pd.DataFrame]:

    def process_devices_parts_file(device_slug: str, device_parts_df: pd.DataFrame, 
                                   parts_df: pd.DataFrame, devices_df: pd.DataFrame) -> pd.DataFrame:
        """
        Process a single part file and write it to the output path.
        """
        # TODO Not a pandas way to do this
        unknown_parts = [part for part in device_parts_df['Part.slug'] if part not in parts_df['slug'].values]
        if unknown_parts:
            raise KeyError("Device '" + device_slug + "' has unknown part(s) " + str(unknown_parts))
        result_df = pd.DataFrame()
        result_df['Device.slug'] = device_parts_df['Device.slug']
        result_df['Part.slug'] = device_parts_df['Part.slug']
        result_df['amount'] = device_parts_df.apply(lambda row: evaluate(str(row['amount'])), axis=1)
        # part_url = parts_df['product_url'][device_parts_df['Part.slug']].fillna("A part").apply(str)
        # device_url = devices_df['project_url'][device_parts_df['Device.slug']].apply(str)
        result_df['source'] = "" # part_url + " in " + device_url
        return result_df

    def process_device_devices_file(df, device_dict):
        """
        Process a single devices file and write it to the output path.
        """
        # TODO Currently this only works reliably for hierarchy depth of at most 1, i.e. not deeper,
        # TODO    i.e. if a device depends on a device which itself depends on a device, it fails
        # TODO Not a pandas way to do this
        result_df = pd.DataFrame()
        for index, row in df.iterrows():
            subdevice_df = device_dict[row['Subdevice.slug']].copy()
            subdevice_df['Device.slug'] = row['Device.slug']
    #        subdevice_df = subdevice_df.assign(deviceslug=str(df['Device.slug']))
            for _ in range(int(eval(str(row['amount'])))):
                result_df = pd.concat([result_df, subdevice_df])
        return result_df

    def evaluate(amount: str):
        return eval(amount)

    device_dict = dict()

    # First read in all parts files
    # TODO Not a pandas way to make this
    for device_slug in devices['slug']:
        device_parts_file = "./datawarehouse_pipeline/assets/devices/fairtronics/devices/" + device_slug + ".parts.csv"
        device_parts_df = pd.read_csv(device_parts_file, comment='#')
        output_df = process_devices_parts_file(device_slug, device_parts_df, parts, devices)
        device_dict[device_slug] = output_df
    # Then, after all devices have been processed to parts, look for devices in the devices to include them
    for device_slug in devices['slug']:
        device_devices_file = "./datawarehouse_pipeline/assets/devices/fairtronics/devices/" + device_slug + ".devices.csv"
        if os.path.isfile(device_devices_file):
            device_parts_df = pd.read_csv(device_devices_file, comment='#')
            output_df = process_device_devices_file(device_parts_df, device_dict)
            device_dict[device_slug] = pd.concat([device_dict[device_slug], output_df])
    # Now, concatenate them all
    full_df = pd.concat(device_dict.values(), sort=False, ignore_index=True)
    # Now that we transitively have all the parts of the device
    os.makedirs("pipeline_run/result", exist_ok=True)
    full_df.to_csv("pipeline_run/result/device_parts.csv", 
                   columns=['Device.slug', 'Part.slug', 'amount', 'source'],
                   index=False)

    dataframe = full_df.rename(columns={"Device.slug": "device_slug", "Part.slug": "part_slug"})    
    metadata = dict()
    metadata["name"] = "List of parts of any device used in Fairtronics"
    metadata["source"] = MetadataValue.md("Recursively compiled from device->device- and device->part-relationsships")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
