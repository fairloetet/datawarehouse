from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

#import pandera as pa
#from dagster_pandera import pandera_schema_to_dagster_type
#from pandera.typing import Series
#
#class ManufacturersModel(pa.SchemaModel):
#    slug: Series[str] = pa.Field(description="Key")
#    name: Series[str] = pa.Field(description="Name of mine")
#    short_name: Series[str] = pa.Field(description="Short name für mines")
#    company_url: Series[str] = pa.Field(description="Web address of company")


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def devices_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/devices/fairtronics/devices_base.csv")))


@asset(io_manager_key="devices_duckdb_io_manager",
#       dagster_type=pandera_schema_to_dagster_type(ManufacturersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[devices_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="devices",
       compute_kind="Fairtronics")
def devices(context: AssetExecutionContext) -> Output[pd.DataFrame]:

    devices_df = pd.read_csv("./datawarehouse_pipeline/assets/devices/fairtronics/devices_base.csv", 
                             na_values={'short_name': ''}, keep_default_na=False, comment='#') 
    devices_df['short_name'] = devices_df['short_name'].fillna(devices_df['name'])
    os.makedirs("pipeline_run/result", exist_ok=True)
    devices_df.to_csv("pipeline_run/result/devices.csv", 
                    columns=['slug', 'name', 'short_name', 'Manufacturer.slug', 'dependency', 
                             'description', 'project_url'],
                    index=False)
    dataframe = devices_df.rename(columns={"Manufacturer.slug": "manufacturer_slug"})    
    metadata = dict()
    metadata["name"] = "List of devies as used in Fairtronics"
    metadata["source"] = MetadataValue.md("")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
