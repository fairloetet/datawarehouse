C:\Users\Seba\AppData\Local\Programs\Python\Python38\python.exe "C:\Program Files\JetBrains\PyCharm Community Edition 2020.2\plugins\python-ce\helpers\pydev\pydevconsole.py" --mode=client --port=62736
import sys; print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['C:\\Users\\Seba\\PycharmProjects\\orga', 'C:/Users/Seba/PycharmProjects/orga'])
PyDev console: starting.
Python 3.8.0 (tags/v3.8.0:fa919fd, Oct 14 2019, 19:37:50) [MSC v.1916 64 bit (AMD64)] on win32
runfile('C:/Users/Seba/PycharmProjects/orga/pocs/bom/convert_bom.py', args=['C:\\Users\\Seba\\PycharmProjects\\orga/data/manufacturers/manufacturers.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/parts/parts.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/products/products.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/components/components.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/components/component_categories.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/components/component_attributes.csv'], wdir='C:/Users/Seba/PycharmProjects/orga/pocs/bom')

Converting reform2-trackball.csv:
x  Found yageo-cc0603npo for part CC0603JRNPO9BN180 from Yageo
x  Found yageo-cc0603x7r for part CC0603JPX7R9BB104 from Yageo
x  Found tdk-c1608np0 for part C1608X8L1C105K080AC from TDK
x  Found osram-lwq38e for part LW Q38E-Q2OO-3K5L from OSRAM
x  Couldn't find a match for part number BZT52-B5V6J of manufacturer Nexperia. Suggesting nexperia-bzt52b5v6j
x  Couldn't find a match for part number 0ZCJ0075AF2E of manufacturer Bel Fuse. Suggesting bel_fuse-0zcj
x  Found murata-blm18 for part BLM18PG221SH1D from Murata
x  Found jst-b4bphks for part B4B-PH-K-S(LF)(SN) from JST
x  Couldn't find a match for part number FH12-6S-0.5SH(55) of manufacturer Hirose. Suggesting hirose-fh126
x  Missing manufacturer or part number for entry with value BADGE. Setting to unknown
x  Missing manufacturer or part number for entry with value Mounting_Hole. Setting to unknown
x  Missing manufacturer or part number for entry with value LOGO. Setting to unknown
x  Found yageo-rc0603 for part RC0603FR-074K7L from Yageo
x  Found vishay-crcw0603 for part CRCW060310K0JNEAC from Vishay Dale
x  Found yageo-rc0603 for part RC0603FR-07475RL from Yageo
x  Couldn't find a match for part number RT0603DRD0722RL of manufacturer Yageo. Suggesting yageo-rt0603
x  Couldn't find a match for part number CPG135001D02 of manufacturer Kailh. Suggesting kailh-cpg135001d02
x  - The same brown choc switch as in the keyboard, siehe dort
x  -> Das # am Beginn dieses Eintrags in *.parts.csv entfernen
x  Found ck-KMR2 for part KMR221GLFS from C&K
x  Found apem-dm01 for part DM01 from Apem
x  Found microchip-ATMEGA32U2-AU for part ATMEGA32U2-AU from Microchip
x  Found texas_instruments-TLV75533PDBVR for part TLV75533PDBVR from Texas Instruments
x  Couldn't find a match for part number USBLC6-2SC6 of manufacturer STMicroelectronics. Suggesting stmicroelectronics-usblc62sc6
x  Couldn't find a match for manufacturer 'Abracon'. Suggesting abracon
x  Couldn't find a match for part number ABM8AIG-16.000MHz-4-T of manufacturer Abracon. Suggesting abracon-abm8aig
Wrote reform2-trackball.parts.csv with 13 identified parts out of 20 with data.
3 entries had missing data, please review them. They may be deleted or remain unknown.

Additionally:
x   PCB Controllermodul: reform2-trackball-r1-gerbers\reform2-trackball-job.gbrjob:
x      Name = reform2-trackball
x      Size = 87mm x 59mm
x      Layer count = 2
x      Board thickness = 1.60mm
x   Cable
x     - Zum Sensor 6-pin 0.5mm pitch flex cable: Würth Elektronik 687606050002 zwischen Sensor und FB-Platine -> Trackpad
x     - Verbindung zum Mainboard: USB/SYSCTL Cable -> Trackpad
x  Keycaps
x      Trackball Button Big (SLA) - MNT Research - MREFXTB120R01
x        2x reform2-trackball-button-circular: siehe csv, wirklich epoxy_resin? "SLA printed"
x      Trackball Button Small (SLA) - MNT Research MREFXTB220R01
x         3x reform2-trackball-button-rectangular: siehe csv, wirklich epoxy_resin? "SLA printed"
x   Fassungen
x      lid.csv: Trackball Lid (SLA) MNT Research MREFXTBL20R01
x         1x reform2-trackball-lid: siehe csv. epoxy_resin ok? "SLA-printed"
x      support.csv: Trackball Cup (SLA) MNT Research MREFXTBC20R01 - 3D Printed PLA... P?
x   Ball
x      MNT Research MREFBALB20R01
x      Trackball POM ball (25 mm diameter). We ordered custom black POM (https://de.wikipedia.org/wiki/Polyoxymethylene)
x      25mm diameter POM sphere, black
x      25mm Durchmesser entspricht 8181,23 mm3 = 8,18123 cm3. Dicht ist 1.42 g/cm3 => Gewicht also 11,6g
x    Gewinde
x      TB M2 Thread Insert - KVT 300113472 (Distributor) / Tappex HiMOULD 017/117 (Produktanbieter) (laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
x      Produktanbieter: https://www.tappex.co.uk/products/brass-threaded-inserts/himould?type=type-b&thread=M2&length=3.925&extras=#selector
x      Distributor (inkl. 3D-Modell): https://www.bossard.com/eshop/de-de/gewindeeinsaetze-fuer-kunststoffmaterialien/gewindeeinsaetze-zum-einlegen/tappex-himould/p/37868/
x      "Mount 2x TB M2 Thread"
x      Werkstoff: Messing
x    Schrauben
x      2x M2x5-965H + 4x M2x4-7985H + 2x M2x4-7985H, schon in der großen Liste drin
x      Befestigung an Motherboard: 4x M2x4-7985H, schon in der großen Liste drin
x   Lot

Seeking for data on the new parts, especially which component it is based on:
x  For part nexperia-bzt52b5v6j :
x    Octopart says it's this: BZT52-B Series 5.6 V ±1.96% 590 mW SMT Single Zener Diode - SOD-123
x    The data of part nexperia-bzt52b5v6j doesn't know at least the mandatory attribute contactplating or any of its alternatives (termination,contactmaterial)
x    Uh, couldn't find any matching component
x  For part bel_fuse-0zcj :
x    Octopart says it's this: PTC Resettable Fuse 0.75A(hold) 1.5A(trip) 16VDC 100A 0.6W 0.2s 0.09Ohm SMD Solder Pad 1206 T/R Automotive
x    -> Note that category ptc-resettable-fuses should be maintained because it doesn't have any mandatory attribute, which means that every component matches
x    So we suggest polymeric_ptc_resettable_fuse_2920 because it's the first known component which matches
x  For part hirose-fh126 :
x    Octopart says it's this: Connectors for Connecting - 0.5/1-mm Pitch FPC/FFC - FH12 Series (HIROSE ELECTRIC)
x    connector_ffc-fpc_33_circuits_0.5mm won't fit, because its value Bronze of the mandatory attribute material doesn't match the requested value Polyamide
x    10-fpc-zif-btm-cont-embt-pkg-4ckt couldn't fit,  because we didn't store a value for the mandatory attribute contactmaterial
x    Uh, couldn't find any matching component
x  For part yageo-rt0603 :
x    Octopart says it's this: Res Thin Film 0603 22 Ohm 0.5% 1/10W ±25ppm/°C Molded SMD Paper T/R
x    resistor_thick_film_chip_2512 won't fit, because its value 2512 of the mandatory attribute case_package doesn't match the requested value 0603
x    resistor_thick_film_chip_1206 won't fit, because its value 1206 of the mandatory attribute case_package doesn't match the requested value 0603
x    resistor_thick_film_chip_0603 won't fit, because its value ThickFilm of the mandatory attribute composition doesn't match the requested value ThinFilm
x    resistor_thick_film_chip_0402 won't fit, because its value 0402 of the mandatory attribute case_package doesn't match the requested value 0603
x    resistor_thick_film_chip_0201 won't fit, because its value 0201 of the mandatory attribute case_package doesn't match the requested value 0603
x    resistor_metal-strip_chip_1206 won't fit, because its value 1206 of the mandatory attribute case_package doesn't match the requested value 0603
x    Uh, couldn't find any matching component
x  For part kailh-cpg135001d02 :
x    Uh, found no data on Octopart
x  For part stmicroelectronics-usblc62sc6 :
x    Octopart says it's this: USBLC6 Series 2 Line 6 V Uni / Bi-Directional ESD Protection - SOT-23-6
x    tvs_diode_do214aa_tin-plated won't fit, because its value SMB of the mandatory attribute case_package doesn't match the requested value SOT-23
x    tvs_diode_do214ac_tin-plated won't fit, because its value SMA of the mandatory attribute case_package doesn't match the requested value SOT-23
x    tvs_diode_array_sot143_tin-plated_gold-wired won't fit, because its value SOT-143 of the mandatory attribute case_package doesn't match the requested value SOT-23
x    Uh, couldn't find any matching component
x  For part abracon-abm8aig :
x    Octopart says it's this: Crystal 16MHz ±30ppm (Tol) ±100ppm (Stability) 18pF FUND 70Ohm Automotive 4-Pin Ultra Mini-CSMD T/R
x    crystal_ceramic_smd4_3.2x2.5x0.8 matches all mandatory attributes within the category crystals
x    -> But check that value 0.8mm of plausibility attribute height is within the range of the requested value 800µm
x    crystal_ceramic_smd4_3.2x2.5x0.8 even passes all plausibility checks, how pleasant!
x    So we suggest crystal_ceramic_smd4_3.2x2.5x0.8 because it's the first known component which matches

Add and complete the following 1 entries to products.csv:
reform2-trackball,"MISSING_NAME",MISSING_MANUFACTURER,MISSING_DEPENDENCY,"MISSING_DESCRIPTION",reform2-trackball.csv

Add and complete the following 1 entries to manufacturers.csv:
abracon,Abracon,,MISSING_FMD_AVAILABLITY,,,

Add and complete the following 4 entries to component.csv:
x component-suitable-for-part-nexperia-bzt52b5v6j,"BZT52-B Series 5.6 V ±1.96% 590 mW SMT Single Zener Diode - SOD-123",MISSING_WEIGHT,MISSING_UNIT,zener-diodes,lifecyclestatus:Production(LastUpdated:1dayago);manufacturerlifecyclestatus:RELEASEDFORSUPPLY(LastUpdated:1dayago),MISSING_DATE,"Nexperia BZT52-B5V6J: BZT52-B Series 5.6 V ±1.96% 590 mW SMT Single Zener Diode - SOD-123"
x component-suitable-for-part-hirose-fh126,"Connectors for Connecting - 0.5/1-mm Pitch FPC/FFC - FH12 Series (HIROSE ELECTRIC)",MISSING_WEIGHT,MISSING_UNIT,ffc-fpc,actuatormaterial:Polyphenylene;color:Beige/Brown;connectortype:Receptacle;contactlocation:Bottom;contactmaterial:Bronze;contactpitch:500µm;contactplating:Gold;contactresistance:50mΩ;contactvoltagerating_ac_:50V;currentrating:500mA;depth:5.6mm;flammabilityrating:UL94V-0;gender:Female;height:2mm;housingcolor:Beige;housingmaterial:Polyamide;insulationresistance:500MΩ;leadfree:LeadFree;leadpitch:500µm;length:7.1mm;lifecyclestatus:Production(LastUpdated:11monthsago);material:Polyamide;maxcurrentrating:400mA;maxoperatingtemperature:85°C;maxvoltagerating_ac_:50V;minoperatingtemperature:-40°C;mount:SurfaceMount;numberofcontacts:6;numberofpositions:6;numberofrows:1;orientation:RightAngle;packaging:Digi-Reel®;pitch:500µm;radiationhardening:No;reachsvhc:NoSVHC;rohs:Compliant;termination:Solder;voltagerating:50V;voltagerating_ac_:50V;width:7.1mm;wire_cabletype:Straight,MISSING_DATE,"Hirose FH12-6S-0.5SH(55): Connectors for Connecting - 0.5/1-mm Pitch FPC/FFC - FH12 Series (HIROSE ELECTRIC)"
x component-suitable-for-part-yageo-rt0603,"Res Thin Film 0603 22 Ohm 0.5% 1/10W ±25ppm/°C Molded SMD Paper T/R",MISSING_WEIGHT,MISSING_UNIT,chip-smd-resistors,case_package:0603;casecode_metric_:0603;composition:ThinFilm;height:550µm;leadfree:LeadFree;length:1.6002mm;maxoperatingtemperature:125°C;maxpowerdissipation:100mW;minoperatingtemperature:-55°C;numberofterminations:2;packaging:Tape&Reel(TR);powerrating:100mW;resistance:22Ω;rohs:Compliant;scheduleB:8533210045;temperaturecoefficient:25ppm/°C;tolerance:0.1%;voltagerating:75V;voltagerating_dc_:50V;width:787.4µm,MISSING_DATE,"Yageo RT0603DRD0722RL: Res Thin Film 0603 22 Ohm 0.5% 1/10W ±25ppm/°C Molded SMD Paper T/R"
x component-suitable-for-part-stmicroelectronics-usblc62sc6,"USBLC6 Series 2 Line 6 V Uni / Bi-Directional ESD Protection - SOT-23-6",MISSING_WEIGHT,MISSING_UNIT,tvs-diodes,breakdownvoltage:6V;capacitance:2.5pF;case_package:SOT-23;clampingvoltage:17V;contactplating:Tin;datarate:480Mbps;depth:1.75mm;direction:Unidirectional;elementconfiguration:Dual;esdprotection:Yes;height:1.3mm;leadfree:LeadFree;leakagecurrent:150nA;length:3.05mm;lifecyclestatus:Production(LastUpdated:1yearago);maxbreakdownvoltage:6V;maxjunctiontemperature:125°C;maxoperatingtemperature:125°C;maxrepetitivereversevoltage_vrrm_:5.25V;maxreverseleakagecurrent:150nA;minbreakdownvoltage:6V;minoperatingtemperature:-40°C;mount:SurfaceMount;numberofbidirectionalchannels:2;numberofchannels:2;numberofelements:1;numberofpins:6;operatingsupplyvoltage:1.1V;packaging:TapeandReel;peakpulsecurrent:5A;polarity:Bidirectional/Unidirectional;powerlineprotection:Yes;radiationhardening:No;reachsvhc:NoSVHC;reversebreakdownvoltage:6V;reversestandoffvoltage:5.25V;rohs:Compliant;termination:SMD/SMT;testcurrent:1mA;width:1.75mm;workingvoltage:5V,MISSING_DATE,"STMicroelectronics USBLC6-2SC6: USBLC6 Series 2 Line 6 V Uni / Bi-Directional ESD Protection - SOT-23-6"

Add and complete the following 7 entries to parts.csv (including helpful data in the source column to the rightmost):
x nexperia-bzt52b5v6j,"BZT52-B Series 5.6 V ±1.96% 590 mW SMT Single Zener Diode - SOD-123",component-suitable-for-part-nexperia-bzt52b5v6j,nexperia,BZT52-B5V6J,"Part family = BZT52; Category = Zener Diodes; Lifecycle Status = Production (Last Updated: 1 day ago); Manufacturer Lifecycle Status = RELEASED FOR SUPPLY (Last Updated: 1 day ago)"
x bel_fuse-0zcj,"PTC Resettable Fuse 0.75A(hold) 1.5A(trip) 16VDC 100A 0.6W 0.2s 0.09Ohm SMD Solder Pad 1206 T/R Automotive",polymeric_ptc_resettable_fuse_2920,bel_fuse,0ZCJ0075AF2E,"Part family = 0ZCJ; Category = PTC Resettable Fuses; Case/Package = 1206; Case Code (Imperial) = 1206; Case Code (Metric) = 3216; Current Rating = 100 A; Height = 1.25 mm; Hold Current = 750 mA; Lead Free = Lead Free; Lifecycle Status = Production (Last Updated: 11 months ago); Max Current Rating = 100 A; Max Operating Temperature = 85 °C; Maximum Resistance 1 Hour (R1max) = 290 mΩ; Min Operating Temperature = -40 °C; Packaging = Tape & Reel (TR); RoHS = Compliant; Time to Trip = 200 ms; Trip Current = 1.5 A; Voltage Rating = 16 V"
x hirose-fh126,"Connectors for Connecting - 0.5/1-mm Pitch FPC/FFC - FH12 Series (HIROSE ELECTRIC)",component-suitable-for-part-hirose-fh126,hirose,FH12-6S-0.5SH(55),"Part family = FH12; Category = FFC / FPC; Actuator Material = Polyphenylene; Color = Beige, Brown; Connector Type = Receptacle; Contact Location = Bottom; Contact Material = Bronze; Contact Pitch = 500 µm; Contact Plating = Gold; Contact Resistance = 50 mΩ; Contact Voltage Rating (AC) = 50 V; Current Rating = 500 mA; Depth = 5.6 mm; Flammability Rating = UL94 V-0; Gender = Female; Height = 2 mm; Housing Color = Beige; Housing Material = Polyamide; Insulation Resistance = 500 MΩ; Lead Free = Lead Free; Lead Pitch = 500 µm; Length = 7.1 mm; Lifecycle Status = Production (Last Updated: 11 months ago); Material = Polyamide; Max Current Rating = 400 mA; Max Operating Temperature = 85 °C; Max Voltage Rating (AC) = 50 V; Min Operating Temperature = -40 °C; Mount = Surface Mount; Number of Contacts = 6; Number of Positions = 6; Number of Rows = 1; Orientation = Right Angle; Packaging = Digi-Reel®; Pitch = 500 µm; Radiation Hardening = No; REACH SVHC = No SVHC; RoHS = Compliant; Termination = Solder; Voltage Rating = 50 V; Voltage Rating (AC) = 50 V; Width = 7.1 mm; Wire/Cable Type = Straight"
x yageo-rt0603,"Res Thin Film 0603 22 Ohm 0.5% 1/10W ±25ppm/°C Molded SMD Paper T/R",component-suitable-for-part-yageo-rt0603,yageo,RT0603DRD0722RL,"Part family = RT0603; Category = Chip SMD Resistors; Case/Package = 0603; Case Code (Metric) = 0603; Composition = Thin Film; Height = 550 µm; Lead Free = Lead Free; Length = 1.6002 mm; Max Operating Temperature = 125 °C; Max Power Dissipation = 100 mW; Min Operating Temperature = -55 °C; Number of Terminations = 2; Packaging = Tape & Reel (TR); Power Rating = 100 mW; Resistance = 22 Ω; RoHS = Compliant; Schedule B = 8533210045; Temperature Coefficient = 25 ppm/°C; Tolerance = 0.1 %; Voltage Rating = 75 V; Voltage Rating (DC) = 50 V; Width = 787.4 µm"
x kailh-cpg135001d02,"MISSING_NAME",MISSING_COMPONENT,kailh,CPG135001D02,No match in Octopart for this part
x stmicroelectronics-usblc62sc6,"USBLC6 Series 2 Line 6 V Uni / Bi-Directional ESD Protection - SOT-23-6",component-suitable-for-part-stmicroelectronics-usblc62sc6,stmicroelectronics,USBLC6-2SC6,"Part family = USBLC6; Category = TVS Diodes; Breakdown Voltage = 6 V; Capacitance = 2.5 pF; Case/Package = SOT-23; Clamping Voltage = 17 V; Contact Plating = Tin; Data Rate = 480 Mbps; Depth = 1.75 mm; Direction = Unidirectional; Element Configuration = Dual; ESD Protection = Yes; Height = 1.3 mm; Lead Free = Lead Free; Leakage Current = 150 nA; Length = 3.05 mm; Lifecycle Status = Production (Last Updated: 1 year ago); Max Breakdown Voltage = 6 V; Max Junction Temperature (Tj) = 125 °C; Max Operating Temperature = 125 °C; Max Repetitive Reverse Voltage (Vrrm) = 5.25 V; Max Reverse Leakage Current = 150 nA; Min Breakdown Voltage = 6 V; Min Operating Temperature = -40 °C; Mount = Surface Mount; Number of Bidirectional Channels = 2; Number of Channels = 2; Number of Elements = 1; Number of Pins = 6; Operating Supply Voltage = 1.1 V; Packaging = Tape and Reel; Peak Pulse Current = 5 A; Polarity = Bidirectional, Unidirectional; Power Line Protection = Yes; Radiation Hardening = No; REACH SVHC = No SVHC; Reverse Breakdown Voltage = 6 V; Reverse Standoff Voltage = 5.25 V; RoHS = Compliant; Termination = SMD/SMT; Test Current = 1 mA; Width = 1.75 mm; Working Voltage = 5 V"
x abracon-abm8aig,"Crystal 16MHz ±30ppm (Tol) ±100ppm (Stability) 18pF FUND 70Ohm Automotive 4-Pin Ultra Mini-CSMD T/R",crystal_ceramic_smd4_3.2x2.5x0.8,abracon,ABM8AIG-16.000MHz-4-T,"Part family = ABM8; Category = Crystals; ESR (Equivalent Series Resistance) = 70 Ω; Frequency = 16 MHz; Frequency Stability = 100 ppm; Frequency Tolerance = 0.003 %; Height = 800 µm; Length = 3.2 mm; Lifecycle Status = Production (Last Updated: 1 year ago); Load Capacitance = 18 pF; Max Operating Temperature = 125 °C; Min Operating Temperature = -40 °C; Mount = Surface Mount; RoHS = Compliant; Schedule B = 8541600080; Width = 2.5 mm"

There may be entries converted for mounting holes, labels, or logos, which should just be deleted.
But don't forget to add pcb, solder, cables, and assembly parts as well which are not part of a BOM.

