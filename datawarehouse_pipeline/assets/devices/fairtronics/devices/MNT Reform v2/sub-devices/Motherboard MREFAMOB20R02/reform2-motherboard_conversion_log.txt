Converting reform2-motherboard.csv:
x  Couldn't find a match for part 3000 of manufacturer Keystone. Suggesting keystone-3000
x  Couldn't find a match for part EEE-FTH101XAP of manufacturer Panasonic. Suggesting panasonic-eeefth101xap
x  Couldn't find a match for part UMK107BJ105KA-T of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-umk107bj105kat
x  Found yageo-cc0603x7r for part CC0603JPX7R9BB104 from Yageo
x  Found murata-grm188r60J476ME15D for part GRM188R60J476ME15D from Murata
x  Found murata-grm188r6YA106MA73D for part GRM188R6YA106MA73D from Murata
x  Couldn't find a match for part C1608X5R1V475K080AC of manufacturer TDK. Suggesting tdk-c1608x5r1v475k080ac
x  Couldn't find a match for part TMK107B7474KA-TR of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-tmk107b7474katr
x  Found ucc-EMZR500ARA221MHA0G for part EMZR500ARA221MHA0G from UCC
x  Found ucc-EMZR500ARA221MHA0G for part EMZR500ARA221MHA0G from UCC
x  Couldn't find a match for part C1608X7R1H334K080AC of manufacturer TDK. Suggesting tdk-c1608x7r1h334k080ac
x  Couldn't find a match for part VJ0603A6R8DXQCW1BC of manufacturer Vishay. Suggesting vishay-vj0603a6r8dxqcw1bc
x  Found taiyo_yuden-mk107 for part UMK107BBJ225KA-T from Taiyo Yuden
x  Couldn't find a match for part C0603C333J4REC7411 of manufacturer KEMET. Suggesting kemet-c0603c333j4rec7411
x  Found kemet-C0603C103K5RAC3190 for part C0603C103K5RAC3190 from KEMET
x  Couldn't find a match for part C0603C681J5GACTU of manufacturer KEMET. Suggesting kemet-c0603c681j5gactu
x  Couldn't find a match for part GRM1885C1H202JA01D of manufacturer Murata. Suggesting murata-grm1885c1h202ja01d
x  Found yageo-cc0603npo for part CC0603JRNPO9BN180 from Yageo
x  Couldn't find a match for part BZT52C10-7-F of manufacturer Diodes, Inc.. Suggesting diodes-bzt52c107f
x  Couldn't find a match for part SP0503BAHT of manufacturer Littelfuse. Suggesting littelfuse-sp0503baht
x  Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
x  Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
x  Couldn't find a match for part SMAJ36CA-E3/5A of manufacturer Vishay. Suggesting vishay-smaj36cae35a
x  Couldn't find a match for part SD0603S040S0R2 of manufacturer AVX. Suggesting avx-sd0603s040s0r2
x  Couldn't find a match for part BAT46WJ,115 of manufacturer Nexperia. Suggesting nexperia-bat46wj115
x  Found mcc-sk5 for part SK54A-LTP from MCC
x  Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
x  Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
x  Couldn't find a match for part SBR0560S1-7 of manufacturer Diodes, Inc.. Suggesting diodes-b05
x  Couldn't find a match for part RBR3MM60ATFTR of manufacturer ROHM. Suggesting rohm-rbr3mm60atftr
x  Couldn't find a match for part BZT52C6V2-7-F of manufacturer Diodes, Inc.. Suggesting diodes-bzt52c6v27f
x  Couldn't find a match for part SML-D12Y1WT86 of manufacturer ROHM. Suggesting rohm-smld12y1wt86
x  Couldn't find a match for part 0157004.DR of manufacturer Littelfuse. Suggesting littelfuse-0157004dr
x  Couldn't find a match for part BLM18KG101TN1D of manufacturer Murata. Suggesting murata-blm18kg101tn1d
x  Found murata-BLM18PG221SH1D for part BLM18PG221SH1D from Murata
x  Missing manufacturer or part number for entry with value NGFF-Mount1. Setting to unknown
x  Missing manufacturer or part number for entry with value LOGO_REFORM. Setting to unknown
x  Missing manufacturer or part number for entry with value LABEL_MPCIE1. Setting to unknown
x  Missing manufacturer or part number for entry with value LABEL_MPCIE2. Setting to unknown
x  Missing manufacturer or part number for entry with value LABEL_CPU. Setting to unknown
x  Missing manufacturer or part number for entry with value BADGE. Setting to unknown
x  Missing manufacturer or part number for entry with value NOTOUCH. Setting to unknown
x  Missing manufacturer or part number for entry with value NGFF-Mount2. Setting to unknown
x  Couldn't find a match for part 9774010243 of manufacturer Wurth. Suggesting wurth-9774010243
x  - Spacer aus steel, tin-plated, siehe https://www.we-online.de/katalog/datasheet/9774010243.pdf, Familie https://www.we-online.de/katalog/de/SMSI_SMT_STEEL_SPACER_M2_THREAD_INTERNAL
x  - Ähnliches Teil mit FMD nicht gefunden, höchstens https://www.harwin.com/products/R30-6010202/ mit parametrisiertem FMD https://cdn.harwin.com/pdfs/Material_composition_for_R30-601.pdf, besteht aber aus Messing, Nickel-plated und andere Form (unthreaded, nur ein Durchmesser)
x  - STEP file -> STL file -> conversion
x  Missing manufacturer or part number for entry with value NGFF-Mount3. Setting to unknown
x  Missing manufacturer or part number for entry with value MountingHole_Pad. Setting to unknown
x  Missing manufacturer or part number for entry with value OSHWA_DE17. Setting to unknown
x  Found switchcraft-RAPC712X for part RAPC712X from Switchcraft
x  Found te-1-2199230-6 for part 1-2199230-6 from TE
x  Found molex-48099-5701 for part 48099-5701 from Molex
x  Couldn't find a match for part 504050-0591 of manufacturer Molex. Suggesting molex-5040500591
x  Couldn't find a match for manufacturer 'JST'. Suggesting jst
x  Couldn't find a match for part B4B-PH-K-S(LF)(SN) of manufacturer JST. Suggesting jst-b4bphkslfsn
x  Couldn't find a match for part FH12-33S-0.5SH(55) of manufacturer Hirose. Suggesting hirose-fh1233s05sh55
x  Missing manufacturer or part number for entry with value LINE_IN. Setting to unknown
x  Found wurth-61300311121 for part 61300311121 from Wurth
x  Couldn't find a match for part 20021111-00010T4LF of manufacturer Amphenol FCI. Suggesting amphenol-2002111100010t4lf
x  Found wurth-61300311121 for part 61300311121 from Wurth
x  Found molex-87914-1616 for part 87914-1616 from Molex
x  Found molex-87758-3016 for part 87758-3016 from Molex
x  Found te-1775059-1 for part 1775059-1 from TE
x  Found wurth-685119134923 for part 685119134923 from Wurth
x  Couldn't find a match for part 692121030100 of manufacturer Wurth. Suggesting wurth-692121030100
x  Found cui-SJ-43516-SMT-TR for part SJ-43516-SMT-TR from CUI
x  Couldn't find a match for part 629105136821 of manufacturer Wurth. Suggesting wurth-629105136821
x  Couldn't find a match for part 7447709220 of manufacturer Wurth. Suggesting wurth-7447709220
x  Couldn't find a match for part LQH32PB150MN0L of manufacturer Murata. Suggesting murata-lqh32
x  Found vishay-IMC1210 for part IMC1210ER100K from Vishay Dale
x  Found bourns-SRR1210 for part SRR1210-680M from Bourns
x  Couldn't find a match for part CBC3225T100MRV of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-cbc3225t
x  Couldn't find a match for part BRL3225T2R2M of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-brl3225t
x  Found pulse-J0G-0003NL for part J0G-0003NL from Pulse
x  Found nexperia-pmv for part PMV50EPEAR from Nexperia
x  Couldn't find a match for part PMV50ENEAR of manufacturer Nexperia. Suggesting nexperia-pmv50enear
x  Couldn't find a match for part SI7461DP-T1-E3 of manufacturer Vishay Siliconix. Suggesting vishay-si7461dpt1e3
x  - Ein MOSFET
x  - https://www.vishay.com/search?searchChoice=part&query=SI7461DP-T1-E3 und https://www.vishay.com/docs/72567/si7461dp.pdf
x  - PowerPAK SO-8 scheint mit SO-8 in der Größe nicht viel zu tun zu haben, d.h. mosfet_so-8_4.9x3.9x1.75 passt nicht
x  - Aber: " The pin arrangement (drain, source, gate pins) and the pin dimensions are the same as standard SO-8 devices (see figure 2).
x    Therefore, the PowerPAK connection pads matc h directly to those of the SO-8. The only difference is the extended drain connection area."
x    Es fehlt also diese Metallplatte darunter. Hat sonst nirgendjemand
x  -> Nachgefragt am 24.1., keine Antwort
x  - Zur Not halt SO-8 ohne diese Platte, da gibt's was von anderen Herstellern
x  - PowerPAK SO-8 kann an Stelle SO-8 eingesetzt werden, hat aber keine Beinchen, ist flacher und füllt statt dessen die ganze Fläche aus und hat eine Bodenplatte für den Ground und Kühlung
x    Weil "flacher" gleicht "größer" aus und "Bodenplatte" gleicht "Beinchen" aus habe ich einfach einen SO-8 genommen
x  Couldn't find a match for part SI7850DP-T1-E3 of manufacturer Vishay Siliconix. Suggesting vishay-si7850dpt1e3
x  - Ebenso, siehe https://www.vishay.com/docs/71625/si7850dp.pdf
x  - Siehe Vorgänger
x  Couldn't find a match for part RC0603FR-0710KL of manufacturer Yageo. Suggesting yageo-rc0603fr0710kl
x  Found vishay-crcw0603100KJNEAC for part CRCW0603100KJNEAC from Vishay Dale
x  Couldn't find a match for part RC0603FR-1049R9L of manufacturer Yageo. Suggesting yageo-rc0603fr1049r9l
x  Found yageo-rc0603FR-074K7L for part RC0603FR-074K7L from Yageo
x  Found vishay-crcw060333R0FKEAC for part CRCW060333R0FKEAC from Vishay Dale
x  Found yageo-rc0603FR-07680RL for part RC0603FR-07680RL from Yageo
x  Found vishay-crcw060347K0FKEAC for part CRCW060347K0FKEAC from Vishay Dale
x  Found vishay-crcw0603100RFKEAC for part CRCW0603100RFKEAC from Vishay Dale
x  Found vishay-crcw06030000Z0EAC for part CRCW06030000Z0EAC from Vishay Dale
x  Found yageo-rc0603FR-071ML for part RC0603FR-071ML from Yageo
x  Couldn't find a match for part RC0603FR-0762RL of manufacturer Yageo. Suggesting yageo-rc0603fr0762rl
x  Found yageo-rc0603FR-071K5L for part RC0603FR-071K5L from Yageo
x  Couldn't find a match for part CRCW060349K9FKEAC of manufacturer Vishay Dale. Suggesting vishay-crcw060349k9fkeac
x  Couldn't find a match for part LTR18EZPFSR020 of manufacturer ROHM. Suggesting rohm-ltr18
x  Couldn't find a match for part CRCW0603150KFKEAC of manufacturer Vishay Dale. Suggesting vishay-crcw0603150kfkeac
x  Couldn't find a match for part CR0603-FX-2702ELF of manufacturer Bourns. Suggesting bourns-cr0603fx2702elf
x  Found vishay-crcw06033K30FKEAC for part CRCW06033K30FKEAC from Vishay Dale
x  Couldn't find a match for part 35224R7JT of manufacturer TE Connectivity. Suggesting te-3522
x  Found yageo-rc0603FR-07475RL for part RC0603FR-07475RL from Yageo
x  Couldn't find a match for part LTR18EZPFU10L0 of manufacturer ROHM. Suggesting rohm-ltr18fu10l0
x  Couldn't find a match for part RC0603FR-07330RL of manufacturer Yageo. Suggesting yageo-rc0603fr07330rl
x  Found yageo-rc0603FR-079K53L for part RC0603FR-079K53L from Yageo
x  Found vishay-crcw060390K9FKEAC for part CRCW060390K9FKEAC from Vishay Dale
x  Found yageo-rc0603FR-0751KL for part RC0603FR-0751KL from Yageo
x  Found yageo-rc0603FR-07220RL for part RC0603FR-07220RL from Yageo
x  Couldn't find a match for part RC0603FR-071KL of manufacturer Yageo. Suggesting yageo-rc0603fr071kl
x  Found yageo-rc0603FR-0727RL for part RC0603FR-0727RL from Yageo
x  Found yageo-rc0603FR-0724K9L for part RC0603FR-0724K9L from Yageo
x  Couldn't find a match for part DM01 of manufacturer Apem. Suggesting apem-dm01
x  Couldn't find a match for part DHA-04TQ of manufacturer Diptronics. Suggesting diptronics-dha04
x  Couldn't find a match for part UK-B0206-G3.8-250-JZ of manufacturer USAKRO. Suggesting usakro-ukb0206g38250jz
x  Missing manufacturer or part number for entry with value IMX_PWM4. Setting to unknown
x  Missing manufacturer or part number for entry with value T_M4_NMI. Setting to unknown
x  Missing manufacturer or part number for entry with value SAI2_RXC. Setting to unknown
x  Missing manufacturer or part number for entry with value T_OTG. Setting to unknown
x  Missing manufacturer or part number for entry with value T_PMIC_ON_REQ. Setting to unknown
x  Found te-1717254-1 for part 1717254-1 from TE
x  Found texas_instruments-SN65DSI86IPAPQ1 for part SN65DSI86IPAPQ1 from Texas Instruments
x  Found molex-67910-5700 for part 67910-5700 from Molex
x  Couldn't find a match for part LM2677S-3.3/NOPB of manufacturer Texas Instruments. Suggesting texas_instruments-lm2677s
x  Couldn't find a match for part TLV62568DBVR of manufacturer Texas Instruments. Suggesting texas_instruments-tlv62568dbvr
x  - XML siehe Desktop
x  - Von Spez wäre ic_power_management_sot-23_5-pin_nipdau-plated_2.9x1.6x1.45 passend, aber dieser ist um einges schwerer
x  - Woran könnte man erkennen, dass zwei verschiedene components angesagt sind?
x  - Oder genügt ob der letztlichen geringen Unterscheide doch ein Durchschnitts-component?
x  - https://www.ti.com/store/ti/en/p/product/?p=TLV62568DBVR
x  - Familie: https://www.ti.com/product/TLV62568#product-details##params
x  -> Remove # in front of this entry within *.parts.csv
x  Couldn't find a match for part LMR16006YQ3 of manufacturer Texas Instruments. Suggesting texas_instruments-lmr16006yq3
x  - XML siehe Desktop
x  - Q3 gibt es nicht, aber https://www.ti.com/product/LMR16006Y-Q1
x  -> Remove # in front of this entry within *.parts.csv
x  Found texas_instruments-tps2561drc for part TPS2561DRCR from Texas Instruments
x  Found texas_instruments-tlv1117dcy for part TLV1117-18CDCYR from Texas Instruments
x  Found nxp-lpc11u00d48 for part LPC11U24FBD48-301 from NXP
x  Couldn't find a match for part LTC4020EUHF#PBF of manufacturer Analog Devices. Suggesting analog_devices-ltc4020euhfpbf
x  Couldn't find a match for part AP22815AWT-7 of manufacturer Diodes, Inc.. Suggesting diodes-ap22815awt7
x  - https://www.diodes.com/part/view/AP22815, ohne MDS
x  - Die Bauform TSOT25 ist um Toleranzmilimeter ähnlich dem SOT25
x  - Die Familie: https://www.diodes.com/products/power-management/power-switches/#collection-9676=~(Packages~(~'SOT25~'TSOT25))
x  - Für SOT25 hat Octopart leider nur Diodes: https://octopart.com/search?category_id=5467&case_package=SOT-25
x  -> Prüfen ob nicht vielleicht der 5-pin SOT-23-5 so ähnlich ist! Ok, TSOT ist etwas dünner, aber SOT-5 = SOT-23-5 (https://en.wikipedia.org/wiki/Small-outline_transistor#SOT23-5,_SOT353,_SOT553)
x  - SOT-23-5 Ersatz mit FMD gefunden: https://octopart.com/max4915beuk%2Bt-maxim+integrated-44348904?r=sp = https://www.maximintegrated.com/en/products/analog/analog-switches-multiplexers/MAX4915B.html
x  Couldn't find a match for part TPD12S521 of manufacturer Texas Instruments. Suggesting texas_instruments-tpd12s521
x  Found idt-5V41066PGGI8 for part 5V41066PGGI8 from IDT
x  Couldn't find a match for part SP3012-06UTG of manufacturer Littelfuse. Suggesting littelfuse-sp301206utg
x    - Requested on 03-08-2021, keine Antwort
x    - Substitut laut Octopart: https://www.ti.com/product/TPD6E05U06#product-details##pps, genommen
x  -> Remove # in front of this entry within *.parts.csv
x  Couldn't find a match for part SN74AVC1T45 of manufacturer Texas Instruments. Suggesting texas_instruments-sn74avc1t45
x  Couldn't find a match for part WM8960CGEFL/V of manufacturer Cirrus Logic. Suggesting cirrus_logic-wm8960
x  Found analog_devices-LTC6803IG-4PBF for part LTC6803IG-4#PBF from Analog Devices
x  Couldn't find a match for part PCF8523T/1,118 of manufacturer NXP. Suggesting nxp-pcf8523t
x  Found texas_instruments-INA260AIPWR for part INA260AIPWR from Texas Instruments
x  Found texas_instruments-lm2677s for part LM2677SX-5 from Texas Instruments
x  Found texas_instruments-TXS0108EPW for part TXS0108EPW from Texas Instruments
x  Found texas_instruments-TUSB8041IPAPRQ1 for part TUSB8041IPAPRQ1 from Texas Instruments
x  Found abracon-abm8aig-24000MHz-R40-4-T for part ABM8AIG-24.000MHz-R40-4-T from Abracon
x  Found abracon-abm8aig-12000MHz-2-T for part ABM8AIG-12.000MHz-2-T from Abracon
x  Found abracon-abm8aig-25000MHz-R40-4-T for part ABM8AIG-25.000MHz-R40-4-T from Abracon
x  Couldn't find a match for part ECS-.327-7-34B-TR of manufacturer ECS. Suggesting ecs-ecx31b
Wrote reform2-motherboard.parts.csv with 60 identified parts out of 99 with data.
17 entries had missing data, please review them. They may be deleted or remain unknown.

Additionally:
x   PCB:reform2-motherboard-gerbers-r2c-x2\reform2-motherboard-job.gbrjob:
x      Name = reform2-motherboard
x      Size = 276mm x 94mm
x      Layer count = 6
x      Board thickness = 1.60mm
x      Welches Finish?
x   Any cables not listed otherwhere?
x      USB/SYSCTL Cable - MREFCBLU20R01, JST-PH 4P cable, 24AWG, 15cm length, siehe PNG anbei (laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
x      Laut https://mntre.com/media/reform_md/mnt-reform2-diy-manual-r1.pdf gibt es "3 black internal 4-pin cables (for USB and SYSCTL UART)"
x      2x internal USB cable
x       "Connect MIPI-DSI port of CPU Module to MIPI-DSI socket on Motherboard PCBA using MIPI-DSI Cable." (https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
x          -> Ersatz durch Molex 152661032 oder 982661032, FMD angefragt
   Da muss noch eine Knopfzelle (12 mm Coin Cell CR 1216 oder CR 1220) auf dem Mainboard sein, zumindest gibt es ja das keystone-3000
       CR = Lithium-Rundzelle 3V, Durchmesser 12mm, Höhe 1,6/2,0mm. 0,7g/0,9g. Das müsste Lithium-Mangan sein. Anbieter:
       - renata https://www.renata.com/en/products/lithium-batteries/cr1216/, https://www.renata.com/en/products/lithium-batteries/cr1220/. Am 7.2. angefragt
       - Varta kannte vergessen. sonst noch? Panasonic hat keine FMD, könnte man aber anfragen. Duracell hat sowas nicht.
x   Spacer laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md
x      Spacer A - Würth Elektronik 960030021, 2x 3mm
x      Spacer B - Würth Elektronik 960040021, 2x 4mm
x     Statt dessen war aber der Würth 960030021 vier mal in der BOM... komisch. Das ist mit 2.4mm auch ein sehr schmaler Spacer. Trotzdem wohl drin
x   Lot

xAdd and complete the following 1 entries to products.csv:
xreform2-motherboard,MISSING_NAME,MISSING_MANUFACTURER,UNKNOWN_DEPENDENCY,MISSING_DESCRIPTION,reform2-motherboard.csv

xAdd and complete the following 1 entries to manufacturers.csv:
x jst,JST,,MISSING_URL

Add and complete the following 60 entries to parts.csv:
x keystone-3000,MISSING_NAME,MISSING_COMPONENT,keystone,3000
x panasonic-eeefth101xap,MISSING_NAME,MISSING_COMPONENT,panasonic,EEE-FTH101XAP
x taiyo_yuden-umk107bj105kat,MISSING_NAME,MISSING_COMPONENT,taiyo_yuden,UMK107BJ105KA-T
x tdk-c1608x5r1v475k080ac,MISSING_NAME,MISSING_COMPONENT,tdk,C1608X5R1V475K080AC
x taiyo_yuden-tmk107b7474katr,MISSING_NAME,MISSING_COMPONENT,taiyo_yuden,TMK107B7474KA-TR
x tdk-c1608x7r1h334k080ac,MISSING_NAME,MISSING_COMPONENT,tdk,C1608X7R1H334K080AC
x vishay-vj0603a6r8dxqcw1bc,MISSING_NAME,MISSING_COMPONENT,vishay,VJ0603A6R8DXQCW1BC
x kemet-c0603c333j4rec7411,MISSING_NAME,MISSING_COMPONENT,kemet,C0603C333J4REC7411
x kemet-c0603c681j5gactu,MISSING_NAME,MISSING_COMPONENT,kemet,C0603C681J5GACTU
x murata-grm1885c1h202ja01d,MISSING_NAME,MISSING_COMPONENT,murata,GRM1885C1H202JA01D
x diodes-bzt52c107f,MISSING_NAME,MISSING_COMPONENT,diodes,BZT52C10-7-F
x littelfuse-sp0503baht,MISSING_NAME,MISSING_COMPONENT,littelfuse,SP0503BAHT
x vishay-smaj36cae35a,MISSING_NAME,MISSING_COMPONENT,vishay,SMAJ36CA-E3/5A
x avx-sd0603s040s0r2,MISSING_NAME,MISSING_COMPONENT,avx,SD0603S040S0R2
x nexperia-bat46wj115,MISSING_NAME,MISSING_COMPONENT,nexperia,BAT46WJ,115
x diodes-b05,MISSING_NAME,MISSING_COMPONENT,diodes,SBR0560S1-7
x rohm-rbr3mm60atftr,MISSING_NAME,MISSING_COMPONENT,rohm,RBR3MM60ATFTR
x diodes-bzt52c6v27f,MISSING_NAME,MISSING_COMPONENT,diodes,BZT52C6V2-7-F
x rohm-smld12y1wt86,MISSING_NAME,MISSING_COMPONENT,rohm,SML-D12Y1WT86
x littelfuse-0157004dr,MISSING_NAME,MISSING_COMPONENT,littelfuse,0157004.DR
x murata-blm18kg101tn1d,MISSING_NAME,MISSING_COMPONENT,murata,BLM18KG101TN1D
x wurth-9774010243,MISSING_NAME,MISSING_COMPONENT,wurth,9774010243
x molex-5040500591,MISSING_NAME,MISSING_COMPONENT,molex,504050-0591
x jst-b4bphkslfsn,MISSING_NAME,MISSING_COMPONENT,jst,B4B-PH-K-S(LF)(SN)
x hirose-fh1233s05sh55,MISSING_NAME,MISSING_COMPONENT,hirose,FH12-33S-0.5SH(55)
x amphenol-2002111100010t4lf,MISSING_NAME,MISSING_COMPONENT,amphenol,20021111-00010T4LF
x wurth-692121030100,MISSING_NAME,MISSING_COMPONENT,wurth,692121030100
x wurth-629105136821,MISSING_NAME,MISSING_COMPONENT,wurth,629105136821
x wurth-7447709220,MISSING_NAME,MISSING_COMPONENT,wurth,7447709220
x murata-lqh32,MISSING_NAME,MISSING_COMPONENT,murata,LQH32PB150MN0L
x taiyo_yuden-cbc3225t,MISSING_NAME,MISSING_COMPONENT,taiyo_yuden,CBC3225T100MRV
x taiyo_yuden-brl3225t,MISSING_NAME,MISSING_COMPONENT,taiyo_yuden,BRL3225T2R2M
x nexperia-pmv50enear,MISSING_NAME,MISSING_COMPONENT,nexperia,PMV50ENEAR
x vishay-si7461dpt1e3,MISSING_NAME,MISSING_COMPONENT,vishay,SI7461DP-T1-E3
x vishay-si7850dpt1e3,MISSING_NAME,MISSING_COMPONENT,vishay,SI7850DP-T1-E3
x yageo-rc0603fr0710kl,MISSING_NAME,MISSING_COMPONENT,yageo,RC0603FR-0710KL
x yageo-rc0603fr1049r9l,MISSING_NAME,MISSING_COMPONENT,yageo,RC0603FR-1049R9L
x yageo-rc0603fr0762rl,MISSING_NAME,MISSING_COMPONENT,yageo,RC0603FR-0762RL
x vishay-crcw060349k9fkeac,MISSING_NAME,MISSING_COMPONENT,vishay,CRCW060349K9FKEAC
x rohm-ltr18,MISSING_NAME,MISSING_COMPONENT,rohm,LTR18EZPFSR020
x vishay-crcw0603150kfkeac,MISSING_NAME,MISSING_COMPONENT,vishay,CRCW0603150KFKEAC
x bourns-cr0603fx2702elf,MISSING_NAME,MISSING_COMPONENT,bourns,CR0603-FX-2702ELF
x te-3522,MISSING_NAME,MISSING_COMPONENT,te,35224R7JT
x rohm-ltr18fu10l0,MISSING_NAME,MISSING_COMPONENT,rohm,LTR18EZPFU10L0
x yageo-rc0603fr07330rl,MISSING_NAME,MISSING_COMPONENT,yageo,RC0603FR-07330RL
x yageo-rc0603fr071kl,MISSING_NAME,MISSING_COMPONENT,yageo,RC0603FR-071KL
x apem-dm01,MISSING_NAME,MISSING_COMPONENT,apem,DM01
x diptronics-dha04,MISSING_NAME,MISSING_COMPONENT,diptronics,DHA-04TQ
x usakro-ukb0206g38250jz,MISSING_NAME,MISSING_COMPONENT,usakro,UK-B0206-G3.8-250-JZ
x texas_instruments-lm2677s,MISSING_NAME,MISSING_COMPONENT,texas_instruments,LM2677S-3.3/NOPB
x texas_instruments-tlv62568dbvr,MISSING_NAME,MISSING_COMPONENT,texas_instruments,TLV62568DBVR
x texas_instruments-lmr16006yq3,MISSING_NAME,MISSING_COMPONENT,texas_instruments,LMR16006YQ3
x analog_devices-ltc4020euhfpbf,MISSING_NAME,MISSING_COMPONENT,analog_devices,LTC4020EUHF#PBF
x diodes-ap22815awt7,MISSING_NAME,MISSING_COMPONENT,diodes,AP22815AWT-7
x texas_instruments-tpd12s521,MISSING_NAME,MISSING_COMPONENT,texas_instruments,TPD12S521
x littelfuse-sp301206utg,MISSING_NAME,MISSING_COMPONENT,littelfuse,SP3012-06UTG
x texas_instruments-sn74avc1t45,MISSING_NAME,MISSING_COMPONENT,texas_instruments,SN74AVC1T45
x cirrus_logic-wm8960,MISSING_NAME,MISSING_COMPONENT,cirrus_logic,WM8960CGEFL/V
x nxp-pcf8523t,MISSING_NAME,MISSING_COMPONENT,nxp,PCF8523T/1,118
x ecs-ecx31b,MISSING_NAME,MISSING_COMPONENT,ecs,ECS-.327-7-34B-TR

There may be entries converted for mounting holes, labels, or logos, which should just be deleted.
But don't forget to add pcb, solder, cables, and assembly parts as well which are not part of a BOM.

