C:\Users\Seba\AppData\Local\Programs\Python\Python38\python.exe "C:\Program Files\JetBrains\PyCharm Community Edition 2020.2\plugins\python-ce\helpers\pydev\pydevconsole.py" --mode=client --port=59288
import sys; print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['C:\\Users\\Seba\\PycharmProjects\\orga', 'C:/Users/Seba/PycharmProjects/orga'])
PyDev console: starting.
Python 3.8.0 (tags/v3.8.0:fa919fd, Oct 14 2019, 19:37:50) [MSC v.1916 64 bit (AMD64)] on win32
runfile('C:/Users/Seba/PycharmProjects/orga/pocs/bom/convert_bom.py', args=['C:\\Users\\Seba\\PycharmProjects\\orga/data/manufacturers/manufacturers.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/parts/parts.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/products/products.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/components/components.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/components/component_categories.csv', 'C:\\Users\\Seba\\PycharmProjects\\orga/data/components/component_attributes.csv'], wdir='C:/Users/Seba/PycharmProjects/orga/pocs/bom')

Converting reform2-oled.csv:
x  Couldn't find a match for part number LMK107BBJ106KALT of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-mk107
x  Found taiyo_yuden-mk107 for part UMK107BJ105KA-T from Taiyo Yuden
x  Missing manufacturer or part number for entry with value MNT. Setting to unknown
x  Missing manufacturer or part number for entry with value MH1. Setting to unknown
x  Missing manufacturer or part number for entry with value MH2. Setting to unknown
x  Found molex-2005280040 for part 200528-0040 from Molex
x  Couldn't find a match for part number AF0603FR-07330KL of manufacturer Yageo. Suggesting yageo-af0603
x  Found yageo-rc0603 for part RC0603FR-074K7L from Yageo
x  Couldn't find a match for part number ENH-OB00910003 of manufacturer Enrich Electronics. Suggesting enrich-enhob00910003
x  - Tja, das ist das OLED Display
x  - White
x  - Hersteller: http://www.rxxdisplay.com, auf Alibaba: https://enrich1.en.alibaba.com/
x  - Vermutlich http://www.rxxdisplay.com/chanpinzhanshi/120.html = https://german.alibaba.com/product-detail/0-91-inch-oled-display-128x32-oled-i2c-interface-with-pcb-4pin-1600132441842.html = http://www.oledlcddisplay.com/e_productshow/?75-091-inch-micro-OLED-display-128x32-dots-SPI-interface-display-75.html
x  - https://german.alibaba.com/product-detail/0-91-inch-oled-display-128x32-oled-i2c-interface-with-pcb-4pin-1600132441842.html
x  - 30x11.5mm x 1.227mm, Einzelbruttogewicht 0.020kg (also mit Einzelverpackung, oder?)
x  - http://www.oledlcddisplay.com/e_productshow/?75-091-inch-micro-OLED-display-128x32-dots-SPI-interface-display-75.html
x  - Produktnummer "ENH0.91 Inch OLED" oder ENH-OB00910003, 0.91 inch, 128x32 dots von Enrich Electronics
x  - Anfrage gestellt, nicht geantwortet
x  - Alternative MCOT128032AY-WS von Midas (https://www.midasdisplays.com/product-explorer/oleds/cog-graphic-oleds/mcot128032a/mcot128032ay-ws), 1.05g !, angefragt, nicht geantwortet
x  - Alternative https://www.newhavendisplay.com/nhd22312832ucw3-p-9582.html (deutlich größer! Glas?) ebenfalls angefragt (Kontaktformular), nicht geantwortet
x  - ALternative https://www.densitron.com/products/product/dd-2832we-4ak (etwas größer) ebenfallsd angefragt (Kontaktformular), nicht geantwortet
x  - Muss ich die Hersteller durchgehen: https://octopart.com/search?q=OLED&currency=USD&specs=0&resolution_width_=128&resolution_height_=32
x    - Adafruit hat sowas (https://www.adafruit.com/product/931, https://www.adafruit.com/product/2675), aber sie kaufen auch nur ein: https://forums.adafruit.com/viewtopic.php?f=8&p=878625
x    - AVX ist eine Sackgasse. Gab's wohl mal als LCD bei Northstar?
x    - Midas: https://www.midasdisplays.com/product-explorer/oleds/cog-graphic-oleds/mcot128032a/mcot128032ay-ws
x      Dieser MCOT128032AY-WS passt genau! Wird mit 1.05g angegeben
x    - Newhaven: https://www.newhavendisplay.com/nhd22312832ucw3-p-9582.html, aber deutlich größer
x    - Jetzt reicht's
x  - LCA zu OLED, siehe https://www.sciencedirect.com/science/article/abs/pii/S0959652616309131, hier vorversion: https://coek.info/pdf-life-cycle-assessment-of-organic-light-emitting-diode-display-as-emerging-materi.html
x    Leider ohne Gewichtsangaben. Es wird thematisiert, dass man die Herstellung der Chemikalien kaum herausbekommt, aber es werden Substitute gefunden auf strange Weise
x    Die FU ist farbiges 5-inch (136.6 mm x 69.8 mm) AMOLED. Es wird die Aufbaustruktur erklärt, die Schichtendicken, die Produktion der Chemikalien,
x    der Produktionsprozess mit den eingehenden Stoffen... aber nirgends steht irgendeine Dichte der Stoffe, Gewichtsverhältnisse
x    oder auch nur ein konkretes Gewicht der FU. Mir bleibt nichts anderes übrig, als die Dichten der Substitute zu ermitteln und mit den ebenfalls
x    ermittelten Volumina der Schichten zu multiplizieren. Sehr mühsam...
x  - Sonst habe ich andere Literatur gefunden, aber keine die Inventotydaten hat.
x  - Alternative: Das LCD entsprechend klein als Substitut. Das innolux-N125HCE-GN1-mobile hat 162g, hier sind es 10g. Außerdem lasse ich den Stahlrahmen weg.
x  -> Als Device modelliert und als solches im MNT laptop integriert, nicht hier als Part
Wrote reform2-oled.parts.csv with 3 identified parts out of 6 with data.
3 entries had missing data, please review them. They may be deleted or remain unknown.

Additionally:
x   PCB: reform2-d4-oled.gerbers\reform2-oled-job.gbrjob:
x      Name = reform2-oled
x      Size = 80mm x 12mm
x      Layer count = 2
x      Board thickness = 1.60mm
x   Cable
x      wuerth-686704050001 - 4P (1mm pitch) FPC, Würth Elektronik 686704050001 in https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md
x      https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md: "Connect OLED PCBA to the Keyboard (MREFAKB) with OLED Cable (blue side up)."
x      Substitut Molex 15267-0702 geholt
x   Lot

Seeking for data on the new parts, especially which component it is based on:
x  For part taiyo_yuden-mk107 :
x    Octopart says it's this: Multilayer Ceramic Capacitor 10uF 10V 10% 0603 Paper T/R
x    The data of part taiyo_yuden-mk107 doesn't know at least the mandatory attribute case_package or any of its alternatives (casecode_imperial_)
x    Uh, couldn't find any matching component
x  For part yageo-af0603 :
x    Octopart says it's this: Res Thick Film 0603 330K Ohm 1% 1/10W ±100ppm/°C Molded SMD Paper T/R
x    resistor_thick_film_chip_2512 won't fit, because its value 2512 of the mandatory attribute case_package doesn't match the requested value 0603
x    resistor_thick_film_chip_1206 won't fit, because its value 1206 of the mandatory attribute case_package doesn't match the requested value 0603
x    resistor_thick_film_chip_0603 matches all mandatory attributes within the category chip-smd-resistors
x    -> But be sure that the value Tin of optinal attribute contactplating fits, because we don't have a requested value for it
x    resistor_thick_film_chip_0603 matches even all optional attributes as far as they are known
x    -> But be sure that the actual value of attribute casecode_metric_ for it is plausible, because we don't know its value
x    -> But be sure that the actual value of attribute numberofpins for it is plausible, because we don't have a requested value
x    -> But be sure that the actual value of attribute mount for it is plausible, because we don't have a requested value
x    resistor_thick_film_chip_0603 even passes all plausibility checks, how pleasant!
x    So we suggest resistor_thick_film_chip_0603 because it's the first known component which matches
x  For part enrich-enhob00910003 :
x    Uh, found no data on Octopart

Add and complete the following 1 entries to products.csv:
x reform2-oled,"MISSING_NAME",MISSING_MANUFACTURER,MISSING_DEPENDENCY,"MISSING_DESCRIPTION",reform2-oled.csv

Add and complete the following 1 entries to component.csv:
x component-suitable-for-part-taiyo_yuden-mk107,"Multilayer Ceramic Capacitor 10uF 10V 10% 0603 Paper T/R",MISSING_WEIGHT,MISSING_UNIT,ceramic-capacitors,capacitance:10µF;height:1mm;rohs:Non-Compliant;tolerance:10%;voltagerating_dc_:10V,MISSING_DATE,"Taiyo Yuden LMK107BBJ106KALT: Multilayer Ceramic Capacitor 10uF 10V 10% 0603 Paper T/R"

Add and complete the following 3 entries to parts.csv (including helpful data in the source column to the rightmost):
x taiyo_yuden-mk107,"Multilayer Ceramic Capacitor 10uF 10V 10% 0603 Paper T/R",component-suitable-for-part-taiyo_yuden-mk107,taiyo_yuden,LMK107BBJ106KALT,"Part family = LMK107; Category = Ceramic Capacitors; Capacitance = 10 µF; Height = 1 mm; RoHS = Non-Compliant; Tolerance = 10 %; Voltage Rating (DC) = 10 V"
x yageo-af0603,"Res Thick Film 0603 330K Ohm 1% 1/10W ±100ppm/°C Molded SMD Paper T/R",resistor_thick_film_chip_0603,yageo,AF0603FR-07330KL,"Part family = AF0603; Category = Chip SMD Resistors; Case/Package = 0603; Case Code (Imperial) = 0603; Case Code (Metric) = 1608; Composition = Thick Film; Features = Anti-Sulfur, Moisture Resistant; Height = 558.8 µm; Length = 1.6002 mm; Max Operating Temperature = 155 °C; Max Power Dissipation = 100 mW; Min Operating Temperature = -55 °C; Number of Terminations = 2; Packaging = Digi-Reel®; Power Rating = 100 mW; Resistance = 330 kΩ; RoHS = Compliant; Temperature Coefficient = 100 ppm/°C; Termination = SMD/SMT; Tolerance = 1 %; Voltage Rating = 50 V; Width = 787.4 µm"
x enrich-enhob00910003,"MISSING_NAME",MISSING_COMPONENT,enrich,ENH-OB00910003,No match in Octopart for this part

There may be entries converted for mounting holes, labels, or logos, which should just be deleted.
But don't forget to add pcb, solder, cables, and assembly parts as well which are not part of a BOM.

