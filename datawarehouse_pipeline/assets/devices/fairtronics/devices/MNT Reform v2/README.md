# Modellierung des MNT Reform Laptop in Fairtronics

Die meisten Quellen hatte ich mir im April/Mai 2021 gezogen.

## Quellen
* Es gibt ein _Handbuch_ mit interessanten Informationen und direkten Links zu den Zulieferern unter
  * https://mntre.com/reform2/handbook/index.html
  * https://mntre.com/media/reform_md/mnt-reform2-operator-handbook.pdf
  * darunter insbesondere auch https://mntre.com/reform2/handbook/schematics.html#assembly-parts
* Systemschaltbild: https://mntre.com/reform2-handbook/system.html mit vielen (wiederholten) Informationen!
* Hauptrepository https://source.mnt.re/reform/reform
  * Motherboard: https://source.mnt.re/reform/reform/-/tree/master/reform2-motherboard-pcb
  * Keyboard: https://source.mnt.re/reform/reform/-/tree/master/reform2-keyboard-pcb
  * Trackball: https://source.mnt.re/reform/reform/-/tree/master/reform2-trackball-pcb
  * Trackball Sensor: https://source.mnt.re/reform/reform/-/tree/master/reform2-trackball-sensor-pcb
  * OLED: https://source.mnt.re/reform/reform/-/tree/master/reform2-oled-pcb
  * Trackpad: https://source.mnt.re/reform/reform/-/tree/master/reform2-trackpad-pcb
  * Battery: https://source.mnt.re/reform/reform/-/tree/master/reform2-batterypack-pcb
* Assembly Parts: https://mntre.com/reform2/handbook/schematics.html#assembly-parts, siehe evtl. auch https://mntre.com/media/reform_md/mnt-reform2-diy-manual-r1.pdf
* BOM + Platinen Rev 2.0 Bebilderung: 
  * Motherboard: https://mntre.com/reform2-ibom/reform2-motherboard/ibom.html
  * Keyboard: https://mntre.com/reform2-ibom/reform2-keyboard/ibom.html
  * Trackball: https://mntre.com/reform2-ibom/reform2-trackball/ibom.html
  * Trackball Sensor: https://mntre.com/reform2-ibom/reform2-trackball-sensor/ibom.html
  * Trackpad: https://mntre.com/reform2-ibom/reform2-trackpad/ibom.html
  * 2x Battery Pack: https://mntre.com/reform2-ibom/reform2-batterypack/ibom.html
  * Statusdisplay: https://mntre.com/reform2-ibom/reform2-oled/ibom.html
* Gehäuse: https://source.mnt.re/reform/reform/-/tree/master/reform2-case-cnc/reform2-production-case-20201013
* Genutzte Software: https://kicad.org/, https://www.openscad.org/, Autocad / Autodesk Fusion fürs Gehäuse
* Assembly Guide (inkl. Parts Reference): https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md
* Hier ein Video wo es zusammengebaut wird, da sieht man vielleicht fehlende Details: https://www.youtube.com/watch?v=_CMwIo3J-x8
* Evtl. hilft auch https://community.mnt.re/ oder https://mastodon.social/@mntmn
* Werbevideo: https://vimeo.com/416633701

## Allgemeine Eigenschaften und Infos und Unterschiede zu marktüblichen Laptops 

* Es fehlen: Mikro, Kamera, GPS, Bluetooth, Lüfter, Wi-fi optional
* CPU und GPU auf eingestecktem SoM, nicht auf Mainboard 
* Größe/Maße: 29 x 20.5 x 4 cm.
* Gewicht: ca. 1,9 kg, ohne Netzteil. Ein Bild von 2012 existiert allerdings mit 2,06kg auf der Waage.

## Lücken in der Modellierung

* Das Netzteil inkl. Kabel. Ich kenne das Modell, aber der Hersteller bietet natürlich keinerlei Informationen. Man kann es bestellen, das Teil dann (vermutlich) aufbrechen und alle Parts ausmessen, aber das ist mir jetzt zu aufwändig.
* Das WiFi-Modul mit der WiFi-Antenne und deren Halterung. Es ist nicht Teil des Lieferumfanges, sondern kann als interne Steckkarte dazugekauft werden. Es gibt ein empfohlenes Modell das ich gebraucht sogar besorgt habe. Ein PDF-Schaltplan ist vorhanden und ein Abbild der Platine mit den Teilereferenzen, aber keine BOM, so dass man mühsam die physikalisch erkennbaren Teile erraten und zuordnen müsste. Ich habe schon vieles - mit Hilfe auch von Thorsten Stubbe - erhoben, insgesamt wäre es aber noch viel Arbeit.
* Das LCD Display habe ich zwar schon modelliert, ich bin aber sehr unzufrieden damit, u.a. weil dadurch viele Kommawerte in den Komponentenanzahlen entstehen. Die LCA-Datenbasis auf die man zurückgreifen kann ist mies und alt, das sagt auch das IZM. Kurz: Ich werde es so lassen.
* Ich habe festgestellt, dass ich vermutlich oft die Größenangaben von SMD-Bauteilen verwechselt habe, konkret den metric code mit dem imperial code. Wie man z.B. hier sieht sind die Bezeichnungen teilweise identisch, daher muss man wissen, welchen der beiden codes ein Hersteller meint und dann hoffentlich auch so in der BOM steht. Es geht pro Teil um wenige Milligramm, aber es gibt hunderte davon auf den einzelnen Platinen des Laptops. Das aufzuräumen ist mir nun auch zu viel.
* Es gibt viele Substitute und einige schwierige, nur annähende Umsetzungen. Die sind unten dokumentiert inkl. Leidensweg.

## Informationen zu den Teilen / Modulen

### Batterypack

#### Konvertierung des BOM

-  Couldn't find a match for part 504050-0591 of manufacturer Molex. Suggesting molex-5040500591
-  Found keystone-54 for part 54 from Keystone

#### PCB

Data from the Gerber files:
- Name = reform2-batterypack
- Size = 91mm x 66mm
- Layer count = 2
- Board thickness = 1.60mm

Lot

#### Batterien

Laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md
- 8 LiFePO4-Akkus im Standardformat 18650, zusammen 14.4 Ah/3.2 V
- Offensichtlich zwei Packs a 4 Batterien vorhanden
- battery cells are sourced from Heter/Enerpower via NKON (Netherlands)
- Battery Cell LiFePO4 18650 JGNE HTCFR18650
- JGNE = Shandong Goldencell Electronics Technology Co.,Ltd, http://www.goldencellbattery.com/
- Vermutlich dieser http://www.goldencellbattery.com/product/28.html, also JGCFR18650-1800mAh-3.2V
  - Abmaße: Höhe 65.2±0.3mm, Durchmesser: 18.25±0.1, genaueres dort
  - Gewicht: 41.5g (about)
  - https://www.nkon.nl/de/jgne-18650-1800mah-5-4a-lifepo4.html
  - Compatible cells: https://www.batteryspace.com/lifepo4-18650-rechargeable-cell-3-2v-1500-mah-8-4a-rate-4-32wh-ul-listed-un38-3-passed-ndgr.aspx, https://enerprof.de/akkus/akkus-lifepo4/akkuzellen-lifepo4/akkuzellen-lifepo4-18650/32/enerpower-18650-lifepo4-3-2v-1800-mah?c=26, https://www.18650batterystore.com/Lithium-Werks-p/lithiumwerks-apr18650m1b.htm
  - Siehe auch hier: https://enerpower.de/en/lifepo4-batteries/.
- Aufbau:
    - Die Kathode des Lithium-Ionen-Akkus besteht aus Lithium-Metalloxid, das variable Anteile an Nickel, Mangan und Kobalt ("NMC") enthalten kann, z.B.  LiNi(0,8)Mn(0,1)Co(0,1)O(2) als NMC811
    - Die Anode ist meist aus Graphit gefertigt.
    - Das Elektroly enthält Salze wie Lithiumhexafluorophosphat in einem aprotischen Lösungsmittel wie Diethylcarbonat gelöst. Bei Lithium-Polymer-Akkus wird an dieser Stelle ein Polymer aus Polyvinylidenfluorid oder Polyvinylidenfluorid-Hexafluorpropen verwendet.
    - Separator: Um Kurzschlüsse zu vermeiden, wird zwischen den Elektroden ein Separator aus Vliesstoffen oder polymeren Folien verbaut.
- Eigentlich suche ich ja Batterien mit LiFePO4 (Lithium-Eisenphosphat) als Kathodenmaterial. Typischer sind Li-ion mit Lithium-Mangandioxid oder Lithium-Kobaltdioxid (NMC = Lithium-Nickel-Mangan-Cobalt-Oxide)
    - Lithium-Ionen-Akkus sind deutlich kleiner und leichter
    - In den meisten Fällen liegt die Energiedichte von Lithium-Ionen-Akkus bei etwa 150 Wh/kg bis 200 Wh/kg. Bei Lithium-Eisenphosphat-Batterien liegt sie in der Regel bei 90 Wh/kg bis 120 Wh/kg.
    - Lithium-Eisenphosphat-Batterien haben etwa 1000 bis 10,000 Lebenszyklen. Lithium-Ionen-Batterien von etwa 500 bis 1000 liegen.
    - Lithium-Eisenphosphat-Batterien neigen bei mechanischer Beschädigung aber nicht zu thermischem Durchgehen.
    - Im Vergleich zum relativ umweltfreundlichen Lithiumeisenphosphat ist NMC wie viele Nickel- und Cobaltverbindungen vergleichsweise gefährlich und kann vermutlich Krebs erzeugen, wenn es in den Körper gelangt.
- Gewicht 41.5g, Abmaße 65mm, Durchmesser: 18.2mm (Standardformat 18650)
- Gemäß "Xiong Shu et.al.: Life-cycle assessment of the environmental impact of the batteries used in pure electric passenger cars" sind die Gewichtsverhältnisse bei NMC und LFP nachzu gleich, siehe dort Table 3. Etwas mehr Kathode, dafür etwas weniger Elektrolyt bei LFP
- Aber andere Chemikalen bei Kathode (klar), Anode und Elektrolyt, siehe dort Table A.1-6. Und die sind auch für NMC ganz anders. Und in "Mats Zackrisson et.al.:Life cycle assessment of lithium-ion batteries for plug-in hybrid electric  vehicles e Critical issues" bzw. dessen "Life cycle assessment of long life lithium electrode for electric vehicle batteries" ist es nochmal anders :-(
- Batterien ecoinvent:
    - battery production, Li-ion, NMC111, rechargeable, prismatic, GLO (https://v38.ecoquery.ecoinvent.org/Details/UPR/0e486ecb-42db-486b-8bcd-c133be69bd55/8b738ea0-f89e-4627-8679-433616064e82)
        - 1 kg of Li-ion battery pack used e.g. for mechanical drive of an electric vehicle
        - battery cell production, Li-ion, NMC111, GLO (https://v38.ecoquery.ecoinvent.org/Details/UPR/9258510c-afe0-476a-ae1c-75ed84d34179/8b738ea0-f89e-4627-8679-433616064e82)
            - Cells are made of nickel-manganese-cobalt (NMC111) cathode and a graphite-based anode
            - The specific energy capacity of a cell is 0.197 kWh/kg cell
    - battery production, NMC811, Li-ion, rechargeable, prismatic, GLO (https://v38.ecoquery.ecoinvent.org/Details/UPR/a9e61a0e-d860-4589-b386-e29255f79140/8b738ea0-f89e-4627-8679-433616064e82)
        - 1 kg of Li-ion battery pack used e.g. for mechanical drive of an electric vehicle
        - battery cell production, Li-ion, NMC811, GLO (https://v38.ecoquery.ecoinvent.org/Details/UPR/02998d1c-4542-4252-8b0d-80820ea85129/8b738ea0-f89e-4627-8679-433616064e82)
            - energy capacity of a cell is 0.209 kWh/kg cell
            - Cells are made of nickel-manganese-cobalt (NMC811) cathode and a silicon coated graphite-based anode
    - battery production, NiMH, rechargeable, prismatic, GLO
        - 1 kg of a NiMH battery for laptop applications
        - This dataset was already contained in the ecoinvent database version 2
    - battery production, NCA, Li-ion, rechargeable, prismatic, GLO
        - 1 kg of Li-ion battery pack used e.g. for mechanical drive of an electric vehicle
        - battery cell production, Li-ion, NCA, GLO (https://v38.ecoquery.ecoinvent.org/Details/UPR/90c70c7c-9644-45b4-86d7-afd9bed6b965/8b738ea0-f89e-4627-8679-433616064e82)
            - Cells are made of nickel-cobalt-aluminium (NCA) cathode and a silicon coated graphite-based anode
            - The specific energy capacity of a cell is 0.224 kWh/kg cell
    - battery production, NaCl, rechargeable
        - 1kg of a high temperature sodium salt battery
    - battery production, Li-ion, rechargeable, prismatic
        - lithium-ion battery, e.g. for mechanical drive of an electrical vehicle
        - 14 single cells, a steel box, a battery management system and cables are taken into acount.
        - battery cell production, Li-ion, GLO (https://v38.ecoquery.ecoinvent.org/Details/UPR/c0b447b0-78f4-4b5f-baca-95c2584575cf/8b738ea0-f89e-4627-8679-433616064e82)
            - cathode, LiMn2O4, for lithium-ion battery

#### Kabel

- Battery Cable Picolock - Molex - 151320502 -> molex-151320502 (FMD zugeschickt)


### Case, was alles zusammenhält und Drumher

#### Zitate aus den Quellen

* Gehäuse aus eloxiertem, schwarz-anodiertem 6061 Aluminum (milled, bead-blasted, black-anodized 6061 aluminum)
* Plexiglas/Acrylglasplatte (semi-transparentes) an der Unterseite. the bottom lid that is either milled from clear acrylic or aluminum
* CNC body parts (aluminum and acrylic) are made by a contractor of our partner Neuform in Shenzhen
* Gummifüßchen

#### Konvertierung aus den Gehäusedateien

- bottom-closing oder bottom-plate, siehe csv. Plexiglas/Acrylglasplatte (semi-transparentes) an der Unterseite. the bottom lid that is either milled from clear acrylic or aluminum
- main-box, siehe csv. Gehäuse aus eloxiertem, schwarz-anodiertem 6061 Aluminum (milled, bead-blasted, black-anodized 6061 aluminum), "CNC milled, anodized Al6061"
- keyboard bezel/frame, siehe csv: Keyboard Frame Al6061 Black, "CNC milled, anodized Al6061"
- ports...svg: Laser-cut acrylic port covers (left and right), Lasercut 1mm Acrylic, Right & Left Port Cover Acrylic Black, siehe SVG
  - Inzwischen Metall Siehe https://shop.mntmn.com/products/mnt-reform-steel-port-covers?taxon_id=13 und https://community.mnt.re/t/the-new-metal-side-panels-look-amazing/835
  - Fläckenberechnung aus SVG 50.828125mm x 134.38072mm * 1mm Außenabmaße = 6,83032 cm3
  - Bei dem linken wird etwa 1/4 rausgeschnitten -> 5.12274 cm3 * Dichte 7.5 Stahl = 38.42055g / * Dichte 1.18 Acrylglass = 6.04483324g
  - Beim rechten 2/3 von 1/4 also 1/6 -> 5.69193 cm3 * Dichte 7.5 Stahl = 42.6895g / * 1.18 Acrylglass = 6.7164774g

#### Schrauben und Unterlegscheiben

Laut https://mntre.com/reform2/handbook/schematics.html#assembly-parts:
- 30x M2x4-7985H - Screw M2x4 Black Pan Head DIN 7985H - 4+6+4+2+4+2 = 22 (8 zu wenig)
- 23x M2x5-965H - Screw M2x5 Black Countersunk DIN 965H - 6+2x2+2x2+2+7 = 23
- 18x M2x6-965H - Screw M2x6 Silver Countersunk DIN 965H - 2x4+10+2x4 = 26 (8 zu viel)
- 4x M2x12-965H - Screw M2x12 Silver Countersunk DIN 965H - 4 = 4
- 6x M4x5-965H - Screw M4x5 Silver Countersunk DIN 965H - 2x3 = 6

Laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md, was zu der obigen Anzahl passt (wenn 8 M2x6 zu 8 M2x4 wird)
- Mount TB Sensor PCB on Cup with 2x M2x5-965H
- Mount Cup with Sensor on TB Controller PCB using 4x M2x4-7985H
- Mount TB Lid on TB Cup using 2x M2x4-7985H
- Mount 2x Battery Board in Main Box using 4x pan head M2x6 screws.
- Mount Speaker Assembly below the Display Panel and secure with 2x Speaker Holder. Mount each holder with 2x M2x5-965H screws.
- Mount Left Hinge and Right Hinge in the bottom left and right corners of Screen Back with 3x M4x5-965H screws each
- Mount other half of each hinge to the Main Box with 4x M2x6-965H each.
- Mount Screen Front to Screen Back with 7x M2x5-965H.
- Mount Heatsink on CPU Module and Motherboard PCBA using 4x M2x12-965H screws and 2x 3mm Spacer A and 2x 4mm Spacer B.
- Mount the Motherboard with SOM and Heatsink with 4x M2x4-7985H screws to the main box.
- Mount the assembled trackball (MREFATP) with 4x M2x4-7985H. Or mount the assembled trackpad (MREFATB) with 2x M2x4-7985H.
- Mount the keyboard with 6x M2x4-7985H screws
- Mount the OLED PCBA with 2x M2x4-7985H. Remove protection film.
- Plug the Keyboard Frame into the main box and mount it with 6x M2x5-965H screws.
- Remove protective film from the left and right acrylic Side Panels and mount with 2x M2x5-965H screws each.
- Put the Bottom Plate on the bottom of the Main Box and mount it with 10x M2x6-965H screws

Laut Screw Specs kommen aber andere Maße und Anzahlen statt... daher ignoriert:
- Main Box Screws: https://source.mnt.re/reform/reform/-/blob/master/reform2-case-cnc/reform2-production-case-20201013/mntreform2-main-box-screw-specs-20201008.pdf
- Screen Back Screws: https://source.mnt.re/reform/reform/-/blob/master/reform2-case-cnc/reform2-production-case-20201013/mntreform2-screen-back-screw-specs-20201008.pdf

Laut https://mntre.com/reform2/handbook/schematics.html#assembly-parts zusätzlich:
- 4x M2 Flat Washer Accu HAFZ-M2-A2 ????
 
#### Spacer 

Laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md
- Spacer A Würth Elektronik 960030021
- Spacer B Würth Elektronik 960040021

#### Magnete

- closing magnets:
  - the case is held shut by 8 little neodymium bar magnets
  - 8 Neodymium Bar Magnet - MNT Research - MREFDMAG20R01... komischerweise Neodymium N52, 10x3x2.4mm, Nickel Coating, Thickness Magnetized (laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)

#### Gummifüßchen

- 4 Rubber Foot Transparent - Modulor - 0303782 - ein Laden in Berlin
- Höhe 3,5mm, Durchmesser 12,7mm
- https://www.modulor.de/bumper-selbstklebende-elastikpuffer-rund-transparent-h-3-5-mm-o-12-7-mm-200-stueck.html
- Kunststoff-Puffer aus Polyurethan-Elastomer, transparent
- Kleber Acrylat
- Aber 3D Modell oder Gewciht nicht verfügbar
- Nachgefragt, was denn eine 200er Tüte wiegt kam die Antwort: "Das Gewicht von dem Produkt 0303782 in der Einheit der Verpackung liegt ca. bei 0,98kg."
  - Davon ziehe ich 50g Verpackung ab und teile durch 200, ergibt 4,65g
  - Den Kleber habe ich ignoriert

#### Sonstiges

- Stickers - ignoriert
- Acrylic WiFi antenna holder, noch nicht modelliert, da vom WiFi Modul abhängig


### Keyboard

#### Konvertierung des BOM

- Found yageo-cc0603npo for part CC0603JRNPO9BN180 from Yageo
- Found taiyo_yuden-mk107 for part JMK107BB7475KA-T from Taiyo Yuden
- Found tdk-c1608x8 for part C1608X8L1C105K080AC from TDK
- Found yageo-cc0603x7r for part CC0603JPX7R9BB104 from Yageo
- Found on-mmdl914 for part MMDL914T1G from ON
- Found diodes-b2xx-13-f for part B250-13-F from Diodes
- Found osram-lwq38e for part LW Q38E-Q2OO-3K5L from OSRAM
- Missing manufacturer or part number for entry with value MNT. Setting to unknown
- Missing manufacturer or part number for entry with value MH1. Setting to unknown
- Missing manufacturer or part number for entry with value MH2. Setting to unknown
- Missing manufacturer or part number for entry with value MH3. Setting to unknown
- Missing manufacturer or part number for entry with value MH4. Setting to unknown
- Missing manufacturer or part number for entry with value MH5. Setting to unknown
- Missing manufacturer or part number for entry with value MH6. Setting to unknown
- Couldn't find a match for manufacturer 'JST'. Suggesting jst
- Couldn't find a match for part S4B-PH-SM4-TB(LF)(SN) of manufacturer JST. Suggesting jst-s4bphsm4tblfsn
- Couldn't find a match for part 200528-0040 of manufacturer Molex. Suggesting molex-2005280040
- Couldn't find a match for part 12401610E4#2A of manufacturer Amphenol. Suggesting amphenol-12401610e42a
- Couldn't find a match for part SRP4020TA-100M of manufacturer Bourns. Suggesting bourns-srp4020ta100m
- Couldn't find a match for part CRCW0603715KFKEA of manufacturer Vishay Dale. Suggesting vishay-crcw0603715kfkea
- Found yageo-rc0603FR-071ML for part RC0603FR-071ML from Yageo
- Couldn't find a match for part RC0603FR-0754K9L of manufacturer Yageo. Suggesting yageo-rc0603fr0754k9l
- Couldn't find a match for part ERJ-3EKF5101V of manufacturer Panasonic. Suggesting panasonic-erj3ekf5101v
- Found vishay-crcw06030000Z0EAC for part CRCW06030000Z0EAC from Vishay Dale
- Couldn't find a match for part RC0603FR-0710KL of manufacturer Yageo. Suggesting yageo-rc0603fr0710kl
- Found vishay-crcw06030000Z0EAC for part CRCW06030000Z0EAC from Vishay Dale
- Found yageo-rc0603FR-074K7L for part RC0603FR-074K7L from Yageo
- Couldn't find a match for part CRCW060362K0FKEAC of manufacturer Vishay Dale. Suggesting vishay-crcw060362k0fkeac
- Couldn't find a match for part CRCW0603604KFKEA of manufacturer Vishay Dale. Suggesting vishay-crcw0603604kfkea
- Couldn't find a match for part CPG135001D02 of manufacturer Kailh. Suggesting kailh-cpg135001d02
   - Product page 1: http://www.kailh.com/en/Products/Ks/CS/320.html
   - Product page 2: https://www.kailhswitch.com/mechanical-keyboard-switches/low-profile-key-switches/tactile-mechanical-keyboard-switches.html
   - Data sheet: https://www.kailhswitch.com/uploads/201915927/CPG135001D02_-_Brown_Tactile_Choc-(1).pdf?rnd=254
   - Name: Kailh Choc Brown Switch CPG135001D02 (oder CPG1511F01S11)
   - Abmaße grob 15 x 15 x 3+5+3 mm
   - Ergebnis Labor:
     - Durchsichtiger Oberbau ("Kalih"): 0.236g
     - Schwarzer Unterbau ("N2"): 0.388g
     - Brauner Tastenkopf ("E") mit ein klein wenig Kleber: 0.202g
     - Feder: 0.040g
     - Kontaktfeder inkl. Plastikteilchen: 0.042g
     - Zweiter Kontakt: 0.044g
     - Gesamt: 0.946g
     - Kontakte jeweils 0.3mm*0.1mm plattiert beidseitig
     - Kontaktfeder interessanterweise leichter obschon größer
   - Bei Octopart haben Tactile Switches meist folgende Materialien
     - Actuator (Bedienfläche, also das braune hier): Nylon oder Polyamide. Nylon ist ein Polyamid, für Nylon habe ich Prozesse, also Nylon
     - Housing (das drumherum): meist Nylon
     - Contact Material: meist Silber (??), dann Bronze, Edelstahl und Messing. Die Kontaktfeder ist Bronze/Messingfarben, der zweite Kontakt Stahlfarbend. Nur für Stahl und Brass/Messing habe ich Prozesse.
     - Contact Plating: allermeistens Silber
   - Die Namen der Bestandteile: Case, Contact, Bracket, Button, Moving Blade, Cover (aus Metall), Mounting Terminal, Plating for Terminal, Plating for Contact
-  Couldn't find a match for part KMR221GLFS of manufacturer C&K. Suggesting ck-kmr221glfs
-  Couldn't find a match for part DM01 of manufacturer Apem. Suggesting apem-dm01
-  Found microchip-ATMEGA32U4-AU for part ATMEGA32U4-AU from Microchip
-  Couldn't find a match for part TPS61185RGET of manufacturer Texas Instruments. Suggesting texas_instruments-tps61185rget
-  Couldn't find a match for part MCP1700T-3302E/TT of manufacturer Microchip. Suggesting microchip-mcp1700t3302ett
-  Found abracon-abm8aig-16000MHz-4-T for part ABM8AIG-16.000MHz-4-T from Abracon

#### PCB

- Name = reform2-keyboard
- Size = 276mm x 118mm
- Layer count = 2
- Board thickness = 1.60mm

Lot

#### Kabel 

- Es sind ja 2 Kabel, ein USB und ein UART/SYSCTL laut https://mntre.com/media/reform_md/mnt-reform2-diy-manual-r1.pdf und https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md:
  - "Connect one USB/SYSCTL Cable to the keyboard's internal USB port." 4-pin
  - "Connect another USB/SYSCTL Cable to the keyboard internal UART/SYSCTL port." 4-pin

Laut https://mntre.com/reform2/handbook/schematics.html#assembly-parts 3 davon!
- 3x mnt-MREFCBLU20R01 - USB/SYSCTL Cable JST-PH 4P - https://source.mnt.re/reform/reform/-/blob/master/reform2-cables/mnt-reform2-serial-cable.png
- https://mntre.com/reform2/handbook/_images/kbd-callouts.png sagt auch, dass hier ein USB-Kabel ran kommt oder?

#### Key caps

- After trying out the MBK concave keycaps, we talked to FKcaps about a collaboration. They were able to manufacture a new version of their caps in record speed that fit our laser engraving approach: a translucent ABS body painted with a thin layer of black PU.
- uses a split spacebar and only two different keycap sizes, 1U (72?) and 1.5U (10 Stück), Blank keycaps (no labels).
- 72x FKcaps MBK 1U or Kailh BSPG1350-06001P2, 10x FKcaps MBK 1.5U or Kailh BSPG1350-06009. Translucent ABS coated with black PU. (laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
- Dimensions: 17.5x16.5mm (1u/homing); 26.5x16.5mm (1.5u); 35.5x16.5mm... (2u?), gemäß https://splitkb.com/products/mbk-choc-low-profile-keycaps
- 3D-Daten von https://www.thingiverse.com/thing:4564253/files, dort alles perfekt vorhanden.

### Motherboard

#### Konvertierung des BOM

- Couldn't find a match for part 3000 of manufacturer Keystone. Suggesting keystone-3000
- Couldn't find a match for part EEE-FTH101XAP of manufacturer Panasonic. Suggesting panasonic-eeefth101xap
- Couldn't find a match for part UMK107BJ105KA-T of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-umk107bj105kat
- Found yageo-cc0603x7r for part CC0603JPX7R9BB104 from Yageo
- Found murata-grm188r60J476ME15D for part GRM188R60J476ME15D from Murata
- Found murata-grm188r6YA106MA73D for part GRM188R6YA106MA73D from Murata
- Couldn't find a match for part C1608X5R1V475K080AC of manufacturer TDK. Suggesting tdk-c1608x5r1v475k080ac
- Couldn't find a match for part TMK107B7474KA-TR of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-tmk107b7474katr
- Found ucc-EMZR500ARA221MHA0G for part EMZR500ARA221MHA0G from UCC
- Found ucc-EMZR500ARA221MHA0G for part EMZR500ARA221MHA0G from UCC
- Couldn't find a match for part C1608X7R1H334K080AC of manufacturer TDK. Suggesting tdk-c1608x7r1h334k080ac
- Couldn't find a match for part VJ0603A6R8DXQCW1BC of manufacturer Vishay. Suggesting vishay-vj0603a6r8dxqcw1bc
- Found taiyo_yuden-mk107 for part UMK107BBJ225KA-T from Taiyo Yuden
- Couldn't find a match for part C0603C333J4REC7411 of manufacturer KEMET. Suggesting kemet-c0603c333j4rec7411
- Found kemet-C0603C103K5RAC3190 for part C0603C103K5RAC3190 from KEMET
- Couldn't find a match for part C0603C681J5GACTU of manufacturer KEMET. Suggesting kemet-c0603c681j5gactu
- Couldn't find a match for part GRM1885C1H202JA01D of manufacturer Murata. Suggesting murata-grm1885c1h202ja01d
- Found yageo-cc0603npo for part CC0603JRNPO9BN180 from Yageo
- Couldn't find a match for part BZT52C10-7-F of manufacturer Diodes, Inc.. Suggesting diodes-bzt52c107f
- Couldn't find a match for part SP0503BAHT of manufacturer Littelfuse. Suggesting littelfuse-sp0503baht
- Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
- Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
- Couldn't find a match for part SMAJ36CA-E3/5A of manufacturer Vishay. Suggesting vishay-smaj36cae35a
- Couldn't find a match for part SD0603S040S0R2 of manufacturer AVX. Suggesting avx-sd0603s040s0r2
- Couldn't find a match for part BAT46WJ,115 of manufacturer Nexperia. Suggesting nexperia-bat46wj115
- Found mcc-sk5 for part SK54A-LTP from MCC
- Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
- Found rohm-SMLEN3WBC8W1 for part SMLEN3WBC8W1 from ROHM
- Couldn't find a match for part SBR0560S1-7 of manufacturer Diodes, Inc.. Suggesting diodes-b05
- Couldn't find a match for part RBR3MM60ATFTR of manufacturer ROHM. Suggesting rohm-rbr3mm60atftr
- Couldn't find a match for part BZT52C6V2-7-F of manufacturer Diodes, Inc.. Suggesting diodes-bzt52c6v27f
- Couldn't find a match for part SML-D12Y1WT86 of manufacturer ROHM. Suggesting rohm-smld12y1wt86
- Couldn't find a match for part 0157004.DR of manufacturer Littelfuse. Suggesting littelfuse-0157004dr
- Couldn't find a match for part BLM18KG101TN1D of manufacturer Murata. Suggesting murata-blm18kg101tn1d
- Found murata-BLM18PG221SH1D for part BLM18PG221SH1D from Murata
- Missing manufacturer or part number for entry with value NGFF-Mount1. Setting to unknown
- Missing manufacturer or part number for entry with value LOGO_REFORM. Setting to unknown
- Missing manufacturer or part number for entry with value LABEL_MPCIE1. Setting to unknown
- Missing manufacturer or part number for entry with value LABEL_MPCIE2. Setting to unknown
- Missing manufacturer or part number for entry with value LABEL_CPU. Setting to unknown
- Missing manufacturer or part number for entry with value BADGE. Setting to unknown
- Missing manufacturer or part number for entry with value NOTOUCH. Setting to unknown
- Missing manufacturer or part number for entry with value NGFF-Mount2. Setting to unknown
- Couldn't find a match for part 9774010243 of manufacturer Wurth. Suggesting wurth-9774010243
   - Spacer aus steel, tin-plated, siehe https://www.we-online.de/katalog/datasheet/9774010243.pdf, Familie https://www.we-online.de/katalog/de/SMSI_SMT_STEEL_SPACER_M2_THREAD_INTERNAL
   - Ähnliches Teil mit FMD nicht gefunden, höchstens https://www.harwin.com/products/R30-6010202/ mit parametrisiertem FMD https://cdn.harwin.com/pdfs/Material_composition_for_R30-601.pdf, besteht aber aus Messing, Nickel-plated und andere Form (unthreaded, nur ein Durchmesser)
   - STEP file -> STL file -> conversion
- Missing manufacturer or part number for entry with value NGFF-Mount3. Setting to unknown
- Missing manufacturer or part number for entry with value MountingHole_Pad. Setting to unknown
- Missing manufacturer or part number for entry with value OSHWA_DE17. Setting to unknown
- Found switchcraft-RAPC712X for part RAPC712X from Switchcraft
- Found te-1-2199230-6 for part 1-2199230-6 from TE
- Found molex-48099-5701 for part 48099-5701 from Molex
- Couldn't find a match for part 504050-0591 of manufacturer Molex. Suggesting molex-5040500591
- Couldn't find a match for manufacturer 'JST'. Suggesting jst
- Couldn't find a match for part B4B-PH-K-S(LF)(SN) of manufacturer JST. Suggesting jst-b4bphkslfsn
- Couldn't find a match for part FH12-33S-0.5SH(55) of manufacturer Hirose. Suggesting hirose-fh1233s05sh55
- Missing manufacturer or part number for entry with value LINE_IN. Setting to unknown
- Found wurth-61300311121 for part 61300311121 from Wurth
- Couldn't find a match for part 20021111-00010T4LF of manufacturer Amphenol FCI. Suggesting amphenol-2002111100010t4lf
- Found wurth-61300311121 for part 61300311121 from Wurth
- Found molex-87914-1616 for part 87914-1616 from Molex
- Found molex-87758-3016 for part 87758-3016 from Molex
- Found te-1775059-1 for part 1775059-1 from TE
- Found wurth-685119134923 for part 685119134923 from Wurth
- Couldn't find a match for part 692121030100 of manufacturer Wurth. Suggesting wurth-692121030100
- Found cui-SJ-43516-SMT-TR for part SJ-43516-SMT-TR from CUI
- Couldn't find a match for part 629105136821 of manufacturer Wurth. Suggesting wurth-629105136821
- Couldn't find a match for part 7447709220 of manufacturer Wurth. Suggesting wurth-7447709220
- Couldn't find a match for part LQH32PB150MN0L of manufacturer Murata. Suggesting murata-lqh32
- Found vishay-IMC1210 for part IMC1210ER100K from Vishay Dale
- Found bourns-SRR1210 for part SRR1210-680M from Bourns
- Couldn't find a match for part CBC3225T100MRV of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-cbc3225t
- Couldn't find a match for part BRL3225T2R2M of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-brl3225t
- Found pulse-J0G-0003NL for part J0G-0003NL from Pulse
- Found nexperia-pmv for part PMV50EPEAR from Nexperia
- Couldn't find a match for part PMV50ENEAR of manufacturer Nexperia. Suggesting nexperia-pmv50enear
- Couldn't find a match for part SI7461DP-T1-E3 of manufacturer Vishay Siliconix. Suggesting vishay-si7461dpt1e3
  - Ein MOSFET
  - https://www.vishay.com/search?searchChoice=part&query=SI7461DP-T1-E3 und https://www.vishay.com/docs/72567/si7461dp.pdf
  - PowerPAK SO-8 scheint mit SO-8 in der Größe nicht viel zu tun zu haben, d.h. mosfet_so-8_4.9x3.9x1.75 passt nicht
  - Aber: " The pin arrangement (drain, source, gate pins) and the pin dimensions are the same as standard SO-8 devices (see figure 2).
  - Therefore, the PowerPAK connection pads matc h directly to those of the SO-8. The only difference is the extended drain connection area."
  - Es fehlt also diese Metallplatte darunter. Hat sonst nirgendjemand
  - Nachgefragt am 24.1., keine Antwort
  - Zur Not halt SO-8 ohne diese Platte, da gibt's was von anderen Herstellern
  - PowerPAK SO-8 kann an Stelle SO-8 eingesetzt werden, hat aber keine Beinchen, ist flacher und füllt statt dessen die ganze Fläche aus und hat eine Bodenplatte für den Ground und Kühlung
  - Weil "flacher" gleicht "größer" aus und "Bodenplatte" gleicht "Beinchen" aus habe ich einfach einen SO-8 genommen
- Couldn't find a match for part SI7850DP-T1-E3 of manufacturer Vishay Siliconix. Suggesting vishay-si7850dpt1e3
  - Ebenso, siehe https://www.vishay.com/docs/71625/si7850dp.pdf
  - Siehe Vorgänger
- Couldn't find a match for part RC0603FR-0710KL of manufacturer Yageo. Suggesting yageo-rc0603fr0710kl
- Found vishay-crcw0603100KJNEAC for part CRCW0603100KJNEAC from Vishay Dale
- Couldn't find a match for part RC0603FR-1049R9L of manufacturer Yageo. Suggesting yageo-rc0603fr1049r9l
- Found yageo-rc0603FR-074K7L for part RC0603FR-074K7L from Yageo
- Found vishay-crcw060333R0FKEAC for part CRCW060333R0FKEAC from Vishay Dale
- Found yageo-rc0603FR-07680RL for part RC0603FR-07680RL from Yageo
- Found vishay-crcw060347K0FKEAC for part CRCW060347K0FKEAC from Vishay Dale
- Found vishay-crcw0603100RFKEAC for part CRCW0603100RFKEAC from Vishay Dale
- Found vishay-crcw06030000Z0EAC for part CRCW06030000Z0EAC from Vishay Dale
- Found yageo-rc0603FR-071ML for part RC0603FR-071ML from Yageo
- Couldn't find a match for part RC0603FR-0762RL of manufacturer Yageo. Suggesting yageo-rc0603fr0762rl
- Found yageo-rc0603FR-071K5L for part RC0603FR-071K5L from Yageo
- Couldn't find a match for part CRCW060349K9FKEAC of manufacturer Vishay Dale. Suggesting vishay-crcw060349k9fkeac
- Couldn't find a match for part LTR18EZPFSR020 of manufacturer ROHM. Suggesting rohm-ltr18
- Couldn't find a match for part CRCW0603150KFKEAC of manufacturer Vishay Dale. Suggesting vishay-crcw0603150kfkeac
- Couldn't find a match for part CR0603-FX-2702ELF of manufacturer Bourns. Suggesting bourns-cr0603fx2702elf
- Found vishay-crcw06033K30FKEAC for part CRCW06033K30FKEAC from Vishay Dale
- Couldn't find a match for part 35224R7JT of manufacturer TE Connectivity. Suggesting te-3522
- Found yageo-rc0603FR-07475RL for part RC0603FR-07475RL from Yageo
- Couldn't find a match for part LTR18EZPFU10L0 of manufacturer ROHM. Suggesting rohm-ltr18fu10l0
- Couldn't find a match for part RC0603FR-07330RL of manufacturer Yageo. Suggesting yageo-rc0603fr07330rl
- Found yageo-rc0603FR-079K53L for part RC0603FR-079K53L from Yageo
- Found vishay-crcw060390K9FKEAC for part CRCW060390K9FKEAC from Vishay Dale
- Found yageo-rc0603FR-0751KL for part RC0603FR-0751KL from Yageo
- Found yageo-rc0603FR-07220RL for part RC0603FR-07220RL from Yageo
- Couldn't find a match for part RC0603FR-071KL of manufacturer Yageo. Suggesting yageo-rc0603fr071kl
- Found yageo-rc0603FR-0727RL for part RC0603FR-0727RL from Yageo
- Found yageo-rc0603FR-0724K9L for part RC0603FR-0724K9L from Yageo
- Couldn't find a match for part DM01 of manufacturer Apem. Suggesting apem-dm01
- Couldn't find a match for part DHA-04TQ of manufacturer Diptronics. Suggesting diptronics-dha04
- Couldn't find a match for part UK-B0206-G3.8-250-JZ of manufacturer USAKRO. Suggesting usakro-ukb0206g38250jz
- Missing manufacturer or part number for entry with value IMX_PWM4. Setting to unknown
- Missing manufacturer or part number for entry with value T_M4_NMI. Setting to unknown
- Missing manufacturer or part number for entry with value SAI2_RXC. Setting to unknown
- Missing manufacturer or part number for entry with value T_OTG. Setting to unknown
- Missing manufacturer or part number for entry with value T_PMIC_ON_REQ. Setting to unknown
- Found te-1717254-1 for part 1717254-1 from TE
- Found texas_instruments-SN65DSI86IPAPQ1 for part SN65DSI86IPAPQ1 from Texas Instruments
- Found molex-67910-5700 for part 67910-5700 from Molex
- Couldn't find a match for part LM2677S-3.3/NOPB of manufacturer Texas Instruments. Suggesting texas_instruments-lm2677s
- Couldn't find a match for part TLV62568DBVR of manufacturer Texas Instruments. Suggesting texas_instruments-tlv62568dbvr
   - XML siehe Desktop
   - Von Spez wäre ic_power_management_sot-23_5-pin_nipdau-plated_2.9x1.6x1.45 passend, aber dieser ist um einges schwerer
   - Woran könnte man erkennen, dass zwei verschiedene components angesagt sind?
   - Oder genügt ob der letztlichen geringen Unterscheide doch ein Durchschnitts-component?
   - https://www.ti.com/store/ti/en/p/product/?p=TLV62568DBVR
   - Familie: https://www.ti.com/product/TLV62568#product-details##params
- Couldn't find a match for part LMR16006YQ3 of manufacturer Texas Instruments. Suggesting texas_instruments-lmr16006yq3
  - XML siehe Desktop
  - Q3 gibt es nicht, aber https://www.ti.com/product/LMR16006Y-Q1
  - Remove # in front of this entry within *.parts.csv
- Found texas_instruments-tps2561drc for part TPS2561DRCR from Texas Instruments
- Found texas_instruments-tlv1117dcy for part TLV1117-18CDCYR from Texas Instruments
- Found nxp-lpc11u00d48 for part LPC11U24FBD48-301 from NXP
- Couldn't find a match for part LTC4020EUHF#PBF of manufacturer Analog Devices. Suggesting analog_devices-ltc4020euhfpbf
- Couldn't find a match for part AP22815AWT-7 of manufacturer Diodes, Inc.. Suggesting diodes-ap22815awt7
  - https://www.diodes.com/part/view/AP22815, ohne MDS
  - Die Bauform TSOT25 ist um Toleranzmilimeter ähnlich dem SOT25
  - Die Familie: https://www.diodes.com/products/power-management/power-switches/#collection-9676=~(Packages~(~'SOT25~'TSOT25))
  - Für SOT25 hat Octopart leider nur Diodes: https://octopart.com/search?category_id=5467&case_package=SOT-25
  - Prüfen ob nicht vielleicht der 5-pin SOT-23-5 so ähnlich ist! Ok, TSOT ist etwas dünner, aber SOT-5 = SOT-23-5 (https://en.wikipedia.org/wiki/Small-outline_transistor#SOT23-5,_SOT353,_SOT553)
  - SOT-23-5 Ersatz mit FMD gefunden: https://octopart.com/max4915beuk%2Bt-maxim+integrated-44348904?r=sp = https://www.maximintegrated.com/en/products/analog/analog-switches-multiplexers/MAX4915B.html
-  Couldn't find a match for part TPD12S521 of manufacturer Texas Instruments. Suggesting texas_instruments-tpd12s521
-  Found idt-5V41066PGGI8 for part 5V41066PGGI8 from IDT
-  Couldn't find a match for part SP3012-06UTG of manufacturer Littelfuse. Suggesting littelfuse-sp301206utg
    - Requested on 03-08-2021, keine Antwort
    - Substitut laut Octopart: https://www.ti.com/product/TPD6E05U06#product-details##pps, genommen
-  Couldn't find a match for part SN74AVC1T45 of manufacturer Texas Instruments. Suggesting texas_instruments-sn74avc1t45
-  Couldn't find a match for part WM8960CGEFL/V of manufacturer Cirrus Logic. Suggesting cirrus_logic-wm8960
-  Found analog_devices-LTC6803IG-4PBF for part LTC6803IG-4#PBF from Analog Devices
-  Couldn't find a match for part PCF8523T/1,118 of manufacturer NXP. Suggesting nxp-pcf8523t
-  Found texas_instruments-INA260AIPWR for part INA260AIPWR from Texas Instruments
-  Found texas_instruments-lm2677s for part LM2677SX-5 from Texas Instruments
-  Found texas_instruments-TXS0108EPW for part TXS0108EPW from Texas Instruments
-  Found texas_instruments-TUSB8041IPAPRQ1 for part TUSB8041IPAPRQ1 from Texas Instruments
-  Found abracon-abm8aig-24000MHz-R40-4-T for part ABM8AIG-24.000MHz-R40-4-T from Abracon
-  Found abracon-abm8aig-12000MHz-2-T for part ABM8AIG-12.000MHz-2-T from Abracon
-  Found abracon-abm8aig-25000MHz-R40-4-T for part ABM8AIG-25.000MHz-R40-4-T from Abracon
-  Couldn't find a match for part ECS-.327-7-34B-TR of manufacturer ECS. Suggesting ecs-ecx31b

#### PCB

- Name = reform2-motherboard
- Size = 276mm x 94mm
- Layer count = 6
- Board thickness = 1.60mm
- Welches Finish?

Lot

#### Kabel

- Any cables not listed otherwhere?
- USB/SYSCTL Cable - MREFCBLU20R01, JST-PH 4P cable, 24AWG, 15cm length, siehe PNG anbei (laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
- Laut https://mntre.com/media/reform_md/mnt-reform2-diy-manual-r1.pdf gibt es "3 black internal 4-pin cables (for USB and SYSCTL UART)"
- 2x internal USB cable
  - "Connect MIPI-DSI port of CPU Module to MIPI-DSI socket on Motherboard PCBA using MIPI-DSI Cable." (https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
  - Ersatz durch Molex 152661032 oder 982661032, FMD angefragt

#### Knopfzelle

- Da muss noch eine Knopfzelle (12 mm Coin Cell CR 1216 oder CR 1220) auf dem Mainboard sein, zumindest gibt es ja das keystone-3000
- CR = Lithium-Rundzelle 3V, Durchmesser 12mm, Höhe 1,6/2,0mm. 0,7g/0,9g. Das müsste Lithium-Mangan sein. Anbieter:
  - renata https://www.renata.com/en/products/lithium-batteries/cr1216/, https://www.renata.com/en/products/lithium-batteries/cr1220/. Am 7.2. angefragt
  - Varta kannte vergessen. sonst noch? Panasonic hat keine FMD, könnte man aber anfragen. Duracell hat sowas nicht.

#### Spacer

- Spacer laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md
  - Spacer A - Würth Elektronik 960030021, 2x 3mm
  - Spacer B - Würth Elektronik 960040021, 2x 4mm
- Statt dessen war aber der Würth 960030021 vier mal in der BOM... komisch. Das ist mit 2.4mm auch ein sehr schmaler Spacer. Trotzdem wohl drin


### OLED

#### Konvertierung aus BOM

-  Couldn't find a match for part number LMK107BBJ106KALT of manufacturer Taiyo Yuden. Suggesting taiyo_yuden-mk107
-  Found taiyo_yuden-mk107 for part UMK107BJ105KA-T from Taiyo Yuden
-  Missing manufacturer or part number for entry with value MNT. Setting to unknown
-  Missing manufacturer or part number for entry with value MH1. Setting to unknown
-  Missing manufacturer or part number for entry with value MH2. Setting to unknown
-  Found molex-2005280040 for part 200528-0040 from Molex
-  Couldn't find a match for part number AF0603FR-07330KL of manufacturer Yageo. Suggesting yageo-af0603
-  Found yageo-rc0603 for part RC0603FR-074K7L from Yageo
-  Couldn't find a match for part number ENH-OB00910003 of manufacturer Enrich Electronics. Suggesting enrich-enhob00910003
   - Tja, das ist das OLED Display
   - White
   - Hersteller: http://www.rxxdisplay.com, auf Alibaba: https://enrich1.en.alibaba.com/
   - Vermutlich http://www.rxxdisplay.com/chanpinzhanshi/120.html = https://german.alibaba.com/product-detail/0-91-inch-oled-display-128x32-oled-i2c-interface-with-pcb-4pin-1600132441842.html = http://www.oledlcddisplay.com/e_productshow/?75-091-inch-micro-OLED-display-128x32-dots-SPI-interface-display-75.html
   - https://german.alibaba.com/product-detail/0-91-inch-oled-display-128x32-oled-i2c-interface-with-pcb-4pin-1600132441842.html
   - 30x11.5mm x 1.227mm, Einzelbruttogewicht 0.020kg (also mit Einzelverpackung, oder?)
   - http://www.oledlcddisplay.com/e_productshow/?75-091-inch-micro-OLED-display-128x32-dots-SPI-interface-display-75.html
   - Produktnummer "ENH0.91 Inch OLED" oder ENH-OB00910003, 0.91 inch, 128x32 dots von Enrich Electronics
   - Anfrage gestellt, nicht geantwortet
   - Alternative MCOT128032AY-WS von Midas (https://www.midasdisplays.com/product-explorer/oleds/cog-graphic-oleds/mcot128032a/mcot128032ay-ws), 1.05g !, angefragt, nicht geantwortet
   - Alternative https://www.newhavendisplay.com/nhd22312832ucw3-p-9582.html (deutlich größer! Glas?) ebenfalls angefragt (Kontaktformular), nicht geantwortet
   - ALternative https://www.densitron.com/products/product/dd-2832we-4ak (etwas größer) ebenfallsd angefragt (Kontaktformular), nicht geantwortet
   - Muss ich die Hersteller durchgehen: https://octopart.com/search?q=OLED&currency=USD&specs=0&resolution_width_=128&resolution_height_=32
     - Adafruit hat sowas (https://www.adafruit.com/product/931, https://www.adafruit.com/product/2675), aber sie kaufen auch nur ein: https://forums.adafruit.com/viewtopic.php?f=8&p=878625
     - AVX ist eine Sackgasse. Gab's wohl mal als LCD bei Northstar?
     - Midas: https://www.midasdisplays.com/product-explorer/oleds/cog-graphic-oleds/mcot128032a/mcot128032ay-ws
       Dieser MCOT128032AY-WS passt genau! Wird mit 1.05g angegeben
     - Newhaven: https://www.newhavendisplay.com/nhd22312832ucw3-p-9582.html, aber deutlich größer
     - Jetzt reicht's
   - LCA zu OLED, siehe https://www.sciencedirect.com/science/article/abs/pii/S0959652616309131, hier vorversion: https://coek.info/pdf-life-cycle-assessment-of-organic-light-emitting-diode-display-as-emerging-materi.html
     - Leider ohne Gewichtsangaben. Es wird thematisiert, dass man die Herstellung der Chemikalien kaum herausbekommt, aber es werden Substitute gefunden auf strange Weise
     - Die FU ist farbiges 5-inch (136.6 mm x 69.8 mm) AMOLED. Es wird die Aufbaustruktur erklärt, die Schichtendicken, die Produktion der Chemikalien, der Produktionsprozess mit den eingehenden Stoffen... aber nirgends steht irgendeine Dichte der Stoffe, Gewichtsverhältnisse  oder auch nur ein konkretes Gewicht der FU. Mir bleibt nichts anderes übrig, als die Dichten der Substitute zu ermitteln und mit den ebenfalls  ermittelten Volumina der Schichten zu multiplizieren. Sehr mühsam...
  - Sonst habe ich andere Literatur gefunden, aber keine die Inventotydaten hat.
  - Alternative: Das LCD entsprechend klein als Substitut. Das innolux-N125HCE-GN1-mobile hat 162g, hier sind es 10g. Außerdem lasse ich den Stahlrahmen weg.
    - Als Device modelliert und als solches im MNT laptop integriert, nicht hier als Part

#### PCB

- Name = reform2-oled
- Size = 80mm x 12mm
- Layer count = 2
- Board thickness = 1.60mm
- Lot

#### Kabel

- wuerth-686704050001 - 4P (1mm pitch) FPC, Würth Elektronik 686704050001 in https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md
- https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md: "Connect OLED PCBA to the Keyboard (MREFAKB) with OLED Cable (blue side up)."
- Substitut Molex 15267-0702 geholt


### Screen

#### The screen itself

- Display Full HD (1920x1080 pixels) with 24-bit color
- 12.5" IPS eDP / a-Si TFT-LCD , LCM
- a-SI = https://en.wikipedia.org/wiki/Amorphous_silicon
- TFT-LCD = https://en.wikipedia.org/wiki/Thin-film-transistor_liquid-crystal_display
- IPS (In-Plane Switching), eine besondere Art der TFT-LCDs
- eDP = embedded Display Port, interner Anschluss
- Weight 162/170g (Typ./Max.), laut https://www.lcds-center.com/parameter_Innolux_N125HCE-GN1(LCM_12.5-inch_1920x1080).html und Datenblatt in diesem Ordner
- embedded DisplayPort
- from Innolux, model number N125HCE-GN1 (http://www.panelook.com/N125HCE-GN1_Innolux_12.5_LCM_overview_28140.html)
- Man nehme den ecoinvent Datensatz für ein a-Si TFT LCD module, der allerdings für AMLCD, nicht IPS-TFT, zu dem 15'' ist und Daten hat von Ende 90er.
  - Das beinhalttet aber auch PWBs, Kabel und Rahmen. Was wir benötigen ist nur
  - das Glas = Material lcd_glass (davon nur die Produkt- nicht die Prozessmaterialien, was sich zu gut 1kg summmiert, d.h. ecoinvent-Verschnittfaktor 1.028 ist drin),
  - das Panel (davon nur die Produkt- nicht die Prozessmaterialien, aber auch diese alleine summieren sich zu etwa 1.9kg pro kg Einheit),
  - das Backlight, von letzterem wohl auch nicht den Rahmen (?)
- Es wird wohl angenommen, dass dort das Kabel direkt rausführt. Stecker? Das ganze ist so komplex dass man ein eigenes Produkt daraus machen muss

#### Gehäuseteile

- screen back: siehe csv, "CNC milled, anodized Al6061"
- screen front: siehe csv, "CNC milled, anodized Al6061"

#### Kabel

- wuerth-687733050002 - MIPI-DSI Cable FPC 0.5mm 33P 50mm. Achtung, da ist schon so ein Kabel beim Motherboard, ist das hier dann doppelt? Gemäß https://mntre.com/reform2/handbook/schematics.html#assembly-parts gibt es nur eins.
- mnt-MNT190722001 - eDP I-PEX to DuPont 2mm 2x15P Cable...
  - = eDP DuPont (2mm pitch, 2x15P) to IPEX cable
  - = We ordered custom 30 pin I-PEX cables to connect the display to the motherboard’s eDP 2 mm DuPont header.
- siehe PDF anbei
- Internal display cables are sourced from Sino-Media in Hong Kong (??)
- Das Kabel wird auf Mothboardseite in J24, einem 2x15 Header gesteckt, Part ist molex-87758-3016
- Das Gegenstück ist laut Kabelbeschreibung "HSG&TER:DP2.0PITCH HSG BLACK 2*15P AND TER:DP-T G/F"
- Das könnte Housing https://www.molex.com/molex/products/part-detail/crimp_housings/0511103050 mit Terminal https://www.molex.com/molex/products/part-detail/crimp_terminals/0503948054 (Gold) oder https://www.molex.com/molex/products/part-detail/crimp_terminals/0503948055 (Zinn) sein
- An Displayseite ist laut Kabel-PDF ein Connector von I-PEX: https://www.i-pex.com/product/cabline-vs,
  - dort der 20454-230T-02 (siehe Parts), BODY+SHELL. Siehe auch https://www.esskabel.de/wp-content/uploads/2019/07/CABLINE-VS.pdf
- Die I-PEX CABLINE-VS Serie wird von der VESA (Video Electronics Standard Association) als globaler Standard definiert
- Bei TE werden sie LCEDI (L CD C oaxial E mbedded D isplay I nterface) genannt (https://www.te.com/content/dam/te-com/documents/consumer-devices/global/lcedi-presentation-03-28-11.pdf) oder LVDS steht für Low Voltage Differential Signalling (https://www.te.com/deu-de/plp/lcedi-connectors/Y30m2.html)
  - Perfekt ist z.B. https://www.te.com/deu-de/product-2023347-2.html, leider ohne FMD wie die anderen auch
  - Hier immerhin ein paar Infos zu den Materialien: https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Specification+Or+Standard%7F108-78659%7FB%7Fpdf%7FJapanese%7FJPN_SS_108-78659_B.pdf%7F2023347-2
- Bei Molex scheint es sowas gar nciht zu geben, ebensowenig bei Samtec. Bei Octopart und Mouser finde ich es auch nicht. Zu den TE-Teilen keine äquivalenzen gefunden
- In dem schon erwähnten https://www.esskabel.de/wp-content/uploads/2019/07/CABLINE-VS.pdf stehen Materialien, aber keine Gewichte. Man müsste so ein Teil kaufen.
- Eine Kabelkonfiguration damit bestellt! Angekommen. Allerdings ist das CABLINE-VS II = https://www.i-pex.com/product/cabline-vs-II. Und Draht angelötet...
  - Das wäre von der Partnummer 20846-030T-01 oder -02. -02 hat eine etwas dickere NiAu-Beschichtung bei den Kontakten
  - Die Abmaße des CABLINE-VS (https://www.i-pex.com/sites/default/files/downloads/pdf/CATALOG_CABLINE-VS_E.pdf) im Vergleich zum CABELINE-VS-II (https://www.i-pex.com/sites/default/files/downloads/pdf/CATALOG_CABLINE-VS_II_E.pdf) sind ähnlich,
  - Allerdings gibt es beim CABLINE-VS (https://www.i-pex.com/product/cabline-vs) nicht das Blech im Plug Lock Bar Assembly des CABLINE-VS II (https://www.i-pex.com/product/cabline-vs-II), für den auch als "fully-shielded" geworben wird.
  - Die Materialien scheinen trotz der unterschiedlichen Farben in den Bildern die gleichen zu sein
  - Zur Erläuertung der einzelnen Teile, gemäß https://www.i-pex.com/product/cabline-vs-II:
    - Plug, all together
      - Plug, Shell-B und Housing zusammen
        - Plug Housing Assembly ist der reine Steckeranteil male, sozusagen die obere oder untere Hälfte
          - Die Kontakte - hier 30 an der Zahl, je Breite 0.5mm - bestehen aus Phosphor Bronze mit Au 0.03/0.38 um und Ni 1.00/1.27 um bei der -01/-02-Variante
          - Die sind eingebettet in ein Housing aus schwarzem LCP-Plastik
          - Ein Teil davon ist auch Shell-B, die den reinen Kontaktteil umfasst und ähnlich Shell-A ist, bestehend aus Phosphor Bronze mit teilweiser AuNi-Beschichtung, vernachlässigbar
          - Die lassen sich nicht separieren
        - Plug Shell-A wird als andere Hälfte da drauf gesteckt, in der mir gelieferten Kabelkonfiguration leider verlötet und sogar verklebt mit Shell-B
          - besteht wie Shell-B aus Phosphor Bronze mit teilweiser AuNi-Beschichtung, vernachlässigbar
        - Gewicht Plug Housing Assembly + Plug Shell-A: 0.145g. Ich bekomme Shell-A nicht vom Rest, das Assembly eh nicht auseinander
        - Die Volumenanteile sind per Anschein grob 6 Teile
          - Shell-A = Shell-B mit unbekannter Blechdicke etwa je 1 Teil. Dichte 8,8
          - Plastik-Housing 3 Teile, Dichte 1,4 => 3/1,4 = 2,1
          - Kontakte 1 Teil inkl. Beschichtung, Dichte 8,8
          - Das ergibt 2 * 8,8 + 3 * 1,4 + 1 * 8,8 = 30,6 Gewichtsanteile
    - Plug Lock Bar Assembly ist das Teil was beweglich in eine Art Scharnier, das Housing und Shell bilden, steckt und mit dem die Verbindung mit dem Recptable vermutlich zugeklippt wird.
      - besteht aus einem Blech aus Phosphor Bronze, Gewicht: 0.097g. Dichte 8,8
      - und einem Bügel aus SUS = Stainless Steel, Gewicht: 0.065g, Dichte 7.5
    - Receptable der Stecker female, der aber natürlich nicht Teil der Kabelkonfiguration ist und am Display sein muss
  - Phosphorbronze besteht laut https://de.wikipedia.org/wiki/Bronze aus 92,5% Kupfer, 7% Zinn und 0,5% Phosphor mit Dichte 8,8 laut https://www.mmhb.de/produkte/aluminiumbronze-phosphorbronze/datenblatt/
- Das Kabel hat eine Ummantelung laut PDF,
  - zum einen ACETATE TAPE (Isolierband), 12*215 mm ohne Dicke, vermutlich bestehend aus
    - Material Acetate, da könnte man ecoinvent "ethylene vinyl acetate (co)polymer" oder ethylenevinylacetate foil nehmen
    - Kleber Acrylic adhesive, welcher auch immer
      - Dicke könnte 7 mil = milli-inch bzw. 1/1000 inch, entsprechend 7 * 0,0254 mm / 25,4 µm sein - beliebiges Beispiel aus dem Netz
      - Im Netzwerkkabel von ecoinvent könnte es PE sheath sein, eine Folie aus allerdings PE / LDPE = polyethylene mit 1g pro Meter Kabel
      - Siehe auch https://www.kochkabel.ch/normen-und-mehr/34-geschaeftsfelder/zubehoer/54-isolierhuelle-schimr-mantel.html und https://www.kochkabel.ch/normen-und-mehr/34-geschaeftsfelder/zubehoer/58-polymere-isolierwerkstoffe.html
      - Ich nehme ethylene_vinyl_acetate_copolymer mit Dicke 0.0254mm und Größe 12*215mm
  - zum anderen Conductive Cloth,  12*120 mm, allerdings Dicke unklar. Vermutlich ist das eine abschirmende Schicht, um die herum dann das Isolierband kommt
    - Im Netzwerkkabel bei ecoinvent heißt es "Tinned copper braid", ein Drahtgeflecht, und wird zu copper vereinfacht, mit 12g per 1m Kabel
    - Im Druckerkabel ist es "File Aluminium layer", also Aluminium mit 1g pro Meter Kabel
    - Siehe auch https://www.kochkabel.ch/normen-und-mehr/34-geschaeftsfelder/zubehoer/56-schirmtypen.html legt beschichtete Alufolie nah
    - Da steht u.a., dass Folie (wie hier!) nahezu das ganze Kabel abdeckt, anders als Geflecht, welches zudem bei hohren Frequenzen nicht mehr viel bringt
    - "Cloth" klingt allerdings eher nach Geflecht...
    - Ich nehme Aluminium. Dicke ist unklar, ich nehme 0.05mm. Größe: 12*210mm

#### Scharniere

- 2x Scharniere
  - hinges: made by Smooth Group / Smooth Technology (Shenzhen), model number SMS-ZZ-219, http://en.smoothgroup.cc/goods/detail/98.htm
  - Right Hinge - Smooth Technology - SMS-ZZ-219-L
  - Left Hinge - Smooth Technology - SMS-ZZ-219-R
  - Leider keine Angabe zum Gewicht
  - Material: Metal Injection Mo(u)lding (MIM),  https://de.wikipedia.org/wiki/Metallpulverspritzguss
  - Material: bestehend aus Metal Powder ("Stainless Steel", in der Regel 1.4404/316L bzw. 1.4542/17-4 PH) und Binder (z.B. Polypropylene)
    - Vom Binder bleibt nur 1-4% im Ergebnis drin. Man könnte vereinfacht die Dichte des Stahls entsprechend verringern.
    - FMD erfragt, keine Antwort
    - Gewicht erfragt -> about 20g, eher leicht, wenn man unten schaut
  - Nicht unähnliche Scharniere wiegen (vermultich beide zusammen, Verpackung?)
    - 63.5g: https://www.amazon.de/Dell-Latitude-E5530-Hinge-Right/dp/B01IPB8H5A/ref=sr_1_21_sspa?keywords=laptop+scharnier&qid=1654869753&sprefix=laptop+scharnier%2Caps%2C97&sr=8-21-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUE0TjkzVVBOQUJQUkkmZW5jcnlwdGVkSWQ9QTAwODkyNTMzSkIyOEpaOE1LV1BRJmVuY3J5cHRlZEFkSWQ9QTA5NzcxNTBLUE8wVVkxTTdZMDYmd2lkZ2V0TmFtZT1zcF9idGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl
    - 82g: https://www.amazon.de/Zahara-Laptop-Display-Replacement-925297-001/dp/B07TV79LW4/ref=sr_1_32_sspa?keywords=laptop+scharnier&qid=1654869763&sprefix=laptop+scharnier%2Caps%2C97&sr=8-32-spons&psc=1&smid=A8FTK2VMPEKQY&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzVVg2UklBMzM0WDgzJmVuY3J5cHRlZElkPUEwMTY1NjUwMTlUNlBUTVNaWjZOVyZlbmNyeXB0ZWRBZElkPUEwMjY3NzI2MkZTVENOOEo3WkcyVSZ3aWRnZXROYW1lPXNwX2J0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=
    - 87g: https://www.amazon.de/-/en/Gintai-Replacement-A515-51-A515-51G-A515-41G/dp/B07SL563B7/ref=sr_1_89?keywords=laptop+scharnier&qid=1654869793&sprefix=laptop+scharnier%2Caps%2C97&sr=8-89
    - 200g (?): https://www.amazon.de/-/en/dp/B09F5T1ZZS/ref=sr_1_104?keywords=laptop+scharnier&qid=1654869809&sprefix=laptop+scharnier%2Caps%2C97&sr=8-104
    - 53g: https://www.amazon.de/-/en/dp/B09Q5QRS6D/ref=sr_1_127?keywords=laptop+scharnier&qid=1654869834&sprefix=laptop+scharnier%2Caps%2C97&sr=8-127
    - 100g (?): https://www.amazon.de/-/en/dp/B0B19N739S/ref=sr_1_132?keywords=laptop+scharnier&qid=1654869846&sprefix=laptop+scharnier%2Caps%2C97&sr=8-132

####  Screws

- siehe PDF
- In Case modelliert


### Speaker

Warum tauchen alle Speaker-Dinge in https://mntre.com/reform2/handbook/schematics.html#assembly-parts nur einmal auf?

Laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md aber unter "Subassembly: Speaker Assembly" 2x Speaker

#### Speaker selbst

- speaker 2x
  -  PUI Audio AS01808AO-3-R

#### Befestigung

- Speaker Holder (SLA) - MNT Research - MREFXSPK20R01
- speaker-closer 2x: siehe csv "SLA printed"
- Veraltet: speaker-mount 2x: siehe csv  "SLA printed"
- Veraltet: speaker-spacer 2x: siehe csv "SLA printed"
- In https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md nur der Holder erwähnt
- Aber https://mntre.com/reform2/handbook/_images/speaker.png zeigt es

#### Kabel

- cables 2x
  - mnt-MREFCBLS20R01
  - MREFCBLS20R01, JST-PH 4P cable, 24AWG, 10-26.5cm length
  - siehe PNG


### Trackball Controller

#### Konvertierung des BOM

- Found yageo-cc0603npo for part CC0603JRNPO9BN180 from Yageo
- Found yageo-cc0603x7r for part CC0603JPX7R9BB104 from Yageo
- Found tdk-c1608np0 for part C1608X8L1C105K080AC from TDK
- Found osram-lwq38e for part LW Q38E-Q2OO-3K5L from OSRAM
- Couldn't find a match for part number BZT52-B5V6J of manufacturer Nexperia. Suggesting nexperia-bzt52b5v6j
- Couldn't find a match for part number 0ZCJ0075AF2E of manufacturer Bel Fuse. Suggesting bel_fuse-0zcj
- Found murata-blm18 for part BLM18PG221SH1D from Murata
- Found jst-b4bphks for part B4B-PH-K-S(LF)(SN) from JST
- Couldn't find a match for part number FH12-6S-0.5SH(55) of manufacturer Hirose. Suggesting hirose-fh126
- Missing manufacturer or part number for entry with value BADGE. Setting to unknown
- Missing manufacturer or part number for entry with value Mounting_Hole. Setting to unknown
- Missing manufacturer or part number for entry with value LOGO. Setting to unknown
- Found yageo-rc0603 for part RC0603FR-074K7L from Yageo
- Found vishay-crcw0603 for part CRCW060310K0JNEAC from Vishay Dale
- Found yageo-rc0603 for part RC0603FR-07475RL from Yageo
- Couldn't find a match for part number RT0603DRD0722RL of manufacturer Yageo. Suggesting yageo-rt0603
- Couldn't find a match for part number CPG135001D02 of manufacturer Kailh. Suggesting kailh-cpg135001d02
  - The same brown choc switch as in the keyboard, siehe dort
- Found ck-KMR2 for part KMR221GLFS from C&K
- Found apem-dm01 for part DM01 from Apem
-  Found microchip-ATMEGA32U2-AU for part ATMEGA32U2-AU from Microchip
-  Found texas_instruments-TLV75533PDBVR for part TLV75533PDBVR from Texas Instruments
-  Couldn't find a match for part number USBLC6-2SC6 of manufacturer STMicroelectronics. Suggesting stmicroelectronics-usblc62sc6
-  Couldn't find a match for manufacturer 'Abracon'. Suggesting abracon
-  Couldn't find a match for part number ABM8AIG-16.000MHz-4-T of manufacturer Abracon. Suggesting abracon-abm8aig

#### PCB

- Name = reform2-trackball
- Size = 87mm x 59mm
- Layer count = 2
- Board thickness = 1.60mm
- Lot

#### Kabel

- Zum Sensor 6-pin 0.5mm pitch flex cable: Würth Elektronik 687606050002 zwischen Sensor und FB-Platine -> Trackpad
- Verbindung zum Mainboard: USB/SYSCTL Cable -> Trackpad

### Keycaps

- Trackball Button Big (SLA) - MNT Research - MREFXTB120R01
  -  2x reform2-trackball-button-circular: siehe csv, wirklich epoxy_resin? "SLA printed"
- Trackball Button Small (SLA) - MNT Research MREFXTB220R01
  - 3x reform2-trackball-button-rectangular: siehe csv, wirklich epoxy_resin? "SLA printed"

#### Fassung

- lid.csv: Trackball Lid (SLA) MNT Research MREFXTBL20R01
   - 1x reform2-trackball-lid: siehe csv. epoxy_resin ok? "SLA-printed"
- support.csv: Trackball Cup (SLA) MNT Research MREFXTBC20R01 - 3D Printed PLA... P?

#### Ball
 
- MNT Research MREFBALB20R01
- Trackball POM ball (25 mm diameter). We ordered custom black POM (https://de.wikipedia.org/wiki/Polyoxymethylene)
- 25mm diameter POM sphere, black
- 25mm Durchmesser entspricht 8181,23 mm3 = 8,18123 cm3. Dicht ist 1.42 g/cm3 => Gewicht also 11,6g

#### Gewinde

- TB M2 Thread Insert - KVT 300113472 (Distributor) / Tappex HiMOULD 017/117 (Produktanbieter) (laut https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
- Produktanbieter: https://www.tappex.co.uk/products/brass-threaded-inserts/himould?type=type-b&thread=M2&length=3.925&extras=#selector
  - Distributor (inkl. 3D-Modell): https://www.bossard.com/eshop/de-de/gewindeeinsaetze-fuer-kunststoffmaterialien/gewindeeinsaetze-zum-einlegen/tappex-himould/p/37868/
  - "Mount 2x TB M2 Thread"
  - Werkstoff: Messing

#### Schrauben
  
- 2x M2x5-965H + 4x M2x4-7985H + 2x M2x4-7985H, schon in der großen Liste drin
- Befestigung an Motherboard: 4x M2x4-7985H, schon in der großen Liste drin


### Trackball Sensor

#### Konvertierung des BOM

-  Found tdk-c1608np0 for part C1608X8L1C105K080AC from TDK
-  Found yageo-cc0603x7r for part CC0603JPX7R9BB104 from Yageo
-  Found taiyo_yuden-mk107 for part LMK107BBJ106KALT from Taiyo Yuden
-  Couldn't find a match for manufacturer 'NOSTUFF'. Suggesting nostuff
-  Couldn't find a match for part number NOSTUFF of manufacturer NOSTUFF. Suggesting nostuff-nostuff
-  Couldn't find a match for part number FH12-6S-0.5SH(55) of manufacturer Hirose. Suggesting hirose-fh126
-  Couldn't find a match for part number PAT9125EL of manufacturer PixArt Imaging. Suggesting pixart_imaging-pat9125el
   - Aha, wieder PixArt. Octopart kann kaum was dazu sagen
   - https://www.pixart.com/products-detail/72/PAT9125EL-TKIT___TKMT und https://datasheetspdf.com/pdf-file/1309986/PixArt/PAT9125EL/1
   - Zusammensetzung keine typischer IC, auch LGA passt nicht so recht, es ist aber ein LGA 8-pin mit Package size 3.50*3.20*1.0, Gewicht unbekannt
   - Mal anfragen, dann auch für das Nager-IT-Teil: https://websupport.pixart.com mit User s.jekutsch@fairloetet.de / Sebamed0 (ohne !)
     - Habe ich gemacht, Antwort: "Regarding your Service Requests for the full material list for PAW3552DB-VJXT & PAT9125EL-TKIT / TKMT, kindly note that we only provide these documents to customers in close engagement with PixArt's projects. Hence, I sincerely apologize for not being able to provide them to the association-based parties such as FairLötet at this point."
   - Einen noch verfügbaren LGA-8 habe ich nicht gefunden. Aufgrund der Abmaße und der sowieso sonderbaren Art (Spiegel..) habe ich einen LLGA 16 3x3x1.0 substituiert
         - der immerhin einem Motion Sensor entstammt: H3LIS100DL von ST https://www.st.com/en/mems-and-sensors/h3lis100dl.html#quality-reliability

#### PCB

- reform2-trackball-sensor-d3-gerbers\reform2-trackball-sensor-job.gbrjob:
  - Name = reform2-trackball-sensor
  - Size = 24mm x 18mm
  - Layer count = 2
  - Board thickness = 1.00mm
  - Lot

#### Kabel

- FPC cable zur Verbindung des Sensors -> In Platine vermerkt
- Das gleiche wie beim Trackpad, also wuerth-687606050002! 6-pin 0.5mm pitch flex cable


### Trackpad

#### Konvertierung des BOM

- Found yageo-cc0603npo for part CC0603JRNPO9BN180 from Yageo
- Found yageo-cc0603x7r for part CC0603JPX7R9BB104 from Yageo
- Found taiyo_yuden-mk107 for part UMK107BJ105KA-T from Taiyo Yuden
- Found nexperia-bzt52b5v6j for part BZT52-B5V6J from Nexperia
- Found bel_fuse-0zcj for part 0ZCJ0075AF2E from Bel Fuse
- Found murata-blm18 for part BLM18PG221SH1D from Murata
- Found jst-b4bphks for part B4B-PH-K-S(LF)(SN) from JST
- Found hirose-fh126 for part FH12-6S-0.5SH(55) from Hirose
- Found yageo-rc0603 for part RC0603FR-074K7L from Yageo
- Found vishay-crcw0603 for part CRCW060310K0JNEAC from Vishay Dale
- Found yageo-rt0603 for part RT0603DRD0722RL from Yageo
- Couldn't find a match for part number PTLP2 of manufacturer Diptronics. Suggesting diptronics-ptlp2
- Found apem-dm01 for part DM01 from Apem
- Found microchip-ATMEGA32U2-AU for part ATMEGA32U2-AU from Microchip
- Found texas_instruments-TLV75533PDBVR for part TLV75533PDBVR from Texas Instruments
- Found stmicroelectronics-usblc62sc6 for part USBLC6-2SC6 from STMicroelectronics
- Found abracon-abm8aig for part ABM8AIG-16.000MHz-4-T from Abracon

#### Das Touchpad itself

- 1x capacitive multi-touch sensor module TPS65-201A-S oder TPS65-201A-B by Azoteq.
- https://www.azoteq.com/product/i2c-trackpad-modules/
- -S oder -B ist Unterschied bei Mouser nicht erkennbar, im Datasheet ist ein S aber nicht zu finden
- https://www.azoteq.com/images/stories/pdf/proxsense_i2c_trackpad_datasheet.pdf anbei
- FMD angefragt
  - Antwort bekommen für den IC alleine, aber zudem für einen UFQFPN48, also ohne Beinchen (Ultra thin Fine pitch Quad Flat Package No-leads)
  - Der wiegt auch nur halb so viel (97,350mg) wie meine Modellierungsalternative ic_lqfp48_7x7x1.4 (184.357140mg)
  - Dennoch bei meiner geblieben, weil es auf der Platine halt so aussieht
  - Dieses Modell habe ich auch nicht aufgenommen (zu viel Tipparbeit), ist aber hier anbei
  - Laut Beschreibung sei es "with die STM8TL53", das wäre dies: https://www.st.com/en/microcontrollers-microprocessors/stm8tl53c4.html#quality-reliability
    - Das Gewicht passt auch einigermaßen (weil es auch UFQFPN 48 7x7x0.55 mm ist), aber die FMDs sind nicht gleich. Alles sehr verwirrend.
- Sieht aus als wäre es einfach eine Leiterplatte, die irgendwie kapazitiv/sensitiv wirkt
- Könnte man modellieren mit der Infos im Datenblatt (siehe PDF):
  - 65mm x 49mm rectangular trackpad with rounded corners
  - 4 Schichten: Mylar (bei -B vorhanden), Adhesive, PCB und Components
    - PCB: 2-layer, FR4 PCB, 35um Copper, Electroless Nickel Immersion Gold finish, 1mm Dicke
    - Adhesive Specification: 3M 468 200MP, Thickness = 0.13mm
    - Mylar Thickness 2.23mm - 1.0mm PCB - 0.13mm Adhesive - 0.8mm 0603 MLCC height = 0.3mm... da steht 0.2mm, vielleicht weil der MLCC dicker ist.
    - In einem Bild hier von MNT sieht man, die die Platine bestückt sein könnte:
      - IQS550 Touch IC,
        - ein QFN(7x7)-48 gemäß https://www.azoteq.com/images/stories/pdf/iqs5xx-b000_trackpad_datasheet.pdf
        - ein (L)QFP-48 gemäß Bild
      - C1, C2, C3, C4 MLCC von 0402 vielleicht
      - C2 MLCC von 0603 sicher
      - R1, R2, R3 0603 Widerstände
      - FPC-Connector wie beim Trackball, also ein hirose-fh126
- In https://www.mouser.de/ProductDetail/Azoteq/TPS65-201A-S?qs=pfd5qewlna5Lh8O0E8DcUQ%3D%3D steht, dass es 13g wiegt
   - In der Summe haben wir nun aber nur 9.5224g, auf ein Hinzufügen der entspr. Menge des unknown-unknown parts habe ich aber verzichtet. Könnte ja z.B. die Verpackung sein.

#### Fassung

- reform2-trackball-support: siehe stl. "SLA printed support" "TP Carrier" "3D Printed PLA" ...P?
- Trackpad Holder (SLA or FDM-PETG) - MNT Research - MREFXTPH20R01 (???)

#### PCB

- reform2-trackpad-ctrl-d3-gerbers\reform2-trackpad-job.gbrjob:
  - Name = reform2-trackpad
  - Size = 45mm x 21mm
  - Layer count = 2
  - Board thickness = 1.60mm
- Lot

#### Kabel

- FPC cable zum Sensor
  - TP Sensor Cable 6-pin 0.5mm pitch flex cable, 50mm Länge = wuerth-687606050002
  - Molex hat was unter https://www.molex.com/molex/products/family/premoflex, 1:1 passen diese:
    - https://www.molex.com/molex/products/part-detail/cable/0151660053
    - https://www.molex.com/molex/products/part-detail/cable/0152660053
    - https://www.molex.com/molex/products/part-detail/cable/0982660053

- Cable zum Motherboard
  - "Connect MREFATB/MREFATP to motherboard header UI2 with one USB/SYSCTL Cable" (https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md)
  - In den Devices des Laptops drin

####  Glasabdeckung

- make the trackpad surface from frosted and back-printed glass instead of a mylar sticker
- The new design integrates the trackpad surface seamlessly into the aluminum body.
- The glass surface connects to the SLA printed support via double layered adhesive and to the aluminum through a gasket foil.
- Mehrere Lagen (siehe Bild)?
- Unter https://source.mnt.re/reform/reform/-/blob/master/ASSEMBLY.md, Kapitel "Subassembly: TP Glass Assembly" und "Subassembly: Trackpad (MREFATP)" steht noch einiges:
  - Source frosted glass surface with black backprint (All edges and corners deburred, max 0.3mm radius) according to https://source.mnt.re/reform/reform/-/blob/master/reform2-trackpad-pcb/trackpad-glass-glue-gasket-20200807.step
  - Attach adhesive tape layer (0.14mm 3M 9495MP) on bottom side according to https://source.mnt.re/reform/reform/-/blob/master/reform2-trackpad-pcb/revised-gasket.dwg
  - Attach gasket layer (0.14mm 3M 9495MP + 0.125mm black mylar) on top side according to https://source.mnt.re/reform/reform/-/blob/master/reform2-trackpad-pcb/revised-gasket.dwg
- Und noch irritierender: Da gibt es ein TP Sensor PCBA und ein TP Controller PCBA. Der Sensor PCBA scheint wohl das Azoteq TPS65-201A-B Teil zu sein

#### Schrauben

- Befestigung an Motherboard: 2x M2x4-7985H, schon in der großen Liste drin








  * SOM: 
    * NXP/Freescale i.MX8MQ mit 4x ARM Cortex-A53 cores (1.5 GHz) (https://www.nxp.com/docs/en/data-sheet/IMX8MDQLQCEC.pdf), 1x Cortex-M4F core.
    * CPU und RAM sind auf einem austauschbaren Modul in SO-DIMM-Größe.
    * The CPU, GPU, and hardware implementations of standard interfaces like USB 3.0, PCIe, and MIPI DSI are all located in the NXP i.MX8M, which is called a System-on-Chip (SoC).
    * Both SoC and memory chips sit on a System-on-Module (SoM) that we source from Boundary Devices, a US company that specializes in i.MX reference designs. We selected their "Nitrogen8M SOM" module for Reform because it is the only available module for which you can download the complete schematics and understand what every component does. -> https://boundarydevices.com/product/nitrogen8m-som/ . To download the schematic, first create a Boundary Devices user account (https://boundarydevices.com/register/)
    * i.MX8M modules are sourced from Boundary Devices (California).
    * The SoM features an Atheros Ethernet PHY chip (AR8035).
    * Grafikprozessor: Vivante GC7000Lite GPU
    * 2x USB 2.0 intern (für Eingabegeräte) -> i.MX8M has 2x USB 3.0 controllers
    * Es gibt alternative SoMs:
      * Boundary Devices Nit8MQ SOM 4r16e ist aktuell vorgesehen. Ich habe 2r8e, Unterschied ist aber sicher nur 4 vs. 2GB RAM und 16 vs. 8GB eMMC = Grafik)
      * RBZ/MNT LS1028A ist eine Fremdentwicklung auf Open Hardware Basis, meine Modellierungsprobleme bestehen aber auch hier, man könnte aber mal vergleichen Siehe https://source.mnt.re/reform/mnt-reform-layerscape-ls1028a-som
      * Reform Kintex-7 SoM ist eine MNT-Entwicklung für eher experimentellen Einsatz von Opeb Risc Cores. Noch nicht ganz fertig, aber wenig Bewegung. Siehe https://source.mnt.re/reform/reform-kintex-som
      * Raspberry Pi CM4 SoM, Entwicklung von MNT, noch in Entwicklung, siehe https://source.mnt.re/reform/mnt-reform-raspberry-pi-cm4-som und https://mntre.com/media/reform_md/2022-03-03-reform-march-update.html. Nicht wirklich realistisch, außerdem ist das wohl erstmal nur ein Adapter, oder? 

  * Batterien / Batteriepack
    * 8 LiFePO4-Akkus im Standardformat 18650, zusammen 14.4 Ah/3.2 V
    * battery cells are sourced from Heter/Enerpower via NKON (Netherlands)
    * Compatible cells: https://www.batteryspace.com/lifepo4-18650-rechargeable-cell-3-2v-1500-mah-8-4a-rate-4-32wh-ul-listed-un38-3-passed-ndgr.aspx, https://enerprof.de/akkus/akkus-lifepo4/akkuzellen-lifepo4/akkuzellen-lifepo4-18650/32/enerpower-18650-lifepo4-3-2v-1800-mah?c=26, https://www.18650batterystore.com/Lithium-Werks-p/lithiumwerks-apr18650m1b.htm
    * Offensichtlich zwei Packs a 4 Batterien vorhanden
    * Sind das diese? https://eu.nkon.nl/jgne-18650-1800mah-5-4a-lifepo4.html, siehe auch hier: https://enerpower.de/en/lifepo4-batteries/.
    * JGNE = Shandong Goldencell Electronics Technology Co.,Ltd, http://www.goldencellbattery.com/
    * Vermutlich dieser http://www.goldencellbattery.com/product/28.html, also JGCFR18650-1800mAh-3.2V
    * Abmaße: Höhe 65.2±0.3mm, Durchmesser: 18.25±0.1, genaueres dort
    * Gewicht: 41.5g (about)
  * RAM
    * 4 GB LPDDR4
  * SSD
    * 256GB oder 1TB NVMe-SSD (Samsung EVO)
    * NVMe SSD Transcend MTE220S (1TB) dabei, oder Samsung EVO (NVMe-SSD)
    * Bildschirm
    * Display Full HD (1920x1080 pixels) with 24-bit color, 12.5" IPS eDP (embedded DisplayPort) from Innolux, model number N125HCE-GN1 (http://www.panelook.com/N125HCE-GN1_Innolux_12.5_LCM_overview_28140.html)
  * Systemanzeige / Statusanzeige
    * 128 x 32 pixel OLED-Systemanzeige
    * Hersteller und part-number ist ENH-OB00910003 von Enrich Electronics, oder?
    * Produktnummer "ENH0.91 Inch OLED" oder ENH-OB00910003, 0.91 inch, 128x32 dots von Enrich Electronics
    * Vermutlich http://www.rxxdisplay.com/chanpinzhanshi/120.html = https://german.alibaba.com/product-detail/0-91-inch-oled-display-128x32-oled-i2c-interface-with-pcb-4pin-1600132441842.html = http://www.oledlcddisplay.com/e_productshow/?75-091-inch-micro-OLED-display-128x32-dots-SPI-interface-display-75.html
    * 30x11.5mm x 1.227mm, Einzelbruttogewicht 0.020kg (also mit Einzelverpackung, oder?)
  * Tastatur:
    * 80 oder 82 mechanical, low-profile Kailh Choc Brown switches
    * After trying out the MBK concave keycaps, we talked to FKcaps about a collaboration. They were able to manufacture a new version of their caps in record speed that fit our laser engraving approach: a translucent ABS body painted with a thin layer of black PU.
    * uses a split spacebar and only two different keycap sizes, 1U (72?) and 1.5U (10 Stück), Blank keycaps (no labels).
    * While keyboard switches are soldered by PCBWay, keycaps are installed by us in Berlin.
    * http://www.kailh.com/en/Products/Ks/CS/320.html, https://www.kailhswitch.com/mechanical-keyboard-switches/low-profile-key-switches/tactile-mechanical-keyboard-switches.html
    * Abmaße Taster grob 15 x 15 x 3+5+3 mm
    * Tastatur-Hintergrundbeleuchtung wie? LEDs sind in BOM
  * Trackball:
    * Reform optischer USB Trackball mit 5 mechanischen Tasten (Kailh Choc Brown) <- weiterhin?
    * Trackball controller module
    * Trackball sensor module with FPC cable
    * Trackball keycaps, cup, and lid (SLA printed)
    * Trackball POM ball (25 mm diameter). We ordered custom black POM
    * trackball design with 5 buttons and a tiny Pixart PAT9125EL surface tracking sensor
    * For the ball’s optical motion tracking, we are using a Pixart PAT9125EL laser sensor interfacing with a ATmega32U2 MCU with open source firmware.
    * We developed a custom SLA-printed mechanical housing for the trackball. We print the "cup" holding the trackball POM sphere ourselves, as well. The mechanical parts are then screwed on the controller and sensor PCBs sponsored by OSH Park for the beta round
    * Es scheint eine Platine für Trackball Sensor und für Trackball selbst zu geben
  * Touchpad
    * Reform capacitive USB trackpad
    * Trackpad controller module
    * Trackpad sensor module with FPC cable
    * Touchpad: The trackpad is based on the capacitive multi-touch sensor module TPS65-201A-S by Azoteq.
    * Like the trackball, we drive it with a custom ATmega32U2 PCB
    * make the trackpad surface from frosted and back-printed glass instead of a mylar sticker
    * The new design integrates the trackpad surface seamlessly into the aluminum body.
    * The glass surface connects to the SLA printed support via double layered adhesive and to the aluminum through a gasket foil.
  * Platine Motherboard
    * made and assembled by PCBWay (https://www.crowdsupply.com/providers/electronics-fabrication/pcbway)
    * spans around 27cm
  * Platine Keyboard
    * made and assembled by PCBWay (https://www.crowdsupply.com/providers/electronics-fabrication/pcbway)
  * Platinen Trackpad
    * made and assembled by PCBWay (https://www.crowdsupply.com/providers/electronics-fabrication/pcbway)
  * Platine Trackball
    * made and assembled by PCBWay (https://www.crowdsupply.com/providers/electronics-fabrication/pcbway)
  * Platine Battery clips
  * Platine Systemdisplay
  * Kühlkörper
    * We developed a custom aluminum heatsink for the i.MX8M that allows Reform to run fully passive-cooled, eliminating the need for a fan.
  * Schrauben:
    * For easy (dis)assembly, Reform uses only 15x(?) M2 screws with Phillips-head everywhere (with one exception: 6x(?) M4 on the hinges). Screws, packing material, printed manuals, etc. are sourced locally by us.
    * Auf den Bildern sind aber mehr zu sehen, oder?
  * Closing magnets: the case is held shut by 8 little neodymium bar magnets
  * Scharniere
    * 2x Scharniere
    * hinges: made by Smooth Group, model number SMS-ZZ-219
    * Hinges are supplied by Smooth Technology (Shenzhen).
  * Befestigung Lautsprecher
    * the speakers are now mounted in the back of the case and fastened with SLA printed closures.
  * Wi-fi
    * miniPCIe Atheros ath9k compatible Wi-Fi card (optional?)
    * mPCIe WiFi card (Compex WLE200NX)
    * mit Molex MIMO antenna ?
  * Kabel Tastatur:
    * The keyboard not only works as a USB HID device, but it also has a direct UART cable connection to the system controller on the motherboard = 1x internal UART cable
  * Kabel Bildschirm
    * We ordered custom 30 pin I-PEX cables to connect the display to the motherboard’s eDP 2 mm DuPont header. = 1x display cable.
    * Internal display cables are sourced from Sino-Media in Hong Kong.
  * Kabel Lautsprecher
    * Speaker soldered to cable
  * Kabel interne USB
    * 2x internal USB cable, eins für die Tastatur, das andere für Trackball oder -pad
  * Kabel Batterie
    * 2x battery cable
  * Kabel Systemdisplay
    * 1x OLED FPC cable
  * Stromversorgung
    * 24 V 2 A international power supply (110/230 V)
    * Netzteil extern?
    * Power Supply 24V 2.5A 60W ist Mean Well GST60A24
    * Power Cable IEC 60320 C14
  * SD Card mit dabei? Laut https://mntre.com/media/reform_md/mnt-reform2-diy-manual-r1.pdf schon
    * 32 GB
  * Knopfzelle auf dem Mainboard
    * Lithium Coin Cell CR 1216 oder CR 1220
    * Nicht wieder aufladbar
    * Durchmesser 12mm, Höhe 1,6/2,0mm, 0,7g/0,9g
  * Heatsink:
    * custom aluminum heatsink
    * CNC milled, anodized Al6061

