# Data on product composition

Since the target audience for Fairtronics is the device manufacturer, the user should have access to all the data needed. The question is, in which format this data usually comes. 

## Real world (professional) product data

Electronics manufacturing services (CAM) like https://www.seeedstudio.com/fusion_pcb.html suggest that mostly two types of data are used: PCB layout data and a list of components (bill of material) to be mounted on the PCB. (Seeed is just an example, see http://www.opencircuits.com/PCB_Manufacturers for some more.)

Electronics design software support this: 
* https://librepcb.org/ has a proprietary all-in-one file format .lpp, but the export feature in LibrePCB seperates "Fabrication data" (for PCB) from "Bill of material". The fabrication data doesn't seem to contain specific material data. LibrePCB has a simple, non-customizable BOM export functionality
* Autodesk Eagle, having a [restricted free version for hobbyists](https://www.autodesk.com/products/eagle/free-download), does the same with yet to different proprietary file formats: .brd for PCB layout and .sch for Schematics (inkl. BoM). BOM exporting is done via rather simple internal export or via ULP scripts, an DOM-iteration-based programming language
* https://kicad-pcb.org/ (free) does quite the same in a yet different way, again. There's a BOM export functionality with surprising columns (is there a problem with the libraries?), but it's customizable via Python scripts.
* See http://www.opencircuits.com/Software_tool for a list of tools.

### Printed circuit board (PCB)

PCBs are mostly specified as [Gerber](https://en.wikipedia.org/wiki/Gerber_format) = RS-274-X files, a ZIP- or RAR-packed set of PCB 2D-design layers (copper, paste, solder mask, etc., both top and bottom) plus additional information (e.g. drills) with various file extensions.

See https://www.microchip.com/doclisting/TechDoc.aspx?type=Gerber for some example Gerber files, and take online viewer http://www.gerber-viewer.com/ to display them layer by layer.

The official documentation is here: https://www.ucamco.com/en/gerber (with an official viewer at https://gerber.ucamco.com/). See also http://gerbv.geda-project.org/ or https://gerbview.com/ as valuable resources.

For Gerber Tools see https://www.mikrocontroller.net/articles/Gerber-Tools 

Other formats:
* A somewhat outdated format is [Excellion](https://en.wikipedia.org/w/index.php?title=Excellon_format&redirect=no) = RS-274-C (?).
* The [Eagle PCB designer](https://www.autodesk.com/products/eagle/overview) has its [own .brd format](https://www.file-extension.info/de/format/brd), which may be converted to Gerber.
* KiPAD has - just like - Eagle it's own format, including the ability to convert it to e.g. Gerber
* PLT plotter files are also used, DXF, CSM, IGES (Initial Graphics Exchange Specification), STEP (Standard for the Exchange of Product data), [PTC file formats](https://en.wikipedia.org/wiki/PTC_Creo_Elements/Pro)  

### Bill of material (BoM)

Bill of materials list all parts to be mounted on the PCB. It typically lacks any other parts as cables (manually soldered on the board), pluggable SIMM or devices, housing, packaging ,etc. 
The components are not described by electrical or mechanical properties, but by specific products to be purchased.
Note that the BoM is not part of the Gerber files for PCBs.

There seems to be no general, mostly used file format for BoMs instead of being simply Google Spreadsheets, Excel fieles or based on CSVs or equivalent. 
These seem to be the most relevant columns:
* _Package_ or Type / Footprint (?) / Package Type / Case type (for SMDs) / Typ : Examples of package types include small outline packages, single in-line packages and ceramic column grid array packages.
* _Value_: E.g. 10pF as capacitance or 47k as resistance.
* _Quantity_ or Qty: This is the total number of the component on one board. If there are three of the same component then you would enter 3 here and list the three separate designators on the BoM. If Qty is omitted, each row represents 1 component, of course, and in case there are two similar components, the BoM contains two similar rows.

Additionally, the following columns are used as well:
* _(Manufacturer) Part number_ or MPN / Partno: Unique identifier of the component, typically a series of numbers and letters. There's no worldwide identifier of electronics products, but each manufacturer (KEMET, Murata, NXP, Vishay, Würth, etc, etc.) and even [distributor](http://www.opencircuits.com/Supplier) has its own numbering scheme. It's like the EAN (European Article Number) with the bar code on each consumer product. There are dozen of pert number database search engines (which can map MRNs to value+package) which also compute substitutes. Here are a few with APIs:
  * https://octopart.com/api/home
  * https://ciiva.com/what-is-ciiva
  * https://www.alldatasheet.com/bottom_bar/alldatasheet_api.html
  * https://de.farnell.com/epass-api
  * https://www.mouser.de/api-search/
  * https://www.digikey.com/en/resources/api-solutions
  * https://cdn.ihs.com/www/pdf/1018/IHS-Markit-Parts-XML-Web-Services-2018.pdf
  * https://sourcingbot.com (without API?)
* _Designator_ or Part(s) / Reference: Identifier of the component on the board, typically R1, R2, etc. for resistors or C1, etc. fpr capacitors. It identifies the location of the component on the board and is unique to each part, even if there are multiple instances of the same component on the board. May be used as a weak indicator, which type of component it is. It's plural (i.e. Parts) having a comma seperated list of parts, if Quantity is given. 
* _Description_ or Designation: Free text, supports the information on the other columns
* Component provider or Manufacturer or Supplier: Full name
* Part name / Name / Device: ?? Maybe an CAD software internal structure, a combination of component type (for schematics) and component package (for PCB layout), or the identifier from the component library. 
* Link to purchase

Note that for example, a BoM is extracted in Eagle from the Schematics, which often does not offer any decision what material kind of component is used in a specific device.

Here's python code how to parse a BoM: https://octopart.com/api/docs/v3/bom-quickstart

This specifies flat, one-tier BoMs, see [Structured BOM](https://en.wikipedia.org/wiki/Bill_of_material_based_on_characteristics) and [Multi-level BOM](https://en.wikipedia.org/wiki/Bill_of_materials#Multi-level_BOM) as well.

It's not clear whether for example the length of a cable or wires to be mounted on a PCB is part of the BoM.

BOM-related tools:
* Import and Management:
  * There's a [BOM Tool](https://octopart.com/bom-tool) with seems to recognize quite some typical BoM representations. 
  * [BOM Manager](https://www.siliconexpert.com/products/bom-manager) from SiliconExpert
  * See also the commercial [OpenBOM](https://www.openbom.com/) service with quite some features.
  * https://www.mouser.de/bomtool/
* BOM import in general LCA software
  * The extension [GaBi DfX](http://www.gabi-software.com/international/software/gabi-dfx/functionalities/bom-import/) for the GaBi LCA includes BOM import
  * The Japanese [MiLCA](https://www.milca-milca.net/english/index.php) commercial LCA solution (with limited free version which installation of didn't work) has an BOM import function, see https://milca-milca.net/download-files/MiLCAguidebook_En.pdf, chapter 4.3.
* Uploading BoMs to component distributors:
  * https://www.digikey.de/en/resources/bom-manager
  * https://www.ttiinc.com/content/ttiinc/en/apps/bom-upload.html
  * .. did not look up the others
* Additionally, there are quite a few PCB fabrication online services with the ability to import BOMs, add PCB data, and finally order populated PCBs:
  * https://www.eurocircuits.com/how-to-upload-or-import-bom-cpl-files-user-guide/ 
  * https://www.seeedstudio.com/fusion_pcb.html

As an example, a capacitor with 100nF of package type 0603, which usually is an [MLCC](https://en.wikipedia.org/wiki/Ceramic_capacitor#Multi-layer_ceramic_capacitors_(MLCC)), has been specified in various open source BoMs like this:

| _Source_ | Part | Value | Package | Description | MPN | Manufacturer | 
|----------|------|-------|---------|-------------|-----|--------------|
| _Eagle Export_ | C4 | 100nF | RESC0603_N | Generic chip capacitor | | |
| _Eagle Export_ | C63 | 0.1uF | C0603 | NON-POLARIZED CAP | C0805F104K5RACTU | |
| _Eagle Export_ | C5 | 100n | C0603-ROUND | CAPACITOR, European symbol | | |
| _Eagle Export_ | C4 | 100n | C0603 | Ceramic Chip Capacitor | | | C0603 |
| _Eagle Export_ | C6 | 100n | C0603 | Capacitors | | | CAPACITOR-0603 |
| _Eagle Export_ | C7 | 100nF | C0603 | Ceramic Capacitors | | | C-0603 |
| _Original BOM_ | C49,C114,C126,C167 | | | capacitor, 0.1uF, 0603, ceramic, X5R, 10V | | |
| _KiCAD Export_ | C10 | 100n | LibreSolar:C_0603_1608 | | CC0603KRX7R9BB104 | Yageo | |
| _Original BOM_ | C209, C403, C404, C405, C406, C407, C510, C717, C720, C721, C722, C1209, C1401 | 100nF | 603 | | |
| _Original BOM_ | C4 | | 0603 100nF | | MC0603B104K500CT | Multicomp | |
| _Eagle Export_ | C4 | 100n | C0603-B | | | |
| _LibrePCB Export_ | C5 | 0,1µF | C-0603 | | | |
| _KiCAD Export_ | C1 | | 0603 | 0.1uF | | 
| _Original BOM_ | | 0.1uF | | | CC0603JPX7R9BB104 | Yageo

Note that this is a simple example! For specific ICs, for example, there's no package or value but an MPN alone.
It should be done with an IC or an cable (mostly not part of PCB-CAD project data).

### Circuit Schematics (SCH)

A circuit schematic is a graphical representation of an electrical circuit. It neither contains any physical layout data (-> PCB), nor a list of specific electric components (-> BoM), just symbolic representation of componets and their connections.
Anyway, a BoM may be compiled from it.

Often these are transported via [SCH files](https://filext.com/file-extension/SCH). 


### Supply chain data

Sometimes, a BoM is general in a sense that part specifications are only given by its required electrical and mechanical properties, not by referencing specific manufacturers or suppliers. 
For Open Hardware projects this is the right level, but for commercial hardware of course a list of suppliers is available as well. 
In many professional organisations, this is hidden in CRM systems like SAP.

Some companies even disclose the suppliers, but mostly not on a device/component level. A notable exception is Fairphone for which [at least the suppliers of clusters of components are disclosed](https://www.fairphone.com/wp-content/uploads/2019/09/FP3_List_Only_Suppliers.pdf), and the [Nager-IT mouse](https://www.nager-it.de/static/pdf/lieferkette.pdf).


### Analysis data

Having a device at hand, dismantling it may reveal BoMs. See also services like http://www.laptopschematic.com/ or https://laptopdesktopschematic.com/

    

## Example Open Hardware Data

Here's a list of Open Hardware projects with its ways to publish hardware data. Most of them have more than one variant running, and most consist of more than one device, in this caes I arbitrary picked one.

| Project   | Type | Device | Data provided | Data rescued | Remark |
|-----------|------|--------|---------------|--------------|--------|
| [Aperatus](https://www.apertus.org/) AXIOM | Digital Camera | [Power Board v0.30](https://wiki.apertus.org/index.php/Beta_Power_Board) of [Axiom Beta](https://wiki.apertus.org/index.php/AXIOM_Beta) | [Eagle with Gerber&BoM exported](https://wiki.apertus.org/index.php/AXIOM_Beta_Power_Board_v0.30) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/data/Axiom_Beta_Power_Board_V0.30/20191222_axiom_beta_power_board_v0_30.csv), [Gerber](https://gitlab.com/fairloetet/orga/tree/master/data/Axiom_Beta_Power_Board_V0.30/axiom_beta_power_board_v0.30.zip) |  |
| [Elphel](https://www3.elphel.com/products) | Camera | [System board gen.10393](https://wiki.elphel.com/wiki/10393) rev.C | [Gerber](https://wiki.elphel.com/images/5/56/10393c_gerber.tar.gz), [PDF-BoM](https://wiki.elphel.com/images/b/b3/10393c.pdf), also CSM-placement files available | [Gerber](https://gitlab.com/fairloetet/orga/tree/master/data/Elphel_10393c/10393c_gerber.zip) |  |
| [Arduino](https://www.arduino.cc/) | Microcontroller | [Uno Rev-3](https://store.arduino.cc/arduino-uno-rev3) | [Eagle](https://content.arduino.cc/assets/UNO-TH_Rev3e-reference.zip) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/data/Arduino_Uno/UNO-TH_Rev3e.csv), [Gerber](https://gitlab.com/fairloetet/orga/tree/master/data/Arduino_Uno/UNO-TH_Rev3e.zip) |  |
| [NodeMCU](https://www.nodemcu.com/) | Microcontroller | [Dev Kit v1.0](https://github.com/nodemcu/nodemcu-devkit-v1.0) | Gerber, PDF-BoM | #TODO |  |
| [OpenMoko](http://wiki.openmoko.org/wiki/Main_Page) | Mobile Phone | [GTA04/Phoenux](http://wiki.openmoko.org/wiki/GTA04) | PCB???, [Excel-BoM](http://projects.goldelico.com/p/gta04-main/downloads/get/GTA04A4-BOM.xls), [PDF-Schematics](http://projects.goldelico.com/p/gta04-main/downloads/get/GTA04.sch.pdf) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/data/Phoenux/GTA04A4-BOM.csv) |  |
| [AX84](https://ax84.com) | Tube Guitar Amplifier | [P1](https://ax84.com/archive/ax84.com/p1.html) | [PDF](https://ax84.com/archive/ax84.com/static/p1/AX84_P1_101004.pdf) | - | |
| [Ethernut](http://www.ethernut.de/) | Microcontroller | [5.0 Rev-F board](http://www.ethernut.de/en/hardware/enut5/enut50f.html) | [Eagle](http://www.ethernut.de/arc/enut50f.zip) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/data/enut50f/enut50f.csv), [Gerber](https://gitlab.com/fairloetet/orga/tree/master/data/enut50f/enut50f_2019-12-22.zip) |  |
| [Libre Solar](https://libre.solar/) | Renewable Energy | [MPPT charger with USB](https://libre.solar/devices/mppt-1210-hus/) | [KiCad](https://github.com/LibreSolar/mppt-1210-hus/tree/master/kicad) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/data/mppt-1210-hus/mppt-1210-hus.csv), [Gerber](https://gitlab.com/fairloetet/orga/tree/master/data/mppt-1210-hus/mppt-1210-hus.zip) |  |
| [OpenPandora](https://openpandora.org/) | Pocket Computer | [Pandora](https://pandorawiki.org/Pandora) Mainboard | [ZIPed Gerber & Excel-BoM](www.openpandora.org/downloads/files/Pandora_PCB.zip) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/data/OpenPandora/BOM_mainboard_rev6d_3730.csv), [Gerber](https://gitlab.com/fairloetet/orga/tree/master/data/OpenPandora/Gerber.zip) | successor project [Pyra Wiki](https://pyra-handheld.com/wiki/index.php?title=Main_Page)
| [Olimex DIY Laptop](https://www.olimex.com/Products/DIY-Laptop/) | Laptop | [TERES-A64 Mainboard Rev-C](https://github.com/OLIMEX/DIY-LAPTOP/tree/master/HARDWARE) | [KiCad](https://github.com/OLIMEX/DIY-LAPTOP/tree/master/HARDWARE/A64-TERES/TERES-PCB1-A64-MAIN/Rev.C) | #TODO | |
| [SenseBox](https://sensebox.de/) | Umweltmessstation | [MCU board](https://github.com/sensebox/senseBoxMCU-core) v13 | [Eagle BRD](https://github.com/sensebox/senseBoxMCU-core/tree/master/hardware/senseBox-MCU_v13.brd), [Eagle SCH](https://github.com/sensebox/senseBoxMCU-core/tree/master/hardware/senseBox-MCU_v13.sch), [Gerber](https://github.com/sensebox/senseBoxMCU-core/blob/master/hardware/senseBox-MCU_v13.zip) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/data/SensoBoxMCU-core/senseBox-MCU_v13.csv), [Gerber](https://gitlab.com/fairloetet/orga/tree/master/data/SensoBoxMCU-core/senseBox-MCU_v13.zip) |  |
| [Seeed](http://wiki.seeedstudio.com/) -uino Boards | Arduino Shields | [Bluetooth Shield V2.0](http://wiki.seeedstudio.com/Bluetooth_Shield_V2/) | [Eagle](https://github.com/SeeedDocument/Bluetooth_Shield_V2/blob/master/res/Buletooth_Shield_v2.0_sch_pcb.zip) | #TODO | |
| [MNT Reform](https://source.mntmn.com/MNT/reform) | Laptop | Reform v2 Motherboard | [KiCad](https://source.mntmn.com/MNT/reform/src/branch/reform2-nitrogen8m/reform2-motherboard/reform2-motherboard) | [BoM](https://gitlab.com/fairloetet/orga/tree/master/source/devices/MNT%20Reform%20v2/Motherboard) | | 


## Compiling Fairtronics data from real world device data

Apart from GUI-based composition of a device within the Fairtronics app, the ability to read "real world data" is an option.

BoM information we could use is:
* Any information to derive information of any component, compiled out of Manufacturer, MPN, Part name, (Description), (Value), even Designator
* Quantity
* plus any information on the completeness of the BoM provided

Since BoM occur in various formats, we need to implement an intelligent BoM reader.

PCB information we could use is:
* Size of PCB
* Type of PCB (single or double layered)
* Number of solder points / holes from solder mask / paste
* Amount of copper 

We may simply guess these values based on the BoM. At least, there's one Python library for reading Gerber files: https://github.com/curtacircuitos/pcb-tools

SCH data doesn't seem to be often available in machine readible format.


## General module data

Our understanding of devices composed out of components is often misleading since IT hardware is mostly composed out of modules which are devices by themselves. 
For example, a desktop computer contains a hard disk module which consists of electronic components.
In this sense it would be nice to either have the ability to compose devices out of other devices = modules as well or to have modules modelled as components.
For the latter, we need data on general modules = devices. Typical sources of data are:
* Life cycle assessments of devices
* WEEE analysis   

## Fairtronics predefined products

### Nager-IT computer mouse

Finished

### Arduino Uno

### MNT reform v2 laptop

