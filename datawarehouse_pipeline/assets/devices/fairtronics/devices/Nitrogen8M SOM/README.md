Beim Extrahieren dieser BOM wurde wie folgt vorgegangen:
* Unter https://boundarydevices.com/product/nitrogen8m-som/ gibt es(nur per Registrierung) den Schaltplan 
  SCH_Nit8MQ_SOM_2r8e_REV10_20190228-2.pdf
* Man sieht in dem PDF, dass hinter jedem Bauteil ein Objekt liegt mit allen Informationen zu dem Bauteil.
* Mithilfe von MuPDF (https://www.mupdf.com/index.html) wurde per `mutool show {FILE} grep` der Content des PDFs extrahiert, 
  siehe FILE-Contents.txt. Dort sieht man, dass vieles in JScript-Routinen steckt, sehr sonderbar.
* Durch großflächiges Löschen des Rests, diverse Suchen/Ersetzen-Aktionen und ein paar Reparaturen kann man eine CSV daraus machen,
  siehe FILE-Extract.csv. Das besondere ist u.a., dass für manche Teile Alternativen angegeben sind.
* Leider enthält die Liste nun nicht die Komponentenreferenzen (R57, C33, ...), sondern interne Ids (BD...).
  Die Referenzen sind im Content leider gar nicht auffindbar.
  Da sowieso geprüft werden muss, ob nun alle Teile da sind, musste nun manuell doch noch eine Zuordnung der 
  Schaltplan-Referenzen zu den extrahierten Einträgen geschehen. So entstand eine BOM -> reform2-som.csv
* Man muss sich den Schaltplan anschauen, denn ein und dasselbe Teil taucht mehrfach auf, hier konkret U1 und U2, 
  einzeln mehrfach vorhanden als U1A, U1B, usw.
* Am Ende nochmal Abgleich mit dem Inhaltsverzeichnis, in dem alle Components (neben der Netlist und "Ports") 
  aufgelistet werden. Das Inhaltsverzechnis ist entweder mit `mutool show {FILE} outline` zu bekommen oder in
  einem Standard-PDF-Anzeigeprogramm ersichtlich.

Da dies ausschließlich für den MNT Reform moelliert wurde, ignorieren wir das ebenfalls dokumentierte Carrier Board
