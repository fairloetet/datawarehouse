import pandas as pd
from dagster import asset, Output, MetadataValue
from typing import Dict
import country_converter as coco


@asset(io_manager_key="resources_duckdb_io_manager", 
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="resources",
       compute_kind="World Mining Data")
def world_mining_data() -> Output[pd.DataFrame]:
    """
       World Mining Data

       See https://www.world-mining-data.info

       Reading linked Excel sheet from Home > World Mining Data > Data Section
    """
    file_url = "https://www.world-mining-data.info/wmd/downloads/XLS/6.4.%20Production_of_Mineral_Raw_Materials_of_individual_Countries_by_Minerals.xlsx"
    excel_df_dict = pd.read_excel(file_url, sheet_name=None, skiprows=1)
    COUNTRY_COLUMN = "Country"
    UNIT_COLUMN = "unit"
    SOURCE_COLUMN = "data source"
    # See https://notebook.community/konstantinstadler/country_converter/doc/country_converter_examples
    congo_dr = pd.DataFrame.from_dict({
       'name_short': ['D.R. Congo'],
       'name_official': ['Congo (Democratic Republic of the)'],
       'regex': ['Congo, D.R.'],  # This one is missing
       'ISO2': ['CD']})
    cc = coco.CountryConverter(additional_data=congo_dr)
    metadata = dict()
    metadata["Name"] = "World Mining Data"
    metadata["Source"] = MetadataValue.md(
       "Dataset provided in https://www.world-mining-data.info/?World_Mining_Data___Data_Section, "
       "specifically " + file_url)
    result_df_dict: Dict[str, pd.DataFrame] = dict()
    for mineral, result_df in excel_df_dict.items():
        commodity = mineral.strip().replace(".", "").replace(",", " &")
        # Column of the latest data and fetch year
        latest = len(result_df.columns) - 2
        latest_year = result_df.columns[latest]
        # Get Total (last row) and compare to column sum
        total = result_df.tail(1)[latest_year].tolist()[0]
        result_df = result_df[:-1]  # Remove Total row
        sum = result_df[latest_year].sum()
        # TODO Use check_asset? Dann muss ich Total aber mit reinnehmen
        if total != sum:
            print(f"Warning: For {commodity}, total {total} doesn't equal to sum {sum}")
        assert result_df[UNIT_COLUMN].nunique() == 1  # all units must be the same
        unit = str(result_df[UNIT_COLUMN].unique()[0])
        df = pd.DataFrame()
        df['country'] = cc.pandas_convert(series=result_df[COUNTRY_COLUMN], to='ISO2')
        df['value'] = result_df[latest_year]
        df['unit'] = unit.removeprefix("metr. ")
        df['share'] = df['value'] / total
        df['year'] = latest_year
        df['value_desc'] = result_df[COUNTRY_COLUMN] + " production (in " + \
            result_df[UNIT_COLUMN] + ") of " + mineral
        df['share_desc'] = "Share of " + result_df[COUNTRY_COLUMN] + " in world production of " + mineral
        df['source'] = "World Mining Data, " + result_df[SOURCE_COLUMN].map({
                "e": "an estimated figure",
                "p": "a provisional figure",
                "r": "a reported figure"}) + " from " + latest_year
        df['commodity'] = commodity
        result_df = df[['commodity', 'country', 'value', 'unit', 'share', 'year', 'value_desc', 'share_desc', 'source']]
        result_df_dict[commodity] = result_df
        metadata["Number of countries for " + commodity] = MetadataValue.int(len(result_df.index))
    dataframe = pd.concat(result_df_dict.values())
    metadata["Number of minerals"] = MetadataValue.int(len(result_df_dict))
    return Output(value=dataframe, metadata=metadata)
