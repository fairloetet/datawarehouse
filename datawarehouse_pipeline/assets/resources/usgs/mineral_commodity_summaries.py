import pandas as pd
from dagster import asset, Output, MetadataValue, AssetExecutionContext, TableColumn, TableRecord, TableSchema
from typing import Dict
import country_converter as coco
import requests
import zipfile
import io
import string
import datetime
import dateutil.parser as parser
from lxml import objectify
from sciencebasepy import SbSession


@asset(io_manager_key="resources_duckdb_io_manager", 
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="resources",
       compute_kind="USGS")
def usgs_mineral_commodity_summaries(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    """
       Mineral Commodity Summaries, National Minerals Information Center (NMIS), U.S. Geological Survey (USGS)

       See https://www.usgs.gov/centers/national-minerals-information-center/mineral-commodity-summaries

       Fetching a zip'ed folder of individual commodities' data incl. accompanying metadata files
       via the https://www.sciencebase.gov service.
    """
    result_df_dict: Dict[str, pd.DataFrame] = dict()

    # Prepare metadata (general)
    metadata = dict()
    metadata["Name"] = "Mineral Commodity Summaries"
    metadata["Source"] = MetadataValue.md(
       "Dataset provided in https://www.usgs.gov/centers/national-minerals-information-center/"
       "commodity-statistics-and-information, using the  ScienceBase service (https://www.sciencebase.gov)")

    # Prepare country name converter
    unknown = pd.DataFrame.from_dict({
       'name_short': ['Other'],
       'name_official': ['Other Countries'],
       'regex': ['Other countries'],  # Translate fake country
       'ISO2': ['XX']})
    cc = coco.CountryConverter(additional_data=unknown)

    # Fetch link to download the data (see notebook)
    sb = SbSession()
    response = sb.find_items({'q': 'NMIC', 'lq': 'title:"National Minerals Information Center"',
                              'max': 1})  # NMIC = National Minerals Information Center
    nmic_id = response['items'][0]['id']
    nmic_url = response['items'][0]['link']['url']
    nmic_title = response['items'][0]['title']
    context.log.info(f"Found resource for the {nmic_title} ({nmic_url})")
    response = sb.find_items({'parentId': nmic_id,
                              'filter': ['tags={type: Subject}',  # For some reason that seems to be the type
                                         'dateRange={choice: year}']})  # Means: Fetch only most recent item
    mcs_url = response['items'][0]['link']['url']
    mcs_title = response['items'][0]['title']
    context.log.info(f"Found resource for the {mcs_title} ({mcs_url})")
    # This is a hack. Usually, data can only be downloaded with login.
    # For 2023 it results in "https://www.sciencebase.gov/catalog/file/get/63b5f411d34e92aad3caa57f"
    zip_file_url = mcs_url.replace("item", "file/get")
    request = requests.get(zip_file_url)
    full_archive = zipfile.ZipFile(io.BytesIO(request.content), 'r')
    context.log.info(f"Downloaded full {mcs_title} archive from {zip_file_url}")
    assert "world.zip" in full_archive.namelist(), f"There is no 'world.zip' in {zip_file_url}."
    world_archive = zipfile.ZipFile(full_archive.open("world.zip"))

    # The following is full of code to cope with the many errors in the metadata XML files
    #   and missing data in the data CSV files.
    for metadata_file_name in [name for name in world_archive.namelist() if name.endswith("xml")]:

        context.log.info(f"Trying to make sense of {metadata_file_name}.")

        # Check black list of known parsing problems
        if metadata_file_name in [
                "mcs2023-cemen_meta.xml",  # "Cement" with useless types of commodities and parsing problems
                "world/mcs2023-cemen_meta.xml",
                "mcs2023-stond_meta.xml",  # "Stone (Dimension)" has only USA value and none for Other Countries
                "world/mcs2023-stond_meta.xml",
                "mcs2024-plati_meta.xml",  # lxml.etree.XMLSyntaxError: EntityRef: expecting ';', line 410, column 123
                "world/mcs2024-plati_meta.xml"
                ]:
            context.log.warn(f"{metadata_file_name} skipped: It's on blacklist of data files with unresolved problems.")
            continue

        # Get all interesting from the individual metadata. Here's an example of what can be found (click on View):
        #   https://www.sciencebase.gov/catalog/item/63d1a299d34e06fef150062d
        metadata_xml = world_archive.open(metadata_file_name)
        metadata_root = objectify.fromstring(metadata_xml.read(), parser=None)
        identification = metadata_root.idinfo
        citation = identification.citation.citeinfo
        # pubdate: In some cases it's only the year, in others in format%Y%m%d
        pub_date = parser.parse(str(citation.pubdate.text), default=datetime.datetime(2022, 1, 1, 0, 0))
        title = str(citation.title.text)
        link = str(citation.onlink.text)
        description = f"{identification.descript.abstract.text} {identification.descript.supplinf.text} " \
                      f"For details see the {identification.crossref.citeinfo.title.text} " \
                      f"({identification.crossref.citeinfo.onlink.text})."
        data_year = str(identification.timeperd.timeinfo.rngdates.enddate.text)
        # Get column names
        data_file_name = country_column = type_column = value_column = value_description \
                       = commodity_type = value_unit = commodity = None
        prod_found = False
        for detail in metadata_root.eainfo.detailed:
            entity_type_label = str(detail.enttyp.enttypl.text)
            if "salient" in entity_type_label and "csv" in entity_type_label:
                for attribute in detail.attr:
                    attribute_label = str(attribute.attrlabl.text)
                    # This seems to be the only reliable way to fetch the general commodity
                    if attribute_label == "Commodity":
                        commodity = str(attribute.attrdomv.edom.edomv.text)
            elif "world" in entity_type_label and "csv" in entity_type_label:
                data_file_name = "world/" + detail.enttyp.enttypl.text
                for attribute in detail.attr:
                    attribute_label = str(attribute.attrlabl.text)
                    # The lables doesn't have any kind of type, so this occurs somewhat stupid
                    if attribute_label.lower() == "country":
                        country_column = attribute_label
                    elif attribute_label.lower() == "type":
                        type_column = attribute_label
                    elif "Prod" in attribute_label and data_year in attribute_label:
                        value_column = str(attribute.attrlabl.text)
                        value_description = str(attribute.attrdef.text)
                        if hasattr(attribute.attrdomv, 'rdom') and hasattr(attribute.attrdomv.rdom, 'attrunit'):
                            value_unit = attribute.attrdomv.rdom.attrunit.text
                        else:
                            value_unit = attribute.attrdomv.text
                        prod_found = True
                    elif "Cap" in attribute_label and data_year in attribute_label and not prod_found:
                        value_column = str(attribute.attrlabl.text)
                        value_description = str(attribute.attrdef.text)
                        if hasattr(attribute.attrdomv, 'rdom') and hasattr(attribute.attrdomv.rdom, 'attrunit'):
                            value_unit = attribute.attrdomv.rdom.attrunit.text
                        else:
                            value_unit = attribute.attrdomv.text
        if not country_column:
            country_column = "Country"  # Just expect the most obvious
        if not type_column:
            type_column = "Type"  # Just expect the most obvious
        value_unit_abbr = ""
        if not value_unit:
            if value_column:
                parse_value_column = value_column.split('_')
                if len(parse_value_column) > 1:
                    value_unit_abbr = parse_value_column[1]
                    match value_unit_abbr:
                        case "Mt":
                            value_unit = "million metric tons"
                        case "kt":
                            value_unit = "thousand metric tons"
                        case "t":
                            value_unit = "metric tons"
                        case "kg":
                            value_unit = "kilogram"
                        case "mct":
                            value_unit = "million carats"
                        case "kct":
                            value_unit = "thousand carats"
        else:
            value_unit = str(value_unit).lower()
            match value_unit:
                case "million metric tons":
                    value_unit_abbr = "Mt"
                case "thousand metric tons":
                    value_unit_abbr = "kt"
                case "metric tons":
                    value_unit_abbr = "t"
                case "kilogram":
                    value_unit_abbr = "kg"
                case "million carats":
                    value_unit_abbr = "mct" 
                case "thousand carats":
                    value_unit_abbr = "kct"
        if not data_file_name:
            data_file_name = metadata_file_name.replace("meta", "world").replace("xml", "csv")
        if not value_description:
            value_description = ""

        # Ok, go on with actual data. First prepare or guess data file name
        info_in_data_file_name = data_file_name.replace("-", "_").split("_")
        if data_file_name not in world_archive.namelist():
            # Just guess the correct name
            data_file_name = metadata_file_name.replace("meta", "world").replace("xml", "csv")
            if data_file_name not in world_archive.namelist():
                # Second guess the correct name
                data_file_name = info_in_data_file_name[0] + "-" + info_in_data_file_name[1] + "_" + \
                    info_in_data_file_name[2]
                if data_file_name not in world_archive.namelist():
                    context.log.warn(f"{metadata_file_name} skipped: Expected data file not found in archive.")
                    continue

        data_csv = world_archive.open(data_file_name)
        # W denotes "Withheld to avoid disclosing company proprietary data."
        data_df = pd.read_csv(data_csv, na_values=["W", "XX", "NA"], dtype=str)
        if not value_column or value_column not in data_df.columns:
            # Ok, metadata doesn't provide us with (correct) data on the values we were looking for.
            prod_columns = {}
            cap_columns = {}
            for column in data_df.columns:
                if "Prod" in column and column[-4:].isdigit():
                    prod_columns[int(column[-4:])] = column
                elif "Cap" in column and column[-4:].isdigit():
                    cap_columns[int(column[-4:])] = column
            if len(prod_columns) > 0:
                data_year = max(prod_columns.keys())
                value_column = prod_columns[data_year]
                prod_found = True
            elif len(cap_columns) > 0:
                data_year = max(cap_columns.keys())
                value_column = cap_columns[data_year]
            else:
                # The remining cound be "Reserves", but that's not what we are looking for
                context.log.warn(f"{data_file_name} skipped: No production or capacity data can be found.")
                continue
        context.log.info(f"Processing data file {data_file_name} with '{commodity}' commodities")

        # A CSV file may contain multiple commidities reported as one, discriminated by column "Type"
        #   For some specific ones, patch Type explicitely because of too fragmented data
        if commodity in ["Boron", "Kyanite and Related Minerals", "Fluorspar"]:
            data_df[type_column] = "Mine production, " + str(commodity)
        for type, df in data_df.groupby(type_column):

            # Squeeze out all the information within the Type column as best as possible
            info_in_type = str(type).split(', ', 1)

            # Den Type als Basis zu nehmen macht viele Probleme, weil es letztlich ein nicht einheitlich benutzter
            #   semi-strukturierter natürlichsprachiger Text ist. Siehe auch Kommentar und Taktik in den Notizen. 
            #   Hier einige Auffälligkeiten, die derzeit zu schlechten Ergebnissen führen, zumindest in der Benennung 
            #   der Commodity, teilweise aber auch zu völlig falschem führen:
            # [world/mcs2023-antim_world.csv] "Contained antimony"
            # [world/mcs2023-bauxi_world.csv] "Alumina - calcined equivalent weights" alles hinter - gehört zur description
            # [world/mcs2023-bromi_world.csv] "Bromine content"
            # [world/mcs2023-chrom_world.csv] "Marketable chromite ore"
            # [world/mcs2023-cobal_world.csv] "Metric tons of contained cobalt", also ohne Komma
            # [world/mcs2023-coppe_world.csv] "Mine production, recoverable copper content" und "Refinery production, copper"
            # [world/mcs2023-feore_world.csv] "Iron ore - mine production - usable ore -thousand metric tons" und "Iron ore - mine production - iron content - thousand metric tons"
            # [world/mcs2023-fepig_world.csv] "Iron oxide pigments (ocher and red iron oxide)" und ähnliches
            # [world/mcs2023-feste_world.csv] "Raw steel, million metric tons", also andersherum als sonst
            # [world/mcs2023-garne_world.csv] "Garnet (Industrial)"
            # [world/mcs2023-gemst_world.csv] "Thousand carats of gemquality diamond"
            # [world/mcs2023-germa_world.csv] "Primary and secondary refinery production"... fällt wegen lauter NAs aber eh weg
            # [world/mcs2023-gold_world.csv] "Gold - contained content, mine production, metric tons", nochmal eine Variante
            # [world/mcs2023-manga_world.csv] "Manganese content"
            # [world/mcs2023-mercu_world.csv] "Mercury content"
            # [world/mcs2023-mgcomp_world.csv] Type 'Mine production, gross weight of magnesite (magnesium carbonate) in thousand metric tons'
            # [world/mcs2023-mgmet_world.csv] "Magnesium Metal" (type 'Magnesium smelter production') <- Defaultwert
            # [world/mcs2023-mica_world.csv] hat Type 'Mine production - mica scrap and flake' und type 'Mine production - mica sheet', also - statt ,. Zudem sind die Daten bei mica sheet unbrauchbar
            # [world/mcs2023-molyb_world.csv] "Contained molybdenum"
            # [world/mcs2023-nicke_world.csv] Type 'Mine production - nickel, metric tons contained', also - und ,
            # [world/mcs2023-nitro_world.csv] Type 'Plant production, ammonia - contained nitrogen'
            # [world/mcs2023-plati_world.csv] Überall sauber Platinum, beim "World total" ist der type dann aber ' World mine production: Platinum' :-(
            # [world/mcs2023-potas_world.csv] 'Potassium oxide K2O equivalent', im MEtadaten commodity aber schön 'Potash'. "Equivalent"
            # [world/mcs2023-pumic_world.csv] "Pumice and pumicite"
            # [world/mcs2023-raree_world.csv] type 'Rare earths, mine production, rare-earth-oxide equivalent, metric tons'. Die - zeichen scheine ich aktuell blöderweise wegzuschneiden
            # [world/mcs2023-silve_world.csv] type 'Silver - mine production, contained silver - metric tons'... oh je :-(. Die Original commodity heißt schlicht "Silver"
            # [world/mcs2023-simet_world.csv] type 'Plant production, silicon content of combined totals for ferrosilicon and silicon metal production' und type 'Plant production, silicon content of ferrosilicon production', aber durcheinander, d.h. es sind nicht zwei verschiedene, da auch nur 1 World total und überhaupt: keine doppelte Country
            # [world/mcs2023-sodaa_world.csv] type 'Soda ash, total natural and synthetic'... noch eine Variante
            # [world/mcs2023-sulfu_world.csv] type 'Production, all forms, contained sulfur', aber Metadaten Commodity sauber
            # [world/mcs2023-talc_world.csv] Alle möglichen types, aber alle gleich zu verstehen
            # [world/mcs2023-timin_world.csv] Titanium hat nicht nur Ilmenite, sondern auch Rutile, manchmal "Ilmenite (rounded)", die total world production ist dann wieder eigensinnig "World total mine production: ilmentite and rutile (rounded)"
            # [world/mcs2023-titan_world.csv] Ergbt "Titanium and Titanium Dioxide" weil es kein KOmma gibt, der Type heißt aber 'Sponge Metal Production and Sponge and Pigment Yearend Operating Capacity' :-D
            # In mcs2023-vermit_meta.xml steht als data_file "mcs2023-vanad_world.csv" drin, ob wohl die tatasächlich auch vom (korrekten) mcs2023-vanad_meta.xml referenziert wird und überhaupt ein mcs2023-vermit_world.csv existiert! Dateinamen direkt raten ist vermutlich besser!
            # world/mcs2023-yttri_meta.xml skipped: Expected data file not found in archive. Tatsächlich!
            # Und als letzte Variante verweist world/mcs2023-zirco_meta.xml wie erwartet auf mcs2023-zirco_world.csv, tatsächlich ist es aber in mcs2023-zirco-hafni_world.csv

            if len(info_in_type) == 1:
                if not commodity:
                    # Ok, there no other way to near the commodity's name
                    if len(info_in_data_file_name) == 3:  # "world/mcs2023-bismu_world.csv"
                        commodity = info_in_data_file_name[1]
                    else:
                        commodity = data_file_name.partition('.')[0].partition('/')[2]
                    context.log.info(f"[{data_file_name}] Because of missing data, "
                                     f"{commodity} is taken as a commodity name.")
                commodity_type = info_in_type[0]
            else:
                commodity_type, type_commodity = info_in_type
                # Remove punctuation and white space, start with upper case
                commodity = type_commodity.strip().translate(str.maketrans('', '', string.punctuation))
                commodity = commodity[0].upper() + commodity[1:]  # Capitalize first letter

            context.log.info(f"[{data_file_name}] Processing commodity {commodity} (type '{type}')")
            if not value_unit:
                context.log.info(f"[{data_file_name}][{commodity}] The unit of the value is not known.")
            # Correct some country names
            data_df[country_column] = data_df[country_column].astype('str').apply(lambda c: c.replace("\xa0", " "))
            # Making sense of values: a) drop NaN, b) interpreting as float  # , c) drop zeros
            list_of_nan_countries = df[df[value_column].isnull()][country_column].to_list()
            list_of_nan_countries = [country for country in list_of_nan_countries if str(country) != 'nan']
            if len(list_of_nan_countries) > 0:
                df = df.dropna(subset=value_column)
                context.log.warn(f"[{data_file_name}][{commodity}] Countries {', '.join(list_of_nan_countries)} "
                                 "are ignored because of non-numerical value.")
            try:
                df[value_column] = df[value_column].replace("1,000", "1")  # Strange artefact possible
                df = df.astype({value_column: 'float'}, errors='raise')
            except ValueError as e:
                context.log.warn(f"[{data_file_name}][{commodity}] Skipped! Couldn't convert values in column "
                                 f"{value_column} to float: {e}")
                continue
            # Keed zeros, since 'not listed' also means 'no production', different to unknown = "Other countries"
            # World Mining Data also has quite a few 0 values which are also retained
            # df = df.drop(df[df[value_column] == 0.0].index)  # Zero means 'not listed' (like ---) in value column

            # Care about world total for quality check and for computing share for each country
            sum = df[value_column].sum(skipna=True)
            last_row = df.tail(1)
            if len(last_row) == 0:
                context.log.warn(f"[{data_file_name}][{commodity}] Skipped! Not a single non-numerical value remains.")
                continue
            total_name = last_row[country_column].tolist()[0]
            if "total" in total_name:
                if "exclud" in total_name:
                    context.log.warn(f"[{data_file_name}][{commodity}] The world total doesn't seem to contain all data, "
                                     f"because it's denoted as '{total_name}'")
                # Get total (last row), compare it to column sum and remove it
                total = last_row[value_column].tolist()[0]
                df = df.drop(df.index[-1])  # Remove Total row
                sum -= total  # Inital sum includes total as well
                # TODO Use check_asset? Dann muss ich Total aber mit reinnehmen
                diff = total - sum
                if diff != 0:
                    message = f"[{data_file_name}][{commodity}] Reported world total {total} doesn't equal " \
                              f"to sum {sum} of countries' values."
                    if diff > 0:
                        # The total is expected to be correct, i.e. attribute positive difference to unknown
                        df.loc[df[country_column] == 'Other countries', value_column] = \
                            df.loc[df[country_column] == 'Other countries', value_column] + diff
                        # ... and to sum as well
                        sum += diff
                        message += f" Difference of {diff} will be attributed to unknown country."
                    else:
                        # If sum is bigger, leave it as it is
                        message += f" Sum is {-diff} bigger than total and will be taken as world total."
                    context.log.info(message)
            else:
                context.log.info(f"[{data_file_name}][{commodity}] No world total production figure is given. "
                                 f"The sum of all countries' values is taken as such.")

            # Prepare commodity data with final columns
            df['value'] = df[value_column]
            df['unit'] = value_unit_abbr
            df['share'] = df[value_column] / sum
            df['year'] = data_year
            df['country'] = df[country_column].apply(lambda c: ';'.join(
                code if isinstance(code := cc.convert(c, to='ISO2'), list) else [code]))
            df['value_desc'] = f"{commodity_type} of {commodity} {'(in '+value_unit+')' if value_unit else ''}" + \
                               " for country " + df[country_column]
            df['share_desc'] = f"Share of {commodity_type} of {commodity} for country " + df[country_column]
            df['source'] = f"{mcs_title} ({mcs_url})" + \
                           (". Capacity is taken as a substitute, because data on actual production in not available"
                               if not prod_found else "")
            df['commodity'] = commodity
            result_df = df[['commodity', 'country', 'value', 'unit', 'share', 'year', 'value_desc', 'share_desc', 'source']]
            result_df_dict[commodity] = result_df
            
            # Prepare commodity metadata
            metadata[f"{commodity}: Source link"] = MetadataValue.md(f"[{title}]({link})")
            metadata[f"{commodity}: Source description"] = MetadataValue.md(description)
            metadata[f"{commodity}: Year of estimated data"] = MetadataValue.md(str(data_year))
            metadata[f"{commodity}: Description of data"] = MetadataValue.md(value_description)
            metadata[f"{commodity}: Number of origins"] = MetadataValue.int(len(result_df.index))
            metadata[f"{commodity}: World production ({value_unit})"] = MetadataValue.float(float(sum)) # numpy.float64 didn't work with SQLite
    
    dataframe = pd.concat(result_df_dict.values())
    metadata["Number of minerals"] = MetadataValue.int(len(result_df_dict))
    return Output(value=dataframe, metadata=metadata)
