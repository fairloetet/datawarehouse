from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def extractives_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/resources/fairtronics/extractives_base.csv")))


@asset(io_manager_key="resources_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[extractives_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="resources",
       compute_kind="Fairtronics")
def extractives(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    # TODO Work with file_relative_path
    extractives_df = pd.read_csv("./datawarehouse_pipeline/assets/resources/fairtronics/extractives_base.csv",
                                 na_values={'short_name': ''}, keep_default_na=False, comment='#')
    extractives_df['short_name'] = extractives_df['short_name'].fillna(extractives_df['name'])
    dataframe = extractives_df[['slug', 'name', 'short_name', 'synonyms', 'circularity', 
                                'foreign_names', 'source_preference', 'comment']]
    metadata = dict()
    metadata["name"] = "List of extractives used in the Datawarehouse"
    metadata["source"] = MetadataValue.md("Manually compiled")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
