from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
from dagster import ExperimentalWarning
from typing import Dict
import warnings
import os
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def custom_extractives_market():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/resources/fairtronics/custom_extractives_market.csv")))


@asset(io_manager_key="resources_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[custom_extractives_market],
       ins={"world_mining_data": AssetIn(key=["resources", "world_mining_data"]),
            "usgs_mineral_commodity_summaries": AssetIn(key=["resources", "usgs_mineral_commodity_summaries"]),
            "extractives": AssetIn(key=["resources", "extractives"]),
            "regimes": AssetIn(key=["regimes", "regimes"])},
       key_prefix="resources",
       compute_kind="Fairtronics")
def extractives_market(context: AssetExecutionContext,
                       world_mining_data: pd.DataFrame,
                       usgs_mineral_commodity_summaries: pd.DataFrame,
                       extractives: pd.DataFrame,
                       regimes: pd.DataFrame) -> Output[pd.DataFrame]:
    extractive_markets_df = list()
    # TODO Anti-Pattern for Pandas... I just took it from an CSV-reading code. Needs refactoring.
    for _, extractive in extractives.iterrows():
        if len(extractive['source_preference']) == 0:
            context.log.info(f"No source specified for extractive {extractive['name']}")
            continue
        source_preference = extractive['source_preference'].split(";")
        if len(extractive['foreign_names']) == 0:
            context.log.warn(f"No source specific data defined for extractive {extractive['name']}")
            continue
        foreign_keys = dict(item.split(":") for item in extractive['foreign_names'].split(";"))
        for source in source_preference:
            if source in foreign_keys:
                foreign_key = foreign_keys[source]
                try:
                    match source:
                        case "wmd":
                            wmd = world_mining_data[world_mining_data.commodity == foreign_key]
                            extractive_df = pd.merge(wmd, regimes, how="left",
                                                        left_on="country", right_on="code")
                            break
                        case "mcs":
                            mcs = usgs_mineral_commodity_summaries[
                                usgs_mineral_commodity_summaries.commodity == foreign_key]
                            extractive_df = pd.merge(mcs, regimes, how="left",
                                                        left_on="country", right_on="code")
                            break
                except KeyError:
                    context.log.error(f"Key for source {source} doesn't fit for extractive {extractive['name']}")
                    continue
            else:
                context.log.warn(f"No key defined regarding source {source} for extractive {extractive['name']}")
        else:
            context.log.warn(f"No data known for any of the preferred sources of extractive {extractive['name']}")
            continue
        extractive_df['source'] = extractive_df['share_desc'] + " taken from " + extractive_df['source']
        extractive_markets_df.append(extractive_df[['slug', 'value', 'unit', 'share', 'source']]
                                        .rename(columns={'slug': 'specific_regime', 'share': 'amount'})
                                        .assign(general_regime="unknown", extractive=extractive['slug']))

    # Add custom resource market to world extractives market...
    custom_extractives_market_df = pd.read_csv(
        "./datawarehouse_pipeline/assets/resources/fairtronics/custom_extractives_market.csv", comment="#")
    custom_extractives_market_df['amount'] = custom_extractives_market_df['amount'].apply(lambda x : eval(str(x))) 
    extractive_markets_df.append(custom_extractives_market_df)

    # ... to build whole extractives market
    extractives_market_df = pd.concat(extractive_markets_df)

    dataframe = extractives_market_df[['extractive', 'general_regime', 'specific_regime', 'value', 'unit',
                                       'amount', 'source']]
    metadata = dict()
    metadata["name"] = "Country origins of Resources as used in Fairtronics"
    metadata["source"] = MetadataValue.md(
        "Compiled from World Mining Data, complemented by USGS Mineral Commodity Summaries")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
