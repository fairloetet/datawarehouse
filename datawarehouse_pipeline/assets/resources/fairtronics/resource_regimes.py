from dagster import asset, AutoMaterializePolicy, Output, MetadataValue, AssetExecutionContext, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="resources_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"extractives_market": AssetIn(key=["resources", "extractives_market"])},
       key_prefix="resources",
       compute_kind="Fairtronics")
def resource_regimes(context: AssetExecutionContext,
                     extractives_market: pd.DataFrame) -> Output[pd.DataFrame]:

    # Utilize market to generate good old resource_shares
    extractives_market['resource'] = extractives_market['general_regime'] + "-" + extractives_market['extractive']
    os.makedirs("pipeline_run/result", exist_ok=True)
    extractives_market.to_csv("pipeline_run/result/resource_shares.csv", index=False,
                              columns=['specific_regime', 'resource', 'amount', 'source'],
                              header=['Location.slug', 'Resource.slug', 'amount', 'source'])

    dataframe = extractives_market.rename({"specific_regime": "regime_slug", "resource": "resource_slug"}, axis=1) \
                                  .filter(['regime_slug', 'resource_slug', 'amount', 'source'])
    metadata = dict()
    metadata["name"] = "Mapping resources to where they come from in what amount, as used in Fairtronics"
    metadata["source"] = MetadataValue.md("Systematically compiled from the extractives assets.")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
