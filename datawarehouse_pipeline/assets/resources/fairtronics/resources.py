from dagster import asset, AutoMaterializePolicy, Output, MetadataValue, AssetExecutionContext, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="resources_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"extractives_market": AssetIn(key=["resources", "extractives_market"]),
            "extractives": AssetIn(key=["resources", "extractives"]),
            "regimes": AssetIn(key=["regimes", "regimes"])},
       key_prefix="resources",
       compute_kind="Fairtronics")
def resources(context: AssetExecutionContext,
              extractives_market: pd.DataFrame,
              extractives: pd.DataFrame,
              regimes: pd.DataFrame) -> Output[pd.DataFrame]:

    # Merge market with extractives to compile all resources
    # TODO Ekelig dieses zusammenbasteln. Das wird nicht mehr nötig sein, wenn sowieso alles in der Engine geschieht.
    # First, generate resources from specific regimes and their extractives they provide
    resources_from_specific_regimes_df = extractives_market.merge(extractives, how='left', left_on='extractive',
                                                        right_on='slug', suffixes=('_m', '_e'))
    resources_from_specific_regimes_df = resources_from_specific_regimes_df.drop("general_regime", axis=1)\
                                                 .rename({"specific_regime": "regime"}, axis=1)\
                                                 .merge(regimes, how='left', left_on="regime",
                                                        right_on='slug', suffixes=('_r', '_l'))
    # Second, generate resources from general regimes and their extractives they provide
    resources_from_general_regimes_df = extractives_market.merge(extractives, how='left', left_on='extractive',
                                                                     right_on='slug', suffixes=('_m', '_e'))
    resources_from_general_regimes_df = resources_from_general_regimes_df.drop("specific_regime", axis=1)\
                                                             .rename({"general_regime": "regime"}, axis=1)\
                                                             .merge(regimes, how='left', left_on="regime",
                                                                    right_on='slug', suffixes=('_r', '_l'))
    # At third, generate all extractives as resources from unknown origin since some of them may have no market at all
    # TODO Hmmm, directly setting the regime values isn't wise, see regimes/fairtronics/special_regimes_base.csv
    regime_slug = "unknown"
    regime_name = "- Unknown -"
    regime_short_name = "Unknown"
    resources_from_unknown_regime_df = extractives
    resources_from_unknown_regime_df['extractive'] = resources_from_unknown_regime_df['slug']
    resources_from_unknown_regime_df['slug'] = regime_slug + "-" + resources_from_unknown_regime_df['extractive']
    resources_from_unknown_regime_df['name_l'] = regime_name
    resources_from_unknown_regime_df['name_r'] = resources_from_unknown_regime_df['name']
    resources_from_unknown_regime_df['short_name_l'] = regime_short_name
    resources_from_unknown_regime_df['short_name_r'] = resources_from_unknown_regime_df['short_name']
    resources_from_unknown_regime_df['regime'] = regime_slug
    
    # Finally, put them all together, remove duplicates which naturally occur, 
    #       create useful slug, name and short_name and save it
    resources_df = pd.concat([resources_from_general_regimes_df, resources_from_specific_regimes_df,
                              resources_from_unknown_regime_df ]) \
                     .drop_duplicates(subset=['extractive', 'regime'])
    resources_df['slug'] = resources_df['regime'] + "-" + resources_df['extractive']
#    resources_df['name'] = resources_df['name_r']
#    resources_df['name'] = resources_df['name_r'] + " from " + resources_df['name_l']
    resources_df['name'] = resources_df[['name_r', 'name_l']].apply(
        lambda s: " from ".join(e for e in s if not pd.isna(e) and len(e) > 0 and "unknown" not in str(e).lower()), 
        axis=1)
#    resources_df['short_name'] = resources_df['short_name_r']
#    resources_df['short_name'] = resources_df['short_name_l'] + " " + resources_df['short_name_r']
    resources_df['short_name'] = resources_df[['short_name_l', 'short_name_r']].apply(
        lambda s: " from ".join(e for e in s if not pd.isna(e) and len(e) > 0 and "unknown" not in str(e).lower()), 
        axis=1)

    os.makedirs("pipeline_run/result", exist_ok=True)
    resources_df.to_csv("pipeline_run/result/resources.csv", index=False,
                        columns=['slug', 'name', 'short_name', 'extractive', 'regime', 'circularity', 'comment'],
                        header=['slug', 'name', 'short_name', 'extractive', 'location', 'circularity', 'comment'])

    # Utilize market to create a resource_ressoures table
    # TODO This is a simplified version. In general we probably need to group on general_regime and extractive
    # TODO Do we really need this?
    #resource_resources_df = pd.DataFrame()
    #resource_resources_df['demanded_resource'] = \
    #    extractives_market['general_regime'] + "-" + extractives_market['extractive']
    #resource_resources_df['offered_resource'] = \
    #    extractives_market['specific_regime'] + "-" + extractives_market['extractive']
    #resource_resources_df['amount'] = extractives_market['amount']
    #resource_resources_df['source'] = extractives_market['source']
    #resource_resources_df.to_csv("pipeline_run/result/resource_resources.csv", index=False,
    #                             columns=['demanded_resource', 'offered_resource', 'amount', 'source'])

    dataframe = resources_df.rename({"regime": "regime_slug", "extractive": "extractive_slug"}, axis=1) \
                            .filter(['slug', 'name', 'short_name', 'extractive_slug', 'regime_slug', 'comment'])
    metadata = dict()
    metadata["name"] = "List of resources used in Fairtronics"
    metadata["source"] = MetadataValue.md("Systematically compiled from the source assets.")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
