from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
import os
import csv
import re
from string import Template
from typing import Tuple, List
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

#import pandera as pa
#from dagster_pandera import pandera_schema_to_dagster_type
#from pandera.typing import Series
#
#class ManufacturersModel(pa.SchemaModel):
#    slug: Series[str] = pa.Field(description="Key")
#    name: Series[str] = pa.Field(description="Name of mine")
#    short_name: Series[str] = pa.Field(description="Short name für mines")
#    company_url: Series[str] = pa.Field(description="Web address of company")

factors = {
    'kg': (1000.0, 'g'),
    'g': (1.0, 'g'),
    'mg': (0.001, 'g'),
    'm': (1000.0, 'mm'),
    'cm': (10.0, 'mm'),
    'mm': (1.0, 'mm'),
    'µm': (0.001, 'mm')
}


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def part_material_sourcing():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/parts/fairtronics/part_material_sourcing.csv")))


@asset(io_manager_key="parts_duckdb_io_manager",
#       dagster_type=pandera_schema_to_dagster_type(ManufacturersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[part_material_sourcing],
       ins={"parts": AssetIn(key=["parts", "parts"]),
            "components": AssetIn(key=["parts", "components"]),
            "materials": AssetIn(key=["materials", "materials"]),
            "component_manufacturers": AssetIn(key=["regimes", "component_manufacturers"])},
       key_prefix="parts",
       compute_kind="Fairtronics")
def part_materials(context: AssetExecutionContext,
                   parts: pd.DataFrame,
                   components: pd.DataFrame,
                   materials: pd.DataFrame,
                   component_manufacturers: pd.DataFrame) -> Output[pd.DataFrame]:

    sourcings: List[Tuple[re.Pattern, re.Pattern, re.Pattern, re.Pattern, re.Pattern, Tuple[str, str]]] = []

    def evaluate(amount: str, value_dict: dict):
        return eval(amount, value_dict)

    def normalize_value(value: float, unit: str):
        factor = factors[unit][0]
        return value * factor

    def find_material_simple(manu_slug: str, comp_slug: str, substance_slug: str) -> str:
        for manu_pattern, comp_pattern, substance_pattern, sub_product_pattern, homo_mat_pattern,\
                (material_param, material_regex) in sourcings:
            if manu_pattern.match(manu_slug) and comp_pattern.match(comp_slug) and \
               substance_pattern.match(substance_slug):
                # Find matching criterium
                matching_materials_df = materials[
                    materials[material_param].str.match(material_regex) &
                    (materials['substance_slug'] == substance_slug)]
                if len(matching_materials_df) == 0:
                    raise ValueError(f"Couldn't find a material matching {material_param}={material_regex}.")
                if len(matching_materials_df) > 1:
                    raise ValueError(f"Found more than one material matching {material_param}={material_regex}.")
                return matching_materials_df['slug'].values[0]
        raise ValueError(f"Couldn't find a matching rule for substance {substance_slug} in part {part_slug}.")

    # It's special here that we provide the substance_row and sub_product/homo_mat patterns for
    #   selection. TODO It may be better to have extra tiers for sub_product (semi) and homogeneous.
    def find_material(manu_slug: str, comp_slug: str, substance_row) -> str:
        substance_slug = substance_row['Substance.slug']
        sub_product = substance_row['sub_product']
        homo_mat = substance_row['homogeneous_material']
        for manu_pattern, comp_pattern, substance_pattern, sub_product_pattern, homo_mat_pattern,\
                (material_param, material_regex) in sourcings:
            if manu_pattern.match(manu_slug) and comp_pattern.match(comp_slug) and \
               substance_pattern.match(substance_slug) and \
               sub_product_pattern.match(sub_product) and homo_mat_pattern.match(homo_mat):
                # Find matching criterium
                matching_materials_df = materials[
                    materials[material_param].str.match(material_regex) &
                    (materials['substance_slug'] == substance_slug)]
                if len(matching_materials_df) == 0:
                    raise ValueError(f"""Couldn't find a material of substance {substance_slug}
                                         matching {material_param}={material_regex}.""")
                if len(matching_materials_df) > 1:
                    raise ValueError(f"""Found more than one material of substance {substance_slug}
                                         matching {material_param}={material_regex}.""")
                return matching_materials_df['slug'].values[0]
        raise ValueError(
                f"Couldn't find a matching rule for substance {substance_slug} in part {manu_slug}/{comp_slug}.")

    # Compile sourcing patterns
    with open("./datawarehouse_pipeline/assets/parts/fairtronics/part_material_sourcing.csv", 
              mode="r", encoding="utf-8-sig", newline='') as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            manu_pattern = re.compile(row['manufacturer'])
            comp_pattern = re.compile(row['component'])
            substance_pattern = re.compile(row['substance'])
            sub_product_pattern = re.compile(row['sub_product'])
            homo_mat_pattern = re.compile(row['homogeneous_material'])
            material_criterum = row['material'].split("=", 1)
            material_param = "slug" if len(material_criterum) == 1 else material_criterum[0]
            material_regex = material_criterum[0] if len(material_criterum) == 1 else material_criterum[1]
            sourcings.append((manu_pattern, comp_pattern, substance_pattern, sub_product_pattern, homo_mat_pattern,
                              (material_param, material_regex)))

    # Initialize resulting table
    part_materials_dfs = []

    # For all parts
    for part_slug, component_slug, manufacturer_slug, part_weight, part_weight_unit, part_values in \
            parts[['slug', 'component_slug', 'regime_slug', 'weight', 'weight_unit', 'values']].values:

        # Check integrity of data
        if component_slug not in components['slug'].values:
            raise ValueError(f"Component {component_slug}, referenced in part {part_slug} "
                             "doesn't seem to exist in components.csv.")
        # if len(component_row) > 1:
        #     raise ValueError(f"Component {component_slug}, referenced in part {part_slug} "
        #                      "exists more than once in components.csv.")
        # Assumes the slug is unique
        component_row = components[components['slug'] == component_slug].iloc[0]
        try:
            manufacturer_row = component_manufacturers[component_manufacturers['slug'] == manufacturer_slug].iloc[0]
        except IndexError:
            context.log.error(f"Could not find manufacturer {manufacturer_slug} in component manufacturer list.")

        # Compile list of variable values. Precedence: part values precede component (default) values
        values_dict = {}
        component_values = component_row['values']
        if component_values:
            for value in component_values.split(";"):
                name, expr = value.split(":", 1)
                values_dict[name] = expr
        # Provider values overwrite substance values
        manufacturer_values = manufacturer_row['values']
        if manufacturer_values:
            for value in manufacturer_values.split(";"):
                name, expr = value.split(":", 1)
                values_dict[name] = expr
        # Part values even overwrite manufacturer values
        if part_values:
            for value in part_values.split(";"):
                name, expr = value.split(":", 1)
                values_dict[name] = expr
        # The value_dict still contains all kind of strings.
        # Now convert numbers with a unit as suffix to normalized value without suffix, if prefix is of float kind
        for name in values_dict:
            for unit in factors.keys():
                expr = str(values_dict[name])
                if expr.endswith(unit):
                    try:
                        values_dict[name] = normalize_value(float(expr[:-len(unit)]), unit)
                    except ValueError:
                        continue

        # Read the component's substances via the component connection
        component_substances_file_path = "./datawarehouse_pipeline/assets/parts/fairtronics/components/" + \
                                         component_slug + ".substances.csv"
        if not os.path.exists(component_substances_file_path):
            raise ValueError(f"Substances file {component_substances_file_path} doesn't seem to exist.")
        component_substances_df = pd.read_csv(component_substances_file_path,
                                              index_col=False, dtype=str, na_filter=False, comment='#')

        # Check weight availability
        if not part_weight:
            component_weight = component_row['weight']
            if component_weight:
                # If part weight not known, take component weight if that's known
                part_weight = component_weight
                part_weight_unit = component_row['weight_unit']
        if not part_weight and 'weight' not in component_substances_df.columns:
            # If still no part weight and even material weights are missing, give up.
            raise ValueError(f"Substances in {component_substances_file_path}, part {part_slug} "
                             f"and component {component_slug} don't specify absolute weights.")
        # Check substance availability
        # unknown_substances = [substance for substance in component_substances_df['Substance.slug']
        #                       if substance not in substances_df['slug'].values]
        # if unknown_substances:
        #     raise KeyError("Component '" + component_slug + "' has unknown substance(s) " + str(unknown_substances))

        part_materials_df = pd.DataFrame()  # New part

        # Find materials according to sourcing
        # TODO Falls ich nur auf Basis der Substance ein Material suche:
        #part_materials_df['Material.slug'] = component_substances_df['Substance.slug'].\
        #    map(lambda substance_slug: find_material(manufacturer_slug, component_slug, substance_slug))
        # TODO Falls ich alle Spalten herausziehen möchte, müsste ich so allgemein vorgehen:
        part_materials_df['Material.slug'] = component_substances_df.apply(
            lambda substance_row: find_material(manufacturer_slug, component_slug, substance_row), axis=1)
        # TODO Und falls ich doch wieder Variablensubstitution haben möchte:
        # part_materials_df['Material.slug'] = component_materials_df['Substance.slug'].\
        #     map(lambda x: Template(x).substitute(value_dict))

        # Replace template strings for specific columns and evaluate all columns which may contain python formulas
        part_materials_df['quantity'] = component_substances_df['quantity'].\
            map(lambda x: Template(x).substitute(values_dict)). \
            map(lambda x: evaluate(x, values_dict))

        # Normalize material data, i.e. convert weight to gramm, multiply quantity for material (if > 1),
        #    sum up all weights of the same material, and multiply amount with actual weight of part, if available.
        if 'amount' in component_substances_df.columns:
            part_materials_df['amount'] = component_substances_df['amount'].\
                map(lambda x: Template(x).substitute(values_dict)).\
                map(lambda x: evaluate(x, values_dict))
            # If we have an amount (= percentage composition), just normalize it
            part_materials_df['multiplied_amount'] = part_materials_df['amount'] * part_materials_df['quantity']  # TODO Useful?
            part_materials_df['amount'] = part_materials_df['multiplied_amount'] / part_materials_df['multiplied_amount'].sum()
            part_weight_in_g = normalize_value(float(eval(part_weight)), part_weight_unit)
            part_materials_df['amount'] = part_weight_in_g * part_materials_df['amount']
        elif 'weight' in component_substances_df.columns:
            part_materials_df['weight'] = component_substances_df['weight'].\
                map(lambda x: Template(x).substitute(values_dict)).\
                map(lambda x: evaluate(x, values_dict))
            part_materials_df['weight_unit'] = component_substances_df['weight_unit'].\
                map(lambda x: Template(x).substitute(values_dict))
            part_materials_df['norm_weight'] = part_materials_df.apply(
                lambda row: normalize_value(row['weight'], row['weight_unit']), axis=1)
            part_materials_df['multiplied_norm_weight'] = \
                part_materials_df['norm_weight'] * part_materials_df['quantity']
            if part_weight:
                materials_weight_in_g = part_materials_df['multiplied_norm_weight'].sum()
                part_weight_in_g = normalize_value(float(part_weight), part_weight_unit)
                if abs((part_weight_in_g - materials_weight_in_g) / part_weight_in_g) > 0.05:
                    context.log.info("  There's a > 5% difference between the given part weight (" + str(part_weight_in_g) + "g) "
                          "and the summed materials weight (" + str(materials_weight_in_g) + "g) "
                          "in the part " + part_slug + ". The part's weight will be taken.")
                part_materials_df['amount'] = \
                    part_materials_df['multiplied_norm_weight'] / materials_weight_in_g
                part_materials_df['amount'] = part_weight_in_g * part_materials_df['amount']
            else:
                part_materials_df['amount'] = part_materials_df['multiplied_norm_weight']
        else:
            raise ValueError(f"Materials file {component_substances_file_path} "
                             f"doesn't list data on weights (amount or weight).")

        # Disabled because in reading the Database similiar material's weights of a Part are summed up as well
        # materials_df = materials_df.groupby(['Part.slug', 'Material.slug']).sum().reset_index()

        # Collect interesting columns. Note that amount is now in gramm, and that we get from components to parts
        part_materials_df['Part.slug'] = part_slug
        # TODO: Create source from columns sub_product,homogeneous_material,substance,identity
        component_material_source = part_materials_df['Material.slug']
        component_source = component_row['source']
        component_date = component_row['date']
        part_materials_df['source'] = component_material_source + " in " + \
            component_source + " (updated " + component_date + ")" if component_date else ""

        # Part done, add it to the parts collection
        part_materials_dfs.append(part_materials_df[['Part.slug', 'Material.slug', 'amount', 'source']])

    # Collect all in one file
    all_materials_df = pd.concat(part_materials_dfs, sort=False, ignore_index=True)
    os.makedirs("pipeline_run/result", exist_ok=True)
    all_materials_df.to_csv("pipeline_run/result/part_materials.csv", index=False,
                            columns=['Part.slug', 'Material.slug', 'amount', 'source'],
                            header=['Part.slug', 'Material.slug', 'amount', 'source'])

    dataframe = all_materials_df.rename({"Material.slug": "material_slug", "Part.slug": "part_slug"}, axis=1) \
                                .filter(["part_slug", "material_slug", "amount", "source"])
    metadata = dict()
    metadata["name"] = "Part -> Material relations used here"
    metadata["source"] = MetadataValue.md("Systematically compiled from the components composition from substances and" 
                                          "supply chain data.")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
