#!/usr/bin/env bash
#
# Usage: list_materials.sh
#
# Lists all materials used in the CSV files here.

set -euo pipefail

tail -q -n +2 *.materials.csv | cut -d ',' -f 2 | sort | uniq
