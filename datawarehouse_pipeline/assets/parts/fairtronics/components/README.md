# Data on components

Components are the parts, a device is made of. It designates everything material: the PCBs and everything what's mounted on, the solder, cables, etc.
Only electrical components are considered here, i.e. cases, screws, etc are excluded.
Note that many devices contain some parts that are specially designed for this device (e.g. logic chips) and are not publically documented. 

## Component base data

### Types

### Packaging

https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types

https://en.wikipedia.org/wiki/Surface-mount_technology#Packages

http://www.interfacebus.com/semiconductor-diode-packages.html

### Manufacturers

Aluminium Electrolytic Capacitors: https://en.wikipedia.org/wiki/Aluminum_electrolytic_capacitor#Manufacturers_and_products

MLCC: 

## Material contents of components

Component compositions means: Which base/raw materials are electronic components (e.g. capacitors, ICs, PCBs, etc.) made of, i.e. gate-to-gate system boundary. See also https://translate.google.com/translate?hl=&sl=ja&tl=en&u=http%3A%2F%2Fsemicon.jeita.or.jp%2Fcommittee%2Fcommittee2_4_1.html
It's important to understand that there are at least three levels or "are made of":
* Level 1: The actual material contents of a component after production. For example the die of an IC including casing and pins. This level of data is also interesting for electronic waste analysis, and actually is used. Also, for chemical or import regulations, this level of information is sufficient. See for example the EU project GreenElec with results published amoung others in the paper Wesley Van Meensel/Geert Willems/Maarten Cauwe: MATERIAL IDENTIFICATION IN ELECTRONICS (2014)
* Level 2: The contents as above plus waste which is always part of production. For example the waste part because of non-100%-yield for each die of a wafer for producing of ICs. Unclear whether this waste is recycled or not.
* Level 3: The contents including waste as above plus auxilary materials needed. For example the many chemicals used for producing the die of ICs. This is the least level used in LCA analysis. On the problems of featching data of this kind see the report by Maria Erixon: Practical strategies for Acquiring Life Cycle Inventoty Data in the Electronics Industry (1999)  
* Level 4: One of the above plus packaging. Again it's unclear whether this is recycled/reused or not.

There are two primary sources for information on the material contents:
* Data provided by manufactures
* Analytical means

Based on these primary sources, a few secondary sources come in more handy:
* Life Cycle Inventory databases
* Handbooks of various kinds

We have some requirements on the data, at least:
* It should contain the weight of the component.
* The data should be complete in the sense that nearly 100% of the material is listed.
* It should be machine readible.
* It shouldn't be outdated.
* The data should be freely accessible.
* The data should be freely publishable.

Dependent on the data source these criteria are fulfilled or not.

### Company data

Of course, component manufactures know about the composition of their products, but mostly they won't freely share the information. There are some exceptions, though.

#### Scientific base

At least in the earlier days, progress in electronic manufacturing was based on non-coorporate research activities. But utilising this isn't easy:
* It's far from easy to find and understand the relevant papers and articles.
* It is not known which research results are actually used in industrial production.
* Companies have their own research facilities and most probably don't publish critical information on manufacturing processes.

A special case  are life cycle analysises of electronic products or parts, for example chip production. 
The inventory phase of each LCA could be of great interest for us, but
* inventory data often is specialized for the impact analysis planned, not necessarily fitting our needs, especially if we are looking for the material within a component, not for the whole production line
* conference and journal papers almost never include details of the inventory, and finding the full scientific results still isn't an easy task nowadays 

Often, cited sources are of special interest for further research.

Additionally, scientific papers and articles mostly aren't freely accessible.

| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 1-3 | weak | no | very good | very good | very high | ? | depends | 

As a result, we won't look into this any further.

#### Legal obligations

Some data on component composition needs to be published by law. For the electronics industry selling to EU, at least two regulations are relevant:
* [RoHS](https://en.wikipedia.org/wiki/Restriction_of_Hazardous_Substances_Directive) requires to disclose important information on hazardous substances in case of disposal of electronic waste. 
It is to be declared on a product level, but only covers a few materials and doesn't require to disclose the exact amount of these.
* Quite the same applies to [REACh](https://en.wikipedia.org/wiki/Registration,_Evaluation,_Authorisation_and_Restriction_of_Chemicals) regulations.
* Additionally, [conflict minerals](https://en.wikipedia.org/wiki/Conflict_resource) within products needs to be declared in USA, EU, China, etc. in various forms, but only on company level and not by quantity, and - for the EU - not by the manufacturers but by those who import the materials.
* EMV...
* Proposition 65... https://de.wikipedia.org/wiki/The_Safe_Drinking_Water_and_Toxic_Enforcement_Act_of_1986

See https://www.cedm.be/rohs-service/rohs-legislation as well.

| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 1 | too bad | maybe | good | very high | medium | no | none | 

As a result, declarations by law don't help at all.

#### Full material declaration / disclosure (FMD)

For customers, component manufactures may provide material specifications on their websites, but only within a non-disclosure agreement. 
Although not required, few companies freely publish detailed material specifications for many or most of their products.
Here's an explanation why they might want to do that: https://www.bomcheck.net/en/suppliers/full-materials-declaration-tool.

Here are some suppliers with public FMDs:

| Company (with most specific link) | Component types with data | FMD file types (best available) | Instruction how to obtain data |
|-----------------------------------|---------------------------|---------------------------------|--------------------------------|
| [Alliance Memory](https://www.alliancememory.com) | DRAM, SRAM, Flash | non-standard PDF | Go for specific product and click "MDS". Lists even material suppliers! | 
| [Analog Devices / Linear Technology](https://www.analog.com/en/about-adi/quality-reliability/material-declarations.html) | IC semiconductors, MCUs/DSPs, Sensors, etc. | non-standard PDF | Go for specific product, look at "Design Resources" and download the product's material declaration, or use the Material Declarations page https://www.analog.com/en/about-adi/quality-reliability/material-declarations.html 
| [Bourns](https://www.bourns.com/resources/rohs) | Discrete semiconductors, Passives | non-standard PDF | Click on the requested product line, and for the model click on the RoHS Compliant link. Or: Browse to the product page and look for "Related Documents". It's special to Bourns that they make Product Line/Group specific Conflict Mineral Reports, see https://www.bourns.com/resources/conflict-minerals | 
| [Comchip](https://www.comchiptech.com/index.php) | Diodes, Transistors | non-standard PDF | Click on Support Menu, choose Material Data Sheet, and select the part familiy you are looking for. Available for only some of the parts. |
| [Diodes Inc. / Pericom](https://www.diodes.com) | Semiconductors: Discrete, Analog, Logic, Timing (some) | non-standard PDF | Go for a specific product, select the "Specs & Tech Docs" and look for "MDS Reports". Available for some, but not all parts |
| [ECS, Inc.](https://ecsxtal.com/) | Crystals, Oscillators, Filters | non-standard PDF | Look for product line, look under Support Documents for Material Declaration |
| [Frolyt](https://www.frolyt.de/en) | Aluminium Electrolyte Capacitors | ZVEI Umbrella Specification PDF | Go for a specific product family und look for a link labeled "Umbrella Specification" below the "Product Environmental Compliance" section | 
| [Fujicon](http://www.fujicon.com/en/environmental_measures.php) | Aluminium Electrolytic Capacitors | non-standard PDF | Seems to have **only 3** declarations |
| [Harwin](https://www.harwin.com/support/articles/material-composition-full-material-declaration-fmd/) | Connectors, PCB hardware | non-standard PDF | Go to product. The composition information will be listed under the ‘Environmental Compliance’ category, further down the product page in the ‘Downloads’ section. |
| [Infineon](https://www.infineon.com/cms/en/product/) | IC Semiconductors, MCUs, some discrete semiconductors | non-standard PDF | Go for specific product, look for Documents section, and open Material content sheet. Not available for all components. |
| [Maxim Integrated](https://www.maximintegrated.com/en/qa-reliability/emmi.html/tb_tab2) | Semiconductors: MCUs, Power, Analog, Logic, Interface, etc. | IPC 1752A XML | Enter part code (without suffixes), click on the magnifier, wait, and afterwards select the specific part number. Alternatively, go to a specific product family, click on the "Quality and Environmental" tab, and look for the "Material Composition" column. Will not work for all parts. IPC-XML is available. |
| [Micro Commercial Components (MCC)](https://www.mccsemi.com) | Discrete Semiconductor, MOSFETs | non-standard Excel Sheet | Go for a specific products (quite unpleasant to use) and look for the "Material Composition Data Sheet" | 
| [Microchip](https://www.microchip.com/wwwproducts/Rohs) | IC Semiconductor incl. Memory (not DRAM), no MCUs | non-standard PDF | Go to this RoHS page, type in a part number and click on the Package Specification PDF. Alternatively, select a specific product (from the family page), open the RoHS information tab, click on the link below for RoHS data, which will lead you to the RoHS page mentioned before, pre-populated with the part's id.
| [Nexperia](https://www.nexperia.com) | Discrete Semiconductors, Logic ICs, ESD protection | non-standard HTML or PDF | Go for a specific product and look for "chemical content". Available only for some components. | 
| [NIC Components](https://www.niccomp.com/resource/environmental.php) | Passives, Circuit Protection, Connectors & Cables | Standard IPC 1752-1.1 PDF | Select the correct product family in the drop down box below "Environmental Information" and see the appearing links if PIC files which are sorted by component size | 
| [NXP](https://www.nxp.com/company/our-company/about-nxp/corporate-responsibility/environmental-compliance-organization/product-content-search:ENV-PRODUCT-COMPOSITION) | MCUs, Interfaces, Logic, Sensors, RFID/NFC, Security, Wireless | IPC 1752A XML | Apart from the link provided here, it's better to browse to a specific product, look for Environmental Information or Quality Information, and click on the "Material Declaration" link. It's even available in XML format. (Alternative link: https://www.nxp.com/webapp/chemical-content/search ?) | 
| [onsemi](https://www.onsemi.com/PowerSolutions/MaterialComposition.do) | Sensors, Power Management, Amplifiers, Audio/Video, Discretes, Memory (w/o DRAM) Logic, MCUs, Opto | IPC 1752A XML | Directly or via product search
| [Panasonic](https://industrial.panasonic.com/ww/downloads) | Batteries | PDF | Panasonic at least ha some fairly detailed safety sheets for batteries |
| [Renesas / IDT](https://www.idt.com/eu/en/support/document-search?title=&doc_file_all_types[Materials+Composition+Declaration]=Materials+Composition+Declaration) | Clock, Logic, Memory (w/o DRAM), Interface, Power Mgmt, Radio Frequency, etc. | IPC 1752-2 XML | The link displays a list of FMDs. Better: Go to a product, look at the document section and look for Materials Composition Declarations. Only package related FMD seem to be available, not part related. |
| [Rohm](https://www.rohm.com/) | ICs, Discretes, Passives, Opto Electronics, Diodes, Resistors, Tantalum Capacitors, Transistors | non-standard PDF | For some parts you find a "Constitution Materials List" within the "Packaging & Quality" tab of a product's webpage. Additionally, hidden in the depths of Rohm's website, they offer additional FMDs for a few of their products under http://engineering.rohmsemiconductor.com/index.php?/Knowledgebase/List/Index/13  
| [Samtec](https://wwws.samtec.com/tools/material-declaration.aspx) | Connectors, Cables, Optics, Radio Frequency | non-standard PDF | Enter part id, available for a few components. Parts are dynamically configured, the material declarations as well 
| [STMicroelectronics](https://www.st.com/content/st_com/en/about/quality-and-reliability/product-environmental-compliance/material-declaration.html) | Semiconductors of all kinds | IPC 1752A XML | Go to any product page, click on the "Quality & Reliability" tab, and look for the Material Declaration column. |  
| [TDK Electronics / Epcos](https://www.tdk-electronics.tdk.com/en/176050/company/environmental-protection/material-data-sheets) | Capacitors, Inductors, Ferrits, Sensors, Protection | ZVEI Umbrella Specification PDF | TDK publishes more general umbrella specification, not specific to part families but to part types | 
| [TE Connectivity](https://www.te.com/commerce/DocumentDelivery/DDEController) | Cables, Antennas, Connectors, Labels, Passives (no capacitors available, no data on resistors and thermistors), Relais, Auxilaries | IPC 1752A XML, some only non-standard PDF| Input part number, and look at "TE Material Declaration". Or go for a specific product, select the "Documents", and look at "Product Environmental Compliance" | 
| [TT Electronics](https://www.ttelectronics.com/resources/compliance/) | Data on Potentiometer, Trimmer, Encoder | non-standard PDF | Only a few material declarations (for resistors and potentiometers only?) available through the Compliance Resources page or via the products 
| [Texas Instruments](http://www.ti.com/materialcontent/home) | Semiconductor of all kinds, no discrets | IPC 1752A XML | Needs account to log in! This material content search is a bit misleading: Type TI part number and click on Search, but then don't download the offered spreadsheet (it only specified compliance yes/no) but click on the same "TI part number" in the displayed table and there is the "Component Information" in - amoung others - IPC 1752A Class D format. Additionally, don't use the "Quality, reliability & packaging information" link on each product page, it doesn't lead to IPC XML format downloads. But note that there's even information on the manufacturing site of each single TI part! | 
| [Xilinx](https://www.xilinx.com/support/answers/21277.html) | Special ICs | non-standard PDF | For any product, go for the Documentation tab and filter for package specifications.

The following support it notably only via online form, individual e-mail request, or complex search:

| Company (with most specific link) | Component types (* with data) | Instruction how to obtain data | Success? |
|-----------------------------------|-------------------------------|--------------------------------|----------|
| [Abracon](https://abracon.com/) | Timing, RF | Using https://abracon.com/support/regulatory-request-form it's possible to request IPC-1752 files, and it actually works! |
| [Asahi Kasei / AKM](https://www.akm.com/eu/en/support/product-technical-information/environment/) | Active components of various kind | Mentions "Material Components Datasheet" but doesn't seem to provide them on the website |
| [AVX (Kyrocera)](http://www.avx.com/about-avx/environmental-compliance/environmental-compliance-request/?ref=material-declaration) | Small passives and actives | provide full material declarations on request per mail | it worked, a request is forwareded to a local service provider who answers the day after |
| [Cornell Dubilier](https://www.cde.com/) | Capacitors | seems to have declarations (e.g. https://www.cde.com/resources/downloads/fcp_mcd.pdf), but how to access from the website them isn't clear | |
| [Cypress Semiconductor](https://www.cypress.com/search/all?f%5B0%5D=meta_type%3Aquality_packaging&f%5B1%5D=quality_packaging_meta_type%3A534) | Active components | Seem to have >1000 FMDs, but I didn't understand for what parts |
| [Intel / Altera](https://www.intel.com/) | quite a few semiconductors | IPC data through https://www.intel.com/content/www/us/en/programmable/support/quality-and-reliability/environmental/material-decl.html ends in a Sign In, and https://qdms.intel.com/ seems to only provide RoHS, ReACH, etc. declarations |
| [Kemet](https://www.kemet.com/en/us.html) | only on MLCC and Tantalum Capacitors found | https://www.kemet.com/content/dam/kemet/lightning/documents/rohs-and-reach/material-composition-declaration/MCD-Tantalum-April-5-2021.pdf, https://www.kemet.com/content/dam/kemet/lightning/documents/rohs-and-reach/material-composition-declaration/MCD-Ceramic-April-5-2021.pdf | 
| [Keystone Electronics](https://www.keyelco.com/) | mostly accessories | Using contact form with request on IPC 1752(A) Class D file with CAS numbers, they answered within a week |
| [Littelfuse](https://info.littelfuse.com/product-environmental-information-request) | Cicuit Protection, switches, battery management, sensors, some ICs | Using contact formular, answer after quite a few days |
| [Molex](https://www.molex.com/molex/electrical_model/MultiPartComplianceDocuments.action) | Connectors, ...| Product specific IPC 1752A Class D Document link all seem to refer to https://www.molex.com/molex/electrical_model/MultiPartComplianceDocuments.action, a form where MPNs (each 10 digits with leading zeros, with or without dashes) and an e-mail-address need to be specified to request the FMDs. Note, that there's no reply if anything is incorrectly requested | Works well within a day or two |
| [PUI Audio](https://www.puiaudio.com/) | Microphones, Speakers, Piezo, etc | Using contact form with request on IPC 1752(A) Class D file with CAS numbers, they answered within two weeks |
| [Silicon Labs](https://www.silabs.com) | Sensors, Power Management, MCUs | Create account, go to https://ops.silabs.com/rfi/Pages/PartSearch, enter part number, answer check question, and look for IPC 1752-6 XML format or HTML-based Detailed Device Composition (MDDS) | 
| [Venkel](https://www.venkel.com/compliance/material-declarations/) | MLCC | by web form only | Worked immediately |
| [Vishay](https://www.vishay.com/) | many | Under the name "The Constituents of (Semiconductor) Components" you find (via search on Vishay website as well as search engines) some old declarations of still existing parts and packages by Vishay, but it is far from up-to-date and complete |

Hints for looking up this information at a manufacturer's website: Either there's a dedicated webpage where part item numbers can be entered to retrieve a FMD or FMDs are available somewhere at the part's specific page. The mostly used terms are "material data", "MDS", "material composition", "material content", "chemical content" and "material declaration", "list of components" in other cases "RoHS" or "Environmental Conformance", "Compliance", "Environmental Data", or "IPC". 
The information is sometimes appended to all a products data sheets, sometimes summarized in a special part of the website called "Compliance", "Quality", "RoHS", "Environmental ...", or "Material...". 
Data sheets never contain material details. Sometimes it is hidden in the Documents Search section of the web page. In one case, the materials specification was hidden under the package specification. Sometimes FAQs are available on a manufacturers website which could be scanned for "materials" as a word. A Google search on the terms together with the components name and any of these terms is always worth a try. 

A weak alternative are Safety Data Sheet, some legal obligation to provide information on safe handling of products. 
It sometimes contains some vague information on ingredients of products as well.
We never looked for this systematically, but here are helpful sources:

| Company (with most specific link) | Component types with data | FMD file types (best available) | Instruction how to obtain data |
|-----------------------------------|---------------------------|---------------------------------|--------------------------------|
| [Panasonic](https://industrial.panasonic.com/ww/downloads) | Batteries | PDF | Panasonic at least ha some fairly detailed safety sheets for batteries |

Others like 
* 3M
* Advanced Thermal Solutions Inc
* ALPS Electric / Alpine
* Amphenol / Amphenol-ICC
* Amotech
* Apem
* Assem Tech
* ATP
* AUO
* Awinic
* Bel Fuse 
* Bivar
* Broadcom (incl. Avago)
* C & K Switches / Components
* Cardinal Components
* (Nippon) Chemi-Con
* Chilisin
* Cirrus Logic
* Cliff Electronic Components Ltd. 
* Cree
* CTS Corporation / Electronic Components
* CUI Devices
* CviLux 
* Darfon
* Densitron
* Dialog Semiconductor
* Dialight
* E-Switch
* Eaton (Cooper Bussmann)
* Electric Connector Technology (ECT)
* EMINENT
* Enrich Electronics
* Essentra
* Everlight
* Eyang
* Fenghua
* Greyhill
* Harmony Electronics
* Hirose
* Holy Stone International
* Honeywell Sensing/Switches
* Innolux
* Inolux
* INPAQ Technology
* InvenSense
* ISSI
* Johanson Dielectrics (JDI)
* J.S.T. Deutschland GmbH (JST) 
* Kailh / Kaihua
* KEMET (only two are found: https://www.kemet.com/content/dam/kemet/lightning/documents/rohs-and-reach/material-composition-declaration/MCD-Ceramic-April-5-2021.pdf, https://www.kemet.com/content/dam/kemet/lightning/documents/rohs-and-reach/material-composition-declaration/MCD-Tantalum-April-5-2021.pdf)
* KDS
* Kinetic
* Kingbright
* Kingston
* Knitter Switch
* Knowles
* Kobiconn (nicht zu finden)
* Kycon
* Kyrocera (but see AVX above)
* Lite-on (will become Analog Devices)
* Lision
* Lumex
* Maruwa
* Memsensing
* Memory Protection Devices (battery-contacts.com)
* Micron
* Microtech
* Micro Crystal
* Midas Components Limited
* Multicomp Pro
* Murata
* Moda-Innochips
* MWS Wire
* Nanya
* Neutrik
* Newhaven Display
* Nichia
* Nidec Copal
* Ningbo Broad
* NKK Switches
* NTE Electronics
* Omron Electronic
* Osram Opto Semiconductors
* Phoenix Contact
* PixArt
* Prisemi
* Pulse
* Qualcomm
* Qorvo
* RAF Electronic Hardware (declares single materials but not weight of parts)
* Ralec Electronic
* Rean
* Renata
* Richtek
* Royalohm
* Salecom Electronics
* Samsung Electro-Mechanics
* Samsung Semiconductor
* Samwha
* Schaltbau
* Schneider Electric
* Seoul Semiconductor
* Sharp
* Shenzhen JTK wire&cable Co., Ltd / Green Cable Co., Ltd. Shenzhen Jin Taike
* SK Hynix
* Soberton Inc.
* Speed Tech
* Stanley Electric
* Sunlord
* Switchcraft / Conxall
* TA-I
* Taiyo Yuden
* Telemecanique Sensors
* Three Circle / CCTC
* Torch (vermutlich, chinesisch-übersetzt)
* Toshiba
* TXC Corp.
* United Chemi-Con (UCC)
* VCC
* Vishay (only old vague FMDs available, see above) 
* Walsin Technology / Passive System Allience / passivecomponent.com
* Will Semiconductor
* Wima
* Winbond
* Winger (led1.de)
* Würth Elektronik
* Yageo (only vague, but up to date data on MLCCs https://www.yageo.com/en/Download/Index/literature)
* Yuliang Electronics
* ZF switches and sensors / ZF Friedrichshafen AG

don't seem to publicly provide or promise FMD information like this on their websites. 
Some of them mention to get in contact with customer service. We never tried it in these cases.
We also did not look for useful Safety Data Sheets in these cases.

There are material declaration databases for B2B communication, supply chain management, and automatic generation of compliance documents (e.g. RoHS or [Type III Environmental Product Declarations](https://de.wikipedia.org/wiki/Environmental_Product_Declaration)). 
These databases may include FMDs which are not available through the website. They may only be used by electronic manufacturers, and data can be marked confidently.
Here are examples:
* Most noticeable, the [BOMcheck](https://www.bomcheck.net/assets/docs/BOMcheck%20User%20Guide%20for%20Suppliers%20and%20Manufacturers%20-%20release%202.06%20-%20public%20version.pdf) database seems to have a quite extensive user base, and Philips requires suppliers to upload data to BOMcheck.
* Another example is [CDX](https://public.cdxsystem.com/en/web/cdx/electronics) used by HP.
* [International Material Data System / IMDS](https://www.mdsystem.com/imdsnt/startpage/index.jsp). Here'S a statement within Diodes.com: "The International Material Data System, IMDS, is a material data system for automotive OEMs. Diodes IMDS product data is available upon request contact: compliance@diodes.com or your local sales office. Please include your IMDS account number, the Diodes part number and any customer part number."
* [iPoint Material Compliance App](https://www.ipoint-systems.com/solutions/material-compliance/) within the SustainHub, which initially was a EU project
* IHS [BOM Intelligence](https://ihsmarkit.com/products/supply-chain-bom-intelligence.html) & [Parts Intelligence](https://ihsmarkit.com/products/parts-intelligence-electronic-component-research.html)
* [Assent Compliance Platform](https://www.assentcompliance.com/compliance-software/) incl. [The Assent Materials Declaration Tool](https://www.assentcompliance.com/assent-materials-declaration-tool/)
* [Sustainabill](https://sustainabill.io)

There are data format standards on FMDs:
* [IPC 1752(A) XML Class D](http://www.ipc.org/ContentPage.aspx?pageid=Materials-Declaration). One of the handy properties of this format is, that the materials are listed in packs of "homogenous materials". For example, in ICs the materials are specified for the die, bonding wires, moulding, and frame/pins/finishing seperately, so there is a chance to build a parameterized model of it to compoute FMDs for more or less pins, for example. As another example, the FMD for a USB cable seperated the connector from the cable, which allows for lengthening the cable. 
* [ZVEI Umbrella Specification](https://www.zvei.org/en/association/divisions/electronic-components-and-systems-division/material-data-declaration-on-product-level-and-the-umbrella-specification-based-on-product-families-as-an-efficient-example/) which doesn't seem to be supported anymore since April 2017.
* [IEC 62474 XML](http://std.iec.ch/iec62474). See https://www.zvei.org/presse-medien/publikationen/leitfaden-materialdeklarationen-innerhalb-der-lieferkette/ (German) as well.
* DIN 19220
* [JIG–101](https://www.jedec.org/standards-documents/docs/jig-101-ed-40)
* [chemSHERPA](https://chemsherpa.net/english) with its "SubstanceList" in the Composition section. Compliance section only doesn't help.

Looking for a specific part, mostly there's no FMD available, so a substitute needs to be selected. 
There are many [distributors](http://www.opencircuits.com/Supplier) on electronic components. They can be used to search for specific functions, packages, and manufacturers. 
[Mouser](https://www.mouser.de/) for example has a huge database and an easy to use and fast user interface, even an API.

| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 1, 4 | unknown | sometimes | good | very high | medium | no, in case of published data | none | 

As a result, free FMDs are valuable sources, and we should scan the relevant manufactures for their availability.   

#### Inquiries

The manufacturer may provide information on request. Mostly this will be combined with a non-disclosure agreement. 
Additionally, we expect this to take quite some time with a frustrating outcome.
See https://www.lifecyclecenter.se/publications/practical-strategies-for-acquiring-life-cycle-inventory-data-in-the-electronics-industry/ for a study on this.
On the other hand, the handbook cited below are based on inquiry data.

| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 1-4 | high | probably | very good | very high | high | yes | high | 
   
As a result, we should only do this in case we aim to include non-general, manufacturer-specific data.

### Analytical data

#### Physical and chemical analysis

Physical and chemical analysis of individual components may unveil all the relevant data, but also may be quite expensive. 
No open (or even closed) project is known having systematically done this for electronic components.
The result will probably be on a chemically low level, i.e. chemical elements instead of materials  
At least, some data gaps have been filled in the past using these techniques, e.g. analysing the composition of integrated circuits.

See paper by Michael Riess and Peter Müller: Combination of Data Base Systems and Material Assay Testing: Answer Increasingly Complex Material Related Questions with Confidence


| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 1 | theoretically high | probabaly yes | good | very high | high | no | very high |

As a result, we should at least get an idea of the effort this would take and the results this would bring.

#### Waste analysis

Recycling of waste of electrical and electronic equipment (WEEE) is based on quite well documented analysis of what's in it. 
The disadvantage is, that the analysis is not done on a component level but on at least modules (hard disks, cover, PCB populated with components, batteries, etc.) if not devices (smartphone, laptop, etc.), because complete dismantling is expected to be way to expensive.
Even PCBs are measured with most of the components, not unpopulated.
Additionally, only a few materials are analysed which are of special interest for recycling.

Here's an example for 2 LCI databases based on WEEE analysis: https://www.ecosystem.eco/en/article/environmental-analyses-products

| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 1 | bad on component level | very good | bad | weak | low | ? | none | 

As a result, there's little use for this data, if any.

### General LCI databases

Life Cycle Assessments of electronic devices need component composition data to access the (environmental) impact of the production phase.
Typically, these are based on inventory data within Life Cycle Inventory (LCI) databases. 
 
There are quite some LCI databases which contain at least a bit of data on generic electronic components:
* [ecoinvent v3](https://www.ecoinvent.org/database/how-to-use-ecoinvent-3-online/how-to-use-ecoinvent-3-online.html). 
You need to create an account. Search for a component, click on the UPR view and look for Unit Process Exchanges From Technosphere. 
Weight of component is missing. LCI view not available for Guest users.
For the electronic components, as Sources only the ecoinvent v2 reports are mentioned. Additionally, as an example, the data for the MLCC (see Example data below) seem to be based on the v2 data, see next
* [ecoinvent v2](https://www.ecoinvent.org/database/older-versions/ecoinvent-version-2/ecoinvent-version-2.html) is outdated, un-maintained, but well documented via reports (PDF) with unrestricted access. 
Compared to v3, for electronics, there seems to be no update to the data set regarding LCI data (indicators: no update in release notes of 3, equal number of "capacitor" processes), but modelling changed. Through the web, even v2 isn't free accessible, even less than v3... web interface may be broken (java script problems?).
As an example, the data for MLCC (see Example data below) is based on ("main data sources"):
  * Behrendet et. al. (see Handbooks) which itself is sourced by "Philips: Passive Components Chemical Composition of Products, Eindhoven 1994" which insn't available anymore (part of Vishay now)
  * ZVEI Umbrella Specs (see section FMD)
  * Venkel Material Composition MLCCs (see section FMD)
* [GaBi](http://www.gabi-software.com/international/databases/gabi-data-search/). 
Contains 251 processes: Batteries, assembly lines, cables, capacitors of various kinds, coil, diodes, ICs in various packages and pin counts, PWBs, resistors, solder pastes, transistors, LED SMDs, ring core coils, FR4 substrates, thermistors, speaker, microphone and others.
There was an update in 2020. 
Search for a component, look at the Output section at the very end of the selected process data set, and click to open the flow. 
Data is available for only few process and may not be complete in all cases. Some of the original sources are specified.
Regarding electronics, [GaBi Electronics Extension](http://www.gabi-software.com/support/gabi/gabi-6-lci-documentation/extension-database-xi-electronics/) contains many more variations on components than ecoinvent.
As an example, the data for MLCC (see Example data below) is based on:
  * Datapage on passive components, http://www.bourns.com
  * Datapage on passive components, http://www.epcos.com (see TDK in section FMD)
  * Datapage on passive components, http://www.vishay.com
  * Electronic Components Division within the German Electr. and Electronic Manufacturers Assoc. (ZVEI), ZVEI, ZVEI, Available on-line, 2010
  * Individual industry data, 2010
  * Development of Environmental Performance Indicators for ICT Products on the example of Personal Computer, Project consortium of EPIC-ICT, 2004-2006
* [LCA Commons](https://www.lcacommons.gov/lca-collaboration/), the U.S. Life Cycle Inventory Database. 
Search for "electronic" amd you will see very general processes, and only system processes, it seems. Data is under U.S. Public Domain (https://data.nal.usda.gov/dataset/lca-commons). There seems to be a tight relationship with OpenLCA: "The Federal LCA Commons data platform uses the openLCA Collaboration Server application. The Federal LCA Commons allows users to access and download data in the JSON-LD or ILCD formats, or fetch data directly into their local openLCA desktop application."
* [CPM](http://cpmdatabase.cpm.chalmers.se/Start.asp). See Process Class "Manufacturing" which includes electronic parts. Datasets seem to be quite old. License Aggreement: http://cpmdatabase.cpm.chalmers.se/Document/LCADB_License%20agreement_20141111.pdf 
* [JLCAS](https://home.jeita.or.jp/ecb/lcaguide.html). JEITA LCA for semiconductors. Its a special database for semiconductor electronics with only few datasets, compiled by the Japan Electronics and Information Technology Association (JEITA) 
* [ProBas](https://www.probas.umweltbundesamt.de/). See "Prozesskategorien -> Materialien und Produkte -> Sonstige Produkte -> Elektronikprodukte" with 3 special data sets ("IC-Fertigung") on ICs plus one for TFT-LCDs, both based on https://www.umweltbundesamt.de/publikationen/schaffung-einer-datenbasis-zur-ermittlung (German), and one data set for "Wafer" fabrication, based on https://www.umweltbundesamt.de/publikationen/oekologische-oekonomische-aspekte-beim-vergleich 
* [ESU-services Ltd.](http://esu-services.ch/data/public-lci-reports/) offers some free LCI/LCA processes, but currently none of specific eletronics interest, but at least "Flexcell amorphous silicon modules" and "Photovoltaics"  
* [IDEA](http://idea-lca.com/?lang=en) has some data on "Electronic Parts and Devices", see https://nexus.openlca.org/search/Database=idea!Category=29%20Electronic%20Parts%20and%20Devices, but no further helpful process documentation can be found.
* [MY-LCID](https://mylcid.sirim.my/my-lcid/processSearch.xhtml) (Malaysian LCI database) Search for classification "Systems / Electric and electronics". Access only after login. Might be based on GaBi. 
* [EIME](https://codde.fr/en/our-software/eime-en/eime-presentation) is said to have electronics data, but no documentation found

To keep updated on available LCA databases, it's a good idea to visit https://nexus.openlca.org/databases and https://nexus.openlca.org/search from time to time.
Additionally, https://eplca.jrc.ec.europa.eu/LCDN/datasetList.xhtml (General use), https://eplca.jrc.ec.europa.eu/EUFRP/ (for EU funded projects), and https://eplca.jrc.ec.europa.eu/SDPDB/ (for small data providers) are open databases for free datasets, hosted by the EU, and https://www.globallcadataaccess.org/ is a global database for dataset providers.

These databases are based on the primary data mentioned above, e.g. full material disclosures by companies or dedicated analysis to fill data gaps.
In some cases, the primary data source is linked, in many cases they remain non-free, though.
LCI datasets mostly seem to get updates within a 5-year-frame. They are not freely available as a database. 

It is unknown to us, whether the data itself may be freely used. 

There are standards on LCI database formats like SPOLD, SPINE, or ILCD.

| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 3-4 | sometimes good, mostly bad | very good, if not closed | good | very high | low | mostly | high | 

As a result, since LCI databases provide just the general level of data we need, we need to check the open-ness of these and then base our work on it.

### Handbooks and Studies

Just like LCI databases, some handbooks or final project report collect and publish primary data data on component composition:
* Active and passive components, PCB, ray-tube, casing: _Behrendt, S., Kreibich, R., Lundie, S., Pfitzner, R., Scharp, M.: Ökobilanzierung komplexer Elektronikprodukte. 
Innovationen und Umweltentlastungspotentiale durch Lebenszyklusanalyse, 1998_ (German)
* DRAM, TFT-LCD: _S Prakash, R Liu, K Schischke, L Stobbe, CO Gensch: Schaffung einer Datenbasis zur Ermittlung ökologischer Wirkungen der Produkte der Informations- und Kommunikationstechnik (IKT) - Teil C, 2013_
* HDD, CPU, PCB: _Siddharth Prakash, Florian Antony, Dr. Andreas Köhler, Ran Liu: Ökologische und ökonomische Aspekte beim Vergleich von Arbeitsplatzcomputern für den Einsatz in Behörden unter Einbeziehung des Nutzerverhaltens (Öko-APC), 2016_ (German)
* SDD: _B Schödwell, R Zarnekow: Kennzahlen und Indikatoren für die Beurteilung der Ressourceneffizienz von Rechenzentren und Prüfung der praktischen Anwendbarkeit, 2018_ (German)
* IC, PCB: _Nils F. Nissen: Entwicklung eines ökologischen Bewertungsmodells zur Beurteilung elektronischer Systeme, 2001_ (German)
* Active and passive components, PCB, switch: _HL Kramer, G Brändli: Ökologische Bewertung von Elektronik-Komponenten und bestückten Leiterplatten, 2006_ (German)
  * Some more active and passive components: _MT Stutz, HJ Tobler: Simplified LCA für Elektronikbauteile mit neun Bauteilefamilien, 1998/2009_ (German)

The data is based on primary company data. Due to acquisitions, for example the data by Philips Passives and Beyschlag isn't available anymore. 
They merged to BCcomponents which has been acquired by Vishay (http://www.vishay.com/company/brands/bccomponents/). Vishay doesn't publish FMD anymore. 

| Level of information | Extend of information | Machine readibility | Timeliness | Reliability | Effort | Licensed | Costs |    
|----------------------|-----------------------|---------------------|------------|-------------|--------|----------|-------|
| 1-4 | weak | no | mostly bad | very good | low | ? | none | 

As a result, LCI databases are more comfortable to use.

## Example research 

### SMD Ceramic Capacitor (MLCC)

As an example, here's what can be obtained from the sources mentioned above about the following component: 
[MLCC (ceramic) capacitor](https://en.wikipedia.org/wiki/Ceramic_capacitor#Multi-layer_ceramic_capacitors_(MLCC)) 
* Capacitance 10nF (104 = 10*10^4)
* General Purpose, Commercial Grade
* Base metal electrode = BME (i.e. no precious "noble" metals electrode = NME) 
* SMD Chip size (EIA): 0603 (1,6mm x 0,8mm x 0.8mm)
* Dielectric X5R oder X7R (Class 2), based on NIC Components, that makes no difference
* Tolerance: +/- 10%
* Rated Voltage: 50V DC 

LCA sources include full production means (incl. auxilaries), FMDs only what's in the component, that's why we have two tables.
Auxilaries listed here don't include water, electricity, and fuel. 
The first table with the LCA gives material means in rounded mass units (e.g. gramm or kg, some cut at < 0.01) necessary for 1 mass unit of the component. The second table with FMDs have rounded percentages (cut at < 1) of material content.

GaBi doesn't have free data, but at least for MLCC the flow of the components material is published, so we added it to the second (FMD) list.
ecoinvent data is equal vor v3 and v2, but occurs two times anyway, because apart from LCA production data, the report contains a table with FMD data, which for some reason don't fit well to each other.

Regarding the manufacturers, I scanned all suppliers listed on https://www.marketwatch.com/press-release/global-multilayer-ceramic-chip-capacitors-mlcc-market-2018-industry-key-companies-share-trend-segmentation-analysis-forecast-to-2025-2018-10-08

| Source | Data (link) | Most fitting component | Weight | Ceramics | Ni | Co | Sn | Remains | Auxiliaries | Date | Remark |
|---------------|------|------------------------|--------|----------|----|----|----|---------|-------------|------|--------|
| ecoinvent | [Unit Process](https://v36.ecoquery.ecoinvent.org/Details/UPR/8ae366ac-dbde-411e-b8bd-c3605b075b2f/8b738ea0-f89e-4627-8679-433616064e82) | capacitor production, for surface-mounting, GLO | 86mg (See remark) | BaSO4 0.79, TiO2 0.40 | 0.20 | 0.01 | 0.02 | Ag 0.04, Pd 0.001, glass 0.002 | "Chemicals" 0.79 | 2007 | Not BME but NME. Size unknown. BaSO3 is proxy for BaCO3. Waste of 1.13 mentioned, of which 0.48 unused raw material, therefore input of 1.48 raw material
| CPM | [ILCD](http://cpmdatabase.cpm.chalmers.se/ILCD/data/processes/CPM%5Fprocess%5FECOP3223%5Fbb09ea99%2Ddc06%2D4171%2D8e79%2Df341777b9a8f.xml) or [SPINE](http://cpmdatabase.cpm.chalmers.se/Scripts/sheet.asp?ActId=ECOP3223) | Capacitor for surface mounting assembly | 27,94mg (see remark) | "Ceramic" 1.18 | 0.0064 | - | 0.0093 | Ag 0.1, Pd 0.0057 | Butyl acetate 0.08, Dibutyl phthalate 0.07, Ethyl acetate 0.79, Methoxypropanol 0.11, Naphtha 0.09, Polyacrylate 0.03, Solvent 0.03 | 1998 or 2000 | NME, not BME. Only chip size 1206 available (3.2x1.6x1.6) which is 8 times the 0306. Lots of waste (> 1.5) output specified.
| JLCAS | [Excel](https://home.jeita.or.jp/ecb/data/Chip_ceramic_Capacitor.xls) | Chip ceramic capacitor_0603 | 0.326mg | "Ceramic" 0.28 | 0.05 | 0.04 | 0.005 | | Solvent 0.59, Organic binder 0.18, plastic film 0.63 | 2007 | 
| MY-LCID | [ILCD](https://mylcid.sirim.my/my-lcid/showProcess.xhtml?uuid=618c0e49-2758-41cb-8a18-223ddcea1940&stock=PUBLIC) | login necessary |

| Source | Data (link) | Most fitting component | Weight | Ceramics | Ni | Cu | Sn | Remains | Date | Remark |
|---------------|------|------------------------|--------|----------|----|----|----|---------|------|--------|
| ecoinvent | [PDF](https://db.ecoinvent.org/reports/18_I_Electronics_components.pdf?area=463ee7e58cbf8) | capacitor, SMD type, for surface-mounting, at plant (GLO) | 86mg | BaTiO3 39%, CaTiO3 5%, CaMgTiO3 7%, BaTiO3 or CaZrO3 2% | 14% | 1% | 2% | Ag 3% | 2007 | NME, not BME |
| GaBi | [ILCD](http://gabi-documentation-2019.gabi-software.com/xml-data/processes/e2e363bf-720f-4267-b2b9-a8f2a2c30587.xml) | Capacitor ceramic MLCC 0603 (6mg) D 1.6x0.8x0.8 (Base Metals) | 6mg | Al2O3 66% | 16% | 13% | 4% | | 2011 or 2018 | 
| ZVEI | [Umbrella Specification](https://www.zvei.org/fileadmin/user_upload/Verband/Fachverbaende/Electronic_Components_and_Systems/Material_Content_Data/Umbrella_Specification_MLCC.pdf) | MLCC 47 nF 5% X7R 25V 0603 example spec. | 6.29mg | "ceramics" 69% | 12% | 16% | 2% | glass 2% | 2018 |
| Behrendt | PDF | Oberflächenmontierter Kondensator (SMD) | 10-30mg | BaTiO3 78% | Pd+Ag+Ni 19% | | 3% | Pd+Ag ? | 1998 | NME only, not BME
| Kramer | PDF | Vielschicht Keramik Kondensator (0603) | 5.8mg | BaTiO3 82% + doping 4% | 2% | 11% | 1% | | 2006 |
| Venkel | PDF, page 11, 0603 X5R Ceramic Capacitors (C) | [C0603X5R500-104MNP](https://www.venkel.com/ceramic-capacitors/general-purpose/c0603x5r500-104mnp) | 5.5mg | BaTiO3 91% | 3% | 5% | | glass 1% | 2019 (?) |
| Yageo | [PDF](https://www.yageo.com/upload/website/yageo_YAGEO%20MLCC%20SDS-NPO%20X5R%20X7R%20X6S%20X7S%20Y5V_No.%20YP%20SDS-190115-001_20190115_19050911_638.pdf) | [CC0603KRX7R9BB104](https://www.yageo.com/upload/media/product/products/datasheet/mlcc/UPY-GP_NP0_16V-to-50V_18.pdf) | ? | BaTiO3 45-94% | 2-19% | 3-26% | 0-19% | | 2019 |
| Holy Stone Int. | [PDF](https://www.holystoneeurope.com/wp-content/uploads/2018/01/ROHS-material-declaration-MLCC-2019.pdf) | NCC series, C0603B104K050T | ? | Ba 47%, Zr 4%, Ca 2%, Sr 1%, Ti 24% | 15% | 4% | 3% | | 2019 (?) |
| NIC Components | [IPC PDF](https://www.niccomp.com/resource/files/ipc/NMC0603X5R%20Class%206.pdf) | NMC X5R series, NMC0603X5R104K50 | 5.5mg | BaTiO3 82%, BaCO3 5%, Y2O3 5% | 3% | 5%  | | glass (frit) 1% | 2004 |

### USB cable



### Microprocessor Unit (MCU) as an example for a complex CMOS-based logic IC

Here's an example for an MCU:
* ARM-Cortex-M0-based MCU
* Reconsider: Size depends on flash memory, I think! 

By NXP (actually an M4?)
* 256KB Flash + 64KB FlexMem, 32KB RAM
* Component family: https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/general-purpose-mcus/ke-series-cortex-m4-m0-plus/kinetis-ke1xf-168mhz-performance-with-can-5v-microcontrollers-based-on-arm-cortex-m4:KE1xF
* Component: MKE16F256VLH16 https://www.nxp.com/part/MKE16F256VLH16#/
* FMD linked in Website as PDF: https://www.nxp.com/mcds/MKE16F256VLH16.pdf
* FMD as IPC1752: https://www.nxp.com/mcds/MKE16F256VLH16_IPC1752A.xml and https://www.nxp.com/mcds/MKE16F256VLH16_IPC1752_v11.xml

By STMicroelectronics:
* 32 Kbytes Flash
* Component family: https://www.st.com/en/microcontrollers-microprocessors/stm32-mainstream-mcus.html
* Component: https://www.st.com/en/microcontrollers-microprocessors/stm32f030c6.html#quality-reliability
FMD Disappeared???? We have a copy.

By Infineon ARM-Cortex-M0, recommended for automotive window lift???
* RAM 2.0 kByte, Solid Flash 40.0 kByte 
* Component family: https://www.infineon.com/cms/en/product/microcontroller/embedded-power-ics-system-on-chip-/relay-driver-integrated-arm-cortex-m0/
* Component: https://www.infineon.com/cms/en/product/microcontroller/embedded-power-ics-system-on-chip-/relay-driver-integrated-arm-cortex-m0/tle9842-2qx/

What about ON Semiconductor and Texas Instruments and Analog Devices and Microchip?

LCA sources:
* http://cpmdatabase.cpm.chalmers.se/Scripts/sheet.asp?ActId=KI-2010-07-21-197
* Ecoinvent
* ProBas or its sources
* Other literature?
* https://www.zvei.org/fileadmin/user_upload/Verband/Fachverbaende/Electronic_Components_and_Systems/Material_Content_Data/Umbrella_Specification_IC.pdf

ICs consist of:
* Die: Chip consisting mainly of silicon (to be more specific: "doped silicon", but it is never declared, how it's doped) , fabricated from a wafer
* Lead Frame: Base frame to put die on including the pins, reaching near the die
* Die Attach / Glue: Putting die on frame
* Bonding Wire: Connecting pins to die
* Mo(u)ld Compound / Die Encapsulation: The (mainly black) case surrounding die, wire and its encapsulation and most of the frame leaving only the pins of the frame
* Plating / Finish: Covering of the pins supporting electrical connectivity  

Note: In FMD mostly (and in all FMD used here) the material shares are listed as ppm = parts per million, i.e. an alternative to %

| Source | Data (link) | Most fitting component | Weight | Die size | Case size | Package | Pins | Die | Bonding Wire | Die Attach | Mould Compound | Lead Frame | Plating | Auxiliaries | Date | Remark |
|--------|-------------|------------------------|--------|----------|-----------|---------|------|-----|--------------|------------|----------------|------------|---------|-------------|------|--------|
| NXP | see above | see above | 346.55 mg | ? | 10x10x1.4mm | (L)QFP | 64 | 2.5% Si (doped), 0.05% misc. | 0.65% Cu, 0.01% Pd, 0.0007% Au | 0.05% Ag, 0.01% polymers | 65% SiO2, 6.52% polymers, 0.72% epoxy resins, 0.14% carbon black | 22,36% Cu, 0.23% Ag, 0.55% Fe, 0.02% misc., 0.004% Pb, 0.007% Sn, 0.03% Zn | 0.01% Sn | not spec. in FMD | 2017 | |
| ST Microelectronics | see above | see above | 195 mg | ? | 7x7x1.4mm | (L)QFP | 48 | 2.64% Si, 0.048% SiO2, 0.038% Cu, 0.008% Al, 0.007% Co, 0.002% Ti, 0.004% W, 0.005% Si3N4 | 0.185% Ag, 0.008% other metals | (no category) | 55.17% SiO2, 6.15% polymers, 0.38% carbon black | 31.88% Cu, 2.2% Ag, 0.76% Fe, 0.026% P, 0.039% Zn, 0.002% Pb | 0.55% Sn | not spec. in FMD | 2017 | |
| Infineon | see above | see above | 129.64 mg | ? | 7x7x0.5mm | (V)QFN | 48 | 2.64% Si | 0.46% Cu | 0.17% epoxy resin, 0.57% Ag | 41.43% SiO2, 6.05% epoxy resin, 0.14% carbon black | 44.89% Cu, 1.11% Fe, 0.06% Zn, 0.01% P | 0.47% Ag, 2% Sn | not spec. in FMD | 2018 | |  

### Timers and Gates as examples for simple logic ICs

Although there are millions of different logic ICs for any purpose, for simple logic ICs we expect them to be quite similar concerning the material content.
What differentiates them from a material point of view is the number of pins and the packaging. Has the function and complexity of the logic a significant effect on the size of the die or the materials used to manufacture them?
 
For example, Texas Instruments ships lots of ICs with all kind of functionality in many different packaging, and seem to provide FMDs for all of them.
We took the logic gate chips as a substitute for all logic ICs.

GaBi has quite a few variants of IC (although not specified a logic chip) having different packages and pin count: 
BGA 144/256/672, DIP 8/24/42, PLCC 20/44/68, QFP 32/80/240, SO 8/20/44, SSOP 14/24/64, TQFP 32/44/100, and LGA 1366.

### Dynamic RAM as an example for memory IC

Currently, none of the most relevant DRAM manufacturers (SK Hynix, Micron, Samsung, Nanya, Winbond, Powerchip) provide FMDs. 
In most devices, DRAM is built in as DRAM modules (DRAM chips on PCB suitable for use in DRAM connectors), anyway, and module manufacturers are different from them.
Anyway, they won't provide FMDs, even less.  

At least, there's data on EEPROM, SRAM, and Flash Memory from ON Semiconductor, and a quite extensive list of logic circuits at Texas Instruments which may serve as an substitute.

Regarding LCA, there's an data set at ecoinvent, GaBi, and ProBas. The latter is well documented in Prakasch (2016).
GaBi contains DRAM as well as Flash memory chip both of the following kind: TSOP 28/32/56 and TSSOP 8/16/48.

### Printed Circuit Board (PCB), unpopulated

Here are lists of manufacturers:
* https://www.prnewswire.com/news-releases/global-printed-circuit-board-pcb-market-expected-to-reach-an-estimated-89-7-billion-by-2024--with-a-cagr-of-4-3-from-2019-to-2024--300879227.html
* https://www.marketwatch.com/press-release/global-printed-circuit-board-pcb-market-is-expected-to-reach-7764226-million-by-2023-at-a-cagr-of-328-2019-11-14

No data available at AT&S, Hofmann, hmp, Würth, Schweizer, KSG / Häusermann, elvia, Meiko.
There's lots of information at https://www.multi-circuit-boards.eu/en/products/printed-circuit-boards.html and https://www.electronicprint.eu/, but none on material composition directly 

LCA databases only publish data on the materials used during production, not what's left in the PCB for the device. It's nearly impossible to devide the one from the other.
The same applies to the literature mentioned with one exception: Nils Nissen discusses in detail the actual contents alone.
The companies themselves may publish some basic data on the composition which may be used to compile material data formula, but we didn't do a depth search on this.

GaBi has PCB data for 1,2,4,8,10,12,16 layer each with chem-elec AuNi, chemSn elecAuNi, or HASL finish

LCA:
* GaBi: http://gabi-documentation-2019.gabi-software.com/xml-data/processes/4c8354fe-5b5d-4502-8847-cc16bb5d51ea.xml with flow http://gabi-documentation-2019.gabi-software.com/xml-data/flows/19126c2d-9766-4159-b6c6-4897552eb466.xml
* CPM (very simple): http://cpmdatabase.cpm.chalmers.se/Scripts/sheet.asp?ActId=KI-2010-07-14-174

Regaring Nissen (2001), a typical FR4 multilayer PCB consists of epoxy resin, fibreglass, and mainly copper. 
For the former two, he presents detailed percentage values, the formular for the metals is more complex.
For PCB there's a huge difference for the copper used in production (full layer) opposed to the copper which remains as circuit path.
The amount of copper depends on the number of layers, the thickness of the copper layers and the holes. 
Unfortunately, no actual weight is mentioned. Also, the finishing and many auxilary metals are missing.

See also Prakash et.al 2016 for detailed information.

Here's how we calculated 1-layer FR-4-based PCB without finishing:
* The weight is calculated using https://www.leiton.de/leiton-tools-weight-calculation.html. Taking 1 layer, quantity 1, 1000x1000mm, copper thickness 35um (typical for 1 layer), and 1,5mm thickness, results in 3.68kg per m2 or 0.368g per cm2. GaBi only accounts half of it for a similar 1-layer PCB. ecoinvent has quite the same value for a 6-layer PCB.
* Copper-foiled PCBs consist of (1) glass fiber, (2) epoxy resin, and (3) copper. Based on the safety data sheet (https://www.isola-group.com/wp-content/uploads/ED130UV-Laminate-SDS-2016.10.03-1.pdf) of a typical FR4 (https://www.isola-group.com/products/all-printed-circuit-materials/de104/) an average weight distribution is (1+2)80%, (3)20%. According to Nissen (2001), the weight distribution of glass to resin is 56:44, therefore we take (1)45% and (2)35%
* Nissen (2001) lists the contents of glass fiber and epoxy resin, but in the end, we don't need this detail of information
* In addition 
  * solder stop (making the PCB green) which we ignore, because there's a lack of material data
  * finishing (surfacing the bar copper), consisting mostly either of (a) lead-free HAL or (b) chemical gold, see https://www.multi-circuit-boards.eu/en/pcb-design-aid/surface/surfaces.html . For the finishing, we simply add 1/1000 of the copper weight using 98% nickel and 2% gold (taken from https://www.multi-circuit-boards.eu/en/pcb-design-aid/surface/surfaces.html) and take it from the copper.  

### LED (in the meaning of light emitting diodes, not LED lamps)

We didn't find any FMD on SMD-LED or THT-LED (through-hole-technology, i.e. leaded). 
Ecoinvent confirms and simply takes a glass-diode, which is definitely different: "Due to a lack of respective information concerning the actual composition of LEDs, the composition of the dataset 'Diode, glass-, for through-hole mounting, at plant' is used as a proxy."

GaBi has [processes on LEDs](http://www.gabi-software.com/deutsch/datenbanken/gabi-daten-suche/?id=8323&no_cache=1&tx_fufgabilcidocumentation_pi1%5BAdvancedSearch%5D=0&tx_fufgabilcidocumentation_pi1%5Bsuchbegriff%5D=LED&search=Search&tx_fufgabilcidocumentation_pi1%5Bmatch%5D=2&tx_fufgabilcidocumentation_pi1%5BCountry%5D=&tx_fufgabilcidocumentation_pi1%5BProcessTypeName%5D=&tx_fufgabilcidocumentation_pi1%5BDatabaseName%5D=%7B722a641f-c5e9-4c6b-8d51-ef098e9bca4d%7D&tx_fufgabilcidocumentation_pi1%5BProcessDataSource%5D=) 
for example [LED THT 5mm](http://gabi-documentation-2019.gabi-software.com/xml-data/processes/4317217e-191b-49ec-9bfe-f3c278166fb0.xml)
or [LED SMD max 50mA (35mg) without Au](http://gabi-documentation-2019.gabi-software.com/xml-data/processes/4a91d284-76d9-45fa-9f52-b645a76fc7c4.xml) 
but cites only general semiconductor sources plus non-disclosed "Publicly available material declarations data, 2014-2016" and adds:
"LEDs are also based on the parametric IC model. This refers to an estimation that the gallium, arsenic, selenic or other mono-crystalline semiconductor materials may be estimated by the mono-crystalline silicon model since the processing steps are nearly identical from an environmental stand-point. The plastic cap of the THT LED is estimated as epoxy resin because of the provided boundaries of the model. In addition to the standard modules, the parametric model of SMD-type LEDs contains the following modules: - Substrate consisting of ceramics (film cast) and is contained in all datasets - Reflector consisting of aluminum foil - Lens consisting of silicone"

There is literature on how LEDs are constructed, but it mostly covers the semiconductor part. [Wikipedia](https://en.wikipedia.org/wiki/Light-emitting_diode_physics#Materials) gives hints on which materials or used for which colors.
In short, LEDs are axial pn-Diodes with gallium(-arsenide)-phosphate (which emits photons) as semiconductor instead of usual silicon or germanium (which emits heat).
Unsually the casing is quite different to simple diodes.

Additionally, simply measuring sizes of actual LEDs may help. 

### Solder

Here especially: How much solder is used in a device?


## Fairtronics predefined components

| component name | source for materials data | date | remark | alternative source |
|----------------|---------------------------|------|--------|--------------------|
| Aluminium Electrolytic Capacitor SMD 6.3x5.5 | FMD for [NACE 6.3x5.5 Series](https://www.niccomp.com/resource/files/ipc/NACExxx6.3x5.5.pdf) by NIC Components | 2004-10-01 | 2,7% remain non-disclosed. | No other source found yet. |
| Aluminium Electrolytic Capacitor SMD 10x8 | FMD for [NACE 10x8 Series](https://www.niccomp.com/resource/files/ipc/NACExxx10x8.pdf) by NIC Components | 2004-10-01 | 2,7% remain non-disclosed. | No other source found yet. |
| Multilayer Ceramic Capacitor SMD 0603 Class I BME | FMD for [NMC NP0 0603 Series](https://www.niccomp.com/resource/files/ipc/NMC0603NPO%20Class%206.pdf) by NIC Components | 2004-10-01 | | The FMD of 0603 C0G Ceramic Capacitors (C) by Venkel is of same weight, but quite different
| Multilayer Ceramic Capacitor SMD 0603 Class II BME | FMD for [NMC X5R 0603 Series](https://www.niccomp.com/resource/files/ipc/NMC0603X5R%20Class%206.pdf) by NIC Components | 2004-10-01 | | The FMD of 0603 X5R Ceramic Capacitors (C) by Venkel is nearly identical
| Zener Diode SOD-123 | FMD for [MMSZ10: 500 mW 10 V ±5% Zener Diode Voltage Regulator](https://www.onsemi.com/PowerSolutions/MaterialComposition.do?export=pdf&opnId=MMSZ10T3G) by ON Semiconductors | 2020-01-30 | | Quite complicated... Diodes Inc. have similar z-diodes (e.g. https://www.diodes.com/part/view/MMSZ5221B), Nexperia as well (e.g. https://www.nexperia.com/products/diodes/automotive-diodes/automotive-zener-diodes/BZT52-C75.html), but all are quite different. ON Semiconductor seems to have quite some market share and has recent data in IPC format. MCC has z-diodes as well (https://www.mccsemi.com/ProductCategories/ZenerDiodes) but the filtering on the website doesn't work well.
| Zener Diode SOD-523 | FMD for [MM5Z2V4: 500 mW Standard Tolerance Zener Diode Voltage Regulator](https://www.onsemi.com/PowerSolutions/MaterialComposition.do?export=pdf&opnId=SZMM5Z9V1T1G) by ON Semiconductors | 2020-01-30 | | Quite complicated... Diodes Inc. have similar z-diodes (e.g. https://www.diodes.com/part/view/DDZ9699T), Nexperia and MCC probably as well 
| Small Signal Schottky Diode SOD-323 | FMD for [BAT46WJ Schottky barrier diode](https://www.nexperia.com/products/diodes/automotive-diodes/automotive-schottky-diodes-and-rectifiers/automotive-general-purpose-schottky-diodes-250-ma/BAT46WJ.html) by nexperia (Ex-NXP, Ex-Philips) | 2018-04-25 | | BAT46JFILM by ATMicroelectronics (SOD-123 as well), BAT46W by Diodes (only SOD-123) |
| Schottky Barrier Rectifier Diode DO-214AC | FMD for [SK54A-L Schottky Barrier Rectifier](https://www.mccsemi.com/ProductCategories/ProductDetails?productName=SK54A-L&productType=SchottkyBarrierRectifier) by MCC | 2016-12-20 | DO-214AC = SMA, Date by Excel internal properties | SBRT5A50SA by Diodes, but without FMD |
| PTC Resettable Fuse SMD 2920 | FMD for [MF-LSMF185/33X](https://www.bourns.com/products/circuit-protection/resettable-fuses-multifuse-pptc/product/MF-LSMF) by Bourns | 2010-12-15 | gold plated. maybe not typical | No other source found yet |  
| Ferrite Chip Bead SMD 0603 | FMD for [MH1608](https://www.bourns.com/products/magnetic-products/chip-beads/product/MH) by Bourns | 2018-04-17 | 1608 is the metric case code for the imperial case code 0603 | Venkel seems to have data as well |
| Standard Rectifier Diode DO-41 | FMD for [1N4001G](https://www.onsemi.com/products/discretes-drivers/diodes-rectifiers/rectifiers/1n4001) by ON Semiconductor | 2020-02-07 | currently also used as substitue for LED THT, although this one is black | MCC has similar: https://www.mccsemi.com/ProductCategories/ProductDetails?productName=1N4001&productType=StandardRecoveryRectifier
| Standard Rectifier Diode DO-214AC | FMD for [NRD Series](https://www.niccomp.com/products/pSeries.php?pSeries=NRD) by NIC Components | 2008-08-15 | "CORRESPONDS TO 1N4001 THRU 1N4007 IN SURFACE MOUNT PACKAGE", currently also used as substitute for LED SMD |
| Metallized polyester (PET/MKT) film capacitor, 5mm lead spacing, radial boxed, 2.5×6.5×7.3 | FMD for [MKT FILM (Boxed) B32529C1104Mxxx](https://www.tdk-electronics.tdk.com/inf/20/20/db/fc_2009/B32520_529.pdf) | 2019-11-18 | | Didn't look for alternatives |
| Bipolar Electrolytic Capacitor Radial 5,5 x 12,0 mm | Umbrella Spec for [EKSU](https://www.frolyt.de/wp-content/uploads/EKSU-englisch-07-2019.pdf) from Frolyt | 2019-08-12 | | Didn't look for alternatives |
| Thick Film Chip Resistor 0603 | FMD for [CR0603 Series](https://www.bourns.com/products/fixed-resistors/thick-film-chip-resistors/product/CR0603) by Bourns | 2003-01-04 | | Nearly identical at NIC Components: https://www.niccomp.com/products/pSeries.php?pSeries=NRC |
| Single-ended USB-A cable AWG24 2.0m | FMD for [USB Cable Assembly](https://www.te.com/usa-en/product-1487593-1.html) by TE Connectivity | 2020-01-24 | length 2m | |
| Single-ended USB-A cable AWG24 1.3m | calculated from 2m variant | 2020-01-24 | | |
| Mechanical Incremental Rotary Encoder 12mm Shaftless | FMD for [PES12 Encoder](https://www.bourns.com/products/encoders/contacting-encoders?product=PES12) by Bourns| 2008-06-23 | | |
| Ultra Miniature Micro Switch with Lever 13x7x4mm Through Hole | FMD for [UP01DTANLA04 micro switch](https://www.te.com/usa-en/product-1825043-3.html) by TE Connectivity | 2017-10-11 | seems to be the only switch with an FMD at TE | |
| Ultra Miniature Micro Switch without Lever 13x7x4mm Through Hole | calculated from variant with lever | 2017-10-11 | | |
| Solder Wire HS10 Fair | Supply chain for [HS10 Fair](https://fairloetet.de/wp-content/uploads/2018/10/lieferkette_fairloetet_1115.pdf) by Stannol / FairLötet e.V. | 2018-10-01 | based on refurbished solder with unknown ingredients | |
| Solder Paste Sn96.5Ag3Cu0.5 (SAC) | Safety Data Sheet for [M8 REL22](https://aimsolder.com/sites/default/files/rel22_m8-ghs_america-english.pdf) by AIM Solder | 2017-11-16 | Probably does not contain all materials | |
| General Simple Logic IC in P-DIP with 8 pins | FMD for [CD40107BEE4](http://www.ti.com/materialcontent/en/report?pcid=10896&opn=CD40107BEE4) by Texas Instruments | 2020-02-20 | | |
| 1-layer PCB with FR-4 laminat and NiAu (chem. gold) finish | see README.md | 2020-02-20 | | |
| IC BGA 256 17x17x1.41mm | FMD for [TMS320C28346ZFETR](http://www.ti.com/materialcontent/en/report?pcid=224722&opn=TMS320C28346ZFETR) by Texas instruments | 2020-02-26 | It's an MCU, maybe not typical. GaBi has BGA256 with 27x27 and therefore only a third of weight | |
| IC PLCC 44 16.6x16.6x4.57mm | FMD for [SN74ACT8990FN](http://www.ti.com/materialcontent/en/report?pcid=43776&opn=SN74ACT8990FN) by Texas Instruments | 2020-02-26 | It's a bus controller | |
| IC SO 20 12.8x7.5x2.65mm | FMD for [CDC208NSR](http://www.ti.com/materialcontent/en/report?pcid=17372&opn=CDC208NSR) by Texas Instruments | 2020-02-26 | It's a timer chip | |
| IC TQFP 32 5x5x1.0mm | FMD for [TLV320AIC1103](http://www.ti.com/materialcontent/en/report?pcid=81864&opn=TLV320AIC1103PBSR) by Texas Instruments | 2020-02-26 | | |
| IC TSSOP 48 6.1x12.5x1.2mm | FMD for [MSP430FR4133IG48R](http://www.ti.com/materialcontent/en/report?pcid=226084&opn=MSP430FR4133IG48R) by Texas Instruments | 2020-02-26 | It's an MCU, so maybe not typical. gaBi has DRAM/Flash for TSSOP 48 | |
| Resistor THT Metal film 4.5x11mm + 28mm axial | FMD for [NMO100 Series](https://www.niccomp.com/resource/files/ipc/NMO100xxxx.pdf) by NIC Components | 2008-08-13 | NOTE: The materials don't sum-up to the specified part weight, not by magnitudes. It sums up to 0.0008mg, but weight is 0.8g (which is reasonable). We multiplied the values by 10e6. Does it include the wire? Note that only part of the 2x28mm wire will be left after mounted on the PCB | Didn't find an alternative, surprisingly |
| Transistor 15V NPN SOT-23 with 3 leads | FMD for [FMMT617](https://www.diodes.com/assets/Part-Support-Files/FMMT617/MDSSOT23eu.pdf) by Diodes Inc. | 2007-08-01 | | |
| Cable Assembly double-ended tin-plated nano-fit female connectors 20AWG 150mm | FMD for [797582139](https://www.molex.com/molex/products/part-detail/cable_assemblies/0797582139) by Molex | 2020-04-17 | | |
| Power Connector dual row male with 20 2.5mm circuits | FMD for [105308-1210](https://www.molex.com/molex/products/part-detail/crimp_housings/1053081210) by Molex | 2020-04-24 | | |
| Flexible flat jumper cable (FFC) with 33 0.5mm circuits opposite side and tin plating 5.1cm | FMD for [98266-1032](https://www.molex.com/molex/products/part-detail/cable/0982661032) by Molex | 2017-05-03 | | |
| IC SOT-23 with 5 pins and tin finish 2.9x1.6x1.45 | FMD for [TLV75533PDBVR](https://www.ti.com/store/ti/en/p/product/?p=TLV75533PDBVR) by Texas Instruments | 2020-04-26 | | |
| IC (MCU) TQFP 32 7x7x1.0mm with tin finish | FMD for [ATmega32U2-AU](https://www.microchip.com/wwwproducts/en/ATmega32U4) by Microchip | 2015-08-17 | Surprisingly heavy | |
| IC (MCU) TQFP 44 10x10x1.0mm with tin finish | FMD for [ATmega32U2-AU](https://www.microchip.com/wwwproducts/en/ATmega32U4) by Microchip | 2015-08-17 | | |
| Standard Diode 100V SOD-323 | FMD for [MMDL914T1G](https://www.onsemi.com/products/discretes-drivers/diodes-rectifiers/small-signal-switching-diodes/smmdl914t1g) by ON Semiconductor | 2020-05-05 | | |
| SMD Crystal with 3.2x 2.5x 0.8mm ceramic package | FMD for [ECX-32](https://ecsxtal.com/ecx-32) by ECS Inc. | ? | | |
| IC (Logic) thermally enhanced QFP 64 10x10x1.2mm | FMD for [TUSB8041IPAPR-Q1](https://www.ti.com/product/TUSB8041-Q1) by Texas Instruments | 2020-05-15 | | |
| IC (Logic) TOSOP 20 4.4x6.5x1.15mm | FMD for [TXS0108EPWR](https://www.ti.com/product/TXS0108E) by Texas instruments | 2020-05-27 | | |
| IC (Power Management) TO-236 (D2PAK) 7 pin 10.1x8.9x4.5mm | FMD for [LM2677](https://www.ti.com/product/LM2677) by Texas Instruments | 2020-06-05 | | |
| IC (Amplifier) TSSOP 16 5x4.4x1mm | FMD for [INA260AIPWR](https://www.ti.com/store/ti/en/p/product/?p=INA260AIPWR) by Texas Instruments | 2020-06-19 | | |
| IC (Power Management) MSOP 8 3x3x0.8mm | FMD for [LTC4359IMS8#PBF](https://www.analog.com/en/products/ltc4359.html) by Analog Devices | 2020-07-11 | | | 
| IC (Power Management) SSOP 44 13x5.3x1.7mm | FMD for [LTC6803IG-4#PBF](https://www.analog.com/en/products/ltc6803-4.html) by Analog Devices | 2020-07-11 | | |  
| IC (Clock & Timing) TSSOP 20 4.4x6.5x1mm | FMD for [5V41066](https://www.idt.com/eu/en/products/clocks-timing/application-specific-clocks/pci-express-clocks/pci-express-clock-generators/5v41066-4-output-pcie-gen12-synthesizer) by Renesas/IDT | 2008-09-23 or 2011-03-15 | | |
| IC (Curcuit Protection) TSSOP 38 9.7x4.4x0.9mm | FMD for [CM2020-00TR](https://www.onsemi.com/products/isolation-protection-devices/esd-protection-diodes/cm2020-00tr) by ON Semiconductor | 2020-08-14 | | | 
| IC (Linear) TQFN 16 5.10mm x 5.10mm x 0.80mm | FMD for [MAX9715ETE+](https://www.maximintegrated.com/en/products/analog/audio/MAX9715.html) by Maxim Integrated | 2020-08-20 | | |
| IC (Interface) SSOP 28 10.5mm x 5.6mm x 1.85mm | FMD for [MCP3919A1-E/SS](https://www.microchip.com/wwwproducts/en/MCP3919) by Microchip | ? | | |
| IC (Power Management) QFN 28 4.0x4.0x0.75mm | FMD for [LTC4162-F](https://www.analog.com/en/products/ltc4162-f.html) by Analog Devices | 2020-07-11 | | | 
| IC (Power Management) SOT-223 6.5x3.5x1.6mm | FMD for [TLV1117-15CDCYR](https://www.ti.com/store/ti/en/p/product/?p=TLV1117-15CDCYR) by Texas Instruments | 2020-08-28 | | |
| IC (MCU) LQFP 48 7.0x7.0x1.4mm | FMD for [LPC11U24FBD48](https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/general-purpose-mcus/lpc1100-cortex-m0-plus-m0/scalable-entry-level-32-bit-microcontroller-mcu-based-on-arm-cortex-m0-plus-and-cortex-m0-cores:LPC11U00) by NXP | 2020-04-11 | | | 
| IC (Power Management) DFN 10 3.0x3.0x0.9mm | FMD for [TPS2561DRCR](https://www.ti.com/product/TPS2561) by Texas Instruments | 2020-09-18 | | |
| Power Connector DC Power Jack 2.5mm 13.3x13.3x9.0mm | FMD for [6643220-1](https://www.te.com/usa-en/product-6643220-1.html) by TE Connectivity | 2016-07-02 | | | 
| Card Connector M.2 M-key 67 pins 4.2x21.9x8.7mm | FMD for [1-2199230-6](https://www.te.com/usa-en/product-1-2199230-6.html) by TE Connectivity | 2019-11-18 | | | 
| Mini PCI Latch | FMD for [48099-5701](https://www.molex.com/molex/products/datasheet.jsp?part=active/0480995701_ACCESSORIES.xml) by Molex | 2020-05-16 | | |
| Pin Header Connector 4-pin right-angle 4-pin male gold-plated | FMD for [22289042](https://www.molex.com/molex/products/part-detail/pcb_headers/0022289042) by Molex | 2020-05-07 | | |
| FFC/FPC Connector with 33 circuits | FMD for [54132-3362](https://www.molex.com/molex/products/part-detail/ffc_fpc_connectors/0541323362) by Molex | 2020-05-21 | | |
| Pin Header Connector 8-pin 2 rows male tin-plated | FMD for [87914-0816](https://www.molex.com/molex/products/part-detail/pcb_headers/0879140816) by Molex | 2020-10-09 | | |
| Pin Header Connector 10-pin 2-rows female | FMD for [105405-1110](https://www.molex.com/molex/products/part-detail/pcb_headers/1054051110) by Molex | 2020-04-15 | | | 
| Pin Header Connector 3-pin male gold-plated | FMD for [22-28-4033](https://www.molex.com/molex/products/part-detail/pcb_headers/0022284033) by Molex | 2020-10-22 | | |
| Pin Header Connector 4-pin male gold-plated | FMD for [22-28-4043](https://www.molex.com/molex/products/part-detail/pcb_headers/0022284043) by Molex | 2020-10-22 | | |
| SD Card Slot Connector | FMD for [1-1775059-1](https://www.te.com/usa-en/product-1-1775059-1.html) by TE Connectivity | 2016-07-25 | | |
| Pin Header Connector 16-pin 2-rows male | FMD for [87914-1616](https://www.molex.com/molex/products/part-detail/pcb_headers/0879141616) by Molex | 2016-12-06 | | | 
| Pin Header Connector 30-pin 2-rows male | FMD for [87758-3016](https://www.molex.com/molex/products/part-detail/pcb_headers/0877583016) by Molex | 2016-12-06 | | |
| IC (Power Management) SOT-23 5-pin 2.9x1.6x1.45mm | FMD for [TLV71311PDBVR](https://www.ti.com/product/TLV713P) by Texas Instruments | 2020-10-29 | | | 
| HDMI Connector female | FMD for [208658-1001](https://www.molex.com/molex/products/part-detail/io_connectors/2086581001) by Molex | 2020-10-27 | | |
| Mini PCI Express Card Edge Connector female | FMD for [67910-5700](https://www.molex.com/molex/products/part-detail/edge_card_connecto/0679105700) by Molex | 2020-04-24 | | | 
| USB Connector female 14.25x13.8x5.12mm | FMD for [0483930003](https://www.molex.com/molex/products/part-detail/io_connectors/0483930003) by Molex | 2017-05-04 | | |
| 3.50mm Stereo Audio/Phone Jack Connector female 12.3x8.4mm | FMD for [0472570001](https://www.molex.com/molex/products/part-detail/io_connectors/0472570001) by Molex | 2020-11-06 | | |
| Micro-USB B Connector female 7.5x2.825x8.2mm | FMD for [1050170001](https://www.molex.com/molex/products/part-detail/io_connectors/1050170001) by Molex | 2020-04-24 | | |
| IC (Interface) HQFP 64-pin 10x10x1.2mm | FMD for [SN65DSI86IPAPQ1](https://www.ti.com/store/ti/en/p/product/?p=SN65DSI86IPAPQ1&keyMatch=SN65DSI86IPAPQ1) by Texas Instruments | 2020-11-06 | | |
| IC (Interface) HQFP 64-pin 10x10x1.2mm | FMD for [SN65DSI86IPAPQ1](https://www.ti.com/store/ti/en/p/product/?p=SN65DSI86IPAPQ1&keyMatch=SN65DSI86IPAPQ1) by Texas Instruments | 2020-11-06 | | |
| Power Inductor shielded fixed SMD-type 12.5x12.5x6.0mm | FMD for [SRR 1260](https://www.bourns.com/products/magnetic-products/power-inductors-smd-shielded/product/SRR1260) by Bourns | 2016-11-17 | | |
| Wirewound Chip Inductor 3.2x2.5x2.2mm | FMD for [CM322522 (1210)](https://www.bourns.com/products/magnetic-products/chip-inductors-wirewound/product/CM322522%20(1210)) by Bourns | 2014-03-04 | | |
| Power Inductor shielded fixed SMD-type 12x12x10mm | FMD for [SRR1260 Series](https://www.bourns.com/products/magnetic-products/power-inductors-smd-shielded/product/SRR1260) by Bourns | 2010-10-28 | | |
| Ethernet/RJ45 modular jack with LEDs 16.2x13.3x20.5mm | FMD for [480749103](https://www.molex.com/molex/products/part-detail/modular_jacks_plug/0480749103) by Molex | 2020-12-04 | | |
| MOSFET (N- or P-Channel) SO-8 4.9x3.9x1.75mm | FMD for [FDS5680](https://www.onsemi.com/products/discretes-drivers/mosfets/fds5680) by ON Semiconductor | 2020-12-04 | | | 
| MOSFET (N- or P-Channel) SOT23 2.9x1.3x1.0mm | FMD for [PMV50EPEA](https://www.nexperia.com/products/mosfets/automotive-mosfets/PMV50EPEA.html) by Nexperia | 2020-12-10 | | |
| Thick Film Chip Resistor 1206 | FMD for [CR1206 Series](https://www.bourns.com/products/fixed-resistors/thick-film-chip-resistors/product/CR1206) by Bourns | 2003-01-04 | | Nearly identical at NIC Components: https://www.niccomp.com/products/pSeries.php?pSeries=NRC |
| Metal Strip Chip Resistor 1206 | FMD for [CRK1206 Series](https://www.bourns.com/products/resistors/metal-strip-chip-resistors/product/CRK0612) by Bourns | 2018-07-04 | | |
| Thick Film Chip Resistor 0603 automotive grade (AEC-Q200) | FMD for [CR0603A Series](https://www.bourns.com/products/fixed-resistors/thick-film-chip-resistors/product/CR0603A) by Bourns | 2018-06-14 | | |
| DIP switch 2 pole with gold flash plating 7.0x7.5x3.5mm | FMD for [1825057-1](https://www.te.com/usa-en/product-1825057-1.html) by TE Connectivity | 2015-12-14 | | |
| Tactile switch SMT 4.5x2.55x3.3mm | FMD for [1977066-1](https://www.te.com/usa-en/product-1977066-1.html) by TE Connectivity | 2020-04-14 | | |
| Memory (DDR) socket connector with 200 positions | FMD for [1717254-1](https://www.te.com/usa-en/product-1717254-1.html) by TE Connectivity | 2020-06-23 | | |
| LED chip red 0603 1.6x0.8mm | FMD for [SML-E12V8W](https://www.rohm.com/products/led/chip-leds-mono-color-type/standard/sml-e12v8w-product) by Rohm | 2016-05-01 | | |
| Micro Speaker 18x13x2.5mm (2g) | FMD for [AS01808AO-3-R](https://www.puiaudio.com/products/AS01808AO-3-R) by PUI Audio | ? | | |
| Battery holder clip size AA (0.25g) | FMD for [54](https://www.keyelco.com/product.cfm/product_id/826/) by Keystone Electronics | 2020-02-20 | | |

Since the Electronics Extension of GaBi is kind of reference for life cycle data on electronics, here's a list of data sets included in GaBi Electronics Extension (XI) as of beginning of 2020.
References to 2004-2014 means that the data set is similar to the one published in 2004. For news look at http://www.gabi-software.com/support/latestupdate/

| Data set name | Included in Fairtronics data? | Search for free, equivalent FMD successful? |
|---------------|-------------------------------|---------------------------------------------| 
| Alkaline cell - LR series (AA) 
| Alkaline cell - LR series (AAA)
| Alkaline cell - LR series (C)
| Alkaline cell - LR series (D)
| Coin cell (Li/Poly-carbonmonofluoride - RB0.8; RS0.65)
| Cable 1-core power 18AWG PVC (16 g/m) D2.9, tinned Cu wire, PVC insulation
| Cable 1-core signal 24AWG mPPE (3.0 g/m) D1.1, tinned Cu wire, mPPE insulation
| Cable 1-core signal 24AWG PE (4.5 g/m) D1.4, tinned Cu wire, PE insulation
| Cable 1-core signal 24AWG PTFE (3.0 g/m) D0.9, tinned Cu wire, PTFE insulation
| Cable 1-core signal 24AWG PVC (4.5 g/m) D1.4, tinned Cu wire, PVC insulation
| Cable 3-core mains power 10A/13A 16AWG mPPE (60 g/m) D6.3, bare Cu wire, mPPE insulation, mPPE jacket, unshielded
| Cable 3-core mains power 10A/13A 16AWG PVC (100 g/m) D8, bare Cu wire, PVC insulation, PVC jacket, unshielded
| Connector Schuko CEE 7/16 (15 g, 2 pins) combination of plastic with metal; 2- pin connector, 15 g overall weight
| Capacitor Al-capacitor axial THT (21g) D21x40
| Capacitor Al-capacitor axial THT (300mg) D3.3x11
| Capacitor Al-capacitor axial THT (6g) D14x25
| Capacitor Al-capacitor radial THT (110mg) D3x5 | yes, but with 5x12 |
| Capacitor Al-capacitor radial THT (15.41g) D18x41 | see above |
| Capacitor Al-capacitor radial THT (5.65g) D12.5x30
| Capacitor Al-capacitor SMD (1.29g) D10x10.2
| Capacitor Al-capacitor SMD (2.54g) D12.5x13.5
| Capacitor Al-capacitor SMD (300mg) D6.3x5.4
| Capacitor Al-capacitor SMD (5.01g) D16x16.5
| Capacitor Al-capacitor SMD (7.89g) D18x21.5
| Capacitor Al-capacitor SMD (80mg) D3x5.4
| Capacitor Ceramic MLCC 01005 (0.054mg) 0.4x0.2x0.22
| Capacitor Ceramic MLCC 01005 (0.054mg) 0.4x0.2x0.22 (Base Metals)
| Capacitor ceramic MLCC 0201 (0.17mg) 0.6x0.3x0.3
| Capacitor Ceramic MLCC 0201 (0.17mg) 0.6x0.3x0.3 (Base Metals)
| Capacitor ceramic MLCC 0603 (6mg) 1.6x0.8x0.8
| Capacitor Ceramic MLCC 0603 (6mg) 1.6x0.8x0.8 (Base Metals) | yes |
| Capacitor Ceramic MLCC 1210 (50mg) 3.2x1.6x1.6 (Base Metals)
| Capacitor ceramic MLCC 1210 (50mg) 3.2x3.2x1.6
| Capacitor Ceramic MLCC 2220 (450mg) 5.7x5.0x2.5 (Base Metals)
| Capacitor ceramic MLCC 2220 (450mg) 5.7x5x2.5
| Capacitor film-capacitor boxed RM15 (3.2g) 17.7x10x16.5 | yes, but with 2g |
| Capacitor film-capacitor boxed RM27.5 (20.4g) 31x21x31
| Capacitor film-capacitor boxed RM5 (600mg) 7.2x6x11
| Capacitor film-capacitor unboxed RM15 (2.6g) 15x7x12
| Capacitor film-capacitor unboxed RM27.5 (11g) 27.5x11x17.5
| Capacitor film-capacitor unboxed RM7.5 (150mg) 7.5x1.5x6.0
| Capacitor tantal SMD E (500mg) 7.3x4.3x4.1
| Capacitor tantal SMD Y (25mg) 3.2x1.6x1.6 | | not found |
| Capacitor tantal SMD Z (8mg) 2x1.25x1.2
| Coil miniature wound SDR0302 (81mg) D3x2.5
| Coil miniature wound SDR1006 (1.16g) D9.8x5.8
| Coil miniature wound SRP1040 (2.652g) D11.8x4.2
| Coil miniature wound SRR0804 (580mg) D10.5x3.8
| Coil miniature wound SRR1806 (3.12g) D18.3x6.8
| Coil multilayer chip 0402 (1mg) 1x0.5x0.5
| Coil multilayer chip 1812 (108mg) 4.5x3.2x1.5
| Coil quad-chokes (2.5g) 14.5x13.3x8.0
| Diode MELF (130mg) D2.6x5.2
| Diode mMELF (40mg) D1.6x3.8
| Diode power DO214/219 (93mg) 4.3x3.6x2.3
| Diode power THT DO201 (1.12g) D5.3x9.5
| Diode power THT DO35 (150mg) D1.76x3.77 | yes, but DO41 |
| Diode signal DO214/219 (14.8mg) 3.9x1.9x1
| Diode signal SOD123/323/523 (1.59mg) 0.8x0.75x1.6 with Au-Bondwire
| Diode signal SOD123/323/523 (9.26mg) 2.4x1.6x1 with Au-Bondwire
| IC BGA 144 (181mg) 10x10mm MPU generic (130 nm node)
| IC BGA 144 (360mg) 13X13mm MPU generic (130 nm node)
| IC BGA 144 (466mg) 13x13x1.75 [based on models 2004-2014]
| IC BGA 1515 (14.6 g) 40x40 mm CMOS logic (14 nm)
| IC BGA 256 (2.62g) 27x27x2.36 CMOS logic (90 nm node) [based on models 2004-2014] | yes, but with 17x17x1.41 and 0.8g |
| IC BGA 256 (4g) 27x27 mm CMOS logic (45 nm node)
| IC BGA 48 (70mg) 6x6x1.1mm MPU generic (130 nm node) [based on models 2004-2014]
| IC BGA 48 (72mg) 8x6 mm MPU generic (14 nm node)
| IC BGA 672 (4.92g) 35x35x2.36 CMOS logic (14 nm node) [based on models 2004-2014]
| IC BGA 672 (4.92g) 35x35x2.36 CMOS logic (65 nm node) [based on models 2004-2014]
| IC BGA 672 (6.6g) 27x27 mm CMOS (14 nm node)
| IC DFN 10 (22.3 mg) 3x3 mm CMOS logic (14 nm node) (new in 2020) | yes |
| IC DIP 24 (1.7g) 35.5x8.2 mm CMOS logic (250 nm node)
| IC DIP 24 (2.59g) 35.5x8.2x3.8 [based on models 2004-2014]
| IC DIP 42 (6.30g) 54.6x14.1x3.9 [based on models 2004-2014]
| IC DIP 8 (480mg) 10.9x6.6x3.3 [based on models 2004-2014] | yes |
| IC DIP 8 (538mg) 10.9x6.6 mm CMOS logic (250 nm node)
| IC LGA 1366 (~5g) 45x42.5x~2.5 CMOS logic (14 nm node) [based on models 2004-2014]
| IC LGA 1366 (~5g) 45x42.5x~2.5 CMOS logic (32 nm node) [based on models 2004-2014]
| IC PLCC 20 (700mg) 8.9x8.9x3.8 [based on models 2004-2014] | | [candidate](http://www.ti.com/materialcontent/en/search?partType=tiPartNumber&partNumber=LM9044V%2FNOPB)
| IC PLCC 20 (751mg) 9x9 mm CMOS logic (250 nm node)
| IC PLCC 44 (2.30g) 16.6x16.6x3.8 [based on models 2004-2014] | yes |
| IC PLCC 44 (2.6g) 16.6x16.6 mm CMOS logic (250 nm node)
| IC PLCC 68 (4.60g) 24.2x24.2x3.9 [based on models 2004-2014] | | [candidate](http://www.ti.com/materialcontent/en/search?partType=tiPartNumber&partNumber=TL16C554IFNR)
| IC PLCC 68 (5g) 24.2x24.2 mm CMOS logic (250 nm node)
| IC QFN 24 (61.6 mg) 4x6 mm CMOS logic (14 nm node) (new in 2020) | as an TQFN 16 with 65.3mg or QFN 28 with 0.051721g |
| IC QFN 76 (578.8 mg) 10x11 mm CMOS logic (14 nm node) (new in 2020)
| IC QFP 240 (6.20g) 32x32x3.5 | | [candidate](https://www.nxp.com/mcds/MC68EN360AI33L.pdf), which is an MCU
| IC QFP 32 (180mg) 7x7x1.5
| IC QFP 32 (184mg) 7x7 mm CMOS logic (90 nm node)
| IC QFP 80 (1.60g) 14x20x2.7 | 64-pin variant 10x10 (0.4g) | [candidate](https://www.nxp.com/mcds/MC56F84766VLK.pdf), which is an LQFP MCU
| IC SO 20 (500mg) 12.8x7.5x2.3 [based on models 2004-2014] | yes |
| IC SO 20 (530mg) 12.8x7.5 mm CMOS logic (90nm node)
| IC SO 44 (910mg) 28.3x13.3x2.3 [based on models 2004-2014]
| IC SO 8 (76mg) 4.9x3.9 mm CMOS logic (90 nm node)
| IC SO 8 (80mg) 4.9x3.9x1.7 [based on models 2004-2014] | | [candidate](http://www.ti.com/materialcontent/en/search?partType=tiPartNumber&partNumber=SN75451BPS)
| IC SSOP 14 (120mg) 6.0x5.3x1.75 | | [candidate](http://www.ti.com/materialcontent/en/search?partType=tiPartNumber&partNumber=74ACT11074DBR)
| IC SSOP 24 (210mg) 8.2x5.3x1.75 | as 28-pin 229mg 10.5x5.6x1.85 | [candidate](http://www.ti.com/materialcontent/en/search?partType=tiPartNumber&partNumber=SN74LVC827ADBR)
| IC SSOP 24 (123mg) 8.2x5.3 mm CMOS logic (65 nm node)
| IC SSOP 64 (340mg) 26x10.2x1.75
| IC TQFP 100 (513mg) 14x14 mm MPU generic (130 nm node)
| IC TQFP 100 (520mg) 14x14x1.0 | | [candidate](http://www.ti.com/materialcontent/en/search?partType=tiPartNumber&partNumber=DS90C387VJD%2FNOPB)
| IC TQFP 32 (70mg) 5x5x1.0 | yes |
| IC TQFP 32 (146mg) 5x5 mm MPU generic (130 nm node) | as 7x7 and 338mg |
| IC TQFP 44 (260mg) 10x10x1.0
| IC TQFP 44 (272mg) 10x10 mm MPU generic (130 nm node) | yes |
| IC TSOP 28 (215mg) 8x13.4x1.2 DRAM [based on models 2004-2014]
| IC TSOP 28 (215mg) 8x13.4x1.2 flash [based on models 2004-2014]
| IC TSOP 28 (232mg) 8x13.4 mm DRAM (57 nm node)
| IC TSOP 28 (232mg) 8x13.4 mm flash (45 nm node)
| IC TSOP 32 (325mg) 8x20x1.2 DRAM [based on models 2004-2014]
| IC TSOP 32 (325mg) 8x20x1.2 flash [based on models 2004-2014]
| IC TSOP 32 (373mg) 8x20 nm DRAM (57 nm node)
| IC TSOP 32 (373mg) 8x20 nm flash (45 nm node)
| IC TSOP 56 (590mg) 14x20x1.2 DRAM [based on models 2004-2014]
| IC TSOP 56 (590mg) 14x20x1.2 flash [based on models 2004-2014]
| IC TSSOP 16 (59mg) 4.4x5.0 mm DRAM (57 nm node)
| IC TSSOP 16 (59mg) 4.4x5.0 mm flash (45 nm node)
| IC TSSOP 16 (65mg) 4.4x5.0x1.2 DRAM [based on models 2004-2014] | yes, as an amplifier with 98.6mg |
| IC TSSOP 16 (65mg) 4.4x5.0x1.2 flash [based on models 2004-2014] | see above |
| IC TSSOP 48 (102mg) 6.1x12.5x1.2 DRAM [based on models 2004-2014] | yes, as an MCU |
| IC TSSOP 48 (102mg) 6.1x12.5x1.2 flash [based on models 2004-2014] | see above |
| IC TSSOP 48 (187mg) 6.1x12.5 mm DRAM (57 nm node)
| IC TSSOP 48 (187mg) 6.1x12.5 mm flash (45 nm node)
| IC TSSOP 8 (23mg) 3x3 mm DRAM (57 nm node)
| IC TSSOP 8 (23mg) 3x3 mm flash (45 nm node)
| IC TSSOP 8 (28mg) 3.0x2.9x1.2 DRAM [based on models 2004-2014]
| IC TSSOP 8 (28mg) 3.0x2.9x1.2 flash [based on models 2004-2014]
| IC WLP CSP 196 (209mg) 12x12x1.41mm CMOS logic (14 nm node) (WLP CSP updated in 2020)
| IC WLP CSP 196 (209mg) 12x12x1.41mm CMOS logic (22 nm node)
| IC WLP CSP 425 (4.78g) 19x19x1.5mm CMOS logic (14 nm node)
| IC WLP CSP 425 (4.78g) 19x19x1.5mm CMOS logic (22 nm node)
| IC WLP CSP 49 (10.2mg) 3.17x3.17x0.55mm CMOS logic (14 nm node)
| IC WLP CSP 49 (10.2mg) 3.17x3.17x0.55mm CMOS logic (22 nm node)
| IC WLP CSP 196 (209mg) 12x12x1.41mm DRAM (57 nm node)
| IC WLP CSP 425 (4.78g) 19x19x1.5mm DRAM (57 nm node)
| IC WLP CSP 49 (10.2mg) 3.17x3.17x0.55mm DRAM (57 nm node)
| IC WLP CSP 196 (209mg) 12x12x1.41mm flash (45 nm node)
| IC WLP CSP 425 (4.78g) 19x19x1.5mm flash (45 nm node)
| IC WLP CSP 49 (10.2mg) 3.17x3.17x0.55mm flash (45 nm node)
| IC WLP CSP 196 (209mg) 12x12x1.41mm MPU generic (130 nm node)
| IC WLP CSP 425 (4.78g) 19x19x1.5mm MPU generic (130 nm node)
| IC WLP CSP 49 (10.2mg) 3.17x3.17x0.55mm MPU generic (130 nm node)
| Key switch Dip (79mg) 11.39x4.5x1.5 | yes, but only 2 switches; this seem to be more, according to the width | 
| Key switch tact (242mg) 6.2x6.3x1.8 | yes, but smaller: 4.5x2.55x3.3 |
| LED SMD high-efficiency with lens max 0.5A (235mg) Au bondwire 9.0x7.0x4.4 | | no, after extensive search |
| LED SMD high-efficiency with lens max 0.5A (235mg) Flip Chip 9.0x7.0x4.4 | | see above |
| LED SMD high-efficiency with lens max 0.5A (59mg) Au bondwire 3.5x3.5x2.0 | | see above |
| LED SMD high-efficiency with lens max 0.5A (59mg) Flip Chip 3.5x3.5x2.0 | | see above |
| LED SMD high-efficiency with lens max 1.5A (61mg) Au bondwire 3.5x3.5x2.0 | | see above |
| LED SMD high-efficiency with lens max 1.5A (61mg) Flip Chip 3.5x3.5x2.0 | | see above |
| LED SMD high-efficiency with lens max 1A (235mg) Au bondwire 9.0x7.0x4.4 | | see above |
| LED SMD high-efficiency with lens max 1A (235mg) Flip Chip 9.0x7.0x4.4 | | see above |
| LED SMD high-efficiency with lens max 1A (60mg) Au bondwire 3.5x3.5x2.0 | | see above |
| LED SMD high-efficiency with lens max 1A (60mg) Flip Chip 3.5x3.5x2.0 | | see above |
| LED SMD low-efficiency max 50mA (35mg) without Au 3.2x2.8x1.9 | | see above |
| LED THT 5mm (350mg) D5x7
| Printed Wiring Board 1-layer rigid FR4 with chem-elec AuNi finish (Subtractive method) | yes |
| Printed Wiring Board 1-layer rigid FR4 with chemSn elecAuNi finish (Subtractive method)
| Printed Wiring Board 1-layer rigid FR4 with HASL finish (Subtractive method)
| Printed Wiring Board 2-layer rigid FR4 with chem-elec AuNi finish (Subtractive method)
| Printed Wiring Board 2-layer rigid FR4 with chemSn elecAuNi finish (Subtractive method)
| Printed Wiring Board 2-layer rigid FR4 with HASL finish (Subtractive method)
| Printed Wiring Board 4-layer rigid FR4 with chem-elec AuNi finish (Subtractive method)
| Printed Wiring Board 4-layer rigid FR4 with chemSn elecAuNi finish (Subtractive method)
| Printed Wiring Board 4-layer rigid FR4 with HASL finish (Subtractive method)
| Printed Wiring Board 8-layer rigid FR4 with chem-elec AuNi finish (Subtractive method)
| Printed Wiring Board 8-layer rigid FR4 with chemSn elecAuNi finish (Subtractive method)
| Printed Wiring Board 8-layer rigid FR4 with HASL finish (Subtractive method)
| Printed Wiring Board 10-layer rigid FR4 with chem-elec AuNi finish (Subtractive method)
| Printed Wiring Board 10-layer rigid FR4 with chemSn elecAuNi finish (Subtractive method)
| Printed Wiring Board 10-layer rigid FR4 with HASL finish (Subtractive method)
| Printed Wiring Board 12-layer rigid FR4 with chem-elec AuNi finish (Subtractive method)
| Printed Wiring Board 12-layer rigid FR4 with chemSn elecAuNi finish (Subtractive method)
| Printed Wiring Board 12-layer rigid FR4 with HASL finish (Subtractive method)
| Printed Wiring Board 16-layer rigid FR4 with chem-elec AuNi finish (Subtractive method)
| Printed Wiring Board 16-layer rigid FR4 with chemSn elecAuNi finish (Subtractive method)
| Printed Wiring Board 16-layer rigid FR4 with HASL finish (Subtractive method)
| Oscillator crystal (500mg) 11.05x4.65x2.5
| Oscillator crystal (750mg) 11.35x4.65x3.6
| Resistor flat chip 0402 (0.6mg)
| Resistor flat chip 0603 (1.9mg)
| Resistor flat chip 1206 (9.2mg)
| Resistor MELF MMA 0204 (19mg) D1.4x3.6 | | [candidate](https://www.niccomp.com/products/pSeries.php?pSeries=NFR) |
| Resistor MELF MMB 0207 (79mg) D2.2x5.8 | | see above |
| Resistor MELF MMU 0102 (7mg) D1.1x2.2 | | see above |
| Resistor thick film flat chip 01005 (0.04mg)
| Resistor thick film flat chip 0201 (0.15mg)
| Resistor thick film flat chip 0402 (0.75mg)
| Resistor thick film flat chip 0603 (2.1mg) | yes |
| Resistor thick film flat chip 1206 (8.9mg) | yes |
| Resistor THT MBA 0204 (125mg) D1.6x3.6
| Resistor THT MBB 0207 (220mg) D2.5x6.3
| Resistor THT MBE 0414 (700mg) D4.0x11.9 | yes, as metal oxide, 4.5x11, with strange data though...
| Ring Core Coil 30g (With housing)
| Ring Core Coil 30g (Without housing)
| Ring Core Coil 8 g (Without housing)
| Ring Core Coil 80g (With housing)
| Ring Core Coil 80g (Without housing)
| Ring Core Coil 8g (With housing)
| Solder paste AuIn18
| Solder paste AuSn20
| Solder paste InPb40
| Solder paste InSn48
| Solder paste SnAg2.6Cu0.3
| Solder paste SnAg3.5
| Solder paste SnAg3.5Cu0.7 (SAC-Lot)
| Solder paste SnAg3Cu0.5 (SAC-Lot) | yes | 
| Solder paste SnAg4Cu0.6
| Solder paste SnCu0.7
| Solder paste SnCu0.7Ag0.3
| Solder paste SnCu0.7Ni0.05
| Solder paste SnIn10Ag3.1
| Solder paste SnPb34Ag2
| Solder paste SnPb36
| Solder paste SnZn9
| Thermistor SMD NTC 0402 (ca. 4mg)
| Thermistor SMD NTC 0603 (6mg)
| Thermistor SMD NTC 0805 (13mg) 
| Thermistor SMD NTC 1206 (18mg)
| Thermistor SMD PTC (400mg) 6.3x8x3.3
| Thermistor THT NTC, Leaded Disk (120mg) 2.5xD43
| Thermistor THT NTC, Leaded Disk (250mg) 4.5xD41
| Thermistor THT PTC Overcurrent Protection, Leaded Disk (980mg) 12xD35
| Thermistor THT PTC Switch, Leaded Disk (420mg) 6xD13
| Thermistor THT PTC Temp Sensor, Leaded Disk (250mg) 4xD42 PE
| Transistor D2PAK TO263 (1.38g) 10.3x9.6x4.5 | yes, 1.75g | 
| Transistor DPAK TO252 (290mg) 6.6x6.2x2.2
| Transistor power THT/SMD SOT93/TO218 3 leads (4.70g) 15.5x12.9x4.7
| Transistor power THT/SMD SOT93/TO218 7 leads (4.80g) 15.5x12.9x4.7
| Transistor signal SOT-883 (SC-101/XQFN3) (0.855 mg) 1.0 x 0.6 x 0.48 (new in 2020)
| Transistor signal SOT223 3 leads (110mg) 3.8x7.65x2.3
| Transistor signal SOT223 8 leads (180mg) 3.8x7.65x3
| Transistor signal SOT23 3 leads (10mg) 1.4x3x1 | yes |
| Transistor signal SOT23 8 leads (18mg) 1.4x3x2
| Transistor THT SOT82 (720mg) 10.6x7.6x2.5
| Transistor THT TO92 (250mg) D4.8x5.3 | | [candidate 1](https://www.onsemi.com/products/discretes-drivers/jfets/bf256b), [candidate 2](https://www.diodes.com/part/view/ZTX705) |
| Varistor THT VDR, Leaded Disk (2g) D12x25
