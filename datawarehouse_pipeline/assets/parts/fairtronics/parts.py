from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    DataVersion, AssetExecutionContext, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

#import pandera as pa
#from dagster_pandera import pandera_schema_to_dagster_type
#from pandera.typing import Series
#
#class ManufacturersModel(pa.SchemaModel):
#    slug: Series[str] = pa.Field(description="Key")
#    name: Series[str] = pa.Field(description="Name of mine")
#    short_name: Series[str] = pa.Field(description="Short name für mines")
#    company_url: Series[str] = pa.Field(description="Web address of company")

@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def parts_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/parts/fairtronics/parts_base.csv")))


@asset(io_manager_key="parts_duckdb_io_manager",
#       dagster_type=pandera_schema_to_dagster_type(ManufacturersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[parts_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="parts",
       compute_kind="Fairtronics")
def parts(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    # TODO Work with file_relative_path
    parts_df = pd.read_csv("./datawarehouse_pipeline/assets/parts/fairtronics/parts_base.csv", 
                           na_values={'short_name': ''}, keep_default_na=False, comment='#')
    # 'weight' and 'weight_unit' are not included since they are calculated in the Database from actual material amounts
    parts_df['short_name'] = parts_df['short_name'].fillna(parts_df['name'])
    os.makedirs("pipeline_run/result", exist_ok=True)
    parts_df.to_csv("pipeline_run/result/parts.csv", index=False,
                    columns=['slug', 'name', 'short_name', 'Manufacturer.slug', 'category', 'product_url'],
                    header=['slug', 'name', 'short_name', 'Manufacturer.slug', 'category', 'product_url'])
    dataframe = parts_df.rename({"Manufacturer.slug": "regime_slug", "Component.slug": "component_slug"}, axis=1) \
                        .filter(['slug', 'name', 'short_name', 'component_slug', 'regime_slug', 'weight', 
                                 'weight_unit', 'part_number', 'product_url', 'values'])
    metadata = dict()
    metadata["name"] = "List of parts as used in Fairtronics"
    metadata["source"] = MetadataValue.md("")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
