from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

#import pandera as pa
#from dagster_pandera import pandera_schema_to_dagster_type
#from pandera.typing import Series
#
#class ManufacturersModel(pa.SchemaModel):
#    slug: Series[str] = pa.Field(description="Key")
#    name: Series[str] = pa.Field(description="Name of mine")
#    short_name: Series[str] = pa.Field(description="Short name für mines")
#    company_url: Series[str] = pa.Field(description="Web address of company")


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def component_attributes_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/parts/fairtronics/component_attributes_base.csv")))


@asset(io_manager_key="parts_duckdb_io_manager",
#       dagster_type=pandera_schema_to_dagster_type(ManufacturersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[component_attributes_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="parts",
       compute_kind="Fairtronics")
def component_attributes(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    # TODO Work with file_relative_path
    attributes_df = pd.read_csv("./datawarehouse_pipeline/assets/parts/fairtronics/component_attributes_base.csv", comment='#')

    os.makedirs("pipeline_run/result", exist_ok=True)
    attributes_df.to_csv("pipeline_run/result/part_attributes.csv", index=False,
                         columns=['slug', 'name', 'alternatives'],
                         header=['slug', 'name', 'alternatives'])

    dataframe = attributes_df    
    metadata = dict()
    metadata["name"] = "List of component attributes as used in Fairtronics"
    metadata["source"] = MetadataValue.md("")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
