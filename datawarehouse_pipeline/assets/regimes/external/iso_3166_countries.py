from dagster import asset, Output, MetadataValue, AutoMaterializePolicy, AssetExecutionContext, AssetIn
import pandas as pd
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@asset(io_manager_key="regimes_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="regimes",
       compute_kind="External")
def iso_3166_countries(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    df = pd.read_csv("https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv",
                     keep_default_na=False, na_values=[""],  # Namibia has alpha-2 code "NA"
                     dtype={"country-code": str, "region-code": str,
                            "sub-region-code": str, "intermediate-region-code": str})\
           .drop('iso_3166-2', axis=1)
    # TODO Use from importlib import resources instead (but how?)
    add = pd.read_csv("./datawarehouse_pipeline/assets/regimes/external/additional_countries.csv",
                      dtype={"country-code": str, "region-code": str,
                             "sub-region-code": str, "intermediate-region-code": str})
    dataframe = pd.concat([df, add], ignore_index=True)
    metadata = dict()
    metadata["name"] = MetadataValue.md("ISO 3166 countries with regional codes, by [lukes](https://github.com/lukes)")
    metadata["source"] = MetadataValue.md(
        "This list is taken from "
        "[ISO-3166-Countries-with-Regional-Codes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes), "
        "which is \"the result of merging data from two sources, the "
        "[Wikipedia ISO 3166-1](http://en.wikipedia.org/wiki/ISO_3166-1#Officially_assigned_code_elements) "
        "article for alpha and numeric country codes, and the "
        "[UN Statistics](https://unstats.un.org/unsd/methodology/m49/overview) "
        "site for countries' regional, and sub-regional codes\"")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
