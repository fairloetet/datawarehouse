from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
from dagster import ExperimentalWarning
import warnings
import os
warnings.filterwarnings("ignore", category=ExperimentalWarning)

import pandera as pa
from dagster_pandera import pandera_schema_to_dagster_type
from pandera.typing import Series


class SpecialRegimesModel(pa.SchemaModel):
    slug: Series[str] = pa.Field(description="Key")
    name: Series[str] = pa.Field(description="Name of special regime")
    short_name: Series[str] = pa.Field(description="Short name of special regime")
    values: Series[str] = pa.Field(nullable=True, description="Overall values")


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def special_regimes_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/regimes/fairtronics/special_regimes_base.csv")))


@asset(io_manager_key="regimes_duckdb_io_manager",
       dagster_type=pandera_schema_to_dagster_type(SpecialRegimesModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[special_regimes_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="regimes",
       compute_kind="Fairtronics")
def special_regimes(context: AssetExecutionContext) -> Output[pd.DataFrame]:

    # TODO Work with file_relative_path
    dataframe = pd.read_csv("./datawarehouse_pipeline/assets/regimes/fairtronics/special_regimes_base.csv", comment="#",
                            keep_default_na=False, na_values=[""], converters={'values': str})
    metadata = dict()
    metadata["name"] = "List of special regimes as used in Fairtronics"
    metadata["source"] = MetadataValue.md("")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
