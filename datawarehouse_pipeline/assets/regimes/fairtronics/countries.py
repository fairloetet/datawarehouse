from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
from dagster import ExperimentalWarning
import warnings
import os
warnings.filterwarnings("ignore", category=ExperimentalWarning)

import pandera as pa
from dagster_pandera import pandera_schema_to_dagster_type
from pandera.typing import Series

class CountriesModel(pa.SchemaModel):
    slug: Series[str] = pa.Field(description="Key")
    name: Series[str] = pa.Field(description="Name of mine")
    short_name: Series[str] = pa.Field(description="Short name für mines")
    synonyms: Series[str] = pa.Field(nullable=True, description="")
    alpha_2: Series[str] = pa.Field(nullable=True, description="")
    alpha_3 : Series[str] = pa.Field(nullable=True, description="")
    country_code: Series[str] = pa.Field(nullable=True, description="")
    region: Series[str] = pa.Field(nullable=True, description="")
    sub_region: Series[str] = pa.Field(nullable=True, description="")
    intermediate_region: Series[str] = pa.Field(nullable=True, description="")
    region_code: Series[str] = pa.Field(nullable=True, description="")
    sub_region_code: Series[str] = pa.Field(nullable=True, description="")
    intermediate_region_code: Series[str] = pa.Field(nullable=True, description="")
    values: Series[str] = pa.Field(nullable=True, description="Specials")


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def countries_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/regimes/fairtronics/countries_base.csv")))


@asset(io_manager_key="regimes_duckdb_io_manager",
       dagster_type=pandera_schema_to_dagster_type(CountriesModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[countries_base],
       ins={"iso_3166_countries": AssetIn(key=["regimes", "iso_3166_countries"])},
       key_prefix="regimes",
       compute_kind="Fairtronics")
def countries(context: AssetExecutionContext,
              iso_3166_countries: pd.DataFrame) -> Output[pd.DataFrame]:
    for column in ["country-code", "region-code", "sub-region-code", "intermediate-region-code"]:
        iso_3166_countries[column] = \
            iso_3166_countries[column].astype(str).str.zfill(3)
    # TODO Use from importlib import resources instead (but how?)
    df = pd.read_csv("./datawarehouse_pipeline/assets/regimes/fairtronics/countries_base.csv",
                     keep_default_na=False, na_values=[""],   # Namibia has alpha-2 code "NA"
                     converters={"values": str}, comment='#')
    dataframe = df.merge(iso_3166_countries, how="left", left_on="alpha_2", right_on="alpha-2")\
                  .rename(columns={
                            "alpha-3": "alpha_3",
                            "country-code": "country_code",
                            "sub-region": "sub_region",
                            "intermediate-region": "intermediate_region",
                            "region-code": "region_code",
                            "sub-region-code": "sub_region_code",
                            "intermediate-region-code": "intermediate_region_code"}, errors="raise")\
                  .astype({"region_code": str,
                           "country_code": str,
                           "sub_region_code": str,
                           "intermediate_region_code": str}, errors="raise")\
                  .drop("alpha-2", axis=1)  # .assign(short_name=df.name)
    dataframe['name'] = dataframe['name_x']
    dataframe = dataframe[['slug', 'name', 'short_name', 'synonyms', 'alpha_2', 'alpha_3', 'country_code',
                           'region', 'sub_region', 'intermediate_region',
                           'region_code', 'sub_region_code', 'intermediate_region_code',
                           'values']]
    metadata = dict()
    metadata["name"] = "List of countries and its attributes as used in Fairtronics"
    metadata["source"] = MetadataValue.md(
        "Complemented with internal, readable, URL-compatible key and some country synonyms, this list includes "
        "some additionaly data on countries, as provided by the ancestor assets.")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
