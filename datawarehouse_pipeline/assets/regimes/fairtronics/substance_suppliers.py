from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
from dagster import ExperimentalWarning
import warnings
import os
warnings.filterwarnings("ignore", category=ExperimentalWarning)

import pandera as pa
from dagster_pandera import pandera_schema_to_dagster_type
from pandera.typing import Series


class SubstanceSuppliersModel(pa.SchemaModel):
    slug: Series[str] = pa.Field(description="Key")
    name: Series[str] = pa.Field(description="Name of supplier")
    short_name: Series[str] = pa.Field(description="Short name of supplier")
    values: Series[str] = pa.Field(nullable=True, description="Overall values")


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def substance_suppliers_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/regimes/fairtronics/substance_suppliers_base.csv")))


@asset(io_manager_key="regimes_duckdb_io_manager",
       dagster_type=pandera_schema_to_dagster_type(SubstanceSuppliersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[substance_suppliers_base],
       # ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="regimes",
       compute_kind="Fairtronics")
def substance_suppliers(context: AssetExecutionContext) -> Output[pd.DataFrame]:

    # TODO Work with file_relative_path
    dataframe = pd.read_csv("./datawarehouse_pipeline/assets/regimes/fairtronics/substance_suppliers_base.csv", 
                            comment="#", keep_default_na=False, na_values=[""], converters={'values': str})
    metadata = dict()
    metadata["name"] = "List of substance Suppliers as used in Fairtronics"
    metadata["source"] = MetadataValue.md("")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
