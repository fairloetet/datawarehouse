from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

import pandera as pa
from dagster_pandera import pandera_schema_to_dagster_type
from pandera.typing import Series

class ManufacturersModel(pa.SchemaModel):
    slug: Series[str] = pa.Field(description="Key")
    name: Series[str] = pa.Field(description="Name of mine")
    short_name: Series[str] = pa.Field(description="Short name for manufacturer")
    company_url: Series[str] = pa.Field(nullable=True, description="Web address of manufacturer")
    values: Series[str] = pa.Field(nullable=True, description="")


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def component_manufacturers_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/regimes/fairtronics/component_manufacturers_base.csv")))


@asset(io_manager_key="regimes_duckdb_io_manager",
       dagster_type=pandera_schema_to_dagster_type(ManufacturersModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[component_manufacturers_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="regimes",
       compute_kind="Fairtronics")
def component_manufacturers(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    # TODO Work with file_relative_path
    comp_manu_df = pd.read_csv("./datawarehouse_pipeline/assets/regimes/fairtronics/component_manufacturers_base.csv", 
                                keep_default_na=False, na_values=[""], converters={"values": str}, comment='#')
    comp_manu_df['short_name'] = comp_manu_df['short_name'].fillna(comp_manu_df['name'])
    comp_manu_df['source'] = comp_manu_df['company_url']
    dataframe = comp_manu_df.filter(['slug', 'name', 'short_name', 'company_url', 'values'])
    metadata = dict()
    metadata["name"] = "List of manufacturers as used in Fairtronics"
    metadata["source"] = MetadataValue.md("")
    metadata["size"] = len(dataframe.index)
    os.makedirs("pipeline_run/result", exist_ok=True)
    return Output(value=dataframe, metadata=metadata)
