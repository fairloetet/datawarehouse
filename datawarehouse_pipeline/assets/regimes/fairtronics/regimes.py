from dagster import asset, Output, MetadataValue, AutoMaterializePolicy, AssetExecutionContext, AssetIn
from dagster import ExperimentalWarning
import pandas as pd
import os
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)

import pandera as pa
from dagster_pandera import pandera_schema_to_dagster_type
from pandera.typing import Series

class RegimesModel(pa.SchemaModel):
    slug: Series[str] = pa.Field(description="Key")
    name: Series[str] = pa.Field(description="Name of mine")
    short_name: Series[str] = pa.Field(description="Short name für mines")
    type: Series[str] = pa.Field(description="")
    code: Series[str] = pa.Field(nullable=True, description="")
    region: Series[str] = pa.Field(nullable=True, description="")
    sub_region: Series[str] = pa.Field(nullable=True, description="")
    values: Series[str] = pa.Field(nullable=True, description="Specials")


@asset(io_manager_key="regimes_duckdb_io_manager",
       dagster_type=pandera_schema_to_dagster_type(RegimesModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       ins={"countries": AssetIn(key=["regimes", "countries"]),
            "mines": AssetIn(key=["regimes", "mines"]),
            "component_manufacturers": AssetIn(key=["regimes", "component_manufacturers"]),
            "device_makers": AssetIn(key=["regimes", "device_makers"]),
            "substance_suppliers": AssetIn(key=["regimes", "substance_suppliers"]),
            "special_regimes": AssetIn(key=["regimes", "special_regimes"])},
       key_prefix="regimes",
       compute_kind="Fairtronics")
def regimes(context: AssetExecutionContext,
            countries: pd.DataFrame,
            mines: pd.DataFrame,
            component_manufacturers: pd.DataFrame,
            device_makers: pd.DataFrame,
            substance_suppliers: pd.DataFrame,
            special_regimes: pd.DataFrame) -> Output[pd.DataFrame]:

    countries["type"] = "country"
    mines["type"] = "mine"
    component_manufacturers["type"] = "manufacturer"
    device_makers["type"] = "makers"
    substance_suppliers["type"] = "supplier"
    special_regimes["type"] = "special"

    manus_df = pd.concat([component_manufacturers, device_makers, special_regimes])
    manus_df["short_name"] = manus_df["short_name"].fillna(manus_df["name"])
    # Special task No.1: Save these both as old manufacturers list for the engine
    os.makedirs("pipeline_run/result", exist_ok=True)
    manus_df.to_csv("pipeline_run/result/manufacturers.csv", index=False,
                    columns=['slug', 'name', 'short_name', 'company_url'],
                    header=['slug', 'name', 'short_name', 'company_url'])

    locations_df = pd.concat([mines, countries, special_regimes]) \
                     .rename(columns={"alpha_2": "code"})
    locations_df["short_name"] = locations_df["short_name"].fillna(locations_df["name"])
    # Special task No.1: Save these both as old locations list for the engine
    os.makedirs("pipeline_run/result", exist_ok=True)
    locations_df.to_csv("pipeline_run/result/locations.csv", index=False,
                        columns=['slug', 'name', 'short_name','type', 'code', 'region', 'sub_region'],
                        header=['slug', 'name', 'short_name','type', 'code', 'region', 'sub_region'])

    regimes_df = pd.concat([locations_df, manus_df, substance_suppliers]) \
                   .drop_duplicates('slug')
    dataframe = regimes_df.rename({"slug": "typed_slug"}, axis=1) \
                          .filter(["slug", "name", "short_name", "type", "typed_slug", 
                                   "code", "region", "sub_region", "values"])
    dataframe["slug"] = dataframe["typed_slug"]  # TODO: type als postfix?
    metadata = dict()
    metadata["name"] = "List of regimes as used in Fairtronics"
    metadata["source"] = MetadataValue.md("Currently containing special regimes, custom-provided mines, countries, "
                                          "component manufacturers and substance suplliers.")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
