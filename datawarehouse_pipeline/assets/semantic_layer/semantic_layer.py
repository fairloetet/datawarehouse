import os
from typing import Any, Mapping, Optional
from pathlib import Path
from dagster import AssetExecutionContext, AssetKey, AutoMaterializePolicy
from dagster_dbt import DbtCliResource, DagsterDbtTranslator, DagsterDbtTranslatorSettings, dbt_assets, get_asset_key_for_model

# TODO This is creating a Dbt Resource a second time, this time for creating the manifest.json only
# TODO   The first time is the "official" DBT resource for the assets below, the the main Dagster config
dbt_project_dir = Path(__file__).joinpath("..").resolve()  # .joinpath("..", "..", "..").resolve() // file_relative_path(__file__, "../../datawarehouse_dbt")
dbt_resource = DbtCliResource(project_dir=os.fspath(dbt_project_dir))

# This is creating the dbt manifest at run time, i.e. every time Dagster code is executed.
#   This allows for dynamically changing DBT definitions. In production this may cause some unwanted overhead.
#   See https://docs.dagster.io/integrations/dbt/reference#loading-dbt-models-from-a-dbt-project and
#   https://docs.dagster.io/integrations/dbt/using-dbt-with-dagster/load-dbt-models#step-4-understand-the-python-code-in-your-dagster-project 
dbt_parse_invocation = dbt_resource.cli(["parse"]).wait()
dbt_manifest_path = dbt_parse_invocation.target_path.joinpath("manifest.json")

# Since the semantic model may contain models (which translates to assets) with the same name as Dagster defined
#  assets, we need to prefix these assets here. The group name is set here.
# TODO Eigentlich ist das unschön, ich sollte die Assets verschieden nennen. Für den Gruppennamen siehe auch
#   https://docs.dagster.io/integrations/dbt/reference#customizing-group-names
class CustomDagsterDbtTranslator(DagsterDbtTranslator):
    def get_asset_key(self, dbt_resource_props: Mapping[str, Any]) -> AssetKey:
        asset_key = super().get_asset_key(dbt_resource_props)
        if dbt_resource_props["resource_type"] == "model":
            asset_key = asset_key.with_prefix("semantic_layer")
        return asset_key
    def get_group_name(self, dbt_resource_props: Mapping[str, Any]) -> Optional[str]:
        return "semantic_layer"
    def get_auto_materialize_policy(self, dbt_resource_props: Mapping[str, Any]) -> Optional[AutoMaterializePolicy]:
        return AutoMaterializePolicy.eager()    

@dbt_assets(
    manifest=dbt_manifest_path,
    dagster_dbt_translator=CustomDagsterDbtTranslator(settings=DagsterDbtTranslatorSettings(enable_asset_checks=True))
)
def semantic_layer_assets(context: AssetExecutionContext, dbt: DbtCliResource):
    yield from dbt.cli(["build"], context=context).stream()
