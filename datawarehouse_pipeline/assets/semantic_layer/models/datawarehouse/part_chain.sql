SELECT
  FIRST(f.name) AS portfolio,
  FIRST(d.name) AS device,
  FIRST(rd.name) AS device_maker,
  FIRST(p.name) AS part,
  FIRST(cc.name) AS part_category,
  FIRST(rp.name) AS part_manufacturer,
  SUM(fd.amount * dp.amount) AS amount,
FROM
  {{ source('portfolios', 'portfolios') }}  f 
  JOIN
  {{ source('portfolios', 'portfolio_devices') }} fd ON fd.portfolio_slug = f.slug 
  JOIN
  {{ source('devices', 'devices') }} d ON fd.device_slug = d.slug
  JOIN
  {{ source('devices', 'device_parts') }} dp ON dp.device_slug = d.slug
  JOIN
  {{ source('parts', 'parts') }} p ON dp.part_slug = p.slug
  JOIN
  {{ source('parts', 'components') }} c ON c.slug = p.component_slug
  JOIN
  {{ source('parts', 'component_categories') }} cc ON cc.slug = c.category_slug
  JOIN
  {{ source('regimes', 'regimes') }} rd ON d.manufacturer_slug = rd.slug
  JOIN
  {{ source('regimes', 'regimes') }} rp ON p.regime_slug = rp.slug
WHERE
  f.slug = '{{ var("portfolio_slug") }}'
--  AND
--  d.slug IN ('mnt-reform')
--  AND
--  p.slug IN ('pcbway-mnt_reform_motherboard_pcb', 'pcbway-mnt_reform_trackpad_controller_pcb')
GROUP BY
  f.slug, d.slug, p.slug, rd.slug, rp.slug
-- ORDER BY amount DESC