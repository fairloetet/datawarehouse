SELECT
  FIRST(f.name) AS portfolio,
  FIRST(d.name) AS device,
  FIRST(p.name) AS part,
  FIRST(m.name) AS material,
  FIRST(r.name) AS resource,
  FIRST(rr.name) AS regime,
  FIRST(c.name) AS country,
  FIRST(c.region) AS region,
  FIRST(c.sub_region) AS subregion,
  SUM(fd.amount * dp.amount * pm.amount * mr.amount * rrr.amount) AS amount,
  FIRST(rd.name) AS maker,
  FIRST(rp.name) AS manufacturer,
  FIRST(rm.name) AS supplier,
FROM
  {{ source('portfolios', 'portfolios') }}  f 
  JOIN
  {{ source('portfolios', 'portfolio_devices') }} fd ON fd.portfolio_slug = f.slug 
  JOIN
  {{ source('devices', 'devices') }} d ON fd.device_slug = d.slug
  JOIN
  {{ source('devices', 'device_parts') }} dp ON dp.device_slug = d.slug
  JOIN
  {{ source('parts', 'parts') }} p ON dp.part_slug = p.slug
  JOIN
  {{ source('parts', 'part_materials') }} pm ON pm.part_slug = p.slug
  JOIN
  {{ source('materials', 'materials') }} m ON pm.material_slug = m.slug
  JOIN
  {{ source('materials', 'material_resources') }} mr ON mr.material_slug = m.slug
  JOIN
  {{ source('resources', 'resources') }} r ON mr.resource_slug = r.slug
  JOIN
  {{ source('resources', 'resource_regimes') }} rrr ON rrr.resource_slug = r.slug
  JOIN 
  {{ source('regimes', 'regimes') }} rr ON rrr.regime_slug = rr.slug
  FULL JOIN 
  {{ source('regimes', 'countries') }} c ON c.slug = rr.slug
  JOIN
  {{ source('regimes', 'regimes') }} rd ON d.manufacturer_slug = rd.slug
  JOIN
  {{ source('regimes', 'regimes') }} rp ON p.regime_slug = rp.slug
  JOIN
  {{ source('regimes', 'regimes') }} rm ON m.regime_slug = rm.slug
WHERE
  f.slug = '{{ var("portfolio_slug") }}'
--  AND
--  d.slug IN ('mnt-reform')
--  AND
--  p.slug IN ('pcbway-mnt_reform_motherboard_pcb', 'pcbway-mnt_reform_trackpad_controller_pcb')
--  AND
--  m.slug IN ('unknown-copper')
--  AND
--  r.slug IN ('unknown-copper_ore')
--  AND
--  rr.slug IN ('china', 'chile')
GROUP BY
  f.slug, d.slug, p.slug, m.slug, r.slug, rd.slug, rp.slug, rm.slug, rr.slug
-- ORDER BY amount DESC