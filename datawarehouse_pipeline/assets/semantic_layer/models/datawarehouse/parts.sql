SELECT 
    p.name as part, 
    p.short_name as part_short_name, 
    r.name AS manufacturer, 
    cc.name AS category, 
    p.weight as weight, 
    p.weight_unit as unit
FROM 
    {{ source('parts', 'parts') }} p
    JOIN
    {{ source('regimes', 'regimes') }} r ON p.regime_slug = r.slug
    JOIN
    {{ source('parts', 'components') }} c ON p.component_slug = c.slug
    JOIN
    {{ source('parts', 'component_categories') }} cc ON c.category_slug = cc.slug
