SELECT 
  name AS portfolio,
  short_name AS portfolio_short_name
FROM 
  {{ source('portfolios', 'portfolios') }} 