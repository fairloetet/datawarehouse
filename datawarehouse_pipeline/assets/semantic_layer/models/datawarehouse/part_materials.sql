SELECT 
  FIRST(p.name) as part, 
  FIRST(m.name) as material, 
  SUM(pm.amount) as amount, 
  FIRST(rp.name) as manufacturer, 
  FIRST(rm.name) as supplier
FROM 
  {{ source('parts', 'parts') }} p 
  JOIN 
  {{ source('parts', 'part_materials') }} pm ON p.slug = pm.part_slug
  JOIN 
  {{ source('materials', 'materials') }} m ON m.slug = pm.material_slug
  JOIN 
  {{ source('regimes', 'regimes') }} rp ON p.regime_slug = rp.slug
  JOIN 
  {{ source('regimes', 'regimes') }} rm ON m.regime_slug = rm.slug
GROUP BY 
    p.slug, m.slug, rp.slug, rm.slug