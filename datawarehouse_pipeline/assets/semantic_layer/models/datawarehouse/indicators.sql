SELECT 
  i.name AS indicator, 
  i.short_name AS indicator_short_name, 
  i.direction AS direction, 
  i.composed AS composed,
FROM 
  {{ source('indicators', 'indicators') }} i
