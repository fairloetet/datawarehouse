SELECT 
  d.name AS device, 
  d.short_name AS device_short_name, 
  r.name AS maker, 
  d.product_number AS product_number, 
  d.description AS description,
  d.dependency AS dependency
FROM 
  {{ source('devices', 'devices') }} d
  JOIN
  {{ source('regimes', 'regimes') }} r ON d.manufacturer_slug = r.slug
