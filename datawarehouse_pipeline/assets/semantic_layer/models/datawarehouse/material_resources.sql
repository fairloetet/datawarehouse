SELECT 
  FIRST(m.name) AS material, 
  FIRST(r.name) AS resource, 
  SUM(mr.amount) as amount, 
  FIRST(rm.name) as supplier, 
  FIRST(rr.name) as mine
FROM 
  {{ source('materials', 'materials') }} m
  JOIN
  {{ source('materials', 'material_resources') }} mr ON m.slug = mr.material_slug
  JOIN
  {{ source('resources', 'resources') }} r ON r.slug = mr.resource_slug
  JOIN
  {{ source('regimes', 'regimes') }} rm ON m.regime_slug = rm.slug
  JOIN
  {{ source('regimes', 'regimes') }} rr ON r.regime_slug = rr.slug
GROUP BY
  m.slug, r.slug, rm.slug, rr.slug