SELECT
  FIRST(f.name) AS portfolio,
  FIRST(d.name) AS device,
  FIRST(rd.name) AS device_maker,
  SUM(fd.amount) AS amount,
FROM
  {{ source('portfolios', 'portfolios') }}  f 
  JOIN
  {{ source('portfolios', 'portfolio_devices') }} fd ON fd.portfolio_slug = f.slug 
  JOIN
  {{ source('devices', 'devices') }} d ON fd.device_slug = d.slug
  JOIN
  {{ source('devices', 'device_parts') }} dp ON dp.device_slug = d.slug
  JOIN
  {{ source('regimes', 'regimes') }} rd ON d.manufacturer_slug = rd.slug
WHERE
  f.slug = '{{ var("portfolio_slug") }}'
--  AND
--  d.slug IN ('mnt-reform')
--  AND
--  p.slug IN ('pcbway-mnt_reform_motherboard_pcb', 'pcbway-mnt_reform_trackpad_controller_pcb')
GROUP BY
  f.slug, d.slug, rd.slug
-- ORDER BY amount DESC