SELECT 
  m.name AS material,
  m.short_name AS material_short_name,
  r.name AS supplier,
  c.name AS category,
  s.cas AS cas,
  s.density_g_cm3 AS density,
FROM 
  {{ source('materials', 'materials') }} m
  JOIN
  {{ source('materials', 'substances') }} s ON s.slug = m.substance_slug
  JOIN 
  {{ source('materials', 'substance_categories') }} c ON c.slug = s.category_slug
  JOIN
  {{ source('regimes', 'regimes') }} r ON r.slug = m.regime_slug
