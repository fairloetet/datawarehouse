SELECT 
  FIRST(d.name) AS device, 
  FIRST(p.name) AS part, 
  SUM(dp.amount) AS amount, 
  FIRST(rd.name) AS maker, 
  FIRST(rp.name) as manufacturer
FROM 
  {{ source('devices', 'devices') }} d
  JOIN 
  {{ source('devices', 'device_parts') }} dp ON dp.device_slug = d.slug
  JOIN 
  {{ source('parts', 'parts') }} p ON dp.part_slug = p.slug
  JOIN
  {{ source('regimes', 'regimes') }} rp ON p.regime_slug = rp.slug
  JOIN
  {{ source('regimes', 'regimes') }} rd ON d.manufacturer_slug = rd.slug
GROUP BY 
  d.slug, p.slug, rd.slug, rp.slug
