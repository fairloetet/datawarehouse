SELECT 
  r.name AS resource, 
  r.short_name AS resource_short_name, 
  e.circularity AS circularity, 
  rr.name AS mine
FROM 
  {{ source('resources', 'resources') }} r
  JOIN
  {{ source('resources', 'extractives') }} e ON r.extractive_slug = e.slug
  JOIN
  {{ source('regimes', 'regimes') }} rr ON r.regime_slug = rr.slug