SELECT
  FIRST(p.name) AS portfolio,
  FIRST(d.name) AS device,
  SUM(pd.amount) AS amount
FROM
  {{ source('portfolios', 'portfolios') }}  p
  JOIN
  {{ source('portfolios', 'portfolio_devices') }} pd ON p.slug = pd.portfolio_slug
  JOIN
  {{ source('devices', 'devices') }} d ON d.slug = pd.device_slug
GROUP BY
  d.slug, p.slug