from dagster import load_assets_from_package_module, AssetKey
from . import indicators, resources, regimes, materials, parts, devices, portfolios, semantic_layer

indicator_assets = load_assets_from_package_module(package_module=indicators, group_name="indicators")
resource_assets = load_assets_from_package_module(package_module=resources, group_name="resources")
regime_assets = load_assets_from_package_module(package_module=regimes, group_name="regimes")
material_assets = load_assets_from_package_module(package_module=materials, group_name="materials")
part_assets = load_assets_from_package_module(package_module=parts, group_name="parts")
device_assets = load_assets_from_package_module(package_module=devices, group_name="devices")
portfolio_assets = load_assets_from_package_module(package_module=portfolios, group_name="portfolios")
# semantic_layer_assets = load_assets_from_package_module(package_module=semantic_layer, group_name="semantic_layer")

# TODO Benötigt für die Jobs. Ich bin mir sicher, dass dies einfacher geht!
ilostat_assets = [AssetKey(["indicators", "collective_bargaining_coverage"]),
                  AssetKey(["indicators", "employed_persons_having_excessive_working_time"]),
                  AssetKey(["indicators", "fatal_occupational_injuries"]),
                  AssetKey(["indicators", "labour_force"]),
                  AssetKey(["indicators", "mean_working_hours"]),
                  AssetKey(["indicators", "monthly_mining_earnings"]),
                  AssetKey(["indicators", "non_fatal_occupational_injuries"]),
                  AssetKey(["indicators", "price_index_change"]),
                  AssetKey(["indicators", "social_protection_coverage"])]
worldbank_wdi_assets = [AssetKey(["indicators", "avg_working_hours_per_week_of_working_child"]),
                        AssetKey(["indicators", "children_in_employment"]),
                        AssetKey(["indicators", "employed_children_in_work"]),
                        AssetKey(["indicators", "labor_force_participation_rate_grownups"]),
                        AssetKey(["indicators", "population_children_total"]),
                        AssetKey(["indicators", "population_grownups_total"])]
download_assets = [AssetKey(["indicators", "monthly_living_wage"]), 
                   AssetKey(["indicators", "global_slavery_index"])]
usgs_assets = [AssetKey(["resources", "usgs_mineral_commodity_summaries"])]
