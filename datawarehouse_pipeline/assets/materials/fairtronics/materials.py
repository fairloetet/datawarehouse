import os
import pandas as pd
from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def materials_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/materials/fairtronics/materials_base.csv")))


@asset(io_manager_key="materials_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[materials_base],
       ins={"substances": AssetIn(key=["materials", "substances"])},
       key_prefix="materials",
       compute_kind="Fairtronics")
def materials(context: AssetExecutionContext,
              substances: pd.DataFrame) -> Output[pd.DataFrame]:

#    # Create materials from substances + special material, if only the non-unknown regime entries are in materials.csv
##    materials_df = materials_df.merge(substances_df, 'left', left_on='substance', right_on='slug')
##    materials_df['slug'] = materials_df['slug_x']
##    materials_df['name'] = materials_df['name_x'].fillna(materials_df['name_y'])
##    materials_df['short_name'] = materials_df['short_name_x']\
##        .fillna(materials_df['short_name_y'])\
##        .fillna(materials_df['name'])
##    unknown_regime_slug = "unknown"
##    pimped_substances_df = substances_df.copy()
##    pimped_substances_df['slug'] = (unknown_regime_slug + "-") + substances_df['slug']
##    pimped_substances_df['short_name'].fillna(substances_df['name'], inplace=True)
##    pimped_substances_df['substance'] = substances_df['slug']
##    pimped_substances_df['regime'] = unknown_regime_slug
##    pimped_substances_df['comment'] = "No supplier specified."
##    # Es könnte schon ein entspr. unknown-slug schon in materials_df gegeben haben, deshalb der drop_duplicates
##    # Die Kombination regime+substance kann mit verschiedenen values tatsächlich mehrmals vorkommen.
##    # TODO Diese doppelten mit df.value_counts() berichten
##    materials_df = pd.concat([materials_df, pimped_substances_df], ignore_index=True)\
##                     .drop_duplicates(subset=['slug'], keep='first', ignore_index=True)
##    materials_df.to_csv(materials_file, index=False,
##                        columns=['slug', 'name', 'short_name', 'regime', 'substance', 'category',
##                                 'cas', 'density_g_cm3', 'comment'])

    # Create materials from given materials_base and substances for Fairtronics
    materials_df = pd.read_csv("./datawarehouse_pipeline/assets/materials/fairtronics/materials_base.csv", 
                               na_values={'short_name': ''}, keep_default_na=False, comment='#') 
    materials_df = materials_df.merge(substances, 'left', left_on='substance', right_on='slug')
    materials_df['comment'] = materials_df['comment_x'] + ". " + materials_df['comment_y']
    materials_df['slug'] = materials_df['slug_x']
    materials_df['substance_slug'] = materials_df['substance']
    materials_df['name'] = materials_df['name_x'].fillna(materials_df['name_y'])
    materials_df['short_name'] = materials_df['short_name_x'].fillna(materials_df['short_name_y']).fillna(materials_df['name'])
    os.makedirs("pipeline_run/result", exist_ok=True)
    materials_df.to_csv("pipeline_run/result/materials.csv", index=False,
                        columns=['slug', 'name', 'short_name', 'regime', 'category_slug', 'cas', 'density_g_cm3', 'comment'],
                        header=['slug', 'name', 'short_name', 'location', 'category', 'cas', 'density_g_cm3', 'comment'])

    dataframe = materials_df.rename({"regime": "regime_slug"}, axis=1) \
                            .filter(['slug', 'name', 'short_name', 'regime_slug', 'substance_slug', 
                                     'values', 'comment'])
    metadata = dict()
    metadata["name"] = "Materials (= substances from a source) used here"
    metadata["source"] = MetadataValue.md("Compiled from substances and supply chain data.")
    metadata["size"] = len(dataframe.index)

    return Output(value=dataframe, metadata=metadata)
