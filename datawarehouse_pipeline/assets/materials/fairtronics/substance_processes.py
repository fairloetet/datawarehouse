from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def substance_processes_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/materials/fairtronics/substance_processes_base.csv")))


@asset(io_manager_key="materials_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[substance_processes_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="materials",
       compute_kind="Fairtronics")
def substance_processes(context: AssetExecutionContext) -> Output[pd.DataFrame]:

    # TODO Work with file_relative_path
    processes_df = pd.read_csv("./datawarehouse_pipeline/assets/materials/fairtronics/substance_processes_base.csv",
                               keep_default_na=False, comment='#')
    dataframe = processes_df[['output_slug', 'input_slug', 'factor', 'values', 'source']]
    metadata = dict()
    metadata["name"] = "Manufacturing processes for substances"
    metadata["source"] = MetadataValue.md("Taken from ecoinvent")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
