from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import os
import pandas as pd
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def substance_categories_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/materials/fairtronics/substance_categories_base.csv")))


@asset(io_manager_key="materials_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[substance_categories_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="materials",
       compute_kind="Fairtronics")
def substance_categories(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    # TODO Work with file_relative_path
    categories_df = pd.read_csv("./datawarehouse_pipeline/assets/materials/fairtronics/substance_categories_base.csv",
                               na_values=[''], keep_default_na=False, comment='#')
    categories_df['short_name'] = categories_df['short_name'].fillna(categories_df['name'])
    
    os.makedirs("pipeline_run/result", exist_ok=True)
    categories_df.to_csv("pipeline_run/result/material_categories.csv", index=False,
                         columns=['slug', 'name', 'short_name', 'parent'],
                         header=['slug', 'name', 'short_name', 'parent'])
    
    dataframe = categories_df.rename({"parent": "parent_slug"})
    metadata = dict()
    metadata["name"] = "Substances used here"
    metadata["source"] = MetadataValue.md("Manually compiled")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
