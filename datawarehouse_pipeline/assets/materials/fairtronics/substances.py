from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
import os
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def substances_base():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/materials/fairtronics/substances_base.csv")))


@asset(io_manager_key="materials_duckdb_io_manager",
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[substances_base],
#       ins={"devices": AssetIn(key=["devices", "devices"])},
       key_prefix="materials",
       compute_kind="Fairtronics")
def substances(context: AssetExecutionContext) -> Output[pd.DataFrame]:
    # TODO Work with file_relative_path
    substances_df = pd.read_csv("./datawarehouse_pipeline/assets/materials/fairtronics/substances_base.csv",
                                na_values=[''], keep_default_na=False, comment='#')
    dataframe = substances_df.rename({"substitute_slug": "substitute_substance_slug"}, axis=1) \
                             .filter(['slug', 'name', 'short_name', 'synonyms', 'category_slug', 
                                      'substitute_substance_slug', 'cas', 'density_g_cm3', 'values', 'comment'])
    metadata = dict()
    metadata["name"] = "Substances used here"
    metadata["source"] = MetadataValue.md("Manually compiled")
    metadata["size"] = len(dataframe.index)
    return Output(value=dataframe, metadata=metadata)
