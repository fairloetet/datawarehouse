import os
import pandas as pd
import csv
import re
from typing import Dict, Tuple, List
from string import Template
from dagster import asset, observable_source_asset, Output, MetadataValue, AutoMaterializePolicy, \
                    AssetExecutionContext, DataVersion, AssetIn
import pandas as pd
from dagster import ExperimentalWarning
import warnings
warnings.filterwarnings("ignore", category=ExperimentalWarning)
import pandera as pa
from dagster_pandera import pandera_schema_to_dagster_type
from pandera.typing import Series


class MaterialResourcesModel(pa.SchemaModel):
    material_slug: Series[str] = pa.Field(description="Id of material")
    resource_slug: Series[str] = pa.Field(description="Id of resource")
    amount: Series[float] = pa.Field(description="Amount of resource in material")
    source: Series[str] = pa.Field(description="Source for the data")


@observable_source_asset  # Seems to be CPU intensive: (auto_observe_interval_minutes=1)   # Seems to be CPU intensive
def material_resource_sourcing():
    return DataVersion(str(os.path.getmtime("./datawarehouse_pipeline/assets/materials/fairtronics/material_resource_sourcing.csv")))


@asset(io_manager_key="materials_duckdb_io_manager",
       dagster_type=pandera_schema_to_dagster_type(MaterialResourcesModel),
       auto_materialize_policy=AutoMaterializePolicy.eager(),
       deps=[material_resource_sourcing],
       ins={"substances": AssetIn(key=["materials", "substances"]),
            "regimes": AssetIn(key=["regimes", "regimes"]),
            "resources": AssetIn(key=["resources", "resources"]),
            "materials": AssetIn(key=["materials", "materials"]),
            "substance_processes": AssetIn(key=["materials", "substance_processes"])},
       key_prefix="materials",
       compute_kind="Fairtronics")
def material_resources(context: AssetExecutionContext,
                       substances: pd.DataFrame,
                       regimes: pd.DataFrame,
                       resources: pd.DataFrame,
                       materials: pd.DataFrame,
                       substance_processes: pd.DataFrame) -> Output[pd.DataFrame]:

    processes: Dict[str, Dict[str, Tuple[str, Dict[str, str], str]]] = {}
    sourcings: List[Tuple[re.Pattern, re.Pattern, re.Pattern, Tuple[str, str]]] = []

    # Note that in the variable names, "output" means what is given to be produced
    #   and for which the "input"s are collected recursively and returned.
    def compute_inputs_for_output(output: str, output_amount: float, extra_values_dict: Dict[str, str],
                                  output_doc: str) -> Dict[str, Tuple[float, str]]:
        # In parallel, weights (tuple index 0) and documentation (tuple index 1) are compiled
        if output in processes:
            # If we have production info for the material...
            inputs = processes[output]
            inputs_of_inputs: Dict[str, Tuple[float, str]] = {}
            for input in inputs:
                input_factor, input_values_dict, input_doc = inputs[input]
                new_values_dict = input_values_dict | extra_values_dict  # The external dict overwrites the processes values
                try:
                    new_input_factor = float(eval(Template(input_factor).substitute(new_values_dict)))
                except (SyntaxError, KeyError) as err:
                    context.log.error(f"Errorneous term ${err} in expression within substance_processes.csv for input {input} and output {output}.")
                    new_input_factor = 1.0
                new_output_amount = output_amount * new_input_factor
                new_output_doc = (output_doc + ", which is produced by " if output_doc else "") + input_doc
                # ...recursively collected input materials, transforming shares to weight
                # Note that even new_output_amount = 0.0 will be processed further
                new_inputs = compute_inputs_for_output(input, new_output_amount, new_values_dict, new_output_doc)
                # If material already in list, sum amounts. If not, add material
                # TODO Do it more pythonic
                for new_input in new_inputs:
                    if new_input in inputs_of_inputs:
                        inputs_of_inputs[new_input] = \
                            (inputs_of_inputs[new_input][0] + new_inputs[new_input][0],
                                inputs_of_inputs[new_input][1] +
                                (" and " if inputs_of_inputs[new_input][1] and new_inputs[new_input][1] else "") +
                                new_inputs[new_input][1])
                    else:
                        inputs_of_inputs[new_input] = new_inputs[new_input]
            return inputs_of_inputs
        else:
            # ... if not, keep the share->weight transformation in new material assignment
            return {output: (output_amount, output_doc)}

    def find_resource(regime_slug: str, substance_slug: str, extractive_slug: str) -> str:
        # TODO The amount part of the sourcings is not respected yet. (Currently amount=1 is exactly how it works.)
        for regime_pattern, substance_pattern, extractive_pattern, (resource_param, resource_regex) in sourcings:
            if regime_pattern.match(regime_slug) and substance_pattern.match(substance_slug) and \
               extractive_pattern.match(extractive_slug):
                # Find matching criterium
                matching_resources_df = resources[
                    resources[resource_param].str.match(resource_regex) &
                    (resources['extractive_slug'] == extractive_slug)]
                if len(matching_resources_df) == 0:
                    context.log.error(f"Couldn't find a resource of {extractive_slug} matching {resource_param}={resource_regex}.")
                    return "error"  # TODO Well, that's a trick
                if len(matching_resources_df) > 1:
                    context.log.error(f"Found more than one resource of {extractive_slug} matching {resource_param}={resource_regex}.")
                    return "error"  # TODO Well, that's a trick
                return matching_resources_df['slug'].values[0]
        context.log.error(f"Couldn't find a matching rule for substance {extractive_slug} in material {regime_slug}/{substance_slug}.")
        return "error"  # TODO Well, that's a trick

    ## Simplify the substance production to a direct substance -> extractive relation and have compute the
    ##   sourcing of these to finally get a material -> resource relation
    # 1. Get sourcing patterns for the mapping of substances/extractives to material/resources
    # TODO The amount part of the file is not respected yet. (Currently amount=1 is exactly how it works.)
    with open("./datawarehouse_pipeline/assets/materials/fairtronics/material_resource_sourcing.csv", 
              mode="r", encoding="utf-8-sig", newline="") as file:
        csv_reader = csv.DictReader(file)
        for output_input in csv_reader:
            regime_pattern = re.compile(output_input['regime'])
            substance_pattern = re.compile(output_input['substance'])
            extractive_pattern = re.compile(output_input['extractive'])
            resource_criterum = output_input['resource'].split("=", 1)
            resource_param = "slug" if len(resource_criterum) == 1 else resource_criterum[0]
            resource_regex = resource_criterum[0] if len(resource_criterum) == 1 else resource_criterum[1]
            sourcings.append((regime_pattern, substance_pattern, extractive_pattern,
                              (resource_param, resource_regex)))

    # 2. Read in all individual process
    # TODO iterrows() is a Pandas anti-pattern. Maybe there's a different way to do this? Think of how to do it with SQL
    for _, output_input in substance_processes.iterrows():
        output_flow = output_input["output_slug"]
        if str(output_flow).startswith("#"):
            continue
        input_flow = output_input["input_slug"]
        input_factor = str(output_input["factor"])  # Can be a Template, i.e. not allways a number
        input_values = output_input["values"]
        input_values_dict = {}
        if input_values:
            for value in input_values.split(";"):
                name, expr = value.split(":", 1)
                input_values_dict[name] = expr
        input_doc = output_input["source"]
        # TODO Do it more pythonic (e.g. using defaultdict, missing the exeception, though)
        if output_flow in processes:
            if input_flow in processes[output_flow]:
                raise ValueError(f"For output '{output_flow}' more than one input '{input_flow}' is defined.")
                # Only necessary if we allow to have identical output/input pairs:
                # processes[output_flow][input_flow] = \
                #     (processes[output_flow][input_flow][0] + input_factor,
                #      input_values_dict,
                #      processes[output_flow][input_flow][2] +
                #         (" and " if processes[output_flow][input_flow][0] and processes[output_flow][input_flow][2]
                #          else "") + input_doc)
            processes[output_flow][input_flow] = (input_factor, input_values_dict, input_doc)
        else:
            processes[output_flow] = {input_flow: (input_factor, input_values_dict, input_doc)}

    # 3. Now flattern all material productions to simplify them to material -> resource relations
    # TODO Don't do it with a loop but with apply or the like
    out_df = pd.DataFrame()
    for material_slug, substance_slug, regime_slug, material_values in \
            materials[['slug', 'substance_slug', 'regime_slug', 'values']].values:

        if substance_slug not in processes:
            substitute_slug = substances[substances['slug'] == substance_slug]['substitute_substance_slug'].iloc[0]
            if substitute_slug not in processes:
                context.log.warn("  There's no definition nor substitute of how to produce " + substance_slug)
                continue
            else:
                output = substitute_slug
        else:
            output = substance_slug

        # Assumes the slug is unique
        substance_row = substances[substances['slug'] == substance_slug].iloc[0]
        regime_row = regimes[regimes['slug'] == regime_slug].iloc[0]

        # Compile list of variable values. Precedence: regime values precede material values precede
        #   substance values preceed the default values within the substance_processes, if they have been
        #   codified with variables within this dict at all. Constant values within substance_processes
        #   rule as they can not be overwritten using value dicts.
        values_dict = {}
        substance_values = substance_row['values']
        if substance_values:
            for value in str(substance_values).split(";"):
                name, expr = value.split(":", 1)
                values_dict[name] = expr
        # Material values overwrite even substance values
        if material_values:
            for value in str(material_values).split(";"):
                name, expr = value.split(":", 1)
                values_dict[name] = expr
        # regime values even overwrite material values
        regime_values = regime_row['values']
        if regime_values:
            for value in str(regime_values).split(";"):
                name, expr = value.split(":", 1)
                values_dict[name] = expr

        # Now get all input commodities for the given output substance inclduding the comments
        inputs = compute_inputs_for_output(output, 1.0, values_dict, "")

        # Check extractive availability
        # unknown_extractives = [extractive for extractive in inputs.keys() if extractive not in extractives_df['slug'].values]
        # if unknown_extractives:
        #     raise KeyError("Substance '" + substance_slug + "' has unknown extractive(s) " + str(unknown_extractives))

        # Apply sourcing
        material_resources_df = pd.DataFrame()  # New resource
        material_resources_df['Resource.slug'] = inputs.keys()
        # Find resources according to sourcing
        material_resources_df['Resource.slug'] = material_resources_df['Resource.slug'].\
            map(lambda extractive_slug: find_resource(regime_slug, substance_slug, extractive_slug))
        material_resources_df['Material.slug'] = material_slug
        material_resources_df['amount'], material_resources_df['source'] = zip(*inputs.values())
        out_df = pd.concat([out_df, material_resources_df])
    #   Save resulting material-resource relations
    out_df = out_df.astype({'Material.slug': "string", 'Resource.slug': "string", 'amount': "float", 'source': "string"})
    os.makedirs("pipeline_run/result", exist_ok=True)
    out_df.to_csv("pipeline_run/result/material_resources.csv", index=False,
                  columns=['Material.slug', 'Resource.slug', 'amount', 'source'],
                  header=['Material.slug', 'Resource.slug', 'amount', 'source'])

    dataframe = out_df.rename({"Material.slug": "material_slug", "Resource.slug": "resource_slug"}, axis=1) \
                      .filter(['material_slug', 'resource_slug', 'amount', 'source'])
    metadata = dict()
    metadata["name"] = "Material -> Resource relations used here"
    metadata["source"] = MetadataValue.md("Systematically compiled from the substance process and supply chain data.")
    metadata["size"] = len(dataframe.index)

    return Output(value=dataframe, metadata=metadata)
