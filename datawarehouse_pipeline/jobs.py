from dagster import define_asset_job, AssetSelection
from datawarehouse_pipeline.assets import ilostat_assets, worldbank_wdi_assets, usgs_assets, download_assets


ilostat_job = define_asset_job("ilostat_data_update", selection=ilostat_assets, tags={"dagster/max_retries": 3})
worldbank_wdi_job = define_asset_job("worldbank_wdi_data_update", selection=worldbank_wdi_assets)
downloaded_assets_job = define_asset_job("downloaded_assets_create", selection=download_assets)
usgs_mcs_job = define_asset_job("usgs_mcs_update", selection=usgs_assets)
all_assets_job = define_asset_job("all_asset_job", selection=AssetSelection.all())
