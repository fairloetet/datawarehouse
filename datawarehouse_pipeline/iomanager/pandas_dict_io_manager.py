from dagster import UPathIOManager, OutputContext, InputContext, ConfigurableIOManagerFactory
from typing import Optional, Dict
from upath import UPath
import pandas as pd


class PandasDictCSVIOManager(UPathIOManager):
    extension: str = ""

    def dump_to_path(self, context: OutputContext, obj: Dict[str, pd.DataFrame], path: UPath):
        if not path.exists():
            path.mkdir()
        for sheet_name, data_frame in obj.items():
            with (path / sheet_name).with_suffix(".csv").open("wb") as file:
                data_frame.to_csv(file)

    def load_from_path(self, context: InputContext, path: UPath) -> Dict[str, pd.DataFrame]:
        result: Dict[str, pd.DataFrame] = {}
        for file in path.iterdir():
            with file.open("rb") as f:
                data_frame = pd.read_csv(f)
            sheet_name = file.stem
            result[sheet_name] = data_frame
        return result


class LocalPandasDictCSVIOManager(ConfigurableIOManagerFactory):
    base_path: Optional[str] = None

    def create_io_manager(self, context) -> PandasDictCSVIOManager:
        base_path = UPath(self.base_path or context.instance.storage_directory())
        return PandasDictCSVIOManager(base_path=base_path)
