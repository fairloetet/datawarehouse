from dagster import UPathIOManager, OutputContext, InputContext, ConfigurableIOManagerFactory
from typing import Optional
from upath import UPath
import pandas as pd


class PandasCSVIOManager(UPathIOManager):
    extension: str = ".csv"

    def dump_to_path(self, context: OutputContext, obj: pd.DataFrame, path: UPath):
        with path.open("wb") as file:
            obj.to_csv(file)

    def load_from_path(self, context: InputContext, path: UPath) -> pd.DataFrame:
        with path.open("rb") as file:
            return pd.read_csv(file)


class LocalPandasCSVIOManager(ConfigurableIOManagerFactory):
    base_path: Optional[str] = None

    def create_io_manager(self, context) -> PandasCSVIOManager:
        base_path = UPath(self.base_path or context.instance.storage_directory())
        return PandasCSVIOManager(base_path=base_path)
