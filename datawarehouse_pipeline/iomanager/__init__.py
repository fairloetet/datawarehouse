from dagster import FilesystemIOManager
from .pandas_io_manager import LocalPandasCSVIOManager
from .pandas_dict_io_manager import LocalPandasDictCSVIOManager


local_io_manager = FilesystemIOManager(
#    base_dir="data",  # Path is built relative to where `dagster dev` is run. If not set, local_artifact_storage is taken.
)

pandas_io_manager = LocalPandasCSVIOManager(
#    base_path="data",  # Path is built relative to where `dagster dev` is run. If not set, local_artifact_storage is taken.
)

pandas_dict_io_manager = LocalPandasDictCSVIOManager(
#    base_path="data",  # Path is built relative to where `dagster dev` is run. If not set, local_artifact_storage is taken.
)
