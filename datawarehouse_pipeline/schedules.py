from dagster import ScheduleDefinition
from .jobs import ilostat_job, downloaded_assets_job


# fairtonics_schedule = ScheduleDefinition(
#     job=fairtronics_job,
#     cron_schedule="@monthly",
# )

ilostat_schedule = ScheduleDefinition(
    job=ilostat_job,
    # cron_schedule="0 1 2 * *", # At 01:00 on day-of-month 2. ILOSTAT was offline/busy on 1st Jan 0:00, so we try this one
    cron_schedule="0 2 * * 0", # At 02:00 on Sunday.
)

downloaded_assets_schedule = ScheduleDefinition(
    job=downloaded_assets_job,
    cron_schedule="@monthly",  # @reboot isn't allowed
)
