import pandera as pa
from pandera.typing import Series, Index
from dagster_pandera import pandera_schema_to_dagster_type


# See https://docs.dagster.io/integrations/pandas
# Fraglich ob das mit dem "economy" klappt, weil das der Index ist. Wie testet man den?
# from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn, StrictColumnsConstraint
# WorldbankWdiMrvType2 = create_dagster_pandas_dataframe_type(
#     name="WorldbankWdiMrvType2",
#     columns=[
#         PandasColumn.string_column("economy", is_required=True, unique=True),
#         PandasColumn.string_column("country", is_required=True),
#         PandasColumn.float_column("value", is_required=True),
#         PandasColumn.integer_column("year", is_required=True, min_value=1960)
#     ],
#     dataframe_constraints=[StrictColumnsConstraint(["economy", "country", "value", "year"])]
# )

class WorldbankWdiMrvType(pa.DataFrameModel):
    '''
    World Development Indicator with most recent value for each country, if available
    '''
    economy: Series[str] = pa.Field(str_length={"min_value": 3, "max_value": 3},
                                    description="ISO-3166 alpha-3 code for country")
    country: Series[str] = pa.Field(description="Country name as provided by The World Bank")
    value: Series[float] = pa.Field(description="Value within this indicator for a country for "
                                                "the most recent known year.")
    year: Series[int] = pa.Field(ge=1960, description="Year for the value. It's the latest year, data is "
                                                      "available for this country within this indicator.")


class IlostatSdmxType(pa.DataFrameModel):
    '''
    ILOSTAT Indicator with most recent value for each country, if available
    '''
    country: Series[str] = pa.Field(str_length={"min_value": 3, "max_value": 3},
                                    description="ISO-3166 alpha-3 code for country")
    value: Series[float] = pa.Field(description="Value within this indicator for a country for "
                                                "the most recent known year.")
    year: Series[int] = pa.Field(ge=1980, description="Year for the value. It's the latest year, data is "
                                                      "available for this country within this indicator.")
    source: Series[str] = pa.Field(description="Source annotation.")


class FairtronicsIndicatorType(pa.DataFrameModel):
    '''
    Type for indicators in Fairtronics
    '''
    regime_slug: Series[str] = pa.Field(description="Regime slug")
    amount: Series[float] = pa.Field(le=1.0, ge=0.0, description="Amount of indicator between 0 and 1")
    source: Series[str] = pa.Field(description="Source annotation.")


ilostat_sdmx_type = pandera_schema_to_dagster_type(IlostatSdmxType)
wb_wdi_type = pandera_schema_to_dagster_type(WorldbankWdiMrvType)
ft_indicator_type = pandera_schema_to_dagster_type(FairtronicsIndicatorType)
