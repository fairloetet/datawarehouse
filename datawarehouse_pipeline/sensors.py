from dagster import sensor, RunRequest
from .jobs import usgs_mcs_job, worldbank_wdi_job
from .resources import WorldbankWdiResource
from sciencebasepy import SbSession


# TODO It is also possible not to use a job but directly an asset_selection
@sensor(job=usgs_mcs_job,
        # default_status=DefaultSensorStatus.RUNNING,
        minimum_interval_seconds=60*60*24*30)  # Every 30 days
def new_mcs_update_sensor():
    '''
    Checks for updates on the ScienceBase server
    '''
    sb = SbSession()
    # Fetch link to download the data (see notebook)
    response = sb.find_items({'q': 'NMIC', 'lq': 'title:"National Minerals Information Center"',
                              'max': 1})  # NMIC = National Minerals Information Center
    nmic_id = response['items'][0]['id']
    response = sb.find_items({'parentId': nmic_id,
                              'filter': ['tags={type: Subject}', 'dateRange={choice: year}']})
    mcs_id = response['items'][0]['id']
    yield RunRequest(
        run_key=mcs_id  # Only run Job usgs_mcs_job if id of latest mcs has new value
    )

# TODO It is also possible not to use a job but directly an asset_selection
@sensor(job=worldbank_wdi_job,
        # default_status=DefaultSensorStatus.RUNNING,
        minimum_interval_seconds=60*60*24*30)  # Every 30 days
def new_wdi_update_sensor(wb_wdi_api: WorldbankWdiResource):
    '''
    Checks for updates on the World Development Indicators of The World Bank
    '''
    updated_date = wb_wdi_api.get_wdi_update_date()
    yield RunRequest(
        run_key=updated_date  # Only run Job worldbank_wdi_job if 'lastupdated' of WDI has new value
    )
